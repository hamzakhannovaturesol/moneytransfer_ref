<?php return array (
  'account' => 
  array (
    'updating_enabled' => true,
    'can_register' => true,
    'can_delete' => true,
  ),
  'app' => 
  array (
    'name' => 'An Express',
    'env' => 'local',
    'debug' => true,
    'url' => 'http://127.0.0.1:8000',
    'timezone' => 'UTC',
    'locale' => 'en',
    'fallback_locale' => 'en',
    'key' => 'base64:b3s9htX5nTS5UXylIBoFvO63rXUguyjYV5eUkg+5qjs=',
    'cipher' => 'AES-256-CBC',
    'admin_path' => 'admin',
    'paying_path' => 'paying',
    'editor_name' => 'V4Tech',
    'editor_site_url' => 'http://www.v4tech.co.uk/',
    'editor_alert_mail' => 'info@v4tech.co.uk',
    'gtm_user_id' => '',
    'url_force_https' => '',
    'read_only' => false,
    'faker_locale' => 'en_US',
    'providers' => 
    array (
      0 => 'Illuminate\\Auth\\AuthServiceProvider',
      1 => 'Illuminate\\Broadcasting\\BroadcastServiceProvider',
      2 => 'Illuminate\\Bus\\BusServiceProvider',
      3 => 'Illuminate\\Cache\\CacheServiceProvider',
      4 => 'Illuminate\\Foundation\\Providers\\ConsoleSupportServiceProvider',
      5 => 'Illuminate\\Cookie\\CookieServiceProvider',
      6 => 'Illuminate\\Database\\DatabaseServiceProvider',
      7 => 'Illuminate\\Encryption\\EncryptionServiceProvider',
      8 => 'Illuminate\\Filesystem\\FilesystemServiceProvider',
      9 => 'Illuminate\\Foundation\\Providers\\FoundationServiceProvider',
      10 => 'Illuminate\\Hashing\\HashServiceProvider',
      11 => 'Illuminate\\Mail\\MailServiceProvider',
      12 => 'Illuminate\\Notifications\\NotificationServiceProvider',
      13 => 'Illuminate\\Pagination\\PaginationServiceProvider',
      14 => 'Illuminate\\Pipeline\\PipelineServiceProvider',
      15 => 'Illuminate\\Queue\\QueueServiceProvider',
      16 => 'Illuminate\\Redis\\RedisServiceProvider',
      17 => 'Illuminate\\Auth\\Passwords\\PasswordResetServiceProvider',
      18 => 'Illuminate\\Session\\SessionServiceProvider',
      19 => 'Illuminate\\Translation\\TranslationServiceProvider',
      20 => 'Illuminate\\Validation\\ValidationServiceProvider',
      21 => 'Illuminate\\View\\ViewServiceProvider',
      22 => 'App\\Providers\\AppServiceProvider',
      23 => 'App\\Providers\\AuthServiceProvider',
      24 => 'App\\Providers\\BroadcastServiceProvider',
      25 => 'App\\Providers\\EventServiceProvider',
      26 => 'App\\Providers\\RouteServiceProvider',
      27 => 'App\\Providers\\ViewServiceProvider',
      28 => 'App\\Providers\\PurifySetupProvider',
      29 => 'DaveJamesMiller\\Breadcrumbs\\BreadcrumbsServiceProvider',
      30 => 'Barryvdh\\Debugbar\\ServiceProvider',
    ),
    'aliases' => 
    array (
      'App' => 'Illuminate\\Support\\Facades\\App',
      'Artisan' => 'Illuminate\\Support\\Facades\\Artisan',
      'Auth' => 'Illuminate\\Support\\Facades\\Auth',
      'Blade' => 'Illuminate\\Support\\Facades\\Blade',
      'Broadcast' => 'Illuminate\\Support\\Facades\\Broadcast',
      'Bus' => 'Illuminate\\Support\\Facades\\Bus',
      'Cache' => 'Illuminate\\Support\\Facades\\Cache',
      'Config' => 'Illuminate\\Support\\Facades\\Config',
      'Cookie' => 'Illuminate\\Support\\Facades\\Cookie',
      'Crypt' => 'Illuminate\\Support\\Facades\\Crypt',
      'DB' => 'Illuminate\\Support\\Facades\\DB',
      'Eloquent' => 'Illuminate\\Database\\Eloquent\\Model',
      'Event' => 'Illuminate\\Support\\Facades\\Event',
      'File' => 'Illuminate\\Support\\Facades\\File',
      'Gate' => 'Illuminate\\Support\\Facades\\Gate',
      'Hash' => 'Illuminate\\Support\\Facades\\Hash',
      'Lang' => 'Illuminate\\Support\\Facades\\Lang',
      'Log' => 'Illuminate\\Support\\Facades\\Log',
      'Mail' => 'Illuminate\\Support\\Facades\\Mail',
      'Notification' => 'Illuminate\\Support\\Facades\\Notification',
      'Password' => 'Illuminate\\Support\\Facades\\Password',
      'Queue' => 'Illuminate\\Support\\Facades\\Queue',
      'Redirect' => 'Illuminate\\Support\\Facades\\Redirect',
      'Redis' => 'Illuminate\\Support\\Facades\\Redis',
      'Request' => 'Illuminate\\Support\\Facades\\Request',
      'Response' => 'Illuminate\\Support\\Facades\\Response',
      'Route' => 'Illuminate\\Support\\Facades\\Route',
      'Schema' => 'Illuminate\\Support\\Facades\\Schema',
      'Session' => 'Illuminate\\Support\\Facades\\Session',
      'Storage' => 'Illuminate\\Support\\Facades\\Storage',
      'URL' => 'Illuminate\\Support\\Facades\\URL',
      'Validator' => 'Illuminate\\Support\\Facades\\Validator',
      'View' => 'Illuminate\\Support\\Facades\\View',
      'Captcha' => 'Arcanedev\\NoCaptcha\\Facades\\NoCaptcha',
      'SEOMeta' => 'App\\Facades\\SEOMeta',
      'Breadcrumbs' => 'DaveJamesMiller\\Breadcrumbs\\Facades\\Breadcrumbs',
      'Debugbar' => 'Barryvdh\\Debugbar\\Facade',
    ),
  ),
  'auth' => 
  array (
    'defaults' => 
    array (
      'guard' => 'web',
      'passwords' => 'users',
    ),
    'guards' => 
    array (
      'web' => 
      array (
        'driver' => 'session',
        'provider' => 'users',
      ),
      'paying' => 
      array (
        'driver' => 'session',
        'provider' => 'payings',
      ),
      'api' => 
      array (
        'driver' => 'token',
        'provider' => 'users',
      ),
    ),
    'providers' => 
    array (
      'users' => 
      array (
        'driver' => 'eloquent',
        'model' => 'App\\Models\\User',
      ),
      'payings' => 
      array (
        'driver' => 'eloquent',
        'model' => 'App\\Models\\PayingAgents',
      ),
    ),
    'passwords' => 
    array (
      'users' => 
      array (
        'provider' => 'users',
        'table' => 'password_resets',
        'expire' => 60,
      ),
    ),
  ),
  'blog' => 
  array (
    'enabled' => true,
    'show_post_owner' => true,
  ),
  'breadcrumbs' => 
  array (
    'view' => 'breadcrumbs::bootstrap4',
    'files' => 'F:\\wamp64\\www\\moneytransfer_ref\\routes/breadcrumbs.php',
    'unnamed-route-exception' => true,
    'missing-route-bound-breadcrumb-exception' => true,
    'invalid-named-breadcrumb-exception' => true,
    'manager-class' => 'DaveJamesMiller\\Breadcrumbs\\BreadcrumbsManager',
    'generator-class' => 'DaveJamesMiller\\Breadcrumbs\\BreadcrumbsGenerator',
  ),
  'broadcasting' => 
  array (
    'default' => 'pusher',
    'connections' => 
    array (
      'pusher' => 
      array (
        'driver' => 'pusher',
        'key' => '794f6db99b30de05ce15',
        'secret' => '86052bd2d3b10f7d13c7',
        'app_id' => '766412',
        'options' => 
        array (
          'cluster' => 'eu',
          'encrypted' => false,
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
      ),
      'log' => 
      array (
        'driver' => 'log',
      ),
      'null' => 
      array (
        'driver' => 'null',
      ),
    ),
  ),
  'cache' => 
  array (
    'default' => 'file',
    'stores' => 
    array (
      'apc' => 
      array (
        'driver' => 'apc',
      ),
      'array' => 
      array (
        'driver' => 'array',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'cache',
        'connection' => NULL,
      ),
      'file' => 
      array (
        'driver' => 'file',
        'path' => 'F:\\wamp64\\www\\moneytransfer_ref\\storage\\framework/cache/data',
      ),
      'memcached' => 
      array (
        'driver' => 'memcached',
        'persistent_id' => NULL,
        'sasl' => 
        array (
          0 => NULL,
          1 => NULL,
        ),
        'options' => 
        array (
        ),
        'servers' => 
        array (
          0 => 
          array (
            'host' => '127.0.0.1',
            'port' => 11211,
            'weight' => 100,
          ),
        ),
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'cache',
      ),
    ),
    'prefix' => 'an_express_cache',
  ),
  'compile' => 
  array (
    'files' => 
    array (
    ),
    'providers' => 
    array (
    ),
  ),
  'database' => 
  array (
    'default' => 'mysql',
    'connections' => 
    array (
      'sqlite' => 
      array (
        'driver' => 'sqlite',
        'database' => 'moneytransfer_local_ref',
        'prefix' => '',
      ),
      'mysql' => 
      array (
        'driver' => 'mysql',
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'moneytransfer_local_ref',
        'username' => 'root',
        'password' => 'root',
        'unix_socket' => '',
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => '',
        'strict' => true,
        'engine' => NULL,
      ),
      'mysql2' => 
      array (
        'driver' => 'mysql',
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'moneytransfer_local_2',
        'username' => 'root',
        'password' => 'root',
        'unix_socket' => '',
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => '',
        'strict' => true,
        'engine' => NULL,
      ),
      'pgsql' => 
      array (
        'driver' => 'pgsql',
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'moneytransfer_local_ref',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',
        'prefix' => '',
        'schema' => 'public',
        'sslmode' => 'prefer',
      ),
      'sqlsrv' => 
      array (
        'driver' => 'sqlsrv',
        'host' => '127.0.0.1',
        'port' => '3306',
        'database' => 'moneytransfer_local_ref',
        'username' => 'root',
        'password' => 'root',
        'charset' => 'utf8',
        'prefix' => '',
      ),
    ),
    'migrations' => 'migrations',
    'redis' => 
    array (
      'client' => 'predis',
      'default' => 
      array (
        'host' => '127.0.0.1',
        'password' => NULL,
        'port' => '6379',
        'database' => 0,
      ),
      'cache' => 
      array (
        'host' => '127.0.0.1',
        'password' => NULL,
        'port' => '6379',
        'database' => 1,
      ),
    ),
  ),
  'datatables' => 
  array (
    'search' => 
    array (
      'smart' => true,
      'multi_term' => true,
      'case_insensitive' => true,
      'use_wildcards' => false,
    ),
    'index_column' => 'DT_Row_Index',
    'engines' => 
    array (
      'eloquent' => 'Yajra\\DataTables\\EloquentDataTable',
      'query' => 'Yajra\\DataTables\\QueryDataTable',
      'collection' => 'Yajra\\DataTables\\CollectionDataTable',
    ),
    'builders' => 
    array (
      'Illuminate\\Database\\Eloquent\\Relations\\Relation' => 'eloquent',
      'Illuminate\\Database\\Eloquent\\Builder' => 'eloquent',
      'Illuminate\\Database\\Query\\Builder' => 'query',
      'Illuminate\\Support\\Collection' => 'collection',
    ),
    'nulls_last_sql' => '%s %s NULLS LAST',
    'error' => NULL,
    'columns' => 
    array (
      'excess' => 
      array (
        0 => 'rn',
        1 => 'row_num',
      ),
      'escape' => '*',
      'raw' => 
      array (
        0 => 'action',
      ),
      'blacklist' => 
      array (
        0 => 'password',
        1 => 'remember_token',
      ),
      'whitelist' => '*',
    ),
    'json' => 
    array (
      'header' => 
      array (
      ),
      'options' => 0,
    ),
  ),
  'excel' => 
  array (
    'exports' => 
    array (
      'chunk_size' => 1000,
      'temp_path' => 'C:\\Users\\hkhan\\AppData\\Local\\Temp',
      'csv' => 
      array (
        'delimiter' => ',',
        'enclosure' => '"',
        'line_ending' => '
',
        'use_bom' => false,
        'include_separator_line' => false,
        'excel_compatibility' => false,
      ),
    ),
    'imports' => 
    array (
      'read_only' => true,
      'heading_row' => 
      array (
        'formatter' => 'slug',
      ),
      'csv' => 
      array (
        'delimiter' => ',',
        'enclosure' => '"',
        'line_ending' => '
',
        'use_bom' => false,
        'include_separator_line' => false,
        'excel_compatibility' => false,
      ),
    ),
    'extension_detector' => 
    array (
      'xlsx' => 'Xlsx',
      'xlsm' => 'Xlsx',
      'xltx' => 'Xlsx',
      'xltm' => 'Xlsx',
      'xls' => 'Xls',
      'xlt' => 'Xls',
      'ods' => 'Ods',
      'ots' => 'Ods',
      'slk' => 'Slk',
      'xml' => 'Xml',
      'gnumeric' => 'Gnumeric',
      'htm' => 'Html',
      'html' => 'Html',
      'csv' => 'Csv',
      'pdf' => 'Dompdf',
    ),
    'value_binder' => 
    array (
      'default' => 'Maatwebsite\\Excel\\DefaultValueBinder',
    ),
    'transactions' => 
    array (
      'handler' => 'db',
    ),
    'temporary_files' => 
    array (
      'local_path' => 'C:\\Users\\hkhan\\AppData\\Local\\Temp',
      'remote_disk' => NULL,
    ),
  ),
  'filesystems' => 
  array (
    'default' => 'local',
    'cloud' => 's3',
    'disks' => 
    array (
      'local' => 
      array (
        'driver' => 'local',
        'root' => 'F:\\wamp64\\www\\moneytransfer_ref\\storage\\app',
      ),
      'public' => 
      array (
        'driver' => 'local',
        'root' => 'F:\\wamp64\\www\\moneytransfer_ref\\storage\\app/public',
        'url' => 'http://127.0.0.1:8000/storage',
        'visibility' => 'public',
      ),
      's3' => 
      array (
        'driver' => 's3',
        'key' => NULL,
        'secret' => NULL,
        'region' => NULL,
        'bucket' => NULL,
        'url' => NULL,
      ),
    ),
  ),
  'forms' => 
  array (
    'contact' => 
    array (
      'display_name' => 'forms.contact.display_name',
    ),
  ),
  'hashing' => 
  array (
    'driver' => 'bcrypt',
    'bcrypt' => 
    array (
      'rounds' => 10,
    ),
    'argon' => 
    array (
      'memory' => 1024,
      'threads' => 2,
      'time' => 2,
    ),
  ),
  'ide-helper' => 
  array (
    'filename' => '_ide_helper',
    'format' => 'php',
    'meta_filename' => '.phpstorm.meta.php',
    'include_fluent' => true,
    'write_model_magic_where' => true,
    'write_eloquent_model_mixins' => false,
    'include_helpers' => false,
    'helper_files' => 
    array (
      0 => 'F:\\wamp64\\www\\moneytransfer_ref/vendor/laravel/framework/src/Illuminate/Support/helpers.php',
    ),
    'model_locations' => 
    array (
      0 => 'app',
    ),
    'extra' => 
    array (
      'Eloquent' => 
      array (
        0 => 'Illuminate\\Database\\Eloquent\\Builder',
        1 => 'Illuminate\\Database\\Query\\Builder',
      ),
      'Session' => 
      array (
        0 => 'Illuminate\\Session\\Store',
      ),
    ),
    'magic' => 
    array (
      'Log' => 
      array (
        'debug' => 'Monolog\\Logger::addDebug',
        'info' => 'Monolog\\Logger::addInfo',
        'notice' => 'Monolog\\Logger::addNotice',
        'warning' => 'Monolog\\Logger::addWarning',
        'error' => 'Monolog\\Logger::addError',
        'critical' => 'Monolog\\Logger::addCritical',
        'alert' => 'Monolog\\Logger::addAlert',
        'emergency' => 'Monolog\\Logger::addEmergency',
      ),
    ),
    'interfaces' => 
    array (
    ),
    'custom_db_types' => 
    array (
    ),
    'model_camel_case_properties' => false,
    'type_overrides' => 
    array (
      'integer' => 'int',
      'boolean' => 'bool',
    ),
    'include_class_docblocks' => false,
  ),
  'image' => 
  array (
    'driver' => 'gd',
  ),
  'imagecache' => 
  array (
    'route' => 'imagecache',
    'paths' => 
    array (
      0 => 'F:\\wamp64\\www\\moneytransfer_ref\\public\\upload',
      1 => 'F:\\wamp64\\www\\moneytransfer_ref\\public\\images',
      2 => 'F:\\wamp64\\www\\moneytransfer_ref\\public\\storage',
      3 => 'F:\\wamp64\\www\\moneytransfer_ref\\public',
    ),
    'templates' => 
    array (
      'small' => 'App\\Image\\Templates\\Small',
      'medium' => 'App\\Image\\Templates\\Medium',
      'large' => 'App\\Image\\Templates\\Large',
    ),
    'lifetime' => 43200,
  ),
  'laravellocalization' => 
  array (
    'supportedLocales' => 
    array (
      'en' => 
      array (
        'name' => 'labels.localization.en',
        'script' => 'Latn',
        'native' => 'English',
        'regional' => 'en_US',
        'locale_win' => 'English_United States.1252',
        'date_formats' => 
        array (
          'default' => 'm/d/Y h:i:s A',
        ),
      ),
      'fr' => 
      array (
        'name' => 'labels.localization.fr',
        'script' => 'Latn',
        'native' => 'Français',
        'regional' => 'fr_FR',
        'locale_win' => 'French_France.1252',
        'date_formats' => 
        array (
          'default' => 'd/m/Y H:i:s',
        ),
      ),
      'de' => 
      array (
        'name' => 'labels.localization.de',
        'script' => 'Latn',
        'native' => 'Deutsch',
        'regional' => 'de_DE',
        'locale_win' => 'German_Germany.1252',
        'date_formats' => 
        array (
          'default' => 'd.m.Y H:i:s',
        ),
      ),
      'es' => 
      array (
        'name' => 'labels.localization.es',
        'script' => 'Latn',
        'native' => 'Español',
        'regional' => 'es_ES',
        'locale_win' => 'Spanish_Spain.1252',
        'date_formats' => 
        array (
          'default' => 'j/m/Y H:i:s',
        ),
      ),
      'pt' => 
      array (
        'name' => 'labels.localization.pt',
        'script' => 'Latn',
        'native' => 'Português',
        'regional' => 'pt_PT',
        'locale_win' => 'Portuguese_Portugal.1252',
        'date_formats' => 
        array (
          'default' => 'Y/m/d H:i:s',
        ),
      ),
      'ru' => 
      array (
        'name' => 'labels.localization.ru',
        'script' => 'Cyrl',
        'native' => 'Русский',
        'regional' => 'ru_RU',
        'locale_win' => 'Russian_Russia.1251',
        'date_formats' => 
        array (
          'default' => 'd.m.Y H:i:s',
        ),
      ),
      'ar' => 
      array (
        'name' => 'labels.localization.ar',
        'script' => 'Arab',
        'native' => 'عربي',
        'regional' => 'ar_SA',
        'locale_win' => 'Arabic_Saudi Arabia.1256',
        'date_formats' => 
        array (
          'default' => 'd/m/Y H:i:s',
        ),
        'dir' => 'rtl',
      ),
      'zn' => 
      array (
        'name' => 'labels.localization.zn',
        'script' => 'Hans',
        'native' => '中文',
        'regional' => 'zh_TW',
        'locale_win' => 'Chinese_Taiwan.950',
        'date_formats' => 
        array (
          'default' => 'Y-m-d h:i:s A',
        ),
      ),
    ),
    'useAcceptLanguageHeader' => true,
    'hideDefaultLocaleInURL' => true,
    'localesOrder' => 
    array (
    ),
    'localesMapping' => 
    array (
    ),
    'utf8suffix' => '.UTF-8',
    'urlsIgnored' => 
    array (
      0 => '/skipped',
    ),
  ),
  'log-viewer' => 
  array (
    'storage-path' => 'F:\\wamp64\\www\\moneytransfer_ref\\storage\\logs',
    'pattern' => 
    array (
      'prefix' => 'laravel-',
      'date' => '[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]',
      'extension' => '.log',
    ),
    'locale' => 'auto',
    'theme' => 'bootstrap-4',
    'route' => 
    array (
      'enabled' => true,
      'attributes' => 
      array (
        'prefix' => 'log-viewer',
        'middleware' => 
        array (
          0 => 'web',
          1 => 'auth',
          2 => 'can:access backend',
        ),
      ),
    ),
    'per-page' => 30,
    'facade' => 'LogViewer',
    'download' => 
    array (
      'prefix' => 'laravel-',
      'extension' => 'log',
    ),
    'menu' => 
    array (
      'filter-route' => 'log-viewer::logs.filter',
      'icons-enabled' => true,
    ),
    'icons' => 
    array (
      'all' => 'fa fa-fw fa-list',
      'emergency' => 'fa fa-fw fa-bug',
      'alert' => 'fa fa-fw fa-bullhorn',
      'critical' => 'fa fa-fw fa-heartbeat',
      'error' => 'fa fa-fw fa-times-circle',
      'warning' => 'fa fa-fw fa-exclamation-triangle',
      'notice' => 'fa fa-fw fa-exclamation-circle',
      'info' => 'fa fa-fw fa-info-circle',
      'debug' => 'fa fa-fw fa-life-ring',
    ),
    'colors' => 
    array (
      'levels' => 
      array (
        'empty' => '#D1D1D1',
        'all' => '#8A8A8A',
        'emergency' => '#B71C1C',
        'alert' => '#D32F2F',
        'critical' => '#F44336',
        'error' => '#FF5722',
        'warning' => '#FF9100',
        'notice' => '#4CAF50',
        'info' => '#1976D2',
        'debug' => '#90CAF9',
      ),
    ),
    'highlight' => 
    array (
      0 => '^#\\d+',
      1 => '^Stack trace:',
    ),
  ),
  'logging' => 
  array (
    'default' => 'daily',
    'channels' => 
    array (
      'stack' => 
      array (
        'driver' => 'stack',
        'channels' => 
        array (
          0 => 'single',
        ),
      ),
      'single' => 
      array (
        'driver' => 'single',
        'path' => 'F:\\wamp64\\www\\moneytransfer_ref\\storage\\logs/laravel.log',
        'level' => 'debug',
      ),
      'daily' => 
      array (
        'driver' => 'daily',
        'path' => 'F:\\wamp64\\www\\moneytransfer_ref\\storage\\logs/laravel.log',
        'level' => 'debug',
        'days' => 7,
      ),
      'slack' => 
      array (
        'driver' => 'slack',
        'url' => NULL,
        'username' => 'Laravel Log',
        'emoji' => ':boom:',
        'level' => 'critical',
      ),
      'papertrail' => 
      array (
        'driver' => 'monolog',
        'level' => 'debug',
        'handler' => 'Monolog\\Handler\\SyslogUdpHandler',
        'handler_with' => 
        array (
          'host' => NULL,
          'port' => NULL,
        ),
      ),
      'stderr' => 
      array (
        'driver' => 'monolog',
        'handler' => 'Monolog\\Handler\\StreamHandler',
        'with' => 
        array (
          'stream' => 'php://stderr',
        ),
      ),
      'syslog' => 
      array (
        'driver' => 'syslog',
        'level' => 'debug',
      ),
      'errorlog' => 
      array (
        'driver' => 'errorlog',
        'level' => 'debug',
      ),
    ),
  ),
  'mail' => 
  array (
    'driver' => 'smtp',
    'host' => 'smtp.ionos.co.uk',
    'port' => '587',
    'from' => 
    array (
      'address' => 'info@anexpress.com',
      'name' => 'AN Express Ltd',
    ),
    'encryption' => 'tls',
    'username' => 'noreply@anexpress.com',
    'password' => 'Abc12345*',
    'sendmail' => '/usr/sbin/sendmail -bs',
    'markdown' => 
    array (
      'theme' => 'default',
      'paths' => 
      array (
        0 => 'F:\\wamp64\\www\\moneytransfer_ref\\resources\\views/vendor/mail',
      ),
    ),
  ),
  'medialibrary' => 
  array (
    'disk_name' => 'public',
    'max_file_size' => 10485760,
    'queue_name' => '',
    'media_model' => 'Spatie\\MediaLibrary\\Models\\Media',
    's3' => 
    array (
      'domain' => 'https://.s3.amazonaws.com',
    ),
    'remote' => 
    array (
      'extra_headers' => 
      array (
        'CacheControl' => 'max-age=604800',
      ),
    ),
    'responsive_images' => 
    array (
      'width_calculator' => 'Spatie\\MediaLibrary\\ResponsiveImages\\WidthCalculator\\FileSizeOptimizedWidthCalculator',
      'use_tiny_placeholders' => true,
      'tiny_placeholder_generator' => 'Spatie\\MediaLibrary\\ResponsiveImages\\TinyPlaceholderGenerator\\Blurred',
    ),
    'url_generator' => NULL,
    'path_generator' => NULL,
    'image_optimizers' => 
    array (
      'Spatie\\ImageOptimizer\\Optimizers\\Jpegoptim' => 
      array (
        0 => '--strip-all',
        1 => '--all-progressive',
      ),
      'Spatie\\ImageOptimizer\\Optimizers\\Pngquant' => 
      array (
        0 => '--force',
      ),
      'Spatie\\ImageOptimizer\\Optimizers\\Optipng' => 
      array (
        0 => '-i0',
        1 => '-o2',
        2 => '-quiet',
      ),
      'Spatie\\ImageOptimizer\\Optimizers\\Svgo' => 
      array (
        0 => '--disable=cleanupIDs',
      ),
      'Spatie\\ImageOptimizer\\Optimizers\\Gifsicle' => 
      array (
        0 => '-b',
        1 => '-O3',
      ),
    ),
    'image_generators' => 
    array (
      0 => 'Spatie\\MediaLibrary\\ImageGenerators\\FileTypes\\Image',
      1 => 'Spatie\\MediaLibrary\\ImageGenerators\\FileTypes\\Pdf',
      2 => 'Spatie\\MediaLibrary\\ImageGenerators\\FileTypes\\Svg',
      3 => 'Spatie\\MediaLibrary\\ImageGenerators\\FileTypes\\Video',
    ),
    'image_driver' => 'gd',
    'ffmpeg_path' => '/usr/bin/ffmpeg',
    'ffprobe_path' => '/usr/bin/ffprobe',
    'temporary_directory_path' => NULL,
    'jobs' => 
    array (
      'perform_conversions' => 'Spatie\\MediaLibrary\\Jobs\\PerformConversions',
      'generate_responsive_images' => 'Spatie\\MediaLibrary\\Jobs\\GenerateResponsiveImages',
    ),
  ),
  'no-captcha' => 
  array (
    'secret' => '6LeZQb8UAAAAAOc2NuHagKjt5TCwfz4FgdXOOpL4',
    'sitekey' => '6LeZQb8UAAAAACb-muiHRfoi1vhEpdEJfTRLM8ds',
    'lang' => 'en',
    'attributes' => 
    array (
      'data-theme' => NULL,
      'data-type' => NULL,
      'data-size' => NULL,
    ),
  ),
  'permissions' => 
  array (
    'access backend' => 
    array (
      'display_name' => 'permissions.access.backend.display_name',
      'description' => 'permissions.access.backend.description',
      'category' => 'permissions.categories.access',
    ),
    'access paying' => 
    array (
      'display_name' => 'permissions.access.paying.display_name',
      'description' => 'permissions.access.paying.description',
      'category' => 'permissions.categories.access',
    ),
    'access customer' => 
    array (
      'display_name' => 'permissions.access.customer.display_name',
      'description' => 'permissions.access.customer.description',
      'category' => 'permissions.categories.access',
    ),
    'create Agent' => 
    array (
      'display_name' => 'permissions.create.agent.display_name',
      'description' => 'permissions.create.agent.description',
      'category' => 'permissions.categories.agent',
      'children' => 
      array (
        0 => 'create own agent',
      ),
    ),
    'view Agent' => 
    array (
      'display_name' => 'permissions.view.agent.display_name',
      'description' => 'permissions.view.agent.description',
      'category' => 'permissions.categories.agent',
      'children' => 
      array (
        0 => 'view own agent',
      ),
    ),
    'edit Agent' => 
    array (
      'display_name' => 'permissions.edit.agent.display_name',
      'description' => 'permissions.edit.agent.description',
      'category' => 'permissions.categories.agent',
      'children' => 
      array (
        0 => 'edit own agent',
      ),
    ),
    'delete Agent' => 
    array (
      'display_name' => 'permissions.delete.agent.display_name',
      'description' => 'permissions.delete.agent.description',
      'category' => 'permissions.categories.agent',
      'children' => 
      array (
        0 => 'delete own agent',
      ),
    ),
    'create Agent Sub' => 
    array (
      'display_name' => 'permissions.create.agent_sub.display_name',
      'description' => 'permissions.create.agent_sub.description',
      'category' => 'permissions.categories.agent_sub',
      'children' => 
      array (
        0 => 'create own agent_sub',
      ),
    ),
    'view Agent Sub' => 
    array (
      'display_name' => 'permissions.view.agent_sub.display_name',
      'description' => 'permissions.view.agent_sub.description',
      'category' => 'permissions.categories.agent_sub',
      'children' => 
      array (
        0 => 'view own agent_sub',
      ),
    ),
    'edit Agent Sub' => 
    array (
      'display_name' => 'permissions.edit.agent_sub.display_name',
      'description' => 'permissions.edit.agent_sub.description',
      'category' => 'permissions.categories.agent_sub',
      'children' => 
      array (
        0 => 'edit own agent_sub',
      ),
    ),
    'delete Agent Sub' => 
    array (
      'display_name' => 'permissions.delete.agent_sub.display_name',
      'description' => 'permissions.delete.agent_sub.description',
      'category' => 'permissions.categories.agent_sub',
      'children' => 
      array (
        0 => 'delete own agent_sub',
      ),
    ),
    'create customer' => 
    array (
      'display_name' => 'permissions.create.customer.display_name',
      'description' => 'permissions.create.customer.description',
      'category' => 'permissions.categories.customer',
      'children' => 
      array (
        0 => 'create own customer',
      ),
    ),
    'view customer' => 
    array (
      'display_name' => 'permissions.view.customer.display_name',
      'description' => 'permissions.view.customer.description',
      'category' => 'permissions.categories.customer',
      'children' => 
      array (
        0 => 'view own customer',
      ),
    ),
    'edit customer' => 
    array (
      'display_name' => 'permissions.edit.customer.display_name',
      'description' => 'permissions.edit.customer.description',
      'category' => 'permissions.categories.customer',
      'children' => 
      array (
        0 => 'edit own customer',
      ),
    ),
    'delete customer' => 
    array (
      'display_name' => 'permissions.delete.customer.display_name',
      'description' => 'permissions.delete.customer.description',
      'category' => 'permissions.categories.customer',
      'children' => 
      array (
        0 => 'delete own customer',
      ),
    ),
    'create banks' => 
    array (
      'display_name' => 'permissions.create.banks.display_name',
      'description' => 'permissions.create.banks.description',
      'category' => 'permissions.categories.banks',
      'children' => 
      array (
        0 => 'create own banks',
      ),
    ),
    'view banks' => 
    array (
      'display_name' => 'permissions.view.banks.display_name',
      'description' => 'permissions.view.banks.description',
      'category' => 'permissions.categories.banks',
      'children' => 
      array (
        0 => 'view own banks',
      ),
    ),
    'edit banks' => 
    array (
      'display_name' => 'permissions.edit.banks.display_name',
      'description' => 'permissions.edit.banks.description',
      'category' => 'permissions.categories.banks',
      'children' => 
      array (
        0 => 'edit own banks',
      ),
    ),
    'delete banks' => 
    array (
      'display_name' => 'permissions.delete.banks.display_name',
      'description' => 'permissions.delete.banks.description',
      'category' => 'permissions.categories.banks',
      'children' => 
      array (
        0 => 'delete own banks',
      ),
    ),
    'create branches' => 
    array (
      'display_name' => 'permissions.create.branches.display_name',
      'description' => 'permissions.create.branches.description',
      'category' => 'permissions.categories.branches',
      'children' => 
      array (
        0 => 'create own branches',
      ),
    ),
    'view branches' => 
    array (
      'display_name' => 'permissions.view.branches.display_name',
      'description' => 'permissions.view.branches.description',
      'category' => 'permissions.categories.branches',
      'children' => 
      array (
        0 => 'view own branches',
      ),
    ),
    'edit branches' => 
    array (
      'display_name' => 'permissions.edit.branches.display_name',
      'description' => 'permissions.edit.branches.description',
      'category' => 'permissions.categories.branches',
      'children' => 
      array (
        0 => 'edit own branches',
      ),
    ),
    'delete branches' => 
    array (
      'display_name' => 'permissions.delete.branches.display_name',
      'description' => 'permissions.delete.branches.description',
      'category' => 'permissions.categories.branches',
      'children' => 
      array (
        0 => 'delete own branches',
      ),
    ),
    'create contact' => 
    array (
      'display_name' => 'permissions.create.contact.display_name',
      'description' => 'permissions.create.contact.description',
      'category' => 'permissions.categories.contact',
      'children' => 
      array (
        0 => 'create own contact',
      ),
    ),
    'view contact' => 
    array (
      'display_name' => 'permissions.view.contact.display_name',
      'description' => 'permissions.view.contact.description',
      'category' => 'permissions.categories.contact',
      'children' => 
      array (
        0 => 'view own contact',
      ),
    ),
    'edit contact' => 
    array (
      'display_name' => 'permissions.edit.contact.display_name',
      'description' => 'permissions.edit.contact.description',
      'category' => 'permissions.categories.contact',
      'children' => 
      array (
        0 => 'edit own contact',
      ),
    ),
    'delete contact' => 
    array (
      'display_name' => 'permissions.delete.contact.display_name',
      'description' => 'permissions.delete.contact.description',
      'category' => 'permissions.categories.contact',
      'children' => 
      array (
        0 => 'delete own contact',
      ),
    ),
    'view users' => 
    array (
      'display_name' => 'permissions.view.users.display_name',
      'description' => 'permissions.view.users.description',
      'category' => 'permissions.categories.access',
    ),
    'create users' => 
    array (
      'display_name' => 'permissions.create.users.display_name',
      'description' => 'permissions.create.users.description',
      'category' => 'permissions.categories.access',
    ),
    'edit users' => 
    array (
      'display_name' => 'permissions.edit.users.display_name',
      'description' => 'permissions.edit.users.description',
      'category' => 'permissions.categories.access',
    ),
    'delete users' => 
    array (
      'display_name' => 'permissions.delete.users.display_name',
      'description' => 'permissions.delete.users.description',
      'category' => 'permissions.categories.access',
    ),
    'impersonate users' => 
    array (
      'display_name' => 'permissions.impersonate.display_name',
      'description' => 'permissions.impersonate.description',
      'category' => 'permissions.categories.access',
    ),
    'view roles' => 
    array (
      'display_name' => 'permissions.view.roles.display_name',
      'description' => 'permissions.view.roles.description',
      'category' => 'permissions.categories.access',
    ),
    'create roles' => 
    array (
      'display_name' => 'permissions.create.roles.display_name',
      'description' => 'permissions.create.roles.description',
      'category' => 'permissions.categories.access',
    ),
    'edit roles' => 
    array (
      'display_name' => 'permissions.edit.roles.display_name',
      'description' => 'permissions.edit.roles.description',
      'category' => 'permissions.categories.access',
    ),
    'delete roles' => 
    array (
      'display_name' => 'permissions.delete.roles.display_name',
      'description' => 'permissions.delete.roles.description',
      'category' => 'permissions.categories.access',
    ),
    'view payments' => 
    array (
      'display_name' => 'permissions.view.payments.display_name',
      'description' => 'permissions.view.payments.description',
      'category' => 'permissions.categories.payments',
    ),
    'create payments' => 
    array (
      'display_name' => 'permissions.create.payments.display_name',
      'description' => 'permissions.create.payments.description',
      'category' => 'permissions.categories.payments',
    ),
    'edit payments' => 
    array (
      'display_name' => 'permissions.edit.payments.display_name',
      'description' => 'permissions.edit.payments.description',
      'category' => 'permissions.categories.payments',
    ),
    'delete payments' => 
    array (
      'display_name' => 'permissions.delete.payments.display_name',
      'description' => 'permissions.delete.payments.description',
      'category' => 'permissions.categories.payments',
    ),
    'view recharge' => 
    array (
      'display_name' => 'permissions.view.recharge.display_name',
      'description' => 'permissions.view.recharge.description',
      'category' => 'permissions.categories.recharge',
    ),
    'create recharge' => 
    array (
      'display_name' => 'permissions.create.recharge.display_name',
      'description' => 'permissions.create.recharge.description',
      'category' => 'permissions.categories.recharge',
    ),
    'edit recharge' => 
    array (
      'display_name' => 'permissions.edit.recharge.display_name',
      'description' => 'permissions.edit.recharge.description',
      'category' => 'permissions.categories.recharge',
    ),
    'delete recharge' => 
    array (
      'display_name' => 'permissions.delete.recharge.display_name',
      'description' => 'permissions.delete.recharge.description',
      'category' => 'permissions.categories.recharge',
    ),
    'view fee' => 
    array (
      'display_name' => 'permissions.view.fee.display_name',
      'description' => 'permissions.view.fee.description',
      'category' => 'permissions.categories.fee',
    ),
    'create fee' => 
    array (
      'display_name' => 'permissions.create.fee.display_name',
      'description' => 'permissions.create.fee.description',
      'category' => 'permissions.categories.fee',
    ),
    'edit fee' => 
    array (
      'display_name' => 'permissions.edit.fee.display_name',
      'description' => 'permissions.edit.fee.description',
      'category' => 'permissions.categories.fee',
    ),
    'delete fee' => 
    array (
      'display_name' => 'permissions.delete.fee.display_name',
      'description' => 'permissions.delete.fee.description',
      'category' => 'permissions.categories.fee',
    ),
    'view currency' => 
    array (
      'display_name' => 'permissions.view.currency.display_name',
      'description' => 'permissions.view.currency.description',
      'category' => 'permissions.categories.currency',
    ),
    'create currency' => 
    array (
      'display_name' => 'permissions.create.currency.display_name',
      'description' => 'permissions.create.currency.description',
      'category' => 'permissions.categories.currency',
    ),
    'edit currency' => 
    array (
      'display_name' => 'permissions.edit.currency.display_name',
      'description' => 'permissions.edit.currency.description',
      'category' => 'permissions.categories.currency',
    ),
    'delete currency' => 
    array (
      'display_name' => 'permissions.delete.currency.display_name',
      'description' => 'permissions.delete.currency.description',
      'category' => 'permissions.categories.currency',
    ),
    'view margin' => 
    array (
      'display_name' => 'permissions.view.margin.display_name',
      'description' => 'permissions.view.margin.description',
      'category' => 'permissions.categories.margin',
    ),
    'create margin' => 
    array (
      'display_name' => 'permissions.create.margin.display_name',
      'description' => 'permissions.create.margin.description',
      'category' => 'permissions.categories.margin',
    ),
    'edit margin' => 
    array (
      'display_name' => 'permissions.edit.margin.display_name',
      'description' => 'permissions.edit.margin.description',
      'category' => 'permissions.categories.margin',
    ),
    'delete margin' => 
    array (
      'display_name' => 'permissions.delete.margin.display_name',
      'description' => 'permissions.delete.margin.description',
      'category' => 'permissions.categories.margin',
    ),
    'view transaction' => 
    array (
      'display_name' => 'permissions.view.transaction.display_name',
      'description' => 'permissions.view.transaction.description',
      'category' => 'permissions.categories.transaction',
    ),
  ),
  'purify' => 
  array (
    'settings' => 
    array (
      'Core.Encoding' => 'utf-8',
      'Cache.SerializerPath' => 'F:\\wamp64\\www\\moneytransfer_ref\\storage\\purify',
      'HTML.Doctype' => 'XHTML 1.0 Strict',
      'HTML.Allowed' => 'h1,h2,h3,h4,h5,h6,figure[class],figcaption,b,strong,i,em,a[href|title],ul,ol,li,p,br,span,img[width|height|alt|src]',
      'HTML.ForbiddenElements' => '',
      'CSS.AllowedProperties' => 'font,font-size,font-weight,font-style,font-family,text-decoration,padding-left,color,background-color,text-align',
      'AutoFormat.AutoParagraph' => false,
      'AutoFormat.RemoveEmpty' => false,
    ),
  ),
  'queue' => 
  array (
    'default' => 'sync',
    'connections' => 
    array (
      'sync' => 
      array (
        'driver' => 'sync',
      ),
      'database' => 
      array (
        'driver' => 'database',
        'table' => 'jobs',
        'queue' => 'default',
        'retry_after' => 90,
      ),
      'beanstalkd' => 
      array (
        'driver' => 'beanstalkd',
        'host' => 'localhost',
        'queue' => 'default',
        'retry_after' => 90,
      ),
      'sqs' => 
      array (
        'driver' => 'sqs',
        'key' => 'your-public-key',
        'secret' => 'your-secret-key',
        'prefix' => 'https://sqs.us-east-1.amazonaws.com/your-account-id',
        'queue' => 'your-queue-name',
        'region' => 'us-east-1',
      ),
      'redis' => 
      array (
        'driver' => 'redis',
        'connection' => 'default',
        'queue' => 'default',
        'retry_after' => 90,
        'block_for' => NULL,
      ),
    ),
    'failed' => 
    array (
      'database' => 'mysql',
      'table' => 'failed_jobs',
    ),
  ),
  'redirections' => 
  array (
    301 => 'labels.backend.redirections.types.permanent',
    302 => 'labels.backend.redirections.types.temporary',
  ),
  'scout' => 
  array (
    'driver' => 'tntsearch',
    'prefix' => '',
    'queue' => false,
    'chunk' => 
    array (
      'searchable' => 500,
      'unsearchable' => 500,
    ),
    'algolia' => 
    array (
      'id' => '',
      'secret' => '',
    ),
    'tntsearch' => 
    array (
      'storage' => 'F:\\wamp64\\www\\moneytransfer_ref\\storage',
      'fuzziness' => false,
      'fuzzy' => 
      array (
        'prefix_length' => 2,
        'max_expansions' => 50,
        'distance' => 2,
      ),
      'asYouType' => false,
      'searchBoolean' => false,
    ),
  ),
  'seotools' => 
  array (
    'meta' => 
    array (
      'defaults' => 
      array (
        'title' => 'An Express',
        'description' => false,
        'separator' => ' - ',
        'keywords' => 
        array (
        ),
        'canonical' => false,
      ),
      'webmaster_tags' => 
      array (
        'google' => NULL,
        'bing' => NULL,
        'alexa' => NULL,
        'pinterest' => NULL,
        'yandex' => NULL,
      ),
    ),
    'opengraph' => 
    array (
      'defaults' => 
      array (
        'title' => false,
        'description' => false,
        'url' => false,
        'type' => false,
        'site_name' => false,
        'images' => 
        array (
        ),
      ),
    ),
    'twitter' => 
    array (
      'defaults' => 
      array (
      ),
    ),
  ),
  'services' => 
  array (
    'mailgun' => 
    array (
      'domain' => NULL,
      'secret' => NULL,
      'endpoint' => 'api.mailgun.net',
    ),
    'ses' => 
    array (
      'key' => NULL,
      'secret' => NULL,
      'region' => 'us-east-1',
    ),
    'sparkpost' => 
    array (
      'secret' => NULL,
    ),
    'stripe' => 
    array (
      'model' => 'App\\Models\\User',
      'key' => NULL,
      'secret' => NULL,
    ),
    'facebook' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => NULL,
    ),
    'twitter' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => NULL,
    ),
    'google' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => NULL,
    ),
    'linkedin' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => NULL,
    ),
    'github' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => NULL,
    ),
    'bitbucket' => 
    array (
      'client_id' => NULL,
      'client_secret' => NULL,
      'redirect' => NULL,
    ),
  ),
  'session' => 
  array (
    'driver' => 'file',
    'lifetime' => '120',
    'expire_on_close' => false,
    'encrypt' => false,
    'files' => 'F:\\wamp64\\www\\moneytransfer_ref\\storage\\framework/sessions',
    'connection' => NULL,
    'table' => 'sessions',
    'store' => NULL,
    'lottery' => 
    array (
      0 => 2,
      1 => 100,
    ),
    'cookie' => 'an_express_session',
    'path' => '/',
    'domain' => NULL,
    'secure' => false,
    'http_only' => true,
    'same_site' => NULL,
  ),
  'sitemap' => 
  array (
    'use_cache' => false,
    'cache_key' => 'laravel-sitemap.http://127.0.0.1:8000',
    'cache_duration' => 3600,
    'escaping' => true,
    'use_limit_size' => false,
    'max_size' => NULL,
    'use_styles' => true,
    'styles_location' => NULL,
    'use_gzip' => false,
  ),
  'tags' => 
  array (
    'slugger' => 'str_slug',
  ),
  'translatable' => 
  array (
    'fallback_locale' => 'en',
  ),
  'view' => 
  array (
    'paths' => 
    array (
      0 => 'F:\\wamp64\\www\\moneytransfer_ref\\resources\\views',
    ),
    'compiled' => 'F:\\wamp64\\www\\moneytransfer_ref\\storage\\framework\\views',
  ),
  'vue-i18n-generator' => 
  array (
    'langPath' => '/resources/lang',
    'langFiles' => 
    array (
    ),
    'excludes' => 
    array (
    ),
    'jsPath' => '/resources/js/langs/',
    'jsFile' => '/resources/js/vendor/vue-i18n-locales.generated.js',
    'i18nLib' => 'vue-i18n',
    'showOutputMessages' => false,
  ),
  'debugbar' => 
  array (
    'enabled' => false,
    'except' => 
    array (
      0 => 'telescope*',
    ),
    'storage' => 
    array (
      'enabled' => true,
      'driver' => 'file',
      'path' => 'F:\\wamp64\\www\\moneytransfer_ref\\storage\\debugbar',
      'connection' => NULL,
      'provider' => '',
    ),
    'include_vendors' => true,
    'capture_ajax' => true,
    'add_ajax_timing' => false,
    'error_handler' => false,
    'clockwork' => false,
    'collectors' => 
    array (
      'phpinfo' => true,
      'messages' => true,
      'time' => true,
      'memory' => true,
      'exceptions' => true,
      'log' => true,
      'db' => true,
      'views' => true,
      'route' => true,
      'auth' => false,
      'gate' => true,
      'session' => true,
      'symfony_request' => true,
      'mail' => true,
      'laravel' => false,
      'events' => false,
      'default_request' => false,
      'logs' => false,
      'files' => false,
      'config' => false,
      'cache' => false,
      'models' => false,
    ),
    'options' => 
    array (
      'auth' => 
      array (
        'show_name' => true,
      ),
      'db' => 
      array (
        'with_params' => true,
        'backtrace' => true,
        'timeline' => false,
        'explain' => 
        array (
          'enabled' => false,
          'types' => 
          array (
            0 => 'SELECT',
          ),
        ),
        'hints' => true,
      ),
      'mail' => 
      array (
        'full_log' => false,
      ),
      'views' => 
      array (
        'data' => false,
      ),
      'route' => 
      array (
        'label' => true,
      ),
      'logs' => 
      array (
        'file' => NULL,
      ),
      'cache' => 
      array (
        'values' => true,
      ),
    ),
    'inject' => true,
    'route_prefix' => '_debugbar',
    'route_domain' => NULL,
  ),
  'debug-server' => 
  array (
    'host' => 'tcp://127.0.0.1:9912',
  ),
  'honeybadger' => 
  array (
    'api_key' => NULL,
    'environment' => 
    array (
      'filter' => 
      array (
      ),
      'include' => 
      array (
      ),
    ),
    'request' => 
    array (
      'filter' => 
      array (
      ),
    ),
    'version' => NULL,
    'hostname' => 'hkhan',
    'project_root' => 'F:\\wamp64\\www\\moneytransfer_ref',
    'environment_name' => 'local',
    'handlers' => 
    array (
      'exception' => true,
      'error' => true,
    ),
    'client' => 
    array (
      'timeout' => 0,
      'proxy' => 
      array (
      ),
    ),
    'excluded_exceptions' => 
    array (
    ),
  ),
  'trustedproxy' => 
  array (
    'proxies' => NULL,
    'headers' => 30,
  ),
  'tinker' => 
  array (
    'commands' => 
    array (
    ),
    'dont_alias' => 
    array (
    ),
  ),
);
