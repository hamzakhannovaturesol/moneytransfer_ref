@extends('layouts.auth')

@push('styles')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/intlTelInput.css') }}">
<style>
    .hide {
        display: none;
    }

    #valid-msg {
        color: green;
        font-weight: 800;
        font-family: monospace;
    }
</style>
@endpush

@section('section')
<div class="register-form row justify-content-center">
    <div class="col-md-12 col-lg-12 mx-auto pt-6">
        <div class="register-card">
            <h3 class="card-head text-center">Register</h3>
            <div class="card-body">
                <form method="POST" action="{{ route('register') }}" id="register" name="register">
                    @csrf
                    <div class="form-group row">
                        <!-- FIRST NAME  -->
                        <label class="col-md-2 col-form-label"
                            for="first_name">@lang('validation.attributes.first_name')<span
                                style="color: red">*</span></label>
                        <div class="col-md-4">
                            {{ Form::bsText('first_name', 'first name', ['required', 'placeholder' => 'First Name']) }}
                        </div>

                        <!-- MIDDLE NAME  -->
                        <label class="col-md-2 col-form-label" for="middle_name">Middle Name (optional)</label>
                        <div class="col-md-4">
                            {{ Form::bsText('middle_name', 'middle name', ['placeholder' => 'Middle Name']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <!-- LAST NAME  -->
                        <label class="col-md-2 col-form-label"
                            for="last_name">@lang('validation.attributes.last_name')<span
                                style="color: red">*</span></label>
                        <div class="col-md-4">
                            {{ Form::bsText('last_name', 'last name', ['required', 'placeholder' => 'Last Name']) }}
                        </div>

                        <!-- EMAIL  -->
                        <label for="email" class="col-md-2 col-form-label">@lang('validation.attributes.email')</label>
                        <div class="col-md-4">
                            {{ Form::bsEmail('email', rand(0,11211154542120000).'email@email.com', ['required', 'placeholder' => __('validation.attributes.email')]) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <!-- TITLE  -->
                        <label class="col-md-2 col-form-label" for="title">Title</label>
                        <div class="col-md-4">
                            {{ Form::bsSelect('title', ['Mr.' => 'Mr.', 'Miss' => 'Miss', 'Mrs.' => 'Mrs.'], 'Mr.', []) }}
                        </div>



                        <!-- DOB  -->
                        <label class="col-md-2 col-form-label" for="date_of_birth">D.O.B <span
                                style="font-size: 75%;">(Age Limit 18 Years)</span></label>
                        <div class="col-md-4">
                            {{ Form::bsText('dob', null, ['data-defaultDate' => '201-03-01', 'data-enableTime' => 'true',   'placeholder' => 'Please Select Date of Birth']) }}
                        </div>
                        <!-- //DOB  -->



                    </div>

                    <div class="form-group row">


                        <!-- Post Code  -->
                        <label class="col-md-2 col-form-label" for="postcode">Post Code</label>
                        <div class="col-md-4">

                            <!--
                          {{ Form::bsText('postcode', null, ['value' => 'N1 0AA','required', 'placeholder' => 'Post Code', 'name' => 'postcode', 'id'=>'postcode', 'style'=>'']) }}
  		                    <input type=button value="Find"
  		                        onclick="Javascript:getAdress(document.getElementById('postcode').value)">
                        -->
                            <div class="input-group">
                                {{ Form::bsText('postcode', 'post code', ['value' => 'N1 0AA','required', 'placeholder' => 'Post Code', 'name' => 'postcode', 'id'=>'postcode', 'style'=>'']) }}
                                <span class="input-group-btn">
                                    <button type="button"
                                        onclick="Javascript:getAdress(document.getElementById('postcode').value)">Find</button>
                                </span>
                            </div>

                            <!-- <div class="form-group row">
                         <div class="col-md-2">
                              {{ Form::bsText('postcode', null, ['value' => 'N1 0AA','required', 'placeholder' => 'Post Code', 'name' => 'postcode', 'id'=>'postcode', 'style'=>'']) }}
                         </div>
                         <div class="col-md-2">
                              <input type=button value="Find" onclick="Javascript:getAdress(document.getElementById('postcode').value)">
                         </div>
                      </div> -->
                            <div id="SPLSearchArea" style="display:none;position: absolute;
		                    background: white;z-index: 1;border: 0.1px solid darkslateblue;top: 47px;
		                    width: 385px;overflow-y: scroll;height: 300px;left: 0px;"></div>
                        </div>
                        <!-- //Post Code  -->


                        <!-- NATIONALITY  -->
                        <label class="col-md-2 col-form-label" for="nationality">Nationality <span
                                style="color: red">*</span></label>
                        <div class="col-md-4">
                            <select id="nationalityField" placeholder="nationalityField" class="select2 form-control"
                                name="nationality" required>
                                {{-- <option value="">Please Select Country</option> --}}
                            </select>
                        </div>
                        <!-- //NATIONALITY  -->

                    </div>

                    <div class="form-group row">
                        <!-- ADDRESS 1  -->
                        <label class="col-md-2 col-form-label" for="address_1">Address 1<span
                                style="color: red">*</span></label>
                        <div class="col-md-4">
                            {{ Form::bsTextarea('address_1', 'addres 1', ['required', 'placeholder' => 'Address 1', 'rows' => '3']) }}
                        </div>

                        <!-- ADDRESS 2  -->
                        <label class="col-md-2 col-form-label" for="address_2">Address 2</label>
                        <div class="col-md-4">
                            {{ Form::bsTextarea('address_2', 'address 2', ['placeholder' => 'Address 2', 'rows' => '3']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <!-- Country  -->
                        <label class="col-md-2 col-form-label" for="country_id">Country <span
                                style="color: red">*</span></label>
                        <div class="col-md-4">
                            <select id="countryField" placeholder="country_id" class="select2 form-control"
                                name="country_id" required>
                                {{-- <option value="">Please Select Country</option> --}}
                            </select>
                        </div>

                        <script>



                        </script>
                        <!-- City  -->
                        <label class="col-md-2 col-form-label" for="city">City</label>
                        <div class="city-static col-md-4">
                            {{ Form::bsText('city', 'city', ['placeholder' => 'City']) }}
                        </div>
                        <div class="city-dropdown col-md-4" style="display: none;">
                            <select id="cityField" class="select2 form-control" name="city">

                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <!-- PASSWORD  -->
                        <label for="password" class="col-md-2 col-form-label">@lang('validation.attributes.passwordreg')
                            <span style="color: red">*</span>

                            <span class="tooltiptextx">
                                <i id="password_tooltip" class="ti-info-alt"
                                    title="Passwords must contain: \n a minimum of 1 upper case letter [A-Z] and \n a minimum of 1 lower case letter [a-z] and \n a minimum of 1 special character \n a minimum of 1 numeric character [0-9] \n and at least 8 characters."></i>
                            </span>

                        </label>
                        <div class="col-md-4">
                            {{ Form::bsPassword('password', ['placeholder' => __('validation.attributes.passwordreg'),
                            'data-toggle' => 'password-strength-meter','value'=>'Hamzakhan@123']) }}


                        </div>

                        <!-- CONFIRM PASSWORD  -->
                        <label for="password_confirmation"
                            class="col-md-2 col-form-label">@lang('validation.attributes.password_confirmation')</label>
                        <div class="col-md-4">
                            {{ Form::bsPassword('password_confirmation', ['autocomplete'=> 'off', 'placeholder' => __('validation.attributes.password_confirmation'),'value'=>'Hamzakhan@123']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <!-- MOBILE NO  -->
                        <label for="mobile_no" class="col-md-2 col-form-label">@lang('validation.attributes.mobile_no')
                            <span class="tooltiptextx">
                                <i id="mobile_no_tooltip" class="ti-info-alt"
                                    title="Mobile Number Format: XXXX-XXXXXX"></i>
                            </span>
                        </label>
                        <div class="col-md-4">
                            {{ Form::bsText('mobile_no', '143463'.rand(0,999).'6', ['maxlength' => '14', 'class'=> 'mobile-number','required', 'onkeypress'=>'return isNumberKey(event)']) }}

                            <span id="valid-msg" class="hide">✓ Valid</span>
                            <span id="error-msg" class="hide"></span>

                        </div>



                        <!-- PHONE NO -->
                        <label for="phone_no" class="col-md-2 col-form-label">Phone Number</label>
                        <div class="col-md-4">
                            {{ Form::bsText('phone_no', '123456', ['placeholder' => 'Phone Number']) }}
                        </div>
                    </div>


                    <div class="form-group row">
                        <!-- PLACE OF BIRTH  -->
                        <label class="col-md-2 col-form-label" for="place_of_birth">Place of Birth <span
                                style="color: red">*</span></label>
                        <div class="col-md-4">
                            {{ Form::bsText('place_of_birth', '123456', ['required', 'placeholder' => 'Place of Birth']) }}
                        </div>

                        <!-- Occupation required -->
                        <label class="col-md-2 col-form-label" for="place_of_birth">Occupation <span
                                style="color: red">*</span></label>
                        <div class="col-md-4">
                            {{ Form::bsText('occupation', 'Software Developer', ['required', 'placeholder' => 'Occupation']) }}
                        </div>
                    </div>

                    <div class="form-group row">

                        <!-- Annual_Income required  -->
                        <label class="col-md-2 col-form-label" for="">Annual Income <span
                                style="color: red">*</span></label>
                        <div class="col-md-4">
                            {{ Form::bsText('annual_income', '333', ['required', 'placeholder' => 'Annual Income']) }}
                        </div>

                        <!-- Exp_Month_Volume required -->
                        <label class="col-md-2 col-form-label" for="">Exp Month Volume <span
                                style="color: red">*</span></label>
                        <div class="col-md-4">
                            {{ Form::bsText('exp_month_volume', '123', ['required', 'placeholder' => 'Exp Month Volume']) }}
                        </div>

                    </div>


                    <div class="form-group row">

                        <!-- Is there any member required -->
                        <div class="col-md-8 col-form-label" for="">Is there any member of your extended family holding
                            a
                            senior governmental position outside of UK or within the UK? <span
                                style="color: red">*</span></div>

                        <div class="col-md-4">
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input position_radio" name="gov_position"
                                        value="y">&nbsp; Yes
                                </label>
                            </div>
                            <div class="form-check-inline">
                                <label class="form-check-label">
                                    <input type="radio" class="form-check-input position_radio" name="gov_position"
                                        value="n" checked="checked">&nbsp; No
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row" id="show_input_box" style="display: none;">
                        <div class="col-md-12" style="float: none;margin: 0 auto;">
                            <textarea name="gov_position_box" id="gov_position_box" cols="30" rows="10"
                                style="resize: none;" placeholder="Enter description here."></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">

                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="" id="consent_marketing" name="consent_marketing"
                                    value="1" checked="">
                                <label class="custom-control-label" for="consent_marketing">I Agree to the Consent for
                                    Marketing</label>
                            </div>

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">

                            <!-- Acceptance of terms and condition -->
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" id="terms" name="terms" checked>
                                <label class="custom-control-label" for="consent_marketing">I have read and accept the<a
                                        href="https://www.anexpress.com/terms-and-conditions">Terms and
                                        Conditions</a></label>
                            </div>
                            <!-- /Acceptance of terms and condition -->

                        </div>
                    </div>

                    <!-- buttons -->
                    <div class="form-group row">
                        <div class="col-md-12 ml-auto text-center">
                            <button class="btn btn-success pl-8 pr-8">@lang('labels.user.register')</button>
                            <a class="btn btn-primary pl-8 pr-8" href="{{ route('login') }}"
                                style="vertical-align: top;"><i class="fe fe-log-in"></i>Login</a>
                        </div>
                    </div>
                    <!-- //buttons -->


                    <div class="form-group row">
                        <div class="col-md-9 ml-auto">
                            <input type="hidden" name="role" value="customer">
                            {{-- {!! Form::captcha() !!} --}}
                        </div>
                    </div>

            </div>
            <!-- card-body -->

            </form>
        </div>
    </div>
</div>
</div>
@endsection

@push('scripts')
<!-- <script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<!-- tooltip  -->
<script src="https://unpkg.com/popper.js@1"></script>
<script src="https://unpkg.com/tippy.js@4"></script>
<!-- //tooltip -->

<!-- mask js  -->
<script type="text/javascript" src="{{ asset('js/jquery.mask.min.js') }}"></script>
<!-- phone flags -->


<script type="text/javascript" src="{{ asset('js/intlTelInput.min.js') }}"></script>
<script>
    $(document).ready(function () {

        var i = document.querySelector("#mobile_no"), errorMsg = document.querySelector("#error-msg"), validMsg = document.querySelector("#valid-msg");
        var errorMap = ["Invalid number", "Invalid country code", "Too short", "Too long", "Invalid number"];

        var iti = window.intlTelInput(i, {
            initialCountry: "gb",
            utilsScript: "{{ asset('js/utils.js') }}",
        });

        var reset = function () {
            i.classList.remove("error");
            errorMsg.innerHTML = "";
            errorMsg.classList.add("hide");
            validMsg.classList.add("hide");
        };

        // on blur: validate
        i.addEventListener('blur', function () {
            reset();
            if (i.value.trim()) {
                if (iti.isValidNumber()) {
                    validMsg.classList.remove("hide");
                } else {
                    i.classList.add("error");
                    var errorCode = iti.getValidationError();
                    errorMsg.innerHTML = errorMap[errorCode];
                    errorMsg.classList.remove("hide");
                }
            }
        });

        // on keyup / change flag: reset
        i.addEventListener('change', reset);
        i.addEventListener('keyup', reset);






        // var i = document.getElementById("mobile_no");
        // var iti = intlTelInput(i, {
        //     initialCountry: "gb"
        // });


        // $("#mobile_no").intlTelInput({
        //     initialCountry: "gb",
        //     formatOnInit: true
        // });

        // $('#mobile_no').mask('SSSS-YYYYYY', {
        //     'translation': {
        //         S: { pattern: /[0-9]/ },
        //         Y: { pattern: /[0-9]/ }
        //     }
        // });


        getCountries_();
        getAllCountries();
        $('#countryField').select2({
            placeholder: "Please Select Country",
            allowClear: true
        });
        $('#nationalityField').select2({
            placeholder: "Please Select Country of Nationality",
            allowClear: true
        });
        var date = new Date();
        var intYear = date.getFullYear() - 18;
        var intMonth = date.getMonth() + 1;
        var intDay = date.getDate();
        var min = intYear + '-' + intMonth + '-' + intDay;
        $("#dob").flatpickr({
            mode: "single",
            maxDate: new Date(min),
            dateFormat: "Y-m-d",
            defaultDate: ["2001-11-27"]

        });
        $('#countryField').change(function () {
            var val = $('#countryField').val();
            getCities(val);
        });
        $('#nationalityField').change(function () {
            var val = $('#nationalityField').val();
        });


        $(".position_radio").click(function () {
            var position_radio = $(this).val();

            if (position_radio == 'y') {
                $(this).prop("checked", true);
                $("#show_input_box").show();
            } else {
                $(this).prop("checked", true);
                $("#show_input_box").hide();
            }
        });

        // terms and condition checkbox.
        $("#register").submit(function (e) {

            if (!$('#city').val().length === 0) {
                alert("Please enter City!");

                $("#city").focus();
                // e.preventDefault();
                return false;
            }

            var regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/

            if (!regex.test($("#password").val())) {
                alert("Passwords must contain: \n a minimum of 1 upper case letter [A-Z] and \n a minimum of 1 lower case letter [a-z] and \n a minimum of 1 special character \n a minimum of 1 numeric character [0-9] \n and at least 8 characters.");
                return false;
            }

            // trim all input fileds.
            $('input').val(function (_, value) {
                return $.trim(value);
            });

            if (!$('#terms').is(':checked')) {
                alert("Please Acceptance Terms and condition!");
                e.preventDefault();
                return false;
            }

            // let mobile_no = $('#mobile_no').val();
            // if (mobile_no != "") {

            //     mobile_no = mobile_no.replace("(", ""); // remove )
            //     mobile_no = mobile_no.replace(")", ""); // remove (
            //     mobile_no = mobile_no.replace("-", ""); // remove -
            //     mobile_no = mobile_no.replace(" ", ""); // remove space

            //     $('#mobile_no').val(mobile_no);
            // }

            // var dialCode_ = null;
            // var mobile_no = $('#mobile_no').val();
            // if (mobile_no != "") {
            //     // remove dash from phone number
            //     mobile_no = mobile_no.replace("-", ""); // remove masking dash
            //     if (mobile_no.length < 10) {
            //         alert("Please enter correct mobile number.")
            //         return false;
            //     }
            //     if (mobile_no.length == 13) {
            //         mobile_no = mobile_no;
            //     } else {
            //         dialCode_ = $("#mobile_no").intlTelInput("getSelectedCountryData").dialCode
            //         mobile_no = "+" + dialCode_ + mobile_no;
            //     }

            //     $('#mobile_no').val(mobile_no);

            //     console.log($('#mobile_no').val());

            //     // return false;
            // }

            var num = iti.getNumber(), valid = iti.isValidNumber();
            console.log(typeof valid)
            console.log(valid)
            if (valid === false) {
                alert("Please enter valid mobile number.");
                return false;
            }

            $('#mobile_no').val(num);
            console.log(num);
            // return false;
        });
    });

    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }


    // Mobile No Tooltip
    tippy('#mobile_no_tooltip', {
        content: 'Mobile Format: (+XX) XXXX-XXXXXX e.g (+44) 1234-567891',
    });
    // password Tooltip
    tippy('#password_tooltip', {
        content: 'Password Format: Passwords must contain: \n a minimum of 1 upper case letter [A-Z] and \n a minimum of 1 lower case letter [a-z] and \n a minimum of 1 special character \n a minimum of 1 numeric character [0-9] and'
    });

    // Register ajax request.
    function getAdress(postcode_input) {

        if (postcode_input != null && postcode_input != "") {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?php echo csrf_token() ?>'
                }
            });

            $.ajax({
                type: 'GET',
                url: '/getaddresses',
                data: {
                    postcode: postcode_input
                },
                success: function (response) {

                    console.log(response);
                    if (response != null || response != "") {

                        let address = JSON.parse(response.data);

                        if (address.recordcount != "0") {
                            out = "<ul style='list-style-type: none'>";
                            for (let index = 0; index < address.records.length; index++) {
                                const element = address.records[index];
                                out += '<li><a href="javascript:void(0)" onClick=getFullAddress("' + element.id + '")>';
                                out += element.l;
                                out += "</a></li>";
                            }
                            out += "</ul>";

                            document.getElementById("SPLSearchArea").innerHTML = out;
                            $("#SPLSearchArea").show();

                        } else {

                            $("#address_1").val('');
                            $("#city").val('');

                            $('#countryField').val('');
                            $('#countryField').select2().trigger('change');

                            alert("Invalid area postal code.");
                        }
                    }
                }
            });

        } else {
            alert("Please enter postal code.");
            $("#postcode").focus();
        }
    }

    // Get Full Address Details.
    function getFullAddress(AddressID = 0) {
        var address_1;
        var address_2;
        var address_3;
        var country;

        // hide dropdown.
        $("#SPLSearchArea").hide();

        $.ajax({
            type: 'GET',
            url: '/getaddressDetails',
            data: {
                address_id: AddressID
            },
            success: function (response) {

                if (response != null || response != "") {
                    addressDetails = JSON.parse(response.data);
                    console.log(addressDetails.record);

                    // address line 1
                    if (typeof addressDetails.record.line1 != "object") {
                        address_1 = addressDetails.record.line1;
                    } else {
                        address_1 = "";
                    }

                    // address line 2
                    if (typeof addressDetails.record.line2 != "object") {
                        address_2 = addressDetails.record.line2;
                    } else {
                        address_2 = "";
                    }

                    // address line 3
                    if (typeof addressDetails.record.line3 != "object") {
                        address_3 = addressDetails.record.line3;
                    } else {
                        address_3 = "";
                    }

                    // city
                    if (typeof addressDetails.record.town != "object") {
                        city = addressDetails.record.town;
                    } else {
                        city = "";
                    }

                    // country
                    if (typeof addressDetails.record.country != "object") {
                        country = addressDetails.record.country;
                    } else {
                        country = "";
                    }

                    $("#address_1").val(address_1);
                    $("#city").val(city);

                    if (country == "England") {
                        // set countryField
                        $('#countryField').val('222'); // united kingdom
                        $('#countryField').select2().trigger('change');
                    }

                }
            }
        });
    }

    function getCountries_() {
        jQuery.ajax({
            url: "{{ route('api.getTotalCountries') }}",
            method: 'get',
            data: {},
            success: function (result) {
                if (result.length > 0) {
                    $('#countryField').append($("<option></option>").attr("value", '').text(
                        'Please Select Country'));
                    $.each(result, function (key, value) {
                        $('#countryField').append($("<option></option>")
                            .attr("value", value.value).text(value.text));
                    });
                }
            }
        });
    }

    function getAllCountries() {
        jQuery.ajax({
            url: "{{ route('api.AllCountries') }}",
            method: 'get',
            data: {},
            success: function (result) {
                if (result.length > 0) {
                    $('#nationalityField').append($("<option></option>").attr("value", '').text(
                        'Please Select Country of Nationality'));
                    $.each(result, function (key, value) {
                        $('#nationalityField').append($("<option></option>")
                            .attr("value", value.value).text(value.text));
                    });

                }
            }
        });
    }

    function getCities(country_id) {
        jQuery.ajax({
            url: "{{ route('api.getCountryCities') }}",
            method: 'get',
            data: {
                country_id: country_id
            },
            success: function (result) {
                $('#cityField').empty();
                if (result.length > 0) {
                    $('.city-static').css('display', 'none');
                    $('.city-dropdown').css('display', 'block');
                    $('#cityField').select2({
                        placeholder: "Please Select City",
                        allowClear: true
                    });
                    $('#cityField').append($("<option></option>").attr("value", '').text(
                        'Please Select City'));
                    $.each(result, function (key, value) {
                        $('#cityField').append($("<option></option>")
                            .attr("value", value.value).text(value.text));
                    });
                } else {
                    $('.city-dropdown').css('display', 'none');
                    $('.city-static').css('display', 'block');
                }
            }
        });
    }






    // $('select#nationalityField').val('4');
    // $('select#nationalityField').val(4);
    // $('select#nationalityField').select2().trigger('change');


    // $('select#countryField').val('4');
    // $('select#countryField').val(4);
    // $('select#countryField').select2().trigger('change');

    $(document).ready(function () {
        $("body").on('DOMNodeInserted', '#countryField', function () {
            $("select#countryField option:last").attr("selected", "selected");
        });
        $("body").on('DOMNodeInserted', '#nationalityField', function () {
            $("select#nationalityField option:last").attr("selected", "selected");
        });

        $("#password").val("Hamzakhan@123");
        $("#password_confirmation").val("Hamzakhan@123");

    });
</script>
{{-- {!! Captcha::script() !!} --}}
@endpush