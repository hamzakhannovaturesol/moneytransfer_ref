@extends('layouts.auth')

@push('styles')
<link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
<style>
    textarea {
        resize: none;
    }
</style>
@endpush

@section('section')
<div class="register-form row justify-content-center">
    <div class="col-md-12 col-lg-12 mx-auto pt-6">
        <div class="register-card">
            <h3 class="card-head text-center">Register</h3>
            <div class="card-body">
                <form method="POST" action="{{ route('register') }}" id="register" name="register">
                    @csrf
                    <div class="form-group row">
                        <!-- FIRST NAME  -->
                        <label class="col-md-2 col-form-label"
                            for="first_name">@lang('validation.attributes.first_name')</label>
                        <div class="col-md-4">
                            {{ Form::bsText('first_name', null, ['required', 'placeholder' => 'First Name' ]) }}
                        </div>

                        <!-- MIDDLE NAME  -->
                        <label class="col-md-2 col-form-label" for="middle_name">Middle Name (optional)</label>
                        <div class="col-md-4">
                            {{ Form::bsText('middle_name', null, ['placeholder' => 'Middle Name']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <!-- LAST NAME  -->
                        <label class="col-md-2 col-form-label"
                            for="last_name">@lang('validation.attributes.last_name')</label>
                        <div class="col-md-4">
                            {{ Form::bsText('last_name', null, ['required', 'placeholder' => 'Last Name']) }}
                        </div>

                        <!-- EMAIL  -->
                        <label for="email" class="col-md-2 col-form-label">@lang('validation.attributes.email')</label>
                        <div class="col-md-4">
                            {{ Form::bsEmail('email', null, ['required', 'placeholder' => __('validation.attributes.email')]) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <!-- TITLE  -->
                        <label class="col-md-2 col-form-label" for="title">Title</label>
                        <div class="col-md-4">
                            {{ Form::bsSelect('title', ['Mr.' => 'Mr.', 'Miss' => 'Miss', 'Mrs.' => 'Mrs.'], 'Mr.', []) }}
                        </div>

                        <!-- DOB  -->
                        <label class="col-md-2 col-form-label" for="date_of_birth">D.O.B <span
                                style="font-size: 75%;">(Age Limit 18 Years)</span></label>
                        <div class="col-md-4">
                            {{ Form::bsText('dob', null, ['required', 'placeholder' => 'Please Select Date of Birth']) }}
                        </div>
                    </div>

                    <div class="form-group row">
                        <!-- City  -->
                        <label class="col-md-2 col-form-label" for="city">City</label>
                        <div class="city-static col-md-4">
                            {{ Form::bsText('city', null, ['placeholder' => 'City']) }}
                        </div>
                        <div class="city-dropdown col-md-4" style="display: none;">
                            <select id="cityField" class="select2 form-control" name="city">
                                {{-- <option value="">Please Select City</option> --}}
                            </select>
                        </div>
                        <!-- NATIONALITY  -->
                        <label class="col-md-2 col-form-label" for="nationality">Nationality <span
                                style="color: red">*</span></label>
                        {{--   <div class="col-md-4">
                                {{ Form::bsText('nationality', null, ['required', 'placeholder' => 'Nationality']) }}
                    </div> --}}
                    <div class="col-md-4">
                        <select id="nationalityField" placeholder="nationalityField" class="select2 form-control"
                            name="nationality" required>
                            {{-- <option value="">Please Select Country</option> --}}
                        </select>
                    </div>
            </div>

            <div class="form-group row">
                <!-- ADDRESS 1  -->
                <label class="col-md-2 col-form-label" for="address_1">Address 1</label>
                <div class="col-md-4">
                    {{ Form::bsTextarea('address_1', null, ['required', 'placeholder' => 'Address 1', 'rows' => '3']) }}
                </div>

                <!-- ADDRESS 2  -->
                <label class="col-md-2 col-form-label" for="address_2">Address 2</label>
                <div class="col-md-4">
                    {{ Form::bsTextarea('address_2', null, ['placeholder' => 'Address 2', 'rows' => '3']) }}
                </div>
            </div>

            <div class="form-group row">
                <!-- Country  -->
                <label class="col-md-2 col-form-label" for="country_id">Country <span
                        style="color: red">*</span></label>
                <div class="col-md-4">
                    <select id="countryField" placeholder="country_id" class="select2 form-control" name="country_id"
                        required>
                        {{-- <option value="">Please Select Country</option> --}}
                    </select>
                </div>

                <!-- Post Code  -->
                <label class="col-md-2 col-form-label" for="postcode">Post Code</label>
                <div class="col-md-4">
                    {{ Form::bsText('postcode', null, ['value' => 'N1 0AA','required', 'placeholder' => 'Post Code', 'name' => 'postcode', 'id'=>'postcode', 'style'=>'width:100px;display: inline-block;']) }}
                    <input type=button value="Find"
                        onclick="Javascript:getAdress(document.getElementById('postcode').value)">

                    <div id="SPLSearchArea" style="display:none;position: absolute;
                    background: white;z-index: 1;border: 0.1px solid darkslateblue;top: 47px;
                    width: 385px;overflow-y: scroll;height: 300px;left: 0px;" />
                </div>
            </div>

            <div class="form-group row">
                <!-- PASSWORD  -->
                <label for="password" class="col-md-2 col-form-label">@lang('validation.attributes.passwordreg')</label>
                <div class="col-md-4">
                    {{ Form::bsPassword('password', ['autocomplete'=> 'off','placeholder' => __('validation.attributes.passwordreg'), 'data-toggle' => 'password-strength-meter']) }}
                </div>

                <!-- CONFIRM PASSWORD  -->
                <label for="password_confirmation"
                    class="col-md-2 col-form-label">@lang('validation.attributes.password_confirmation')</label>
                <div class="col-md-4">
                    {{ Form::bsPassword('password_confirmation', ['autocomplete'=> 'off', 'placeholder' => __('validation.attributes.password_confirmation')]) }}
                </div>
            </div>

            <div class="form-group row">
                <!-- MOBILE NO  -->
                <label for="mobile_no" class="col-md-2 col-form-label">@lang('validation.attributes.mobile_no')
                    <span class="tooltiptextx">
                        <i id="mobile_no_tooltip" class="ti-info-alt"
                            title="Mobile Number Format: (+XX) XXXX-XXXXXX"></i>
                    </span>
                </label>
                <div class="col-md-4">
                    {{ Form::bsText('mobile_no', null, ['maxlength' => '14', 'class'=> 'phone-number','required', 'placeholder' => '(+XX) XXXX-XXXXXX']) }}
                </div>

                <!-- PHONE NO -->
                <label for="phone_no" class="col-md-2 col-form-label">Phone Number</label>
                <div class="col-md-4">
                    {{ Form::bsText('phone_no', null, ['placeholder' => 'Phone Number']) }}
                </div>
            </div>

            <div class="form-group row">

                <!-- PLACE OF BIRTH  -->
                <label class="col-md-2 col-form-label" for="place_of_birth">Place of Birth</label>
                <div class="col-md-4">
                    {{ Form::bsText('place_of_birth', null, ['required', 'placeholder' => 'Place of Birth']) }}
                </div>

                <!-- Occupation required -->
                <label class="col-md-2 col-form-label" for="place_of_birth">Occupation <span
                        style="color: red">*</span></label>
                <div class="col-md-4">
                    {{ Form::bsText('occupation', null, ['required', 'placeholder' => 'Occupation']) }}
                </div>

            </div>

            <div class="form-group row">

                <!-- Annual_Income required  -->
                <label class="col-md-2 col-form-label" for="">Annual Income <span style="color: red">*</span></label>
                <div class="col-md-4">
                    {{ Form::bsText('annual_income', null, ['required', 'placeholder' => 'Annual Income']) }}
                </div>

                <!-- Exp_Month_Volume required -->
                <label class="col-md-2 col-form-label" for="">Exp Month Volume <span style="color: red">*</span></label>
                <div class="col-md-4">
                    {{ Form::bsText('exp_month_volume', null, ['required', 'placeholder' => 'Exp Month Volume']) }}
                </div>

            </div>


            <div class="form-group row">

                <!-- Is there any member required -->
                <div class="col-md-8 col-form-label" for="">(Is there any member of your extended family holding a
                    senior governmental position outside of UK? <span style="color: red">*</span></div>

                <div class="col-md-4">
                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input position_radio" name="gov_position"
                                value="y">&nbsp; Yes
                        </label>
                    </div>
                    <div class="form-check-inline">
                        <label class="form-check-label">
                            <input type="radio" class="form-check-input position_radio" name="gov_position" value="n"
                                checked="checked">&nbsp; No
                        </label>
                    </div>
                </div>
            </div>

            <div class="form-group row" id="show_input_box" style="display: none;">
                <div class="col-md-12" style="float: none;margin: 0 auto;">
                    <textarea name="gov_position_box" id="gov_position_box" cols="30" rows="10" placeholder=""
                        style="resize: none;"></textarea>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-12">

                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="" id="consent_marketing" name="consent_marketing" value="1"
                            checked="">
                        <label class="custom-control-label" for="consent_marketing">I Agree to the Consent for
                            Marketing</label>
                    </div>

                    <!-- Acceptance of terms and condition -->
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" id="terms" name="terms">
                        <label class="custom-control-label" for="consent_marketing">Acceptance of <a
                                href="/front/termsofuse">terms and condition</a></label>
                    </div>
                    <!-- /Acceptance of terms and condition -->

                </div>
            </div>

            <!-- buttons -->
            <div class="form-group row">
                <div class="col-md-12 ml-auto text-center">
                    <button class="btn btn-success pl-8 pr-8">@lang('labels.user.register')</button>
                    <a class="btn btn-primary pl-8 pr-8" href="{{ route('login') }}" style="vertical-align: top;"><i
                            class="fe fe-log-in"></i>Login</a>
                </div>
            </div>
            <!-- //buttons -->


            <div class="form-group row">
                <div class="col-md-9 ml-auto">
                    <input type="hidden" name="role" value="customer">
                </div>
            </div>

        </div>
        <!-- card-body -->


        </form>
    </div>
</div>
</div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<!-- In Public Folder -->
<script src="{{ asset('js/SPL_AJAX_Full.js') }}"></script>


<!-- tooltip  -->
<script src="https://unpkg.com/popper.js@1"></script>
<script src="https://unpkg.com/tippy.js@4"></script>
<!-- //tooltip -->


<!-- masking -->
<script type="text/javascript" src="https://igorescobar.github.io/jQuery-Mask-Plugin/js/jquery.mask.min.js"></script>

<script>
    $(document).ready(function () {

        $('.phone-number').mask('(+00) 0000-000000');

        // Mobile No Tooltip
        tippy('#mobile_no_tooltip', {
            content: 'Mobile Format: (+XX) XXXX-XXXXXX e.g (+44) 1234-567891',
        });



        getCountries();
        getAllCountries();
        $('#countryField').select2({
            placeholder: "Please Select Country",
            allowClear: true
        });
        $('#nationalityField').select2({
            placeholder: "Please Select Country of Nationality",
            allowClear: true
        });
        var date = new Date();
        var intYear = date.getFullYear() - 18;
        var intMonth = date.getMonth() + 1;
        var intDay = date.getDate();
        var min = intYear + '-' + intMonth + '-' + intDay;
        $("#dob").flatpickr({
            mode: "single",
            maxDate: new Date(min),
            dateFormat: "Y-m-d"
        });
        $('#countryField').change(function () {
            var val = $('#countryField').val();
            getCities(val);
        });
        $('#nationalityField').change(function () {
            var val = $('#nationalityField').val();
        });


        $(".position_radio").click(function () {
            var position_radio = $(this).val();

            if (position_radio == 'y') {
                $(this).prop("checked", true);
                $("#show_input_box").show();
            } else {
                $(this).prop("checked", true);
                $("#show_input_box").hide();
            }
        });

        // terms and condition checkbox.
        $("#register").submit(function (e) {
            if (!$('#terms').is(':checked')) {
                alert("Please Acceptance Terms and condition!");
                e.preventDefault();
                return false;
            }

            let mobile_no = $('#mobile_no').val();
            if (mobile_no != "") {

                mobile_no = mobile_no.replace("(", ""); // remove )
                mobile_no = mobile_no.replace(")", ""); // remove (
                mobile_no = mobile_no.replace("-", ""); // remove -
                mobile_no = mobile_no.replace(" ", ""); // remove space

                $('#mobile_no').val(mobile_no);
            }
        });
    });

    document.getElementById("postcode").value = 'N1 0AA';

    // register ajax request.
    function getAdress(postcode_input)
    {

        if (postcode_input != null && postcode_input != "") {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '<?php echo csrf_token() ?>'
                }
            });

            $.ajax({
                type: 'GET',
                url: '/getaddresses',
                data: {
                    postcode: postcode_input
                },
                success: function (response) {


                    console.log(response);
                    if (response != null || response != "") {

                        let address = JSON.parse(response.data);

                        if (address.recordcount != "0") {
                            out = "<ul style='list-style-type: none'>";
                            for (let index = 0; index < address.records.length; index++) {
                                const element = address.records[index];
                                out += '<li><a href="javascript:void(0)" onClick=getFullAddress("' + element.id + '")>';
                                out += element.l;
                                out += "</a></li>";
                            }
                            out += "</ul>";

                            document.getElementById("SPLSearchArea").innerHTML = out;
                            $("#SPLSearchArea").show();

                        } else {

                            $("#address_1").val('');
                            $("#city").val('');

                            $('#countryField').val('');
                            $('#countryField').select2().trigger('change');

                            // set nationalityField
                            $('#nationalityField').val('');
                            $('#nationalityField').select2().trigger('change');

                            alert("Invalid area postal code.");
                        }
                    }
                }
            });

        } else {
            alert("Please enter postal code.");
            $("#postcode").focus();
        }
    }

    function getFullAddress(AddressID = 0)
    {
        // hide dropdown.
        $("#SPLSearchArea").hide();

        $.ajax({
            type: 'GET',
            url: '/getaddressDetails',
            data: {
                address_id: AddressID
            },
            success: function (response) {

                if (response != null || response != "") {

                    addressDetails = JSON.parse(response.data);
                    console.log(addressDetails.record);

                    let address_1 = addressDetails.record.line1 + " " + addressDetails.record.line2 + " " + addressDetails.record.line3;
                    let city = addressDetails.record.town;
                    $("#address_1").val(address_1);
                    $("#city").val(city);

                    let country = addressDetails.record.country;

                    if (country == "England")
                        // set countryField
                        $('#countryField').val('222'); // united kingdom
                    $('#countryField').select2().trigger('change');

                    // set nationalityField
                    $('#nationalityField').val('222'); // united kingdom
                    $('#nationalityField').select2().trigger('change');
                }
            }
        });
    }

    function getCountries() {
        jQuery.ajax({
            url: "{{ route('api.getTotalCountries') }}",
            method: 'get',
            data: {},
            success: function (result) {
                if (result.length > 0) {
                    $('#countryField').append($("<option></option>").attr("value", '').text(
                        'Please Select Country'));
                    $.each(result, function (key, value) {
                        $('#countryField').append($("<option></option>")
                            .attr("value", value.value).text(value.text));
                    });
                }
            }
        });
    }

    function getAllCountries() {
        jQuery.ajax({
            url: "{{ route('api.AllCountries') }}",
            method: 'get',
            data: {},
            success: function (result) {
                if (result.length > 0) {
                    $('#nationalityField').append($("<option></option>").attr("value", '').text(
                        'Please Select Country of Nationality'));
                    $.each(result, function (key, value) {
                        $('#nationalityField').append($("<option></option>")
                            .attr("value", value.value).text(value.text));
                    });

                }
            }
        });
    }

    function getCities(country_id) {
        jQuery.ajax({
            url: "{{ route('api.getCountryCities') }}",
            method: 'get',
            data: {
                country_id: country_id
            },
            success: function (result) {
                $('#cityField').empty();
                if (result.length > 0) {
                    $('.city-static').css('display', 'none');
                    $('.city-dropdown').css('display', 'block');
                    $('#cityField').select2({
                        placeholder: "Please Select City",
                        allowClear: true
                    });
                    $('#cityField').append($("<option></option>").attr("value", '').text(
                        'Please Select City'));
                    $.each(result, function (key, value) {
                        $('#cityField').append($("<option></option>")
                            .attr("value", value.value).text(value.text));
                    });
                } else {
                    $('.city-dropdown').css('display', 'none');
                    $('.city-static').css('display', 'block');
                }
            }
        });

    }

</script>
{{-- {!! Captcha::script() !!} --}}
@endpush