<!-- @extends('layouts.frontend') -->
@extends('layouts.auth')

<!-- @section('body_class', 'login-page') -->

<!-- @section('section') -->
@section('section')
<div class="login-form row justify-content-center">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="col-md-offset-3 col-md-12">
        <form method="POST" action="{{ route('password.request') }}">
            @csrf

            <div class="login-card">
                <h3 class="card-head text-center">
                    @lang('labels.user.password_reset')
                </h3>
                <div class="card-body">
                    @csrf

                    <input type="hidden" name="token" value="{{ $token }}" />

                    <div class="form-group">
                        <label for="email"
                            >@lang('validation.attributes.email')</label
                        >
                        {{ Form::bsEmail('email', null, ['required', 'placeholder' => __('validation.attributes.email')]) }}
                    </div>

                    <div class="form-group">
                        <label for="password"
                            >@lang('validation.attributes.password')</label
                        >
                        {{ Form::bsPassword('password', ['required', 'placeholder' => __('validation.attributes.password'), 'data-toggle' => 'password-strength-meter']) }}
                    </div>

                    <div class="form-group">
                        <label for="password_confirmation"
                            >@lang('validation.attributes.password_confirmation')</label
                        >
                        {{ Form::bsPassword('password_confirmation', ['required', 'placeholder' => __('validation.attributes.password_confirmation')]) }}
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary">
                            @lang('labels.user.password_reset')
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection
