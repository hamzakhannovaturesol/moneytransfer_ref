<!-- @extends('layouts.frontend') -->
@extends('layouts.auth') @section('body_class', 'login-page')

<!-- Main Content -->
<!-- @section('section') -->
@section('section')

<div class="login-form row justify-content-center">
    @if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
    @endif
    <div class="col-md-offset-3 col-md-6">
        <form method="POST" action="{{ route('password.email') }}">
            @csrf
            <div class="login-card">
                <h3 class="card-head text-center">
                    @lang('labels.user.send_password_link')
                </h3>
                <div class="card-body">
                    @csrf
                    <div class="form-group">
                        <label for="email"
                            >@lang('validation.attributes.email')</label
                        >
                        {{ Form::bsEmail('email', null, ['required', 'placeholder' => __('validation.attributes.email')]) }}
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary">
                            @lang('labels.user.send_password_link')
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection @push('scripts')
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
@endpush
