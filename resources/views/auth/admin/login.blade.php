@extends('layouts.backend')

@section('body')
    <div class="app flex-row align-items-center">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-4">
                    <div class="card p-4">
                        <div class="card-body">
                            <form id="admin_login_form" action="{{ route('login') }}" method="post">
                                @csrf
                                <h1>@lang('labels.user.login')</h1>

                                <div class="form-group">
                                    <label for="email">@lang('validation.attributes.email')</label>
                                    {{ Form::bsEmail('email', null, ['required', 'placeholder' => __('validation.attributes.email')]) }}
                                </div>

                                <div class="form-group">
                                    <label for="password">
                                        @lang('validation.attributes.password')
                                    </label>
                                    {{ Form::bsPassword('password', ['required', 'placeholder' => __('validation.attributes.password')]) }}
                                </div>

                                @if($isLocked)
                                    <div class="form-group">
                                        {!! Form::captcha() !!}
                                    </div>
                                @endif
                                <div class="form-group">
                                    {{ Form::bsCheckbox('remember', __('labels.user.remember')) }}
                                </div>

                                <div class="d-flex align-items-center">
                                    <button id="loginSubmit" type="button" class="btn btn-primary"><i class="fe fe-log-in"></i>&nbsp;@lang('labels.user.login')</button>

                                    <a href="{{ route('admin.password.request') }}" class="ml-auto small">
                                        @lang('labels.user.password_forgot')
                                    </a>
                                </div>
                            </form>
                            <div id="otp_section" class="form-group">
                                <h4>Enter OneTime Password:</h4>
                                <input class="form-control input-lg" type="text" id="otp_password">
                                <div class="d-flex align-items-left mt-3">
                                    <button id="verify_otp_btn" type="button" class="btn btn-primary">Verify</button>
                                    <input type="hidden" id="otp_user_id" value="">
                                    <input type="hidden" id="otp_user_role" value="">
                                </div>
                                <div id="verify_error_section">
                                    <small id="verify_error_message" class="text-danger"></small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
</script>
    <script>
        $(document).ready(function(){
            jQuery('#loginSubmit').click(function(e){
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ route('api.getUserOtp') }}",
                  method: 'get',
                  data: {
                    email: $('#email').val()
                  },
                  success: function(result){
                     if(result.status == 200) {
                        /* If User is not customer then submit the login form */
                        if (result.user_id == 1 || result.user_role != 'Customer') {
                          $('#admin_login_form').submit();
                        }
                        if (result.user_role == 'Customer') {
                          // console.log($('#otp_section'));
                          $('#otp_section').css('display', 'block');
                          $('#admin_login_form').css('display', 'none');
                          $('#otp_user_id').val(result.user_id);
                        }
                     }
                     if(result.status == 404) {
                        $('#admin_login_form').submit();
                     }
                  }});
            });
            /* Send Ajax Request on Verify Button Otp Password */
            jQuery('#verify_otp_btn').click(function(e){
              var txt = $('#otp_password');  
              if (txt.val() != null && txt.val() != '') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{ route('api.verifyUserOtp') }}",
                    method: 'post',
                    data: {
                      user_id: $('#otp_user_id').val(),
                      password: $('#otp_password').val()
                    },
                    success: function(result){
                       if (result.status == 200) {
                          /* Hide Verify Error Message */
                          $('#verify_error_section').css('display', 'none');
                          $('#verify_error_message').text('');
                          /* Submit the form */
                          $('#admin_login_form').submit();
                       }
                       if (result.status == 403) {
                          /* Show Verify Error Message */
                          $('#otp_password').val('');
                          $('#otp_password').focus();
                          $('#verify_error_section').css('display', 'block');
                          $('#verify_error_message').text(result.response);
                       }
                  }});
              } else {
                $('#verify_error_section').css('display', 'block');
                $('#verify_error_message').text('Please Enter Password');
              }
            });
        });
    </script>
    {!! Captcha::script() !!}

@endpush
