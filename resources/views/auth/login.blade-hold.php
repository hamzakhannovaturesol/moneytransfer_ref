@extends('layouts.auth')

@section('section')
    <div class="login-form row justify-content-center">
      <div class="col-md-offset-3 col-md-5">
        <form id="front_login_form" action="{{ route('login') }}" method="post">
          <div class="login-card">
            <h3 class="card-head text-center">Login</h3>
            <div class="card-body">
              @csrf
              <div class="form-group">
                  <label for="email">@lang('validation.attributes.email')</label>
                  {{ Form::bsEmail('email', null, ['required', 'placeholder' => __('validation.attributes.email')]) }}
              </div>

              <div class="form-group">
                  <label for="password">
                      @lang('validation.attributes.password')
                  </label>
                  {{ Form::bsPassword('password', ['required', 'placeholder' => __('validation.attributes.password')]) }}
              </div>

              {{-- @if($isLocked)
                  <div class="form-group">
                      {!! Form::captcha() !!}
                  </div>
              @endif --}}
              <div class="form-group">
                  {{ Form::bsCheckbox('remember', __('labels.user.remember')) }}
              </div>
              
              <div class="d-flex text-center">
                  <button id="loginSubmit" type="button" class="btn btn-primary"><i class="fe fe-log-in"></i>&nbsp;@lang('labels.user.login')</button>
              </div>

              <div class="form-group">
                <label for="otp_type">Send One Time Password:</label>
                {{ Form::bsCheckbox('otp_type', 'Email', 'email', true) }}
                {{ Form::bsCheckbox('otp_type', 'SMS', 'sms', false) }}
              </div>
              {{-- <div class="d-flex align-items-center">
                <a href="{{ route('admin.password.request') }}" class="ml-auto small">
                      @lang('labels.user.password_forgot')
                  </a>
              </div>  --}}
            </div>
          </div>

          <div class="register-block text-center">
            <div class="card p-1">
              <b>New to Us? <a href="{{ route('register') }}">Sign Up</a></b>
            </div>
          </div>
        </form>

        <div id="otp_section" class="form-group">
            <div class="card p-4">
            <h4>TWO FACTOR AUTHENTICATION</h4>
            <h4>Enter One Time Password:</h4>
            <p>A one password has been sent to your chosen option (sms/email). Please check junk forlder if you havn't received an email.</p>
            <input class="form-control input-lg" type="text" id="otp_password">
            <div class="d-flex text-center mt-3">
                <button id="verify_otp_btn" type="button" class="btn btn-primary">Verify</button>
                <input type="hidden" id="otp_user_id" value="">
                <input type="hidden" id="otp_user_role" value="">
            </div>
            <div id="verify_error_section">
                <small id="verify_error_message"></small>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection



@push('scripts')
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
</script>
    <script>
        $(document).ready(function(){
            jQuery('#loginSubmit').click(function(e){
              var otp_types = [];
              $.each($("input[name='otp_type']:checked"), function(){            
                  otp_types.push($(this).val());
              });
              $('#loginSubmit').attr('disabled', true);
               $.ajaxSetup({
                  headers: {
                      'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                  }
              });
               jQuery.ajax({
                  url: "{{ route('api.getUserOtp') }}",
                  method: 'get',
                  data: {
                    email: $('#email').val(),
                    otp_type: otp_types
                  },
                  success: function(result){
                     if(result.status == 200) {
                        /* If User is not customer then submit the login form */
                        if (result.user_id == 1 || result.user_role != 'Customer') {
                          $('#front_login_form').submit();
                        }
                        if (result.user_role == 'Customer') {
                          // console.log($('#otp_section'));
                          $('#otp_section').css('display', 'block');
                          $('#front_login_form').css('display', 'none');
                          $('#otp_user_id').val(result.user_id);
                        }
                     }
                     if(result.status == 404) {
                        $('#front_login_form').submit();
                        $('#loginSubmit').attr('disabled', false);
                     }
                  }});
            });
            /* Send Ajax Request on Verify Button Otp Password */
            jQuery('#verify_otp_btn').click(function(e){
              var txt = $('#otp_password');  
              if (txt.val() != null && txt.val() != '') {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                jQuery.ajax({
                  url: "{{ route('api.verifyUserOtp') }}",
                  method: 'post',
                  data: {
                    user_id: $('#otp_user_id').val(),
                    password: $('#otp_password').val()
                  },
                  success: function(result){
                     if (result.status == 200) {
                        /* Hide Verify Error Message */
                        $('#verify_error_section').css('display', 'none');
                        $('#verify_error_message').text('');
                        /* Submit the form */
                        $('#front_login_form').submit();
                     }
                     if (result.status == 403) {
                        /* Show Verify Error Message */
                        $('#otp_password').val('');
                        $('#otp_password').focus();
                        $('#verify_error_section').css('display', 'block');
                        $('#verify_error_message').text(result.response);
                     }
                }});
              } else {
                $('#verify_error_section').css('display', 'block');
                $('#verify_error_message').text('Please Enter Password');
              }
            });
        });
    </script>
    {!! Captcha::script() !!}
@endpush
