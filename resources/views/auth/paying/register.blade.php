@extends('layouts.auth')

@push('styles')
    <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}">
@endpush

@section('section')
    <div class="register-form row justify-content-center">
        <div class="col-md-12 col-lg-12 mx-auto pt-6">
            <div class="register-card">
                <h3 class="card-head text-center">Register Paying</h3>
                <div class="card-body">
                    <form method="POST" action="{{ route('paying.register') }}">
                        @csrf
                        <div class="form-group row">
                            <!-- NAME  -->
                            <label class="col-md-2 col-form-label" for="P_A_Name">@lang('validation.attributes.name')</label>
                            <div class="col-md-4">
                                {{ Form::bsText('P_A_Name', null, ['required', 'placeholder' => 'Name']) }}
                            </div>

                            <!-- EMAIL  -->
                            <label for="P_A_Email" class="col-md-2 col-form-label">@lang('validation.attributes.email')</label>
                            <div class="col-md-4">
                                {{ Form::bsEmail('P_A_Email', null, ['required', 'placeholder' => __('validation.attributes.email')]) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- Country  -->
                            <label class="col-md-2 col-form-label" for="P_A_Country">Country</label>
                            <div class="col-md-4">
                                <select id="countryField"  placeholder="Country" class="select2 form-control" name="P_A_Country" required>
                                    {{-- <option value="">Please Select Country</option> --}}
                                </select>
                            </div>

                            <!-- City  -->
                            <label class="col-md-2 col-form-label" for="P_A_City">City</label>
                            <div class="city-static col-md-4">
                                {{ Form::bsText('P_A_City', null, ['placeholder' => 'City']) }}
                            </div>
                            <div class="city-dropdown col-md-4" style="display: none;">
                                <select id="cityField" class="select2 form-control" name="P_A_City">
                                    {{-- <option value="">Please Select City</option> --}}
                                </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- ADDRESS 1  -->
                            <label class="col-md-2 col-form-label" for="P_A_Addess">Addess</label>
                            <div class="col-md-4">
                                {{ Form::bsTextarea('P_A_Addess', null, ['required', 'placeholder' => 'Address', 'rows' => '3']) }}
                            </div>
                        </div>

                        <div class="form-group row">
                            <!-- PASSWORD  -->
                            <label for="PA_Password" class="col-md-2 col-form-label">@lang('validation.attributes.passwordreg')</label>
                            <div class="col-md-4">
                                {{ Form::bsPassword('PA_Password', ['placeholder' => __('validation.attributes.passwordreg'), 'data-toggle' => 'password-strength-meter']) }}
                            </div>

                            {{-- <!-- CONFIRM PASSWORD  -->
                            <label for="password_confirmation" class="col-md-2 col-form-label">@lang('validation.attributes.password_confirmation')</label>
                            <div class="col-md-4">
                                {{ Form::bsPassword('password_confirmation', ['placeholder' => __('validation.attributes.password_confirmation')]) }}
                            </div> --}}
                        </div>

                        <div class="form-group row">
                            <!-- MOBILE NO  -->
                            <label for="P_A_Contactnumber" class="col-md-2 col-form-label">Contact Number</label>
                            <div class="col-md-4">
                               {{ Form::bsText('P_A_Contactnumber', null, ['required', 'placeholder' => 'Contact Number']) }}
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-md-12 ml-auto text-center">
                                <button  class="btn btn-success pl-8 pr-8">
                                    @lang('labels.user.register')
                                </button>
                            </div>
                        </div>
                        <div class="registr-btn d-flex align-items-right mt-3">
                           <a class="btn btn-primary pl-8 pr-8" href="{{ route('paying.login') }}"><i class="fe fe-log-in"></i>Login</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
    <script>
        $(document).ready(function(){
            getCountries();
            getAllCountries();
            $('#countryField').select2({
                placeholder: "Please Select Country",
                allowClear: true
            });
             $('#nationalityField').select2({
                placeholder: "Please Select Country of Nationality",
                allowClear: true
            });
            var date = new Date();
            var intYear = date.getFullYear() - 18;
            var intMonth = date.getMonth() + 1;
            var intDay = date.getDate();
            var min = intYear + '-' + intMonth + '-' + intDay;
            $("#dob").flatpickr({
                mode: "single",
                maxDate: new Date(min),
                dateFormat: "Y-m-d"
            });
            $('#countryField').change(function() {
                var val = $('#countryField').val();
               getCities(val);
            });
            $('#nationalityField').change(function() {
                var val = $('#nationalityField').val();
            });
        });
        function getCountries() {
            jQuery.ajax({
              url: "{{ route('api.getTotalCountries') }}",
              method: 'get',
              data: {},
              success: function(result){
                 if(result.length > 0) {
                    $('#countryField').append($("<option></option>").attr("value",'').text('Please Select Country'));
                    $.each(result, function(key, value) {
                        $('#countryField').append($("<option></option>")
                        .attr("value",value.value).text(value.text));
                    });
                 }
              }});
        }

        function getAllCountries() {
            jQuery.ajax({
              url: "{{ route('api.AllCountries') }}",
              method: 'get',
              data: {},
              success: function(result){
                 if(result.length > 0) {
                    $('#nationalityField').append($("<option></option>").attr("value",'').text('Please Select Country of Nationality'));
                    $.each(result, function(key, value) {
                        $('#nationalityField').append($("<option></option>")
                        .attr("value",value.value).text(value.text));
                    });

                 }
              }});
        }

        function getCities(country_id) {
            jQuery.ajax({
              url: "{{ route('api.getCountryCities') }}",
              method: 'get',
              data: {
                country_id: country_id
              },
              success: function(result){
                 $('#cityField').empty();
                 if(result.length > 0) {
                    $('.city-static').css('display', 'none');
                    $('.city-dropdown').css('display', 'block');
                    $('#cityField').select2({
                        placeholder: "Please Select City",
                        allowClear: true
                    });
                    $('#cityField').append($("<option></option>").attr("value",'').text('Please Select City'));
                    $.each(result, function(key, value) {
                        $('#cityField').append($("<option></option>")
                        .attr("value",value.value).text(value.text));
                    });
                 } else {
                    $('.city-dropdown').css('display', 'none');
                    $('.city-static').css('display', 'block');
                 }
              }});
        }
    </script>
    {{-- {!! Captcha::script() !!} --}}
@endpush
