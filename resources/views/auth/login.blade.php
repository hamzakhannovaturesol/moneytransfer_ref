@extends('layouts.auth')
<style>
  .loader {
    position: fixed;
    top: 50%;
    left: 50%;
  }

  @media only screen and (max-width: 767px) {
    .container {
      margin: 0 !important;
    }
  }
</style>
@section('section')
<!--
<div style="display:block">
 <img class="loader" src="{{ asset('loader.gif') }}" alt="loader"/>
</div>
-->

<div class="alert alert-warning">
  <p class="mb-0">
    <strong>Website is under contruction</strong><br>
    Please do not use until further notice
  </p>
</div>

<div class="login-form row justify-content-center">

  <div class="col-md-offset-3 col-md-6">
    <form id="front_login_form" action="{{ route('login') }}" method="post">
      <div class="login-card">
        <h3 class="card-head text-center">Login</h3>
        <div class="card-body">
          @csrf
          <div class="form-group">
            <label for="email">@lang('validation.attributes.email')</label>
            {{ Form::bsEmail('email', null, ['required', 'placeholder' => __('validation.attributes.email')]) }}
          </div>

          <div class="form-group">
            <label for="password">
              @lang('validation.attributes.password')
            </label>
            {{ Form::bsPassword('password', ['required', 'placeholder' => __('validation.attributes.password')]) }}
          </div>

          {{-- @if($isLocked)
                  <div class="form-group">
                      {!! Form::captcha() !!}
                  </div>
              @endif --}}
          <div class="">
            <input type="checkbox" class="" id="remember" name="remember" value="">
            <label class="custom-control-label" for="remember">Remember</label>
          </div>

          <div class="d-flex text-center">
            <button id="loginSubmit" type="button" class="btn btn-primary"><i
                class="fe fe-log-in"></i>&nbsp;@lang('labels.user.login')</button>
          </div>

          <div class="form-group">
            <label for="otp_type">Send One Time Password:</label>
            <div class="custom-control custom-radio">
              <input type="radio" class="" id="otp_type" name="otp_type" value="email" checked="">
              <label class="custom-control-label" for="otp_type">Email</label>
            </div>
            <div class="custom-control custom-radio">
              <input type="radio" class="" id="otp_type" name="otp_type" value="sms">
              <label class="custom-control-label" for="otp_type">SMS</label>
            </div>
          </div>

          <!-- <div class="d-flex align-items-center">
                <a href="{{ route('admin.password.request') }}" class="ml-auto small">
                      @lang('labels.user.password_forgot')
                  </a>
              </div>
               -->

          <div class="d-flex align-items-center">
            <a href="{{ route('password.request') }}" class="ml-auto small">
              @lang('labels.user.password_forgot')
            </a>
          </div>

        </div>
      </div>

      <div class="register-block text-center">
        <div class="p-1">
          <b>New to Us? <a href="{{ route('register') }}">Sign Up</a></b>
        </div>
      </div>
    </form>

    <div id="otp_section" class="form-group">
      <div class="p-4">
        <h4>TWO FACTOR AUTHENTICATION</h4>
        <h5>Enter One Time Password:</h5>
        <p>OTP password has been sent to your chosen option (sms/email).</p>
        <input class="form-control input-lg" type="text" id="otp_password">

        <div class="d-flex text-center mt-3">
          <button id="resend-otp-btn" type="button" class="btn btn-success"><i
              class="fe fe-refresh-ccw"></i>&nbsp;Resend Otp</button>
          <button id="verify_otp_btn" type="button" class="btn btn-primary">Verify</button>
          <input type="hidden" id="otp_user_id" value="">
          <input type="hidden" id="otp_user_role" value="">
        </div>
        <div id="verify_error_section">
          <small id="verify_error_message"></small>
        </div>
        <div id="otp-alert" class="alert alert-success">
          <p><strong>OTP resend successfully</strong></p>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection



@push('scripts')
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
</script>
<script>
  $(document).ready(function () {
    var otp_type_selected;
    jQuery('#loginSubmit').click(function (e) {
      var otp_types = [];
      $.each($("input[name='otp_type']:checked"), function () {
        otp_types.push($(this).val());
        otp_type_selected = $(this).val();
      });
      $('#loginSubmit').attr('disabled', true);
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      jQuery.ajax({
        url: "{{ route('api.getUserOtp') }}",
        method: 'get',
        data: {
          email: $('#email').val(),
          otp_type: otp_types
        },
        success: function (result) {
          if (result.status == 200) {
            /* If User is not customer then submit the login form */
            if (result.user_id == 1 || result.user_role != 'Customer') {
              $('#front_login_form').submit();
            }
            if (result.user_role == 'Customer') {
              // console.log($('#otp_section'));
              $('#otp_section').css('display', 'block');
              $('#front_login_form').css('display', 'none');
              $('#otp_user_id').val(result.user_id);
            }
          }
          if (result.status == 404) {
            $('#front_login_form').submit();
            $('#loginSubmit').attr('disabled', false);
          }
        }
      });
    });
    /* Send Ajax Request on Verify Button Otp Password */
    jQuery('#verify_otp_btn').click(function (e) {
      var txt = $('#otp_password');
      if (txt.val() != null && txt.val() != '') {
        $.ajaxSetup({
          headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
          }
        });
        jQuery.ajax({
          url: "{{ route('api.verifyUserOtp') }}",
          method: 'post',
          data: {
            user_id: $('#otp_user_id').val(),
            password: $('#otp_password').val()
          },
          success: function (result) {
            if (result.status == 200) {
              /* Hide Verify Error Message */
              $('#verify_error_section').css('display', 'none');
              $('#verify_error_message').text('');
              /* Submit the form */
              $('#front_login_form').submit();
            }
            if (result.status == 403) {
              /* Show Verify Error Message */
              $('#otp_password').val('');
              $('#otp_password').focus();
              $('#verify_error_section').css('display', 'block');
              $('#verify_error_message').text(result.response);
            }
          }
        });
      } else {
        $('#verify_error_section').css('display', 'block');
        $('#verify_error_message').text('Please Enter Password');
      }
    });
    /* RESEND OTP BUTTON */
    jQuery('#resend-otp-btn').click(function (e) {
      $('#resend-otp-btn').attr('disabled', true);
      $('#verify_otp_btn').attr('disabled', true);
      $('#verify_error_section').css('display', 'none');
      // var user_id = $('#otp_user_id').val();
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
      });
      jQuery.ajax({
        url: "{{ route('api.resendUserOtp') }}",
        method: 'post',
        data: {
          user_id: $('#otp_user_id').val(),
          otp_type: otp_type_selected
        },
        success: function (result) {
          if (result.status == 200) {
            /* Hide Verify Error Message */
            $('#resend-otp-btn').attr('disabled', false);
            $('#verify_otp_btn').attr('disabled', false);
            $("#otp-alert").removeClass('faded');
            $("#otp-alert").css('display', 'block');
            setTimeout(function () {
              $("#otp-alert").addClass('faded')
            }, 12000);
          }
          if (result.status == 403) {
            /* Show Verify Error Message */
            $('#resend-otp-btn').attr('disabled', false);
            $('#verify_otp_btn').attr('disabled', false);
          }
        }
      });
    });
  });
</script>
{!! Captcha::script() !!}
@endpush