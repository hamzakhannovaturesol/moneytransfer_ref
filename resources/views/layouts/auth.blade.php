<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Money Transfer | {{ config('app.name') }}</title>
    {!! SEOMeta::generate() !!}

    <link rel="stylesheet" href="{{ asset('template/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('css/cookieconsent.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('/template/css/HeaderFooter.css') }}">


    @stack('styles')
    <!-- Styles -->
    @if ($stylePath = Html::asset('frontend', 'frontend.css'))
    <link rel="stylesheet" href="{{ $stylePath }}">
    @endif

    <!-- CDN -->
    <script defer src="{{ asset('js/popper.min.js') }}"></script>
    <script defer src="{{ asset('js/ckeditor.js') }}"></script>
    <script defer src="{{ asset('js/cookieconsent.min.js') }}"></script>

    <script defer src="{{ Html::asset('frontend', 'vendor-frontend.js') }}"></script>
    <script defer src="{{ Html::asset('frontend', 'frontend.js') }}"></script>

    <!-- JS settings -->
    <script type="application/json" data-settings-selector="settings-json">
        {!! json_encode([
            'locale' => app()->getLocale(),
            'appName' => config('app.name'),
            'homePath' => route('admin.home'),
            'adminHomePath' => route('admin.home', [], false),
            'adminPathName' => config('app.admin_path'),
            'editorName' => config('app.editor_name'),
            'editorSiteUrl' => config('app.editor_site_url'),
            'locales' => LaravelLocalization::getSupportedLocales(),
            'user' => $loggedInUser,
            'permissions' => session()->get('permissions'),
            'isImpersonation' => session()->has('admin_user_id') && session()->has('temp_user_id'),
            'usurperName' => session()->get('admin_user_name'),
            'blogEnabled' => config('blog.enabled'),
            'cookieconsent' => [
                'message' => __('labels.cookieconsent.message'),
                'dismiss' => __('labels.cookieconsent.dismiss'),
            ]
        ]) !!}



    </script>

    <!-- Named routes -->
    @routes()
</head>
<body class="front @yield('body_class')">
    <!--start header section-->
    {{-- <header class="header">
      <!--start navbar-->
      <div class="navbar navbar-default navbar-fixed-top">
        <div class="container">
          <div class="row">
            <div class="navbar-header page-scroll">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#myNavbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand page-scroll" href="{{ url('/') }}">
                <img src="{{ url('/template/img/logo.png') }}" alt="logo">
              </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="navbar-collapse collapse" id="myNavbar">
              <ul class="nav navbar-nav navbar-right">
                <li class="active"><a class="page-scroll" href="{{ url('/') }}">Home</a></li>
                <li><a class="page-scroll" href="{{ url('/#features') }}">Features</a></li>
                <li><a class="page-scroll" href="{{ url('/#pricing') }}">Pricing</a></li>
                <li><a class="page-scroll" href="{{ url('/#pricing') }}">Faq</a></li>
                <li><a class="page-scroll" href="{{ url('/#news') }}">News</a></li>
                <li><a class="page-scroll" href="{{ url('/#contact') }}">Contact</a></li>
                <li><a class="page-scroll" href="{{ route('login') }}">Login</a></li>
                <li><a class="page-scroll" href="{{ route('register') }}">Register</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <!--end navbar-->
    </header> --}}
<header>
<div class="jx-monex-header jx-header-3">
<div class="jx-monex-top-bar hidden-xs">
          <div class="container upper">
            <div class="row">

              <div class="col-sm-6">
                <div class="jx-monex-left-topbar">
                    <span class="text"> Welcome to A N Express | International Money Transfer</span>
                </div>
              </div>
              <!--.col-sm-6 end-->
              <div class="col-sm-6">
                <div class="jx-monex-right-topbar">
                  <a href="mailto:info@anexpress.com"><i class="fa fa-envelope"></i> info@anexpress.com</a>
              </div>
              </div>
              <!--.col-sm-6 end-->
            </div>
            <!--.row end-->
          </div>
          <!--.container end-->
        </div>
<div class="jx-monex-sub-top-bar">
  <div class="container">
    <div class="row">
              <div class="col-sm-2 owner">
                  <a href="{{ url('/') }}" class="navbar-brand"><img src="http://web.anexpress.com/wp-content/uploads/2019/03/ANLogo.png" alt="A N Express Money Transfer" class="logo_standard"></a>
              </div>
              <!--.col-sm-4 end-->
              <div class="col-sm-10">
                <div class="jx-monex-header-info">
                    <ul>
                        <li class="top-space">
                            <div class="icon"><i class="fa fa fa-phone"></i></div>
                            <div class="position">
                            <div class="contact">Contact Us</div>
                            <div class="number">020 7426 0113</div>
                            </div>
                        </li>
                        <li class="top-space">
                            <div class="icon"><i class="fa fa-map-marker"></i></div>
                            <div class="position">
                            <div class="location">Our Address</div>
                            <div class="address">208A Whitechapel Rd, Whitechapel, London E1 1BJ</div>
                            </div>
                        </li>
                        <li class="top-space">
                            <div class="icon"><i class="fa fa-clock-o"></i></div>
                            <div class="position">
                            <div class="time">Working Hours</div>
                            <div class="date">Mon - Sat 09:30 - 17:00</div>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- Header Info -->
              </div>
              <!--.col-sm-8 end-->
            </div>
        </div>
    </div>
<div class="jx-monex-menubar setting jx-monex-sticky" id="try">
          <nav class="navbar onemore jx-monex-navbar-default navbar " id="myNavbar">
            <div style="padding: 10px;" class="" >

              <div class="navbar-collapse menunav" id="bs-example-navbar-collapse-1">

                <div class="menu-main-menu-container"><ul id="menu-main-menu" class="jx-monex-mainmenu navbar-right"><li id="menu-item-629" class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home no-mega 0"><a href="{{ url('/login') }}">Home</a></li>
{{-- <li id="menu-item-633" class="menu-item menu-item-type-post_type menu-item-object-page no-mega 0"><a href="{{ url('/#features') }}">Features</a></li>
<li id="menu-item-631" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children no-mega 2"><a href="{{ url('/#faqs') }}">Faq</a>
</li>
<li id="menu-item-685" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children no-mega 3"><a href="{{ url('/#contact') }}">Contact</a>
</li> --}}
@auth
<li id="menu-item-688" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children no-mega 2"><a href="{{ route('logout') }}">Logout</a>
</li>
@endauth
@guest
<li id="menu-item-632" class="menu-item menu-item-type-post_type menu-item-object-page no-mega 0"><a href="{{ route('login') }}">Login</a></li>
<li id="menu-item-632" class="menu-item menu-item-type-post_type menu-item-object-page no-mega 0"><a href="{{ route('register') }}">Register</a></li>
@endguest
</ul></div>
              </div>
              <!-- /.navbar-collapse -->
            </div>
            <!-- /.container -->
          </nav>
        </div>
</div>
</header>
    <!--end header section-->
    {{-- @yield('body') --}}
    <div class="auth-form main-content-wraper">
        <div class="container main-content-inner">
            <section id="hero" class="section-lg section-hero section-circle">
              @yield('section')
            </section>
        </div>
        <!--start footer section-->
        {{-- <footer class="footer-section bg-gray ptb-30 app-footer">
          <div class="footer-wrap">
            <div class="container">
              <div class="row">
                <div class="col-md-6 col-md-offset-3">
                  <div class="footer-single-col text-center">
                    <img src="{{ url('/template/img/logo-color.png') }}" alt="footer logo">
                    <div class="footer-social-list">
                      <ul class="list-inline">
                        <li><a href="#"> <i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"> <i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"> <i class="fa fa-linkedin"></i></a></li>
                        <li><a href="#"> <i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"> <i class="fa fa-youtube"></i></a></li>
                      </ul>
                    </div>
                    <div class="copyright-text">
                      <p>&copy; copyright <a href="#"></a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </footer> --}}
        <!--end footer section-->
 <footer style="margin-bottom: 30px; border-top: 1px solid rgba(226,219,219,0.93) ; padding-top: 80px;">
    <div class="jx-monex-subfooter">
        <div class="container">
          <div class="row">
            <div class="col-sm-3">
              <div class="jx-monex-footer-logo">
                <a href="{{ url('/') }}"><img src="http://web.anexpress.com/wp-content/uploads/2019/03/ANLogo.png" alt=""></a>
                 </div>
                  <div class="space20"></div>
            </div>
            <!-- /.col-sm-3 -->
            <div class="col-sm-3">
              <div class="widget"><div id="nav_menu-3" class="widget_nav_menu"><h6>Money Transfer<i class="fa fa-circle"></i></h6><div class="menu-customer-container"><ul id="menu-customer" class="menu"><li id="menu-item-684" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-684"><a href="#">Customers</a>
<ul class="sub-menu">
    <li id="menu-item-682" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-682"><a href="http://customers.anexpress.com/login">Login</a></li>
    <li id="menu-item-683" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-683"><a href="http://customers.anexpress.com/register">Register</a></li>
</ul>
</li>
</ul></div></div></div>            </div>
            <!-- /.col-sm-3 -->
            <div class="col-sm-3">
              <div class="widget"><div id="nav_menu-4" class="widget_nav_menu"><h6>Knowledge Base<i class="fa fa-circle"></i></h6></div></div>            </div>
            <!-- /.col-sm-3 -->
            <div class="col-sm-3">
              <div class="widget"><div id="widget_monex_subscribe-3" class="widget_monex_subscribe"><div class="jx-monex-widget-newsletter" id="mailchimp-sign-up"><h6>subscribe<i class="fa fa-circle"></i></h6> <!-- widget Title -->
                <p></p>

                <form method="post" action="{{ url('/subscribe') }}" accept-charset="UTF-8" id="mailchimp" name="mc-embedded-subscribe-form" target="_blank" novalidate="" class="jx-monex-form-wrapper has-validation-callback">
                    <span class="ajax-loader"></span>
                    <div class="jx-monex-newsletter-box">
                    <input type="email" name="subscribemail" id="subscribemail" placeholder="Email" data-validation="email" style="width: 210px" required>
                    </div>
                    <div class="jx-monex-newsletter-submit" id="newsletter-submit">
                    <button type="submit" name="subscribe" id="mc-embedded-subscribe"><i class="fa fa-envelope-o"></i></button>
                    </div>
                </form>
              </div></div></div>
              </div>
            <!-- /.col-sm-3 -->
          </div>
          <!-- /.row -->
          <hr>
          <div class="row">
            <div class="col-sm-7">
              <div class="copyright">Copyrights © 2016 - A N Express Ltd - Design by V4Tech Ltd <a href="">A N Express Money Transfer</a></div>
            </div><!-- /.col-sm-8 -->
            <div class="col-sm-5">
               <div class="social">
                        <ul>
						<li class="linkedin herestyle"><a href="javascript:void(0)"><i class="fa fa-linkedin"></i></a></li>
                        <li class="facebook herestyle"><a href="http://www.facebook.com/https://www.facebook.com/pages/category/Financial-Service/A-N-Express-Ltd-524729437632736/"><i class="fa fa-facebook"></i></a></li>
                        <li class="twitter herestyle"><a href="http://www.twitter.com/#anexpress"><i class="fa fa-twitter"></i></a></li>
                        <li class="youtube herestyle"><a href="http://www.youtube.com/#anexpress"><i class="fa fa-youtube"></i></a></li>
                        <li class="googleplus herestyle"><a href="http://www.googleplus.com/#anexpress"><i class="fa fa-google-plus"></i></a></li>
                        <li class="dribbble herestyle"><a href="http://www.dribbble.com/#anexpress"><i class="fa fa-dribbble"></i></a></li>
                        <li class="instagram herestyle"><a href="http://www.instagram.com/#anexpress"><i class="fa fa-instagram"></i></a></li>
                        <li class="pinterest herestyle"><a href="http://www.pinterest.com/#anexpress"><i class="fa fa-pinterest"></i></a></li>
                        <li class="flickr herestyle"><a href="http://www.flickr.com/#anexpress"><i class="fa fa-flickr"></i></a></li>
                        </ul>
                     </div>
            </div><!-- /.social -->
          </div><!-- /.col-sm-4 -->
        </div>
        <!-- /.container -->
      </div>

        </footer>
    </div>
    <!-- template scripts -->
    @stack('scripts')
    <script type="text/javascript">
      $('#mailchimp').submit(function(e) {
          e.preventDefault();
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          let subscribemail = $("#subscribemail").val();
          if( regex.test(subscribemail) )
          {
            let subscribemail = $("#subscribemail").val();
            alert(subscribemail);
          } else {
            alert("Please enter valid email.");
          }
      });
    </script>
</body>
</html>
