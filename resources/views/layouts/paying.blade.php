<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Paying | {{ config('app.name') }}</title>

    <!-- Styles -->
    @if ($stylePath = Html::asset('paying', 'paying.css'))
    <link rel="stylesheet" href="{{ $stylePath }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('template/css/HeaderFooter.css') }}">
    @endif
    <!-- Template Styles -->
    <link rel="stylesheet" href="{{ asset('template/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/themify-icons.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('template/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ asset('css/cookieconsent.min.css') }}">

    <!-- CDN -->
    <script defer src="{{ asset('js/popper.min.js') }}"></script>
    <script defer src="{{ asset('js/ckeditor.js') }}"></script>
    <script defer src="{{ asset('js/cookieconsent.min.js') }}"></script>
    <!-- Template scripts -->
    <script defer src="{{ asset('template/js/jquery-3.3.1.min.js') }}"></script>
    <script defer src="{{ asset('template/js/bootstrap.min.js') }}"></script>
    <script defer src="{{ asset('template/js/owl.carousel.min.js') }}"></script>
    <script defer src="{{ asset('template/js/jquery.easing.min.js') }}"></script>
    <script defer src="{{ asset('template/js/jquery.easeScroll.min.js') }}"></script>
    <script defer src="{{ asset('template/js/jquery.mb.ytplayer.min.js') }}"></script>
    <script defer src="{{ asset('template/js/typed.min.js') }}"></script>
    <script defer src="{{ asset('template/js/magnific-popup.min.js') }}"></script>
    <script defer src="{{ asset('template/js/scripts.js') }}"></script>
    <script defer src="{{ asset('template/js/vendor/modernizr-2.8.1.min.js') }}"></script>

    <!-- Scripts -->
    <script defer src="{{ Html::asset('paying', 'vendor-paying.js') }}"></script>
    <script defer src="{{ Html::asset('paying', 'paying.js') }}"></script>

    <!-- JS settings -->
    <script type="application/json" data-settings-selector="settings-json">
        {!! json_encode([
            'locale' => app()->getLocale(),
            'appName' => config('app.name'),
            'homePath' => route('paying.home'),
            'payingHomePath' => route('paying.home', [], false),
            'payingPathName' => config('app.paying_path'),
            'editorName' => config('app.editor_name'),
            'editorSiteUrl' => config('app.editor_site_url'),
            'locales' => LaravelLocalization::getSupportedLocales(),
            'user' => $loggedInUser,
            'permissions' => session()->get('permissions'),
            'isImpersonation' => session()->has('paying_user_id') && session()->has('temp_user_id'),
            'usurperName' => session()->get('paying_user_name'),
            'blogEnabled' => config('blog.enabled')
        ]) !!}
    </script>

    <!-- Named routes -->
    @routes()
</head>
<body class="@yield('body_class')">
    @yield('body')

    @stack('scripts')
</body>
</html>
