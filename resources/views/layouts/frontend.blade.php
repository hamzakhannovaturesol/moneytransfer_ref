<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">

<head>
    @include('frontend.scripts.gtm')
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>Money Transfer | {{ config('app.name') }}</title>
    {!! SEOMeta::generate() !!}
    <!-- Styles -->
    @if ($stylePath = Html::asset('frontend', 'frontend.css'))
    <link rel="stylesheet" href="{{ $stylePath }}" />
    @endif
    <!-- Template Styles -->
    <link rel="stylesheet" href="{{ asset('template/css/font-awesome.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/css/themify-icons.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/css/magnific-popup.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/css/owl.theme.default.min.css') }}" />

    <link rel="stylesheet" href="{{ asset('template/css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('template/css/responsive.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/cookieconsent.min.css') }}" />

    <link rel="stylesheet" href="{{ asset('template/css/custom_style.css') }}" />

    <!-- custom_soh -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <!-- custom_soh -->

    <!-- CDN -->
    <script defer src="{{ asset('js/popper.min.js') }}"></script>
    <script defer src="{{ asset('js/ckeditor.js') }}"></script>
    <script defer src="{{ asset('js/cookieconsent.min.js') }}"></script>

    <!-- template scripts -->
    <!-- <script defer src="{{ asset('template/js/jquery-3.3.1.min.js') }}"></script> -->

    <script defer src="{{ asset('template/js/owl.carousel.min.js') }}"></script>
    <script defer src="{{ asset('template/js/jquery.easing.min.js') }}"></script>
    <script defer src="{{ asset('template/js/jquery.easeScroll.min.js') }}"></script>
    <script defer src="{{ asset('template/js/jquery.mb.ytplayer.min.js') }}"></script>
    <script defer src="{{ asset('template/js/typed.min.js') }}"></script>
    <script defer src="{{ asset('template/js/magnific-popup.min.js') }}"></script>
    <script defer src="{{ asset('template/js/scripts.js') }}"></script>
    <script defer src="{{ asset('template/js/vendor/modernizr-2.8.1.min.js') }}"></script>

    <!-- Scripts -->
    <script defer src="{{ Html::asset('frontend', 'vendor-frontend.js') }}"></script>
    <script defer src="{{ Html::asset('frontend', 'frontend.js') }}"></script>
    <script type="application/json" data-settings-selector="settings-json">
            {!! json_encode([
                'locale' => app()->getLocale(),
                'appName' => config('app.name'),
                'homePath' => route('fronthome'),
                'frontHomePath' => route('fronthome', [], false),
                'frontPathName' => '',
                'editorName' => config('app.editor_name'),
                'editorSiteUrl' => config('app.editor_site_url'),
                'locales' => LaravelLocalization::getSupportedLocales(),
                'user' => $loggedInUser,
                'permissions' => session()->get('permissions'),
                'isImpersonation' => session()->has('admin_user_id') && session()->has('temp_user_id'),
                'usurperName' => session()->get('admin_user_name'),
                'blogEnabled' => config('blog.enabled')
            ]) !!}
        </script>

    <!-- Named routes -->
    @routes()
</head>

<body class="front @yield('body_class')">
    @yield('body') @stack('scripts')
</body>

</html>