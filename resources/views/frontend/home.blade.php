@extends('layouts.frontend')

@section('body_class', '')

@section('body')
	@if(isset($pendingConfirm))
		<div class="email-confirm-pending">
			<span><i class="fa fa-exclamation-triangle fa-leg"></i> Please Verify Your Email</span>
			<button id="resendEmail" class="btn btn-primary">Resend Email</button>
		</div>
	@endif
    <div id="app"></div>
@endsection

@push('scripts')
<script src="{{ url('/template/js/jquery-3.3.1.min.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

	<script>
		$(document).ready(function() {
			$('#resendEmail').click(function(e){
				e.preventDefault();
				$('#resendEmail').attr("disabled", true)
				$.ajax({
			      type: "get",
			      url: "/user/resendConfirmEmail/",
			      data: {},
			      success: function(result) {
			        swal.fire({
					  type: 'success',
					  title: 'Confirmation Email Sent',
					  text: 'Please check your inbox, Confirmation Email sent successfully',
					})
					$('#resendEmail').attr("disabled", false)
			      },
			      error: function(result) {
			        swal.fire({
					  type: 'error',
					  title: 'Error',
					  text: 'Something went wrong, Please Try Again!'
					})
					$('#resendEmail').attr("disabled", false)
			      }
			    });
			});
		});
	</script>
@endpush