<!DOCTYPE html>
<html lang="en" class="js no-touch csstransforms3d csstransitions js_active  vc_desktop  vc_transform  vc_transform">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--title-->
    <title>AN Express - International Money Transfer</title>

    <!--favicon icon-->
    <link rel="icon" href="{{ url('/template/img/favicon.png') }}" type="image/png" sizes="16x16">
    <!-- font-awesome css -->
    <link rel="stylesheet" href="{{ url('/template/css/font-awesome.min.css') }}">
    <!--themify icon-->
    <link rel="stylesheet" href="{{ url('/template/css/themify-icons.css') }}">
    <!-- magnific popup css-->
    <link rel="stylesheet" href="{{ url('/template/css/magnific-popup.css') }}">
    <!--owl carousel -->
    <link rel="stylesheet" href="{{ url('/template/css/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ url('/template/css/owl.carousel.min.css') }}">
    <!-- bootstrap core css -->

    <!-- custom_soh -->
    <!-- <link rel="stylesheet" href="{{ url('/template/css/bootstrap.min.css') }}"> -->
    <!-- custom_soh -->

    <link rel="stylesheet" href="{{ url('/template/css/style.css') }}">
    <!-- custom css -->
    <!-- responsive css -->
    <link rel="stylesheet" href="{{ url('/template/css/responsive.css') }}">
    <link rel="stylesheet" href="{{ url('/template/css/statichome.css') }}">

  <link rel="stylesheet" href="{{ url('/template/css/custom_style.css') }}">
    <!-- custom_soh -->
    <!-- <link rel="stylesheet" type="text/css" href="{{ url('/template/css/HeaderFooter.css') }}"> -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    <!-- custom_soh -->



    <script src="{{ url('/template/js/vendor/modernizr-2.8.1.min.js') }}"></script>
    <!-- HTML5 Shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="js/vendor/html5shim.js"></script>
    <script src="js/vendor/respond.min.js"></script>
<![endif]-->


</head>


<body class="home page-template page-template-template-home page-template-template-home-php page page-id-10 wpb-js-composer js-comp-ver-5.4.5 vc_responsive" style="overflow: visible;">

    <!-- Preloader -->

    <div id="preloader">
        <div id="loader"></div>
    </div>
    <!--end preloader-->

    <div id="main" class="main-content-wraper">
     <div class="main-content-inner">
     <header>
    <div class="jx-monex-header jx-header-3">
        <div class="jx-monex-top-bar hidden-xs">
            <div class="container upper">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="jx-monex-left-topbar">
                            <span class="text"> Welcome to A N Express | International Money Transfer</span>
                        </div>
                    </div>
                    <div class="col-sm-6 text-right">
                        <div class="jx-monex-right-topbar">
                            <a href="mailto:info@anexpress.com"><i class="fa fa-envelope"></i> info@anexpress.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="jx-monex-sub-top-bar hidden-xs">
            <div class="container">
                <div class="row d-flex align-items-center">
                    <div class="col-sm-2">
                        <a href="{{ url('/') }}" class="navbar-brand"><img src="http://web.anexpress.com/wp-content/uploads/2019/03/ANLogo.png" alt="A N Express Money Transfer" class="logo_standard"></a>
                    </div>
                    <div class="col-sm-10">
                        <div class="jx-monex-header-info">
                            <ul class="d-flex align-items-center py-40 m-0">
                                <li class="top-space d-flex align-items-center">
                                    <div class="icon"><i class="fa fa fa-phone"></i></div>
                                    <div class="position">
                                        <div class="contact">Contact Us</div>
                                        <div class="number">020 7426 0113</div>
                                    </div>
                                </li>
                                <li class="top-space d-flex align-items-center">
                                    <div class="icon"><i class="fa fa-map-marker"></i></div>
                                    <div class="position">
                                        <div class="location">Our Address</div>
                                        <div class="address">208A Whitechapel Rd, Whitechapel, London E1 1BJ</div>
                                    </div>
                                </li>
                                <li class="top-space d-flex align-items-center">
                                    <div class="icon"><i class="fa fa-clock-o"></i></div>
                                    <div class="position">
                                        <div class="time">Working Hours</div>
                                        <div class="date">Mon - Sat 09:30 - 17:00</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="jx-monex-menubar setting jx-monex-sticky 666666" id="try">
            <nav class="navbar onemore jx-monex-navbar-default">
                <div class="container hello custom_soh">
                    <nav class="navbar navbar-light">
                        <div class="container-fluid">
                            <div class="navbar-header">
                                <div class="row d-flex align-items-center">
                                    <div class="col-xs-6">
                                        <a href="{{ url('/') }}" class="navbar-brand sm"><img src="http://www.anexpress.com/wp-content/uploads/2019/03/ANLogo.png" alt="A N Express Money Transfer" class="logo_standard"></a>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                    </div>
                                </div>
                                <!-- <a class="navbar-brand" href="#">WebSiteName</a> -->
                            </div>
                            <div class="collapse navbar-collapse" id="myNavbar">
                                <ul class="nav navbar-nav">
                                    <li class="active"><a href="/front/dashboard">Home</a></li>
                                    <!-- <li class="dropdown">
                                      <a class="dropdown-toggle" data-toggle="dropdown" href="#">Page 1 <span class="caret"></span></a>
                                      <ul class="dropdown-menu">
                                        <li><a href="#">Page 1-1</a></li>
                                        <li><a href="#">Page 1-2</a></li>
                                        <li><a href="#">Page 1-3</a></li>
                                      </ul>
                                    </li>
                                    <li><a href="#">Page 2</a></li>
                                    <li><a href="#">Page 3</a></li> -->
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="{{ route('register') }}"><span class="glyphicon glyphicon-user"></span> Register</a></li>
                                    <li><a href="{{ route('login') }}"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                                </ul>
                            </div>
                        </div>
                    </nav>

                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container -->
            </nav>
        </div>
    </div>
</header>
<!--end header section-->
<!--start hero section-->
<section id="hero" class="section-lg section-hero section-circle">
    <!--background circle shape-->
    <div class="shape shape-style-1 shape-primary">
        <span class="circle-150"></span>
        <span class="circle-50"></span>
        <span class="circle-50"></span>
        <span class="circle-75"></span>
        <span class="circle-100"></span>
        <span class="circle-75"></span>
        <span class="circle-50"></span>
        <span class="circle-100"></span>
        <span class="circle-50"></span>
        <span class="circle-100"></span>
    </div>
    <!--background circle shape-->
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="hero-content-wrap">
                    <div class="hero-content">.
                        <h1>Send Money Now</h1>
                        <p>Fast and secure money transfers to bank accounts & cash pick up in many countries worldwide                                </p>
                        <div class="row">
                          <div class="col-md-8">
                            <div class="search-country">
                              <div class="form-group">
                                  <select class="form-control" id="countriesDropdown">
                                      <option value="">Choose Receiving Country</option>
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="slider-action-btn started-btn col-md-2">
                        <a href="javascript:void(0);" id="btn-get-started" class="btn softo-solid-btn">Proceed</a>
                    </div>
                </div>
                {{-- <div class="slider-action-btn">
                    <a href="#" class="btn softo-solid-btn"><i class="fa fa-apple"></i> Appstore</a>
                    <a href="#" class="btn btn-icon"><i class="fa fa-android"></i></a>
                    <a href="#" class="btn btn-icon"><i class="fa fa-windows"></i></a>
                </div> --}}
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="mobile-slider-area">
            <div class="phone">
                <img src="{{ url('/template/img/iphone-x-frame.png') }}" alt="Phone" class="img-responsive">
                <div class="mobile-slider owl-carousel owl-theme">
                    <div class="item"><img src="{{ url('/template/img/top-screen-1.png') }}" alt="Screen 1"
                       class="img-responsive"></div>
                       <div class="item"><img src="{{ url('/template/img/top-screen-5.png') }}" alt="Screen 1"
                           class="img-responsive"></div>
                           <div class="item"><img src="{{ url('/template/img/top-screen-2.png') }}" alt="Screen 2"
                               class="img-responsive"></div>
                               <div class="item"><img src="{{ url('/template/img/top-screen-6.png') }}" alt="Screen 2"
                                   class="img-responsive"></div>
                                   <div class="item"><img src="{{ url('/template/img/top-screen-3.png') }}" alt="Screen 3"
                                       class="img-responsive"></div>
                                       <div class="item"><img src="{{ url('/template/img/top-screen-7.png') }}" alt="Screen 3"
                                           class="img-responsive"></div>
                                           <div class="item"><img src="{{ url('/template/img/top-screen-8.png') }}" alt="Screen 3"
                                               class="img-responsive"></div>
                                           </div>
                                       </div>

                                       <div class="slider-indicator indicator-color">
                                        <ul>
                                            <li id="prev">
                                                <i class="fa fa-angle-left"></i>
                                            </li>
                                            <li id="next">
                                                <i class="fa fa-angle-right"></i>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-shape">
                        <img src="{{ url('/template/img/wave-shap.png') }}" alt="shape image">
                    </div>
                </section>
                <!--end hero section-->
                <!--start product counter section-->
                <div class="product-counter-section ptb-40">
                    <div class="product-counter-wrap">
                        <div class="container">
                        Easy To Use</div>
                    </div>
                </div>
                <section class="promo-section ptb-90">
                  <div class="promo-section-wrap">
                    <div class="container">
                      <div class="row">
                        <div class="col-md-3 col-sm-6">
                          <div class="bg-secondary single-promo-section text-center">
                            <div class="single-promo-content">
                                <p>AN Express launching app very soon which will be user friendly and easier to use.</p>
                            </div>
                            <div class="line"></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="bg-secondary single-promo-section text-center">
                            <div class="single-promo-content">
                                <span class="ti-vector"></span>
                                <h6>Secure</h6>
                                <p>AN Express is commited to provide its users a secure platform i.e. encrypted data, 2-factor authentication etc.</p>
                            </div>
                            <div class="line"></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="bg-secondary single-promo-section text-center">
                            <div class="single-promo-content">
                                <span class="ti-palette"></span>
                                <h6>Best Exchange Rates</h6>
                                <p>Our app users will enjoy best rates available in the market.</p>
                            </div>
                            <div class="line"></div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <div class="bg-secondary single-promo-section text-center">
                            <div class="single-promo-content">
                                <span class="ti-headphone-alt"></span>
                                <h6>24/7 Access</h6>
                                <p>Send money whenever you like. Our app will enable you to send money 24/7.</p>
                            </div>
                            <div class="line"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--end promo section-->

    <!--start features section-->
    <section id="features" class="bg-secondary ptb-90">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section-heading text-center">
                        <h3>AN Express App  Features</h3>
                        <p>Please see the features on our upcoming app (Web-Mobile)</p>
                    </div>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-4 col-sm-6">
                    <!--feature single start-->
                    <div class="single-feature mb-5">
                        <div class="feature-icon">
                            <div class="icon icon-shape bg-color white-text">
                                <i class="fa fa-object-ungroup"></i>
                            </div>
                        </div>
                        <div class="feature-content">
                            <h5>Responsive web design</h5>
                            <p class="mb-0">Online customer interface is now simple, fast interface with newset technology available in the market.</p>
                        </div>
                    </div>
                    <!--feature single end-->
                    <!--feature single start-->
                    <div class="single-feature mb-5">
                        <div class="feature-icon">
                            <div class="icon icon-shape bg-color white-text">
                                <i class="fa fa-dropbox"></i>
                            </div>
                        </div>
                        <div class="feature-content">
                            <h5>Loaded with features</h5>
                            <p class="mb-0">Browsse history, send money, print receipts, track transaction and many more.</p>
                        </div>
                    </div>
                    <!--feature single end-->
                    <!--feature single start-->
                    <div class="single-feature mb-5">
                        <div class="feature-icon">
                            <div class="icon icon-shape bg-color white-text">
                                <i class="fa fa-smile-o"></i>
                            </div>
                        </div>
                        <div class="feature-content">
                            <h5>Friendly online support</h5>
                            <p class="mb-0">Our support team will be available from with in the app.</p>
                        </div>
                    </div>
                    <!--feature single end-->
                </div>
                <div class="col-md-4 hidden-sm hidden-xs">
                    <div class="feature-image">
                        <img src="{{ url('/template/img/feature-image.png') }}" class="pos-hcenter img-responsive" alt="">
                    </div>
                </div>
                <div class="col-md-4 col-sm-6">
                    <!--feature single start-->
                    <div class="single-feature mb-5">
                        <div class="feature-icon">
                            <div class="icon icon-shape bg-color white-text">
                                <i class="fa fa-file-archive-o"></i>
                            </div>
                        </div>
                        <div class="feature-content">
                            <h5>Free updates forever</h5>
                            <p class="mb-0">We will be updating our app after receiving cusomers feedback. </p>
                        </div>
                    </div>
                    <!--feature single end-->
                    <!--feature single start-->
                    <div class="single-feature mb-5">
                        <div class="feature-icon">
                            <div class="icon icon-shape bg-color white-text">
                                <i class="fa fa-adjust"></i>
                            </div>
                        </div>
                        <div class="feature-content">
                            <h5>Two Factor Authentication</h5>
                            <p class="mb-0">Making it more secure with email and sms one time passwords.</p>
                        </div>
                    </div>
                    <!--feature single end-->
                    <!--feature single start-->
                    <div class="single-feature mb-5">
                        <div class="feature-icon">
                            <div class="icon icon-shape bg-color white-text">
                                <i class="fa fa-smile-o"></i>
                            </div>
                        </div>
                        <div class="feature-content">
                            <h5>Low Fee</h5>
                            <p class="mb-0">Our app users will enjoy low fees.</p>
                        </div>
                    </div>
                    <!--feature single end-->
                </div>
            </div>

        </div>
    </section>
    <!--end features section-->

    <!--start app video section-->
    <div id="video-app" class="video-app-1"
    style="background: url('/template/img/video-play.jpg')no-repeat center center / cover">
    <div class="overlay-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="video-play-button">
                        <a href="https://www.youtube.com/watch?v=9No-FiEInLA"
                        class="video video-play mfp-iframe" data-toggle="modal"
                        data-target="#video-popup">
                        <span class="ti-control-play"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <img src="{{ url('/template/img/bg-wave-sym.png') }}" alt="shape image" class="img-responsive">
</div><!-- end overlay -->
</div>
<!--end app video section-->

<!--start faq section-->
<section id="faqs" class="faq-section ptb-90">
    <div class="faq-section-wrap"></div>
</section>
<!--end faq section-->

<!--start download section-->
<section class="download-section" style="background: url('/template/img/download-bg.jpg')no-repeat center center fixed">
    <div class="container">
        <div class="row">
            <div class="col-md-5 hidden-sm">
                <div class="download-app-img">
                    <img src="{{ url('/template/img/download-app.png') }}" alt="app download" class="img-responsive">
                </div>
            </div>
            <div class="col-md-7 col-sm-12">
                <div class="download-app-text">
                    <h3>Get The App Now !</h3>
                    <p>Apps are coming on Apple and Android devices- very soon</p>
                    <div class="download-app-button">
                        <a href="#" class="download-btn hover-active">
                            <span class="ti-apple"></span>
                            <p>
                                <small>Download On</small>
                                <br> App Store
                            </p>
                        </a>
                        <a href="#" class="download-btn">
                            <span class="ti-android"></span>
                            <p>
                                <small>Git It On</small>
                                <br>Google Play
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end download section-->
<!--contact us section start-->
<section id="contact" class="contact-us ptb-90">
    <div class="contact-us-wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="section-heading">
                        <h3>Contact with us</h3>
                        <p>It's very easy to get in touch with us. Just use the contact form or pay us a
                        visit.</p>
                    </div>
                    <div class="footer-address">
                      <h6>Head Office</h6>
                      <p>208A Whitechapel Rd, Whitechapel, London E1 1BJ
                      </p><div data-attrid="kc:/location/location:address" data-md="1002" lang="en-GB" data-hveid="CAYQAQ" data-ved="2ahUKEwjm6ICxmrzgAhXxQxUIHW0RDfoQkCkwEHoECAYQAQ">
                          <div>
                            <div data-dtype="d3ifr" data-local-attribute="d3adr" data-ved="2ahUKEwjm6ICxmrzgAhXxQxUIHW0RDfoQghwoADAQegQIBhAC">
                              <div jsl="$t t-L9CBsj706lI;$x 0;"></div>
                              <div jsl="$t t-L9CBsj706lI;$x 0;"></div>
                          </div>
                      </div>
                  </div>
                  <ul>
                      <li><i class="fa fa-phone"></i> <span>Phone: 020 7426 0113</span></li>
                      <li><i class="fa fa-envelope-o"></i> <span>Email : <a
                        href="mailto:info@anexpress.com">info@anexpress.com</a></span>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-7">
            <form action="#" method="post" class="contact-us-form" novalidate>
                <h6>&nbsp;</h6>
                <div class="row">
                    <div class="col-xs-12"> </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
</section>

<footer>
    <div class="jx-monex-subfooter">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="jx-monex-footer-logo">
                    <a href="{{ url('/') }}"><img src="http://web.anexpress.com/wp-content/uploads/2019/03/ANLogo.png" alt=""></a>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
              <div id="nav_menu-3" class="widget_nav_menu">
                <h4 class="m-0 mb-10">Money Transfer</h4>
                <div class="menu-customer-container">
                    <ul id="menu-customer" class="menu">
                        <li id="menu-item-684" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-684">
                            <a href="#">Customers</a>
                        </li>
                        <li id="menu-item-682" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-682">
                            <a href="http://customers.anexpress.com/login">Login</a>
                        </li>
                        <li id="menu-item-683" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-683">
                            <a href="http://customers.anexpress.com/register">Register</a>
                        </li>
                    </ul>
                </div>
            </div>
            </div>
        </div>

       <!--  <div class="row">
            <div class="col-sm-3">
              <div class="widget"><div id="nav_menu-4" class="widget_nav_menu"><h6>&nbsp;</h6></div></div></div>
              <div class="col-sm-3">
                  <div class="widget"><div id="widget_monex_subscribe-3" class="widget_monex_subscribe"><div class="jx-monex-widget-newsletter" id="mailchimp-sign-up">
                    <form action="#" method="post" id="mailchimp" name="mc-embedded-subscribe-form" target="_blank" novalidate class="jx-monex-form-wrapper has-validation-callback">
                      <div class="jx-monex-newsletter-submit" id="newsletter-submit"> </div>
                  </form>
              </div></div></div>
          </div>
      </div> -->
      <hr>
      <div class="row">
        <div class="col-md-12">
          <div class="copyright">
            <p>
                Copyrights © 2016 - A N Express Ltd - Design by V4Tech Ltd <a href="">A N Express Money Transfer</a>
            </p>
        </div>
    </div>
</div>
<!-- /.container -->
</div>

</footer>
<!--end footer section-->

</div><!--end main content inner-->
</div>
<!--end main container -->




<!--=========== all js file link ==============-->

<!-- custom_soh -->
<!-- main jQuery -->
<!-- <script src="{{ url('/template/js/jquery-3.3.1.min.js') }}"></script> -->
<!-- bootstrap core js -->
<!-- <script src="{{ url('/template/js/bootstrap.min.js') }}"></script> -->
<!-- smothscroll -->
<!-- custom_soh -->



<script src="{{ url('/template/js/jquery.easeScroll.min.js') }}"></script>
<!--owl carousel-->
<script src="{{ url('/template/js/owl.carousel.min.js') }}"></script>
<!-- scrolling nav -->
<script src="{{ url('/template/js/jquery.easing.min.js') }}"></script>
<!--fullscreen background video js-->
<script src="{{ url('/template/js/jquery.mb.ytplayer.min.js') }}"></script>
<!--typed js -->
<script src="{{ url('/template/js/typed.min.js') }}"></script>
<!--magnific popup js-->
<script src="{{ url('/template/js/magnific-popup.min.js') }}"></script>
<!-- custom script -->
<script src="{{ url('/template/js/scripts.js') }}"></script>
<script>
    $(document).ready(function() {
        $.ajax({
           url: "/api/country/sendto",
           type: "get",
           success: function(data) {
                // var opts = $.parseJSON(data);
                $.each(data, function(i, d) {
                    // You will need to alter the below to get the right values from your json object.  Guessing that d.id / d.modelName are columns in your carModels data
                    // console.log(d)
                    // console.log(i)
                    $('#countriesDropdown').append('<option value="' + d.value + '">' + d.text + '</option>');
                });
            }
        })
    });

    $("#btn-get-started").click(function(){
        var country = $('#countriesDropdown').val();
        var countryName = $('#countriesDropdown :selected').text();
        countryName = countryName.replace(/\s+/g, '-').toLowerCase();
        if (country == '') {
            alert('Please Select Country');
        } else {
            // console.log(countryName, country);
            window.location.href = "/process?cn=" + countryName + "&ci=" + country;
        }
    });
    window.onscroll = function() {myFunction()};

    var ss = document.getElementById("try");
    var sticky = ss.offsetTop;

    function myFunction() {
      if (window.pageYOffset >= sticky) {
        ss.classList.add("sticky")
    } else {
        ss.classList.remove("sticky");
    }
}
</script>
