<?php

return [
    'backend' => [
        'users' => [
            'created' => 'User ID :user created',
            'updated' => 'User ID :user updated',
            'deleted' => 'User ID :user deleted',
        ],

        'customers' => [
            'created' => 'Customer ID :customer created',
            'updated' => 'Customer ID :customer updated',
            'deleted' => 'Customer ID :customer deleted',
        ],

        'form_submissions' => [
            'created' => 'Form submission ID :form_submission created',
        ],
    ],

    'frontend' => [
    ],
];
