<?php

return [
    'backend' => [
        'users' => [
            'created'        => 'User created',
            'updated'        => 'User updated',
            'deleted'        => 'User deleted',
            'bulk_destroyed' => 'Selected users deleted',
            'bulk_enabled'   => 'Selected users enabled',
            'bulk_disabled'  => 'Selected users disabled',
        ],

        'customers' => [
            'created'        => 'Customer created',
            'updated'        => 'Customer updated',
            'deleted'        => 'Customer deleted',
            'bulk_destroyed' => 'Selected customers deleted',
            'bulk_enabled'   => 'Selected customers enabled',
            'bulk_disabled'  => 'Selected customers disabled',
        ],

        'roles' => [
            'created' => 'Role created',
            'updated' => 'Role updated',
            'deleted' => 'Role deleted',
        ],

        'brands' => [
            'created'        => 'Brand created',
            'updated'        => 'Brand updated',
            'deleted'        => 'Brand deleted',
            'bulk_destroyed' => 'Selected brands deleted',
        ],

        'Branches' => [
            'created'        => 'Branch created',
            'updated'        => 'Branch updated',
            'deleted'        => 'Branch deleted',
            'bulk_destroyed' => 'Selected branches deleted',
        ],

        'banks' => [
            'created'        => 'Bank created',
            'updated'        => 'Bank updated',
            'deleted'        => 'Bank deleted',
            'bulk_destroyed' => 'Selected banks deleted',
        ],

        'categories' => [
            'created'        => 'Category created',
            'updated'        => 'Category updated',
            'deleted'        => 'Category deleted',
            'bulk_destroyed' => 'Selected categories deleted',
        ],

        'payments' => [
            'created'        => 'Payment created',
            'updated'        => 'Payment updated',
            'deleted'        => 'Payment deleted',
            'bulk_destroyed' => 'Selected payments deleted',
        ],

        'recharge' => [
            'created'        => 'Recharge Request created',
            'updated'        => 'Recharge Request updated',
            'deleted'        => 'Recharge Request deleted',
            'bulk_destroyed' => 'Selected Recharges deleted',
        ],

        'fee' => [
            'created'        => 'Fee created',
            'updated'        => 'Fee updated',
            'deleted'        => 'Fee deleted',
            'bulk_destroyed' => 'Selected Fee deleted',
        ],

        'margin' => [
            'created'        => 'Margin Type created',
            'updated'        => 'Margin Type updated',
            'deleted'        => 'Margin Type deleted',
            'bulk_destroyed' => 'Selected Margin Types deleted',
        ],

        'actions' => [
            'invalid' => 'Invalid action',
        ],
    ],

    'frontend' => [
        'recipient' => [
            'created'        => 'Recipient created',
            'updated'        => 'Recipient updated',
            'deleted'        => 'Recipient deleted',
            'bulk_destroyed' => 'Selected Recipient deleted',
        ]
    ],
];
