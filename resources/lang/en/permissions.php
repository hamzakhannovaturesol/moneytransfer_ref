<?php

return [
    'categories' => [
        'blog'   => 'Blog',
        'form'   => 'Forms',
        'access' => 'Access',
        'seo'    => 'SEO',
        'customer'    => 'Customers',
        'agent'    => 'Agent',
        'agent_sub'    => 'Agent Sub',
        'payments'    => 'Payments',
        'recharge'    => 'Recharge',
        'fee'    => 'Fee',
        'currency'    => 'Currency',
    ],

    'access' => [
        'backend' => [
            'display_name' => 'Backoffice access',
            'description'  => 'Can access to administration pages.',
        ],
        'paying' => [
            'display_name' => 'Paying Agent access',
            'description'  => 'Can access to Paying Agent pages.',
        ],
        'customer' => [
            'display_name' => 'Customer access',
            'description'  => 'Can access to customer pages.',
        ],
    ],

    'view' => [
        'users' => [
            'display_name' => 'View users',
            'description'  => 'Can view users.',
        ],

        'roles' => [
            'display_name' => 'View roles',
            'description'  => 'Can view roles.',
        ],

        'own' => [
            'posts' => [
                'display_name' => 'View own posts',
                'description'  => 'Can view own posts.',
            ],
        ],

        'customer' => [
            'display_name' => 'View customer',
            'description'  => 'Can view customer.',
        ],

        'agent_sub' => [
            'display_name' => 'View Agent Sub',
            'description'  => 'Can view Agent Sub.',
        ],

        'agent' => [
            'display_name' => 'View Agent',
            'description'  => 'Can view Agent.',
        ],

        'payments' => [
            'display_name' => 'View Payments',
            'description'  => 'Can view Payments.',
        ],

        'recharge' => [
            'display_name' => 'View Recharge Requests',
            'description'  => 'Can view Recharge Requests.',
        ],

        'fee' => [
            'display_name' => 'View Fee',
            'description'  => 'Can view Fee.',
        ],

        'currency' => [
            'display_name' => 'View Currency Rate',
            'description'  => 'Can view Currency Rate.',
        ],

        'transaction' => [
            'display_name' => 'View Transaction',
            'description'  => 'Can view Transaction.',
        ],
    ],

    'create' => [
        'users' => [
            'display_name' => 'Create users',
            'description'  => 'Can create users.',
        ],

        'roles' => [
            'display_name' => 'Create roles',
            'description'  => 'Can create roles.',
        ],

        'customer' => [
            'display_name' => 'Create customer',
            'description'  => 'Can create customer.',
        ],

        'agent_sub' => [
            'display_name' => 'Create Agent Sub',
            'description'  => 'Can create Agent Sub.',
        ],

        'agent' => [
            'display_name' => 'Create Agent',
            'description'  => 'Can create Agent.',
        ],

        'payments' => [
            'display_name' => 'Create Payments',
            'description'  => 'Can create Payments.',
        ],

        'recharge' => [
            'display_name' => 'Create Recharge Requests',
            'description'  => 'Can create Recharge Requests.',
        ],

        'fee' => [
            'display_name' => 'Create Fee',
            'description'  => 'Can create Fee.',
        ],

        'currency' => [
            'display_name' => 'Create Currency Rate',
            'description'  => 'Can create Currency Rate.',
        ],

    ],

    'edit' => [
        'users' => [
            'display_name' => 'Edit users',
            'description'  => 'Can edit users.',
        ],

        'roles' => [
            'display_name' => 'Edit roles',
            'description'  => 'Can edit roles.',
        ],

        'customer' => [
            'display_name' => 'Edit customer',
            'description'  => 'Can edit customer.',
        ],

        'agent_sub' => [
            'display_name' => 'Edit Agent Sub',
            'description'  => 'Can edit Agent Sub.',
        ],

        'agent' => [
            'display_name' => 'Edit Agent',
            'description'  => 'Can edit Agent.',
        ],

        'payments' => [
            'display_name' => 'Edit Payments',
            'description'  => 'Can edit Payments.',
        ],

        'recharge' => [
            'display_name' => 'Edit Recharge Requests',
            'description'  => 'Can edit Recharge Requests.',
        ],

        'fee' => [
            'display_name' => 'Edit Fee',
            'description'  => 'Can edit Fee.',
        ],

        'currency' => [
            'display_name' => 'Edit Currency Rate',
            'description'  => 'Can edit Currency Rate.',
        ],

        'own' => [
            'posts' => [
                'display_name' => 'Edit own posts',
                'description'  => 'Can edit own posts.',
            ],
        ],
    ],

    'delete' => [
        'users' => [
            'display_name' => 'Delete users',
            'description'  => 'Can delete users.',
        ],

        'roles' => [
            'display_name' => 'Delete roles',
            'description'  => 'Can delete roles.',
        ],

        'customer' => [
            'display_name' => 'Delete customer',
            'description'  => 'Can delete customer.',
        ],

        'agent_sub' => [
            'display_name' => 'Delete Agent Sub',
            'description'  => 'Can delete Agent Sub.',
        ],

        'agent' => [
            'display_name' => 'Delete Agent',
            'description'  => 'Can delete Agent.',
        ],


        'payments' => [
            'display_name' => 'Delete Payments',
            'description'  => 'Can delete Payments.',
        ],

        'recharge' => [
            'display_name' => 'Delete Recharge Requests',
            'description'  => 'Can delete Recharge Requests.',
        ],

        'fee' => [
            'display_name' => 'Delete Fee',
            'description'  => 'Can delete Fee.',
        ],

        'currency' => [
            'display_name' => 'Delete Currency Rate',
            'description'  => 'Can delete Currency Rate.',
        ],

        'margin' => [
            'display_name' => 'Delete Margin Rate',
            'description'  => 'Can delete Margin Rate.',
        ],

        'own' => [
            'posts' => [
                'display_name' => 'Delete own posts',
                'description'  => 'Can delete own posts.',
            ],
        ],
    ],

    'publish' => [
        'posts' => [
            'display_name' => 'Publish posts',
            'description'  => 'Can manage posts publication.',
        ],
    ],

    'impersonate' => [
        'display_name' => 'Impersonate user',
        'description'  => 'Can take ownership of others user identities. Useful for tests.',
    ],
];
