<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'        => 'The :attribute must be accepted.',
    'active_url'      => 'The :attribute is not a valid URL.',
    'after'           => 'The :attribute must be a date after :date.',
    'after_or_equal'  => 'The :attribute must be a date after or equal to :date.',
    'alpha'           => 'The :attribute may only contain letters.',
    'alpha_dash'      => 'The :attribute may only contain letters, numbers, and dashes.',
    'alpha_num'       => 'The :attribute may only contain letters and numbers.',
    'array'           => 'The :attribute must be an array.',
    'before'          => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
    'between'         => [
        'numeric' => 'The :attribute must be between :min and :max.',
        'file'    => 'The :attribute must be between :min and :max kilobytes.',
        'string'  => 'The :attribute must be between :min and :max characters.',
        'array'   => 'The :attribute must have between :min and :max items.',
    ],
    'boolean'        => 'The :attribute field must be true or false.',
    'confirmed'      => 'The :attribute confirmation does not match.',
    'date'           => 'The :attribute is not a valid date.',
    'date_format'    => 'The :attribute does not match the format :format.',
    'different'      => 'The :attribute and :other must be different.',
    'digits'         => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions'     => 'The :attribute has invalid image dimensions.',
    'distinct'       => 'The :attribute field has a duplicate value.',
    'email'          => 'The :attribute must be a valid email address.',
    'exists'         => 'The selected :attribute is invalid.',
    'file'           => 'The :attribute must be a file.',
    'filled'         => 'The :attribute field must have a value.',
    'image'          => 'The :attribute must be an image.',
    'in'             => 'The selected :attribute is invalid.',
    'in_array'       => 'The :attribute field does not exist in :other.',
    'integer'        => 'The :attribute must be an integer.',
    'ip'             => 'The :attribute must be a valid IP address.',
    'ipv4'           => 'The :attribute must be a valid IPv4 address.',
    'ipv6'           => 'The :attribute must be a valid IPv6 address.',
    'json'           => 'The :attribute must be a valid JSON string.',
    'max'            => [
        'numeric' => 'The :attribute may not be greater than :max.',
        'file'    => 'The :attribute may not be greater than :max kilobytes.',
        'string'  => 'The :attribute may not be greater than :max characters.',
        'array'   => 'The :attribute may not have more than :max items.',
    ],
    'mimes'     => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min'       => [
        'numeric' => 'The :attribute must be at least :min.',
        'file'    => 'The :attribute must be at least :min kilobytes.',
        'string'  => 'The :attribute must be at least :min characters.',
        'array'   => 'The :attribute must have at least :min items.',
    ],
    'not_in'               => 'The selected :attribute is invalid.',
    'not_regex'            => 'The :attribute format is invalid.',
    'numeric'              => 'The :attribute must be a number.',
    'present'              => 'The :attribute field must be present.',
    'regex'                => 'The :attribute format is invalid.',
    'required'             => 'The :attribute field is required.',
    'required_if'          => 'The :attribute field is required when :other is :value.',
    'required_unless'      => 'The :attribute field is required unless :other is in :values.',
    'required_with'        => 'The :attribute field is required when :values is present.',
    'required_with_all'    => 'The :attribute field is required when :values is present.',
    'required_without'     => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same'                 => 'The :attribute and :other must match.',
    'size'                 => [
        'numeric' => 'The :attribute must be :size.',
        'file'    => 'The :attribute must be :size kilobytes.',
        'string'  => 'The :attribute must be :size characters.',
        'array'   => 'The :attribute must contain :size items.',
    ],
    'string'   => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid zone.',
    'unique'   => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'url'      => 'The :attribute format is invalid.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'name'                      => 'Name',
        'display_name'              => 'Display name',
        'username'                  => 'Pseudo',
        'email'                     => 'Email',
        'first_name'                => 'First name',
        'middle_name'                => 'Middle name (Optional)',
        'last_name'                 => 'Last name',
        'mobile_no'                 => 'Mobile Number',
        'mobile_no_placeholder'     => '441234567890',
        'password'                  => 'Password',
        'passwordreg'               => 'Password',
        'password_confirmation'     => 'Confirm password',
        'old_password'              => 'Old password',
        'new_password'              => 'New password',
        'new_password_confirmation' => 'Confirm new password',
        'postal_code'               => 'Postal code',
        'city'                      => 'City',
        'country'                   => 'Country',
        'address'                   => 'Address',
        'phone'                     => 'Phone',
        'mobile'                    => 'Mobile',
        'age'                       => 'Age',
        'sex'                       => 'Sex',
        'gender'                    => 'Gender',
        'day'                       => 'Day',
        'month'                     => 'Month',
        'year'                      => 'Year',
        'hour'                      => 'Hour',
        'minute'                    => 'Minute',
        'second'                    => 'Second',
        'title'                     => 'Title',
        'content'                   => 'Content',
        'description'               => 'Description',
        'summary'                   => 'Summary',
        'excerpt'                   => 'Excerpt',
        'date'                      => 'Date',
        'time'                      => 'Time',
        'available'                 => 'Available',
        'size'                      => 'Size',
        'roles'                     => 'Roles',
        'permissions'               => 'Permissions',
        'active'                    => 'Active',
        'message'                   => 'Message',
        'g-recaptcha-response'      => 'Captcha',
        'locale'                    => 'Localization',
        'route'                     => 'Route',
        'url'                       => 'URL alias',
        'form_type'                 => 'Form type',
        'form_data'                 => 'Form data',
        'recipients'                => 'Recipients',
        'source_path'               => 'Original path',
        'target_path'               => 'Target path',
        'redirect_type'             => 'Redirect type',
        'timezone'                  => 'Timezone',
        'order'                     => 'Display order',
        'image'                     => 'Image',
        'status'                    => 'Status',
        'pinned'                    => 'Pinned',
        'promoted'                  => 'Promoted',
        'body'                      => 'Body',
        'tags'                      => 'Tags',
        'published_at'              => 'Publish at',
        'unpublished_at'            => 'Unpublish at',
        'metable_type'              => 'Entity',
        'send_from'                 => 'Sender Name',
        'sender_amount'             => 'Amount',
        'sender_fee'                => 'Fee',
        'sender_total_amount'       => 'Total',
        'status'                    => 'Status',
        'sendto_country'            => 'Send To Country',
        'sendfrom_country'          => 'Send From Country',
        'serviceType'               => 'Service Type',
        'recipientTotal'            => 'Recipient Total',
        'exchangeRate'              => 'Exchange Rate',
        'recipientName'             => 'Recipient Name',
        'recipientFatherName'       => 'Father Name',
        'address'                   => 'Address',
        'nationality'               => 'Nationality',
        'phone_no'                  => 'Phone No',
        'postcode'                  => 'Postol Code',
        'source_of_income'          => 'Source of Income',
        'sending_reason'            => 'Sending Reason',
        'recipient_relation'        => 'Relation',
        'bank'                      => 'Bank',
        'branch'                    => 'Branch',
        'branch_code'               => 'Branch Code',
        'account_number'            => 'Account No',
        'dob'                       => 'Date of Birth',
        'bank_name'                 => 'Bank Name',
        'bank_code'                 => 'Bank Code',
        'bank_swift_code'           => 'Bank Swift Code',
        'city'                      => 'City',
        'country_id'                => 'Country',
        'state'                     => 'State',
        'express_bank'              => 'Express Bank',
        'instant_bank'              => 'Instant Bank',
        'rate'                      => 'Rate',
        'place_of_birth'            => 'Place of Birth',
        'consent_marketing'         => 'Consent Marketing',
        'doc_verify_status'         => 'Status Verification',
        'preferred_country'         => 'Preferred Country',
        'send_amount_country'       => 'Send Amount Country',
        'balance'                   => 'Balance',
        'highalert'                 => 'High Alert',
        'subject'                   => 'Subject',
        'message'                   => 'Message',
        'customer_name'             => 'Customer Name',
        'company'                      =>'Company',
        'debit_amount'               => 'Debit Amount',
        'credit_amount'             => 'Credit Amount',
    ],
    'brands' => [
        'name'                      => 'Brand Name',
        'items_count'               => 'Items',
    ],

    'categories' => [
         'name'                     => 'Category Name',
    ],
    'Branches' => [
        'branches_name'         => 'Branch Name',
        'branches_address'      => 'Branch Address',
        'branches_city'         => 'Branch City',
        'country'               => 'Branch Country',
        'state'                 => 'Branch State',
        'pay_type'              => 'Pay Type',
        'sort_br_wise'          => 'Sort Branch Wise',
        'extra_details'         => 'Extra Details',
        'br_code'               => 'Branch Code',
    ],

    'payments' => [
        'approve_status'  => 'Status',
        'approve'  => 'Approve',
        'reject'  => 'Reject',
    ],

    'recharge' => [
        'details'  => 'Details',
        'user'  => 'User',
        'status'  => 'Status',
        'amount'  => 'Amount',
        'country'  => 'Country',
    ],

    'fee' => [
        'FEE'  => 'FEE',
        'MIN'  => 'MIN',
        'MAX'  => 'MAX',
        'country'  => 'Country',
    ],

    'currency' => [
        'Currency_Name'  => 'Currency Name',
        'Currency_Code'  => 'Currency Code',
        'Exchange_Rate'  => 'Exchange Rate',
        'Special_Agent_Rate'  => 'Special Agent Rate',
        'Special_Member_Rate'  => 'Special Member Rate',
        'Country_Margin'  => 'Country Margin',
        'Country'  => 'Country',
        'Currency_Country'  => 'Country',
    ],

    'margin' => [
        'type'  => 'Type',
        'rate_margin'  => 'Rate Margin',
        'fee_margin'  => 'Fee Margin',
    ],
];
