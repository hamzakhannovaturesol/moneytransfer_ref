<?php

return [
    'cancel'   => 'Cancel',
    'save'     => 'Save',
    'close'    => 'Close',
    'create'   => 'Create',
    'delete'   => 'Delete',
    'confirm'  => 'Confirm',
    'show'     => 'Show',
    'edit'     => 'Edit',
    'update'   => 'Update',
    'view'     => 'View',
    'preview'  => 'Preview',
    'back'     => 'Back',
    'send'     => 'Send',
    'login-as' => 'Login as :name',
    'apply'    => 'Apply',

    'users' => [
        'create' => 'Create user',
    ],

    'customers' => [
        'create' => 'Create customer',
    ],

    'agents' => [
        'create' => 'Create agent',
    ],
    'branches' =>[
      'buttons.branches.save_and_publish '  => 'Save'
      //=>   
    ],

    'banks' => [
        'create' => 'Create bank',
    ],

    'branches' => [
        'create' => 'Create branch',
    ],

    'subagents' => [
        'create' => 'Create Sub Agent',
    ],

    'roles' => [
        'create' => 'Create role',
    ],

    'brands' => [
        'create' => 'New Brand',
        'save_and_publish' => 'Save'
    ],

    'payments' => [
        'create' => 'New Payment',
        'save_and_publish' => 'Save',
        'sendT' => 'SendTransaction'
    ],

    'recharge' => [
        'create' => 'New Recharge Request',
        'save_and_publish' => 'Save'
    ],

    'fee' => [
        'create' => 'New Fee',
        'save_and_publish' => 'Save'
    ],

    'currency' => [
        'create' => 'New Currency Rate',
        'save_and_publish' => 'Save'
    ],

    'margin' => [
        'create' => 'New Type Margin',
        'save_and_publish' => 'Save'
    ],

    'categories' => [
        'create' => 'New Category',
        'save_and_publish' => 'Save'
    ]
];
