export default (app, i18n, newPostsCount, pendingPostsCount) => {

  return [
    {
      name: i18n.t('labels.backend.titles.dashboard'),
      url: '/dashboard',
      icon: 'fe fe-trending-up',
      access: true
    },
    {
      divider: true,
      access: true
    },
    {
      name: 'Referral',
      url: '/referral',
      icon: 'fe fe-users',
      access: true
    },
    {
      divider: true,
      access: true
    },
    {
      name: 'Send Money',
      url: '/send',
      icon: 'fe fe-users',
      access: true
    },
    {
      name: 'Transaction Type',
      url: '/transactiontype',
      icon: 'fa fa-usd',
      access: true
    },
    {
      name: 'Balance Request',
      url: '/recharge',
      icon: 'fe fe-credit-card',
      access: true
    },
    {
      name: 'Send Money History',
      url: '/history',
      icon: 'fe fe-activity',
      access: true
    },
    {
      name: 'Recipients',
      url: '/recipients',
      icon: 'fe fe-users',
      access: true
    },
    {
      name: 'Uploads',
      url: '/uploads',
      icon: 'fe fe-file-text',
      access: true
    },
    {
      name: 'Balance Log',
      url: '/balancelog',
      icon: 'fe fe-user',
      access: 'true'
    },
    // {
    //   name: 'Messages',
    //   url: '/messages',
    //   icon: 'fe fe-message-circle',
    //   access: 'true'
    // },
    {
      name: 'Customer Messages',
      url: '/customerMessages',
      icon: 'fe fe-message-circle',
      access: 'true'
    },
    {
      name: 'Logout',
      url: '/logout',
      icon: 'fe fe-message-circle',
      access: 'true'
    },
    {
      divider: true,
      access: true
    }
  ]
}