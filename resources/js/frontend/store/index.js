import Vue from 'vue'
import Vuex from 'vuex'
import * as Cookies from 'js-cookie'
import createPersistedState from 'vuex-persistedstate'

import { createActions } from './actions'
import mutations from './mutations'

Vue.use(Vuex)

export function createStore(route) {
  const actions = createActions(route)

  return new Vuex.Store({
    state: {
      counters: {
        newPostsCount: 0,
        pendingPostsCount: 0,
        publishedPostsCount: 0,
        activeUsersCount: 0,
        formSubmissionsCount: 0
      },
      otp_status: true,
      otp_verify: true,
      payment_details: null
    },
    getters: {
      otpAdded(state) {
        return state.otp_status
      },
      getOtpVerify(state) {
        return state.otp_verify
      }
    },
    actions,
    mutations,
    plugins: [
      createPersistedState({
        key: 'vuex',
        storage: window.localStorage,
        reducer: state => ({
          payment_details: state.payment_details
        })
      })
    ]
  })
}
