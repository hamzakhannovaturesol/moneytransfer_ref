import axios from 'axios'

export function createActions(route) {
  return {
    correctOtpStatus: function(context) {
      context.commit('correctOtpStatus')
    },
    savePaymentDetails: function(context, data) {
      context.commit('savePaymentDetails', data)
    }
  }
}
