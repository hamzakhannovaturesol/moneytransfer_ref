export default {
  SET_COUNTER: (state, { type, counter }) => {
    state.counters[type] = counter
  },
  correctOtpStatus: state => {
    state.otp_status = false
    return state.otp_status
  },
  savePaymentDetails: (state, data) => {
    state.payment_details = data
    state.otp_status = false
  }
}
