import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '../containers/Full'
import IndexFull from '../containers/IndexFull'
import SendIndex from '../containers/SendIndex'
import DashboardFull from '../containers/DashboardFull'

// Views
import Search from '../views/Search'
import Home from '../views/Home'
import Dashboard from '../views/Dashboard'
import UserForm from '../views/UserForm'
import UserList from '../views/UserList'
import RoleForm from '../views/RoleForm'
import RoleList from '../views/RoleList'
import BrandsForm from '../views/BrandsForm'
import BrandsList from '../views/BrandsList'
import Login from '../views/Login'
import chooseMoney from '../components/Send/ChooseMoney'
import RecipientForm from '../components/Send/RecipientForm'
import Payment from '../components/Payment/Index'
import ProcessIndex from '../components/Process/Index'
import Send from '../components/Send/Index'
import Recharge from '../views/Recharge'
import SendHistory from '../views/SendHistory'
import RecipientsList from '../views/Recipients/RecipientsList'
// import RecipientsForm from '../views/Recipients/RecipientsForm'
import RecipientsEditForm from '../views/Recipients/RecipientsEditForm'
import RecipientsCreateForm from '../views/Recipients/RecipientsCreateForm'

import UploadsList from '../views/Uploads/UploadsList'
import UploadsForm from '../views/Uploads/UploadsForm'
import ContactUs from '../components/ContactUs/ContactUs.vue'
import MessageHistory from '../components/MessageHistory/Message.vue'
import ShowMessages from '../components/MessageHistory/ShowMessages'
import ShowSentMessages from '../components/MessageHistory/ShowSentMessages'
import BalanceLog from '../components/BalanceLog/BalanceLog'
import terms from '../components/miscpages/terms'
import PrivacyPolicy from '../components/miscpages/PrivacyPolicy'
import Copyright from '../components/miscpages/Copyright'
import Exclusions from '../components/miscpages/Exclusions'
import Refunds from '../components/miscpages/Refunds'
import Termsofuse from '../components/miscpages/Termsofuse'
import CookiesPolicy from '../components/miscpages/Cookies'

import CustomerTabs from '../components/CustomerMessages/Tabs'
import ViewSent from '../components/CustomerMessages/ViewSent'
import ViewReceived from '../components/CustomerMessages/ViewReceived'

import transactiontype from '../components/TransactionType'
import thanks from '../components/Payment/thanks'
import Logout from '../components/Logout'

import CalculatePoints from '../components/Calculate/Points'
// Referral  
import ReferralList from '../components/Referral/ReferralList'


Vue.use(Router)

export function createRouter(base, i18n) {
  return new Router({
    mode: 'history',
    base: base,
    linkActiveClass: 'open active',
    scrollBehavior: () => ({ y: 0 }),
    routes: [
      {
        path: '/',
        name: 'index',
        redirect: 'dashboard',
        component: IndexFull,
        meta: {
          label: i18n.t('labels.frontend.titles.home')
        },
        children: [
          {
            path: 'search',
            name: 'search',
            component: Search,
            meta: {
              label: i18n.t('labels.search')
            }
          },
          {
            path: 'home',
            name: 'home',
            component: Home,
            meta: {
              label: i18n.t('labels.backend.titles.dashboard')
            }
          },
          {
            path: 'dashboard',
            component: Dashboard,
            meta: {
              label: i18n.t('labels.backend.users.titles.main')
            }
          },          
          {
            path: 'roles',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.roles.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'roles',
                component: RoleList,
                meta: {
                  label: i18n.t('labels.backend.roles.titles.index')
                }
              },
              {
                path: 'create',
                name: 'roles_create',
                component: RoleForm,
                meta: {
                  label: i18n.t('labels.backend.roles.titles.create')
                }
              },
              {
                path: ':id/edit',
                name: 'roles_edit',
                component: RoleForm,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.roles.titles.edit')
                }
              }
            ]
          },
          {
            path: 'brands',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.brands.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'brands',
                component: BrandsList,
                meta: {
                  label: i18n.t('labels.backend.brands.titles.index')
                }
              },
              {
                path: 'create',
                name: 'brands_create',
                component: BrandsForm,
                meta: {
                  label: i18n.t('labels.backend.brands.titles.create')
                }
              },
              {
                path: ':id/edit',
                name: 'brands_edit',
                component: BrandsForm,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.brands.titles.edit')
                }
              }
            ]
          },
          {
            path: 'login',
            redirect: 'dashboard',
            component: Dashboard
          },          
          {
            path: 'en/*',
            component: chooseMoney,
            meta: {
              label: 'Send Money Online'
            }
          }, 
          {
            path: 'send',
            component: SendIndex,
            meta: {
              label: 'Send Money Online'
            },
            children: [
              {
                path: '/',
                name: 'sendmoney',
                component: Send,
                meta: {
                  label: 'Send Money Online'
                }
              },
              {
                path: 'ListReceiver',
                name: 'listReceiver',
                component: RecipientForm,
                meta: {
                  label: 'Send Money Online'
                }
              },
              {
                path: 'Payment',
                name: 'Payment',
                component: Payment,
                meta: {
                  label: 'Send Money Online'
                }
              },
              {
                path: 'thanks',
                name: 'thanks',
                component: thanks,
                meta: {
                  label: 'Thank you'
                }
              }
            ]
          },
          // transactiontype
          {
            path: 'transactiontype',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'Transaction Type'
            },
            children: [
              {
                path: '/',
                name: 'transactiontype',
                component: transactiontype,
                meta: {
                  label: 'Transaction Type'
                }
              }
            ]
          },

          // logout
          {
            path: '/logout',
            component: Logout,
            meta: {
              label: 'Logout'
            }
          },


          {
            path: 'recharge',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'Send Money Online'
            },
            children: [
              {
                path: '/',
                name: 'recharge',
                component: Recharge,
                meta: {
                  label: 'Send Money Online'
                }
              }
            ]
          },
          {
            path: 'history',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'Send Money Online'
            },
            children: [
              {
                path: '/',
                name: 'history',
                component: SendHistory,
                meta: {
                  label: 'Send Money Online'
                }
              }
            ]
          },
          {
            path: 'recipients',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'Send Money Online'
            },
            children: [
              {
                path: '/',
                name: 'recipients',
                component: RecipientsList,
                meta: {
                  label: 'Send Money Online'
                }
              },
              {
                path: 'create',
                name: 'recipients_create',
                component: RecipientsCreateForm,
                meta: {
                  label: 'Create Recipient'
                }
              },
              {
                path: ':id/edit',
                name: 'recipients_edit',
                component: RecipientsEditForm,
                props: true,
                meta: {
                  label: 'Edit Recipient'
                }
              }
            ]
          },
          {
            path: 'balancelog',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'Messages'
            },
            children: [
              {
                path: '/',
                name: 'balancelist',
                component: BalanceLog,
                meta: {
                  label: 'Messages'
                }
              }
            ]
          },
          {
            path: 'contactus',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'Contact Us'
            },
            children: [
              {
                path: '/',
                name: 'contactus',
                component: ContactUs,
                meta: {
                  label: 'Contact Us'
                }
              }
            ]
          },
          // Customer Messages Route
          {
            path: 'customerMessages',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'customer Messages'
            },
            children: [
              {
                path: '/',
                name: 'customertabs',
                component: CustomerTabs,
                meta: {
                  label: 'Messages'
                }
              },
              // view sent message
              {
                path: ':id/sent',
                name: 'ViewSent',
                props: true,
                component: ViewSent,
                meta: {
                  label: 'View Sent Message'
                }
              },
              // view sent message
              {
                path: ':id/received',
                name: 'ViewReceived',
                props: true,
                component: ViewReceived,
                meta: {
                  label: 'View Received Message'
                }
              }
            ]
          },
          // #Customer Messages Route  

          // Calculate Points start
          {
            path: 'calculate',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'Calculate'
            },
            children: [
              {
                path: 'points',
                name: 'points',
                component: CalculatePoints,
                meta: {
                  label: 'Points'
                }
              },              
              
            ]
          },
          // Calculate Points end
          // Referral start
          {
            path: 'referral',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'Referral'
            },
            children: [
              {
                path: '/',
                name: 'referral',
                component: ReferralList,
                meta: {
                  label: 'Referral List'
                }
              },
            ]
          },
          // Referral end
          {
            path: 'messages',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'Messages'
            },
            children: [
              {
                path: '/',
                name: 'messagelist',
                component: MessageHistory,
                meta: {
                  label: 'Messages'
                }
              },
              {
                path: ':id/view',
                name: 'showmessages',
                props: true,
                component: ShowMessages,
                meta: {
                  label: 'Show Messages'
                }
              },
              {
                path: ':id/sent',
                name: 'sentmessages',
                props: true,
                component: ShowSentMessages,
                meta: {
                  label: 'Show Sent Messages'
                }
              }
            ]
          },
          {
            path: 'terms',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'Terms'
            },
            children: [
              {
                path: '/',
                name: 'showterms',
                component: terms,
                meta: {
                  label: 'Show Terms'
                }
              }
            ]
          },
          {
            path: 'privacy',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'Privacy'
            },
            children: [
              {
                path: '/',
                name: 'showprivacy',
                component: PrivacyPolicy,
                meta: {
                  label: 'Show Privacy & Policy'
                }
              }
            ]
          },
          {
            path: 'cookies',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'Cookies'
            },
            children: [
              {
                path: '/',
                name: 'showCookies',
                component: CookiesPolicy,
                meta: {
                  label: 'Show Cookies Policy'
                }
              }
            ]
          },
          {
            path: 'exclusion',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'exclusion'
            },
            children: [
              {
                path: '/',
                name: 'showexclusion',
                component: Exclusions,
                meta: {
                  label: 'Show exclusion Policy'
                }
              }
            ]
          },
          {
            path: 'termsofuse',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'termsofuse'
            },
            children: [
              {
                path: '/',
                name: 'showtermsofuse',
                component: Termsofuse,
                meta: {
                  label: 'Show termsofuse Policy'
                }
              }
            ]
          },
          {
            path: 'refunds',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'refunds'
            },
            children: [
              {
                path: '/',
                name: 'showrefunds',
                component: Refunds,
                meta: {
                  label: 'Show refunds Policy'
                }
              }
            ]
          },
          {
            path: 'copyright',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'copyright'
            },
            children: [
              {
                path: '/',
                name: 'showcopyright',
                component: Copyright,
                meta: {
                  label: 'Show copyright Policy'
                }
              }
            ]
          },
          {
            path: 'uploads',
            component: DashboardFull,
            props: true,
            meta: {
              label: 'Send Money Online'
            },
            children: [
              {
                path: '/',
                name: 'uploads',
                component: UploadsList,
                meta: {
                  label: 'Send Money Online'
                }
              },
              {
                path: 'create',
                name: 'uploads_create',
                component: UploadsForm,
                meta: {
                  label: 'New Upload'
                }
              },
              {
                path: ':id/edit',
                name: 'uploads_edit',
                component: UploadsForm,
                props: true,
                meta: {
                  label: 'Edit Uploads'
                }
              }
            ]
          }
        ]
      },
      {
        path: '/process',
        name: 'process',
        component: ProcessIndex,
        meta: {
          label: 'Send Money Online'
        }
      }
    ]
  })
}
