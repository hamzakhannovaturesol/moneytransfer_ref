export default (app, i18n, newPostsCount, pendingPostsCount) => {
  return [
    {
      name: i18n.t('labels.backend.titles.dashboard'),
      url: '/dashboard',
      icon: 'fe fe-trending-up',
      access: true
    },
     {
      name: 'Vouchers',
      url: '/vouchers',
      icon: 'fe fe-trending-up',
      access: true
    },
    {
      divider: true,
      access: true
    },
    {
      divider: true,
      access: true
    },
    {
      title: true,
      name: 'User Management',
      access:
        app.user.can('view customer') ||
        app.user.can('view customer') ||
        app.user.can('view Agent') ||
        app.user.can('view Agent Sub')
    },
    {
      name: i18n.t('labels.backend.customers.titles.main'),
      url: '/customers',
      icon: 'fe fe-users',
      access: app.user.can('view customer')
    },
    {
      name: i18n.t('labels.backend.agents.titles.main'),
      url: '/agents',
      icon: 'fe fe-users',
      access: app.user.can('view Agent')
    },
    {
      name: i18n.t('labels.backend.subagents.titles.main'),
      url: '/subagents',
      icon: 'fe fe-users',
      access: app.user.can('view Agent Sub')
    },
    {
      name: i18n.t('labels.backend.users.titles.main'),
      url: '/users',
      icon: 'fe fe-users',
      access: app.user.can('view users')
    },
    {
      name: i18n.t('labels.backend.roles.titles.main'),
      url: '/roles',
      icon: 'fe fe-shield',
      access: app.user.can('view roles')
    },
    {
      title: true,
      name: 'Transactions',
      access: app.user.can('view payments')
    },
    {
      name: i18n.t('labels.backend.payments.titles.main'),
      url: '/payments',
      icon: 'fe fe-dollar-sign',
      access: app.user.can('view payments')
    },
    {
      name: i18n.t('labels.backend.banks.titles.main'),
      url: '/banks',
      icon: 'fe fe-dollar-sign',
      access: app.user.can('view banks')
    },
    {
      name: i18n.t('labels.backend.branches.titles.main'),
      url: '/branches',
      icon: 'fe fe-dollar-sign',
      access: app.user.can('view branches')
    },
    {
      title: true,
      name: 'Balance Requests',
      access: app.user.can('view recharge')
    },
    {
      name: i18n.t('labels.backend.recharge.titles.main'),
      url: '/recharge',
      icon: 'fe fe-dollar-sign',
      access: app.user.can('view recharge')
    },
    // {
    //   name: i18n.t('labels.backend.ledger.titles.main'),
    //   url: '/ledger',
    //   icon: 'fe fe-dollar-sign',
    //   access: app.user.can('view ledger')
    // },
    {
      title: true,
      name: 'Fee & Exchange Rate',
      access: app.user.can('view fee')
    },
    {
      name: i18n.t('labels.backend.fee.titles.main'),
      url: '/fee',
      icon: 'fe fe-dollar-sign',
      access: app.user.can('view fee')
    },
    {
      name: i18n.t('labels.backend.currency.titles.main'),
      url: '/currency',
      icon: 'fe fe-dollar-sign',
      access: app.user.can('view currency')
    },
    {
      name: i18n.t('labels.backend.margin.titles.main'),
      url: '/margin',
      icon: 'fe fe-percent',
      access: app.user.can('view margin')
    },
    {
      title: true,
      name: 'Messages',
      access: app.user.can('view contact')
    },
    // {
    //   name: i18n.t('labels.backend.contact.titles.main'),
    //   url: '/contact',
    //   icon: 'fe fe-user',
    //   access: app.user.can('view contact')
    // },
    // {
    //   name: 'Send Message',
    //   url: '/contact/sendmessage',
    //   icon: 'fe fe-message-square',
    //   access: app.user.can('view contact')
    // },    
    // list messages
    {
      name: 'Messages',
      url: '/message',
      icon: 'fe fe-message-square',
      access: app.user.can('view contact')
    },
    // send message
    {
      name: 'Send Message',
      url: '/message/sendMessages',
      icon: 'fe fe-message-square',
      access: app.user.can('view contact')
    },



    // {
    //   title: true,
    //   name: 'Import Data',
    //   access: app.user.can('view importdata')
    // },
    // {
    //   name: i18n.t('labels.backend.importdata.titles.main'),
    //   url: '/importdata',
    //   icon: 'fe fe-user',
    //   access: app.user.can(' view importdata')
    // },
    {
      divider: true,
      access: true
    },
    {
      divider: true,
      access: true
    },
    {
      title: true,
      name: i18n.t('labels.backend.sidebar.payment')
    }
    // {
    //   name: i18n.t('labels.backend.customers.payment.status'),
    //   url: '/payment',
    //   icon: 'fe fe-dollar-sign',
    //   access: app.user.can('view customer')
    // }
  ]
}
