import Vue from 'vue'
import Router from 'vue-router'

// Containers
import Full from '../containers/Full'

// Views
import Search from '../views/Search'
import Dashboard from '../views/Dashboard'
import UserForm from '../views/UserForm'
import UserList from '../views/UserList'
import RoleForm from '../views/RoleForm'
import RoleList from '../views/RoleList'
import BrandsForm from '../views/BrandsForm'
import BrandsList from '../views/BrandsList'
import CustomerList from '../views/Customer/CustomerList'
import CustomerForm from '../views/Customer/CustomerForm'
import AgentList from '../views/Agent/AgentList'
import AgentForm from '../views/Agent/AgentForm'
import SubAgentList from '../views/SubAgent/SubAgentList'
import SubAgentForm from '../views/SubAgent/SubAgentForm'
import PaymentsIndex from '../views/Payments/PaymentsIndex'
import PaymentsForm from '../views/Payments/PaymentsForm'
import PaymentsList from '../views/Payments/PaymentsList'
import PaymentsView from '../views/Payments/PaymentsView'
import RechargeForm from '../views/Recharge/RechargeForm'
import RechargeView from '../views/Recharge/RechargeView'

import RechargeList from '../views/Recharge/RechargeList'
import FeeForm from '../views/Fee/FeeForm'
import FeeList from '../views/Fee/FeeList'
import CurrencyForm from '../views/Currency/CurrencyForm'
import CurrencyList from '../views/Currency/CurrencyList'
import MarginForm from '../views/Margin/MarginForm'
import MarginList from '../views/Margin/MarginList'
import BanksForm from '../views/Banks/BanksForm'
import BanksList from '../views/Banks/BanksList'
import BranchesForm from '../views/Branches/BranchesForm'
import BranchesList from '../views/Branches/BranchesList'
import ContactList from '../views/Contact/ContactList'
import ContactView from '../views/Contact/ContactView'
import SendMessage from '../views/Contact/SendMessage'
// import ImportData from '../views/ImportData/ImportData'
// import Ledger from '../views/CustomerLedger/ledger'

import MessageList from '../views/Messages/MessageList' // Display all messages.
import MessageView from '../views/Messages/MessageView' // Display message details.
import MessageSend from '../views/Messages/MessageSend' // Display message send.

import CustomerView from '../views/Customer/CustomerView' // view customer with its data in tabs
import CustomerListMessages from '../views/Customer/Messages/CustomerListMessages' // Show customer specific all messages.
import CustomerViewMessage from '../views/Customer/Messages/CustomerViewMessage' // Show customer message details

// Vouchers
import VoucherList from '../views/Voucher/VoucherList'
import VoucherForm from '../views/Voucher/VoucherForm'

Vue.use(Router)

export function createRouter(base, i18n) {
  return new Router({
    mode: 'history',
    base: base,
    linkActiveClass: 'open active',
    scrollBehavior: () => ({ y: 0 }),
    routes: [
      {
        path: '/',
        redirect: '/dashboard',
        name: 'home',
        component: Full,
        meta: {
          label: i18n.t('labels.frontend.titles.administration')
        },
        children: [
          {
            path: 'search',
            name: 'search',
            component: Search,
            meta: {
              label: i18n.t('labels.search')
            }
          },
          {
            path: 'dashboard',
            name: 'dashboard',
            component: Dashboard,
            meta: {
              label: i18n.t('labels.backend.titles.dashboard')
            }
          },
          // vouchers routes.
          {
            path: 'vouchers',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: "Voucher"
            },
            children: [
              {
                path: '/',
                name: 'vouchers',
                component: VoucherList,
                meta: {
                  label: "Voucher List"
                }
              },
              {
                path: 'create',
                name: 'voucher_create',
                component: VoucherForm,
                meta: {
                  label: "Voucher Create"
                }
              },
              {
                path: ':id/edit',
                name: 'voucher_edit',
                component: VoucherForm,
                props: true,
                meta: {
                  label: 'Voucher Edit'
                }
              }
            ]
          },
          {
            path: 'users',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.users.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'users',
                component: UserList,
                meta: {
                  label: i18n.t('labels.backend.users.titles.index')
                }
              },
              {
                path: 'create',
                name: 'users_create',
                component: UserForm,
                meta: {
                  label: i18n.t('labels.backend.users.titles.create')
                }
              },
              {
                path: ':id/edit',
                name: 'users_edit',
                component: UserForm,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.users.titles.edit')
                }
              }
            ]
          },
          {
            path: 'roles',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.roles.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'roles',
                component: RoleList,
                meta: {
                  label: i18n.t('labels.backend.roles.titles.index')
                }
              },
              {
                path: 'create',
                name: 'roles_create',
                component: RoleForm,
                meta: {
                  label: i18n.t('labels.backend.roles.titles.create')
                }
              },
              {
                path: ':id/edit',
                name: 'roles_edit',
                component: RoleForm,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.roles.titles.edit')
                }
              }
            ]
          },
          {
            path: 'brands',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.brands.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'brands',
                component: BrandsList,
                meta: {
                  label: i18n.t('labels.backend.brands.titles.index')
                }
              },
              {
                path: 'create',
                name: 'brands_create',
                component: BrandsForm,
                meta: {
                  label: i18n.t('labels.backend.brands.titles.create')
                }
              },
              {
                path: ':id/edit',
                name: 'brands_edit',
                component: BrandsForm,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.brands.titles.edit')
                }
              }
            ]
          },
          {
            path: 'banks',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.banks.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'banks',
                component: BanksList,
                meta: {
                  label: i18n.t('labels.backend.banks.titles.index')
                }
              },
              {
                path: 'create',
                name: 'banks_create',
                component: BanksForm,
                meta: {
                  label: i18n.t('labels.backend.banks.titles.create')
                }
              },
              {
                path: ':id/edit',
                name: 'banks_edit',
                component: BanksForm,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.banks.titles.edit')
                }
              }
            ]
          },
          {
            path: 'branches',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.branches.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'branches',
                component: BranchesList,
                meta: {
                  label: i18n.t('labels.backend.branches.titles.index')
                }
              },
              {
                path: 'create',
                name: 'branches_create',
                component: BranchesForm,
                meta: {
                  label: i18n.t('labels.backend.branches.titles.create')
                }
              },
              {
                path: ':id/edit',
                name: 'branches_edit',
                component: BranchesForm,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.branches.titles.edit')
                }
              }
            ]
          },
          {
            path: 'customers',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.customers.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'customers',
                component: CustomerList,
                meta: {
                  label: i18n.t('labels.backend.customers.titles.index')
                }
              },
              {
                path: 'create',
                name: 'customers_create',
                component: CustomerForm,
                meta: {
                  label: i18n.t('labels.backend.customers.titles.create')
                }
              },
              {
                path: ':id/edit',
                name: 'customers_edit',
                component: CustomerForm,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.customers.titles.edit')
                }
              },
              {
                path: ':id/view',
                name: 'customers_view',
                component: CustomerView,
                props: true,
                meta: {
                  label: 'Details'
                }
              },
              {
                // customer specific message.
                path: '/customers/messages',
                name: 'customer_list_messages',
                component: CustomerListMessages,
                props: true,
                meta: {
                  label: 'Customer List Messages'
                }
              },
              {
                // show customer message view.
                path: '/customers/message/:id/view',
                name: 'customer_view_message',
                component: CustomerViewMessage,
                props: true,
                meta: {
                  label: 'Customer List Messages'
                }
              }
            ]
          },
          {
            path: 'agents',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.agents.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'agents',
                component: AgentList,
                meta: {
                  label: i18n.t('labels.backend.agents.titles.index')
                }
              },
              {
                path: 'create',
                name: 'agents_create',
                component: AgentForm,
                meta: {
                  label: i18n.t('labels.backend.agents.titles.create')
                }
              },
              {
                path: ':id/edit',
                name: 'agents_edit',
                component: AgentForm,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.agents.titles.edit')
                }
              }
            ]
          },
          {
            path: 'subagents',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.subagents.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'subagents',
                component: SubAgentList,
                meta: {
                  label: i18n.t('labels.backend.subagents.titles.index')
                }
              },
              {
                path: 'create',
                name: 'subagents_create',
                component: SubAgentForm,
                meta: {
                  label: i18n.t('labels.backend.subagents.titles.create')
                }
              },
              {
                path: ':id/edit',
                name: 'subagents_edit',
                component: SubAgentForm,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.subagents.titles.edit')
                }
              }
            ]
          },
          {
            path: 'payments',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.payments.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'payments',
                component: PaymentsList,
                meta: {
                  label: i18n.t('labels.backend.payments.titles.index')
                }
              },
              {
                path: 'create',
                name: 'payments_create',
                component: PaymentsForm,
                meta: {
                  label: i18n.t('labels.backend.payments.titles.create')
                }
              },
              {
                path: ':id/edit',
                name: 'payments_edit',
                component: PaymentsForm,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.payments.titles.edit')
                }
              },
              {
                path: ':id/view',
                name: 'payments_view',
                component: PaymentsView,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.users.titles.edit')
                }
              }
            ]
          },
          {
            path: 'recharge',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.recharge.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'recharge',
                component: RechargeList,
                meta: {
                  label: i18n.t('labels.backend.recharge.titles.index')
                }
              },
              {
                path: 'create',
                name: 'recharge_create',
                component: RechargeForm,
                meta: {
                  label: i18n.t('labels.backend.recharge.titles.create')
                }
              },
              {
                path: ':id/edit',
                name: 'recharge_edit',
                component: RechargeForm,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.recharge.titles.edit')
                }
              },
              {
                // recharge view
                path: ':id/view',
                name: 'recharge_view',
                component: RechargeView,
                props: true,
                meta: {
                  label: 'Recharge View'
                }
              }
            ]
          },
          {
            path: 'contact',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.contact.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'contact',
                component: ContactList,
                meta: {
                  label: i18n.t('labels.backend.contact.titles.index')
                }
              },
              {
                path: ':id/view',
                name: 'contact_view',
                component: ContactView,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.contact.titles.edit')
                }
              },
              {
                path: 'sendmessage',
                name: 'create_message',
                component: SendMessage,
                props: true,
                meta: {
                  label: 'Send Message'
                }
              }
            ]
          },

          //////////////// Message /////////////////////
          {
            path: 'message',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: 'Message'
            },
            children: [
              // display all messages
              {
                path: '/',
                name: 'message',
                component: MessageList,
                meta: {
                  label: 'messages'
                }
              },
              // display message details.
              {
                path: ':id/view',
                name: 'message_view',
                component: MessageView,
                props: true,
                meta: {
                  label: 'Message View'
                }
              },
              // send messages
              {
                path: 'sendmessages',
                name: 'message_send',
                component: MessageSend,
                props: true,
                meta: {
                  label: 'Send Message'
                }
              }
            ]
          },

          /////////////////////////////////////
          //           {
          //   path: 'importdata',
          //   component: {
          //     render(c) {
          //       return c('router-view')
          //     }
          //   },
          //   meta: {
          //     label: i18n.t('labels.backend.importdata.titles.main')
          //   },
          //   children: [
          //     {
          //       path: '/',
          //       name: 'importdata',
          //       component: ImportData,
          //       meta: {
          //         label: i18n.t('labels.backend.importdata.titles.index')
          //       }
          //     },
          //     {
          //       path: ':id/view',
          //       name: 'contact_view',
          //       component: ContactView,
          //       props: true,
          //       meta: {
          //         label: i18n.t('labels.backend.importdata.titles.edit')
          //       }
          //     }
          //   ]
          // },
          // {
          //   path: 'ledger',
          //   component: {
          //     render(c) {
          //       return c('router-view')
          //     }
          //   },
          //   meta: {
          //     label: i18n.t('labels.backend.ledger.titles.main')
          //   },
          //   children: [
          //     {
          //       path: '/',
          //       name: 'ledger',
          //       component: Ledger,
          //       meta: {
          //         label: i18n.t('labels.backend.ledger.titles.index')
          //       }
          //     }
          //     // {
          //     //   path: 'create',
          //     //   name: 'recharge_create',
          //     //   component: RechargeForm,
          //     //   meta: {
          //     //     label: i18n.t('labels.backend.recharge.titles.create')
          //     //   }
          //     // },
          //     // {
          //     //   path: ':id/edit',
          //     //   name: 'recharge_edit',
          //     //   component: RechargeForm,
          //     //   props: true,
          //     //   meta: {
          //     //     label: i18n.t('labels.backend.recharge.titles.edit')
          //     //   }
          //     // }
          //   ]
          // },
          {
            path: 'fee',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.fee.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'fee',
                component: FeeList,
                meta: {
                  label: i18n.t('labels.backend.fee.titles.index')
                }
              },
              {
                path: 'create',
                name: 'fee_create',
                component: FeeForm,
                meta: {
                  label: i18n.t('labels.backend.fee.titles.create')
                }
              },
              {
                path: ':id/edit',
                name: 'fee_edit',
                component: FeeForm,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.fee.titles.edit')
                }
              }
            ]
          },
          {
            path: 'currency',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.currency.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'currency',
                component: CurrencyList,
                meta: {
                  label: i18n.t('labels.backend.currency.titles.index')
                }
              },
              {
                path: 'create',
                name: 'currency_create',
                component: CurrencyForm,
                meta: {
                  label: i18n.t('labels.backend.currency.titles.create')
                }
              },
              {
                path: ':id/edit',
                name: 'currency_edit',
                component: CurrencyForm,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.currency.titles.edit')
                }
              }
            ]
          },
          {
            path: 'margin',
            component: {
              render(c) {
                return c('router-view')
              }
            },
            meta: {
              label: i18n.t('labels.backend.margin.titles.main')
            },
            children: [
              {
                path: '/',
                name: 'margin',
                component: MarginList,
                meta: {
                  label: i18n.t('labels.backend.margin.titles.index')
                }
              },
              {
                path: 'create',
                name: 'margin_create',
                component: MarginForm,
                meta: {
                  label: i18n.t('labels.backend.margin.titles.create')
                }
              },
              {
                path: ':id/edit',
                name: 'margin_edit',
                component: MarginForm,
                props: true,
                meta: {
                  label: i18n.t('labels.backend.margin.titles.edit')
                }
              }
            ]
          }
        ]
      }
    ]
  })
}
