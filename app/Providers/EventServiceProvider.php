<?php

namespace App\Providers;

use Illuminate\Auth\Events\Login;
use App\Listeners\UserEventListener;
use App\Listeners\LoginEventListener;
use App\Listeners\PayingLoginEventListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        Login::class => [
            LoginEventListener::class,
        ],
        PayingLogin::class => [
            PayingLoginEventListener::class,
        ],
        'App\Events\UserReferred' => ['App\Listeners\RewardUser'],
    ];

    /**
     * Class event subscribers.
     *
     * @var array
     */
    protected $subscribe = [
        UserEventListener::class,
    ];

    /**
     * Register any events for your application.
     */
    public function boot()
    {
        parent::boot();
    }
}
