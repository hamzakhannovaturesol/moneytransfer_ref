<?php

namespace App\Providers;

use App\Models\Post;
use App\Models\User;
use Illuminate\Support\Facades\URL;
use Laravel\Dusk\DuskServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Repositories\EloquentTagRepository;
use App\Repositories\EloquentRoleRepository;
use App\Repositories\EloquentUserRepository;

use App\Repositories\EloquentVoucherRepository;
use App\Repositories\EloquentReferralRepository;

use App\Repositories\Contracts\TagRepository;
use App\Repositories\Contracts\RoleRepository;
use App\Repositories\Contracts\UserRepository;

use App\Repositories\Contracts\VoucherRepository;
use App\Repositories\Contracts\ReferralRepository;

use App\Repositories\EloquentAccountRepository;
use App\Repositories\Contracts\AccountRepository;
use Illuminate\Database\Eloquent\Relations\Relation;
use App\Repositories\EloquentBrandsRepository;
use App\Repositories\Contracts\BrandsRepository;
use App\Repositories\EloquentRedirectionRepository;
use App\Repositories\Contracts\RedirectionRepository;
use App\Repositories\EloquentOtpRepository;
use App\Repositories\Contracts\OtpRepository;
use App\Repositories\EloquentRecipientRepository;
use App\Repositories\Contracts\RecipientRepository;
use App\Repositories\EloquentPaymentsRepository;
use App\Repositories\Contracts\PaymentsRepository;
use App\Repositories\EloquentRechargeRepository;
use App\Repositories\Contracts\RechargeRepository;
use App\Repositories\EloquentFeeRepository;
use App\Repositories\Contracts\FeeRepository;
use App\Repositories\EloquentCurrencyRepository;
use App\Repositories\Contracts\CurrencyRepository;
use App\Repositories\EloquentMarginRepository;
use App\Repositories\Contracts\MarginRepository;
use App\Repositories\EloquentLedgerRepository;
use App\Repositories\Contracts\LedgerRepository;
use App\Repositories\EloquentBanksRepository;
use App\Repositories\Contracts\BanksRepository;
use App\Repositories\EloquentBranchesRepository;
use App\Repositories\Contracts\BranchesRepository;
use App\Repositories\EloquentContactRepository;
use App\Repositories\Contracts\ContactRepository;
use App\Repositories\EloquentPayingAccountRepository;
use App\Repositories\Contracts\PayingAccountRepository;
use App\Repositories\EloquentPayingRepository;
use App\Repositories\Contracts\PayingRepository;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Relation::morphMap([
            'post' => Post::class,
            'user' => User::class,
        ]);

        if (config('app.url_force_https')) {
            // Force SSL if isSecure does not detect HTTPS
            URL::forceScheme('https');
        }


    if(env('APP_DEBUG')) {
        \DB::listen(function($query) {
            \File::append(
                storage_path('/logs/query.log'),
                $query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL
           );
        });
    }

    }

    /**
     * Register any application services.
     */
    public function register()
    {
        // Dusk, if env is appropriate
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }

        $this->app->bind(
            UserRepository::class,
            EloquentUserRepository::class
        );

        $this->app->bind(
            VoucherRepository::class,
            EloquentVoucherRepository::class
        );
        
        $this->app->bind(
            ReferralRepository::class,
            EloquentReferralRepository::class
        );
        
        
        $this->app->bind(
            AccountRepository::class,
            EloquentAccountRepository::class
        );

        $this->app->bind(
            RoleRepository::class,
            EloquentRoleRepository::class
        );

        $this->app->bind(
            TagRepository::class,
            EloquentTagRepository::class
        );

        $this->app->bind(
            BrandsRepository::class,
            EloquentBrandsRepository::class
        );

        $this->app->bind(
            RedirectionRepository::class,
            EloquentRedirectionRepository::class
        );

        $this->app->bind(
            OtpRepository::class,
            EloquentOtpRepository::class
        );

        $this->app->bind(
            RecipientRepository::class,
            EloquentRecipientRepository::class
        );

        $this->app->bind(
            PaymentsRepository::class,
            EloquentPaymentsRepository::class
        );

        $this->app->bind(
            RechargeRepository::class,
            EloquentRechargeRepository::class
        );

        $this->app->bind(
            LedgerRepository::class,
            EloquentLedgerRepository::class
        );

        $this->app->bind(
            FeeRepository::class,
            EloquentFeeRepository::class
        );

        $this->app->bind(
            CurrencyRepository::class,
            EloquentCurrencyRepository::class
        );

        $this->app->bind(
            MarginRepository::class,
            EloquentMarginRepository::class
        );

        $this->app->bind(
            BanksRepository::class,
            EloquentBanksRepository::class
        );

        $this->app->bind(
            BranchesRepository::class,
            EloquentBranchesRepository::class
        );

        $this->app->bind(
            ContactRepository::class,
            EloquentContactRepository::class
        );

        $this->app->bind(
            PayingAccountRepository::class,
            EloquentPayingAccountRepository::class
        );

        $this->app->bind(
            PayingRepository::class,
            EloquentPayingRepository::class
        );
    }
}
