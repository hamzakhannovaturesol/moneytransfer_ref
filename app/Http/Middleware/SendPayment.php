<?php

namespace App\Http\Middleware;

// use Illuminate\Auth\Middleware\SendPayment as Middleware;

class SendPayment
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function handle($request, \Closure $next)
    {
        if ( ! $request->ajax())
            return response('Forbidden.', 403);

        return $next($request);
    }
}
