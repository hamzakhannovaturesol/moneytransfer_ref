<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRecipientRequest extends FormRequest
{
    /**
     * Determine if the meta is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name'         => 'required',
            'last_name'          => 'required',
            'father_name'        => 'required',
            'mobile'             => 'required',
            'city'               => 'required',
            'country'            => 'required',
            'address_1'          => 'required',
            'bank'               => 'required',
            'branch'             => 'required'
            // 'account_number'     => 'required',
            // 'postal_code'        => 'required',
            // 'email'              => 'required',
            // 'phone'              => 'required',
            // 'sending_reason'     => 'required',
            // 'source_of_income'     => 'required',
            // 'recipient_relation'     => 'required',
        ];
    }
}
