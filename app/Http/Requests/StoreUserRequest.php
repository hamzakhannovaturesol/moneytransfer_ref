<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email'                 =>  'required|email|unique:users',
            'name'                  =>  'required',
            'password'              =>  'required|min:6|confirmed',
            'password_confirmation' =>  'required|min:6',
            'active'                =>  'boolean',
            'first_name'            =>  'required',    
            'middle_name'           =>  'required',    
            'last_name'             =>  'required',    
            'mobile_no'             =>  'required',    
            'address'               =>  'required',    
            'title'                 =>  'required',    
            'dob'                   =>  'required',    
            'place_of_birth'        =>  'required',    
            'country_id'            =>  'required',    
            'city'                  =>  'required',    
            'nationality'           =>  'required',    
            // 'phone_no'              =>  'required',    
            'postcode'              =>  'required',    
            'locale'                =>  'required',    
            'timezone'              =>  'required',    
            'doc_verify_status'     =>  'required',    
            'preferred_country'     =>  'required',    
            'send_amount_country'   =>  'required',    
            'balance'               =>  'required',    
            'thrashold'             =>  'required',    
              

        ];
    }
}
