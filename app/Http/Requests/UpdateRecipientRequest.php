<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRecipientRequest extends FormRequest
{
    /**
     * Determine if the meta is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank'               => 'required',
            'branch'             => 'required',
            'account_number'     => 'required',
            'first_name'         => 'required',
            'last_name'          => 'required',
            'father_name'        => 'required',
            'address_1'          => 'required',
            // 'address_2'          => 'required',
            // 'postal_code'        => 'required',
            'mobile'             => 'required',
            // 'phone'              => 'required',
            // 'email'              => 'required',
            'sending_reason'     => 'required',
            'source_of_income'     => 'required',
            'recipient_relation'     => 'required',       
        ];
    }
}
