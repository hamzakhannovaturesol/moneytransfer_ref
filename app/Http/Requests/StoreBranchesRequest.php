<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBranchesRequest extends FormRequest
{
    /**
     * Determine if the meta is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'Branch_Name'       => 'required',
            'Branch_Address'    => 'required',
            'Branch_City'       => 'required',
            'Country'           => 'required',
            'State'             => 'required',
            'Pay_Type'          => 'required',
            'Sort_BR_Wise'      => 'required',
            'extra_details'     => 'required',
            'BR_Code'           => 'required',

        ];
    }
}
