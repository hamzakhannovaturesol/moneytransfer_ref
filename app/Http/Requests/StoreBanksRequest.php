<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBanksRequest extends FormRequest
{
    /**
     * Determine if the meta is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Bank_Name'          => 'required',             
            'Bank_Code'          => 'required',             
            'Bank_Swift_Code'    => 'required',             
            'Address'            => 'required',             
            'City'               => 'required',             
            'Country'            => 'required',             
            'State'              => 'required',             
            'Express_Bank'       => 'required',             
            'Instant_Bank'       => 'required',             
            'Rate'               => 'required',             
        ];
    }
}
