<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCurrencyRequest extends FormRequest
{
    /**
     * Determine if the meta is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'Currency_Name'  => 'required',
            'Currency_Code'  => 'required',
            'Exchange_Rate'  => 'required',
            'Special_Agent_Rate'  => 'required',
            'Special_Member_Rate'  => 'required',
            'Country_Margin'  => 'required',
            'Country'  => 'required',         
        ];
    }
}
