<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Contracts\TagRepository;
use Mcamara\LaravelLocalization\LaravelLocalization;
use App\Models\User;
use App\Models\Otp;
use App\Models\Countries;
use App\Models\RechargeAccount;
use App\Models\Branches;
use App\Models\Banks;
use App\Models\Cities;
use App\Models\Margin;
use App\Repositories\Contracts\OtpRepository;
use Auth;
use App\textlocal\textlocal;
use Mail;

class Frontend extends Controller
{
	public function __construct(OtpRepository $otp)
    {
        $this->otp = $otp;
        // $this->textlocal = new Textlocal('info@an-express.com', 'oDPCCTpfR9g-UXj4I3d1kT5AAG4pjIr8ijwQsoz665');
    }


	public function getUserOtp(Request $request, User $user_model, Otp $otp_model) {
        $email = $request->email;
        $user = $user_model::select('*')->where('email', $email)->get()->first();
        $otp_type = $request->otp_type;
        // Check if user exists
        if ($user) {
            $user_role = isset($user->roles->first()->name) ? $user->roles->first()->name : '';
            $mobile_no = isset($user->mobile_no) ? $user->mobile_no : '';
        	/******* CHECK IF USER ROLE IS CUSTOMER *******/
        	if ($user_role == 'Customer') {
        		$otp_record = $otp_model::select('*')->where('user_id', $user->id)->get()->first();
		        /******* UPDATE IF OTP RECORD ALREADY EXISTS *******/
		        if ($otp_record) {
                    $password = rand(1,1000000);
		        	$otp_update = [
		        		'password' => $password
		        	];
		        	$otp_update = $otp_model::where('user_id', $user->id)->update($otp_update);
		        	if($otp_update) {
                        foreach($otp_type as $otp) {
                           /******* SEND EMAIL OTP *******/
                            if ($otp == 'email') {
                                $this->sendEmail($email, $password);
                            }
                            /******* SEND SMS OTP *******/
                            if ($otp == 'sms') {
                                $this->sendSMS($password, $mobile_no);
                            }
                        }
		        		return response()->json([
		        			'status' => 200,
		        			'user_id' => $user->id,
		        			'user_role' => $user_role,
			                'response' => 'updated otp'
			            ]);
		        	} else {
		        		return response()->json([
		        			'status' => 403,
			                'response' => 'error updating otp'
			            ]);
		        	}
		        }
		        /* Insert if Otp does not exists*/
		        else {
		        	$otp_array = [
			        	'user_id' => $user->id,
			        	'password' => rand(1,1000000)
			        ];
		        	$otp_create = $this->otp->make($otp_array);
			        // dd($otp_create);
			       	if($this->otp->save($otp_create, $otp_array)) {
			       		return response()->json([
			       			'status' => 200,
			       			'user_id' => $user->id,
		        			'user_role' => $user_role,
		        			'response' => 'created otp'
			            ]);
			       	} else {
			       		return response()->json([
			       			'status' => 403,
			                'response' => 'error creating otp'
			            ]);
			       	}
		        }
        	} else {
        		return response()->json([
	        		'status' => 200,
	        		'user_id' => $user->id,
	        		'user_role' => $user_role,
	                'response' => 'not a customer'
                ]);
        	}
        }
        /* Show Error if user does not exists */
        else {
        	return response()->json([
        		'status' => 404,
            	'response' => 'user does not exists'
            ]);
        }
    }

    public function verifyUserOtp(Request $request, User $user_model, Otp $otp_model)
		{
    	$user_id = $request->user_id;
    	$password = $request->password;
    	$verify = $otp_model::where('user_id', $user_id)->where('password', $password)->get()->first();
    	if($verify) {
    		return response()->json([
        		'status' => 200,
            	'response' => 'Successfully Verified!'
            ]);
    	} else {
    		return response()->json([
        		'status' => 403,
            	'response' => 'Password doesn\'t match. Please Try Again!'
            ]);
    	}
    }

    public function addVerifyImages()
	{
    	// dd('coming here');
    }

    public function getTotalCountries(Countries $countries) {
        // $result = $countries::select('id AS value','country AS text')->where('id', '>', 0)->get();
        // $result = $countries::select('countrycodes.id AS value','countrycodes.country AS text')
        //                     ->get();

        $result = $countries::select('countrycodes.id AS value','countrycodes.country AS text')
                                ->join('recharge-acc','countrycodes.code', '=', 'recharge-acc.County')
                                ->get();
        return $result;
    }

    public function getAllCountries(Countries $countries) {

        $result = $countries::select('countrycodes.id AS value','countrycodes.country AS text')
                            ->join('recharge-acc', 'countrycodes.code', '=', 'recharge-acc.County')
                            ->get();

    	return $result;
    }

    public function AllCountries(Countries $countries) {

        $result = $countries::select('countrycodes.id AS value','countrycodes.country AS text')->get();
    	return $result;
    }

    public function getSendToCountries(Countries $countries) {
        // $result = $countries::select('id AS value','country AS text')->where('id', '>', 0)->get();
        $result = $countries::select('countrycodes.id AS value','countrycodes.country AS text')
                         ->join('currency', 'countrycodes.code', '=', 'currency.Currency_Country')
                         ->get();

        // $result = $countries::select('countrycodes.id AS value','countrycodes.country AS text')
        //                     ->join('recharge-acc', 'countrycodes.code', '=', 'recharge-acc.County')
        //                     ->get();

        return $result;
    }

    public function getAllCountriesCode(Countries $countries) {
        // $result = $countries::select('id AS value','country AS text')->where('id', '>', 0)->get();
        $result = $countries::select('countrycodes.code AS value','countrycodes.country AS text')
                            //->join('currency', 'countrycodes.code', '=', 'currency.Currency_Country')
                            ->get();
        return $result;
    }

    public function getAllCountriesSNO(RechargeAccount $rechargeaccount){
        $result = $rechargeaccount::select('recharge-acc.SNO AS value','recharge-acc.County AS text')
                             //->join('currency', 'recharge-acc.CurrencyCode', '=', 'currency.Currency_Country')
                            ->get();
                           // dd($result);
        return $result;
    }

    public function getCountryCities(Request $request, Cities $cities, Countries $country) {
        $country_id = $request->country;
        $country_code = $country::where('id', $country_id)->value('code');
        $result = $cities::select('Seq AS value', 'City_Name AS text')
                ->where('Country', $country_code)->get();
        return $result;
    }

    public function getCountryBanks(Request $request, Banks $bank, Countries $country) {
        $send_country = json_decode($request->country, true);
        $country_code = $send_country['code'];
        $result = $bank::select('banks.Bank_ID AS value','banks.Bank_Name AS text')
                            ->where('Country', $country_code)->get();
        if ($result) {
            return response()->json([
                'status' => 200,
                'response' => $result
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Not Found'
            ]);
        }
    }

    //Get All Bank Cities by id

    public function getBankCities(Request $request, Branches $branch, Countries $country) {
    	// $send_country = json_decode($request->country, true);
    	// $country_code = $send_country['code'];
        $bank_id = $request->bank_id;
    	$result = $branch::select('branch.Branch_City AS value','branch.Branch_City AS text')
    					->where('Bank_ID', $bank_id)->groupBy('branch.Branch_City')->get();
        if ($result) {
            return response()->json([
                'status' => 200,
                'response' => $result
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Not Found'
            ]);
        }
    }

    // End Bank Cities by id

    public function getBankBranches(Request $request, Branches $branch, Countries $country) {
    	// $send_country = json_decode($request->country, true);
    	// $country_code = $send_country['code'];
        $bank_City = $request->bank_City;
        $bank_id = $request->bank_id;
    	$result = $branch::select('branch.Branch_ID AS value','branch.Branch_Name AS text')
                            ->where('Branch_City', $bank_City)
                            ->where('Bank_ID', $bank_id)
                            ->get();
        if ($result) {
            return response()->json([
                'status' => 200,
                'response' => $result
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Not Found'
            ]);
        }
    }

    public function getBranchCode(Request $request, Branches $branch) {
        $branch_id = $request->branch_id;
        $branch_code = $branch::where('Branch_ID', $branch_id)->value('BR_Code');
        if ($branch_code) {
            return response()->json([
                'status' => 200,
                'code' => $branch_code
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'response' => 'Not Found'
            ]);
        }
    }

    public function getCountryBanksById(Request $request, Banks $bank, Countries $country) {
        // dd($request->all());
        $country_id = $request->country;
        $country_code = $country::where('id', $country_id)->value('code');
        // $send_country = json_decode($request->country, true);
        // $country_code = $send_country['code'];
        $result = $bank::select('banks.Bank_ID AS value','banks.Bank_Name AS text')
                            ->where('Country', $country_code)->get();
        if ($result) {
            return response()->json([
                'status' => 200,
                'response' => $result
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Not Found'
            ]);
        }
    }

    public function getCountryCashBanksById(Request $request, Banks $bank, Countries $country) {
        // dd($request->all());
        $country_id = $request->country;
        $country_code = $country::where('id', $country_id)->value('code');
        // $send_country = json_decode($request->country, true);
        // $country_code = $send_country['code'];
        $result = $bank::select('banks.Bank_ID AS value','banks.Bank_Name AS text')
                            ->where('Country', $country_code)->where('Instant_Bank','Yes')->get();
        if ($result) {
            return response()->json([
                'status' => 200,
                'response' => $result
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Not Found'
            ]);
        }
    }

    /* **************************************************** */
    //    SEND OTP VIA EMAIL
    /* **************************************************** */

    public function sendEmail($email, $password) {
        // dd($email);
        $to_name = 'User Name';
        $to_email = $email;
        $mail_data = array(
          'password' => $password,
          'name'    => $to_name
        );

        // return view('emails.receipt', $mail_data);
        try{
        Mail::send('emails.password-email', $mail_data, function($message) use ($to_name, $to_email) {
              $message->to($to_email, $to_name)
                      ->subject('ANExpress');
              $message->from('support@anexpress.com','ANEXPRESS Support');
            });
            return response()->json([
              'status' => 200,
              'response' => 'Mail Sent Successfuly to User Email'
            ]);
          }
            catch(Exception $e){
        }
    }

    /* **************************************************** */
    //    SEND OTP VIA SMS
    /* **************************************************** */

    public function sendSMS($password, $mobile_no) {
        $username = 'info@an-express.com';
        $hash = 'oDPCCTpfR9g-pJemTl4130kr4MVazHweuEBJiOIFva';
        $pword = "pakistan786";
        // Account details
        $apiKey = urlencode($hash);

        // Message details
        $numbers = array($mobile_no);
        // $numbers = array(923153444574);
        $sender = urlencode('ANExpress');
        $message = rawurlencode('Password: '.$password);

        $numbers = implode(',', $numbers);

        // Prepare data for POST request
        $data = array('pword' => $pword, 'apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

        // Send the POST request with cURL
        $ch = curl_init('http://txtlocal.com/sendsmspost.php');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
    }

    public function getServiceTypes(Margin $margin) {
        $result = $margin::select('type_margin.type_value AS value','type_margin.type AS text')->get();
        if ($result) {
            return response()->json([
                'status' => 200,
                'data' => $result
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'data' => 'Not Found'
            ]);
        }
    }

    public function resendUserOtp(Request $request, User $user_model, Otp $otp_model) {
        $user_id = $request->user_id;
        $otp_type = $request->otp_type;
        $user = $user_model::select('email', 'mobile_no')->where('id', $user_id)->get()->first();
        $email = $user->email;
        $mobile_no = $user->mobile_no;

        $otp_record = $otp_model::select('*')->where('user_id', $user_id)->get()->first();
        /******* UPDATE IF OTP RECORD ALREADY EXISTS *******/
        if ($otp_record) {
          $password = rand(1,1000000);
          $otp_update = [
            'password' => $password
          ];
          $otp_update = $otp_model::where('user_id', $user_id)->update($otp_update);
          if($otp_update) {
            /******* SEND EMAIL OTP *******/
            if ($otp_type == 'email') {
                $this->sendEmail($email, $password);
            }
            /******* SEND SMS OTP *******/
            if ($otp_type == 'sms') {
                $this->sendSMS($password, $mobile_no);
            }
            return response()->json([
              'status' => 200,
              'response' => 'successfully resend otp'
            ]);
          } else {
            return response()->json([
              'status' => 403,
              'response' => 'error re-sending otp'
            ]);
          }
        }
    }

}
