<?php

namespace App\Http\Controllers\Paying;

use App\Models\PayingAgents;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use App\Events\PayingRegistered;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use Auth;
use Route;
use Carbon\Carbon;
use App\Repositories\Contracts\PayingAccountRepository;

class PayingRegisterController extends Controller
{
   
    // use RegistersUsers;

    protected $account;

    public function __construct(PayingAccountRepository $account)
    {
      $this->middleware('guest:paying');
      $this->account = $account;

    }
    
    protected function payingValidator(array $data)
    {
      return Validator::make($data, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);
    }

    public function showRegisterForm()
    {
      return view('auth.paying.register');
    }
    
    public function register(Request $request)
    {

      // $validate = $this->payingValidator($request->all())->validate();

      // dd($paying);
      $data = $request->all();
      $data['serial'] = '123213';
      event(new PayingRegistered($paying = $this->create($data)));
      dd(auth('paying')->login($paying));
      // \Auth::guard('paying')->login($paying);

      return redirect(route('paying.home'));
    }

    protected function create(array $data)
    {
      return $this->account->register($data);
    }
}