<?php

namespace App\Http\Controllers\Paying;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Route;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class PayingLoginController extends Controller
{
    use AuthenticatesUsers;
    public function __construct()
    {
      $this->middleware('guest:paying', ['except' => ['logout']]);
    }
    
    public function showLoginForm()
    {
      return view('auth.paying.login');
    }
    
    public function login(Request $request)
    {
      // Validate the form data
      $this->validate($request, [
        'email'   => 'required|email',
        'password' => 'required|min:6'
      ]);
      
      // Attempt to log the user in
      if (Auth::guard('paying')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)) {
        // if successful, then redirect to their intended location
        return redirect()->intended(route('paying.dashboard'));
      } 
      // if unsuccessful, then redirect back to the login with the form data
      return redirect()->back()->withInput($request->only('email', 'remember'));
    }
    
    public function logout()
    {
        Auth::guard('paying')->logout();
        return redirect('/paying');
    }
}