<?php

namespace App\Http\Controllers\Paying;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Models\Payments;
use App\Http\Requests\StoreContactUsRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Auth;
use DB;
use Mail;
use Illuminate\Support\Str;
use App\Events\MessageReceived;


// use App\Http\Controller\FrontendController;

class AjaxController extends Controller
{

    public function __construct()
    {
    }

    public function payTransaction(Request $request, Payments $payment) {
        $payment_id = $request->payment_id;
        $update_arr = [
            'approve_status' => 'beneficiary_paid'
        ];
        $update = $payment::where('id', $payment_id)->update($update_arr);
        if ($update) {
            return response()->json([
                'status' => 200,
                'response' => 'Successfully status changed'
            ]);
        }
    }

}

