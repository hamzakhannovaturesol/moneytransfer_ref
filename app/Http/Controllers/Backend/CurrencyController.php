<?php

namespace App\Http\Controllers\Backend;

use App\Models\Currency;
use Illuminate\Http\Request;
use App\Utils\RequestSearchQuery;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreCurrencyRequest;
use App\Http\Requests\UpdateCurrencyRequest;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\Contracts\CurrencyRepository;

class CurrencyController extends BackendController
{
    /**
     * @var JobcardRepository
     */
    protected $currency;

    public function __construct(CurrencyRepository $currency)
    {
        //dd($jobcards);
        $this->currency = $currency;
    }
    
    /**
     * Show the application dashboard.
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function search(Request $request)
    {

        /** @var Builder $query */
        $query = $this->currency->query();
        
        $requestSearchQuery = new RequestSearchQuery($request, $query, [
            'Currency_Name',
            'Currency_Code',
        ]);

        if ($request->get('exportData')) {
            return $requestSearchQuery->export([
                'Currency_Name',
                'Currency_Code',
            ],
                [
                    'Currency Name',
                    'Currency Code',
                ],
                'currency');
        }

        return $requestSearchQuery->result([
            'currency.id',
            'Currency_Name',
            'Currency_Code',
            'Currency_Country',
            'Exchange_Rate',
            'Special_Agent_Rate',
            'Special_Member_Rate',
            'Country_Margin',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCurrencyRequest $request)
    {
        //dd($request->all());    
        $currency = $this->currency->make($request->all()); 
        
       $this->currency->save($currency, $request->input());

       return $this->redirectResponse($request, __('alerts.backend.currency.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function show(Currency $currency)
    {
       return $currency;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function edit(Currency $currency)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function update(Currency $currency, UpdateCurrencyRequest $request)
    {
        $currency->fill($request->all());
        
        $this->currency->save($currency, $request->input());
           
        return $this->redirectResponse($request, __('alerts.backend.currency.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Currency  $currency
     * @return \Illuminate\Http\Response
     */
    public function destroy(Currency $currency, Request $request)
    {
        $this->currency->destroy($currency);

        return $this->redirectResponse($request, __('alerts.backend.currency.deleted'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function batchAction(Request $request)
    {
        $action = $request->get('action');
        $ids = $request->get('ids');
        
        switch ($action) {
            case 'destroy':                
                    $this->currency->batchDestroy($ids);
                    return $this->redirectResponse($request, __('alerts.backend.currency.bulk_destroyed'));
                break;
        }

        return $this->redirectResponse($request, __('alerts.backend.actions.invalid'), 'error');
    }

    public function updateCurrency(Request $request, Currency $currency) {
        $field = $request->field;
        $id = $request->id;
        $amount = $request->amount;
        $update_arr = [
            $field => $amount
        ];
        $update = $currency::where('id', $id)->update($update_arr);
        if ($update) {
            return response()->json([
                'status' => 200,
                'message' => 'Currency Updated'
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'Something Went Wrong, Please Try Again'
            ]);
        }
    }
}
