<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Utils\RequestSearchQuery;
use Illuminate\Support\Facades\Gate;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use App\Events\PublicMessages;
use DB;
use App;
use App\Reply;
use App\Repositories\Contracts\UserRepository;


class MessageController extends Controller
{
    protected $reply;

    public function __construct(Reply $reply)
    {
        $this->reply = $reply;
    }
    /**
     * Display admin top notification counter.
     */
    public function notification(Reply $reply) {
        $reply = $reply::where('receiver_id', 1)->where('status','open')->get();

        if(count($reply) > 0){
            $data = array('status' => 200, 'response' => $reply);
        }else{
            $data = array('status' => 500, 'response' => $reply);
        }
        return response()->json($data);
    }
    /**
     * Display user details.
     *
     * @param  \App\Banks  $bank
     * @return \Illuminate\Http\Response
     */
    public function show(Reply $reply)
    {
        // dd($reply);
        return $reply;
    }
    /**
     * Show the application dashboard.
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function search(Request $request)
    {
        // dd($request);
        /** @var Builder $query */
        $query = $this->reply->query("select * from `replies` where parent = '0'");
        // dd($query);

        $requestSearchQuery = new RequestSearchQuery($request, $query, [
                   'id',
                   'name',
                   'email',
                   'phone',
                   'message',
                   'status',
        ]);

        if ($request->get('exportData')) {
            //dd($request->get('exportData'));
            return $requestSearchQuery->export([
                'name',
                'email',
                'phone',
                'message',
                'status',
            ],
                [
                    __('validation.attributes.name'),
                    __('validation.attributes.email'),
                    __('validation.attributes.phone'),
                    __('validation.attribute.message'),
                ],
                'Messages');
        }

        return $requestSearchQuery->resultmessage([
            'id',
            'name',
            'email',
            'phone',
            'message',
            'status',
        ]);
    }

    // send message
    public function store(Request $request, UserRepository $customers) {
    //    return "hello i am in store method";

        $requestSearchQuery = new RequestSearchQuery($request, $customers->query(), [
            'name',
            'email',
        ]);

        $getcustomer = $requestSearchQuery->customerSearchResult(['name'])->count();
        $data = $request->data['userList'][0]['libs'];

        $value = $request->data['value'];
        if( count($value) == 1 ) { // check if one user selected.

            $reply = new Reply();

            $reply->parent = 0; // Sender: SuperAdmin.
            $reply->receiver_id = $request->data['value']['0']['id'];
            $reply->sender_id = '1'; // Sender: SuperAdmin.
            $reply->name = 'Admin';
            $reply->email = 'info@anexpress.com';
            $reply->phone = '020 7426 0113';
            $reply->message = $request->data['message'];
            $reply->save();
            // event(new PublicMessages($reply));
        }else{
            // send multiple users message.
            $data = $request->data['value'];
            foreach($data as $details) {
                $reply = new Reply();
                $reply->parent = 0; // Sender: SuperAdmin.
                $reply->receiver_id = $details['id']; // customer_id
                $reply->sender_id = '1'; // admin.
                $reply->name = 'Admin';
                $reply->email = 'info@anexpress.com';
                $reply->phone = '020 7426 0113';
                $reply->message = $request->data['message'];

                $reply->save();
                // event(new MessageReceived($contactus));
            }
        }

        $response = array('status' => 200);
        return response()->json($response);
    }

    // save admin reply.
    public function saveReply(Request $request, Reply $reply)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;

        if(!$user_id)
        {
            $validator = Validator::make(
            $request->all(),
            array(
                'id' => 'required'
                )
            );
            if($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages'=> $error_messages);
                return response()->json($response_array, 200);
            }
        }

        // get message sender id.
        $message = Reply::where('id', $request->message_id)->first();

        // Update Message Status To Close.
        $message_update = Reply::find($request->message_id);
        $message_update->status =  'close';
        $message_update->save();

        // Save Admin reply.
        $reply->parent = isset($request->message_id)? $request->message_id : ''; // message_id
        $reply->sender_id = $user_id; // loggedIn user id.
        $reply->receiver_id = $message->sender_id; // `super admin` will receive the message.
        $reply->status = 'close'; // close the status of message.
        $reply->name = 'Admin';
        $reply->email = 'info@anexpress.com';
        $reply->phone = '020 7426 0113';
        $reply->message = isset($request->message)? $request->message : ''; // reply message.
        $reply->save();

        # Fire an event.
        // event(new MessageReceived($request->all()));
        try {
          //   event(new MessageReceived($contactus));
        } catch (\Throwable $th) {
            throw $th;
        }
    }


    public function checkStatus(Request $request, Reply $reply)
    {
        // return "i am true";

        $user_id = auth()->user() ? auth()->user()->id : $request->id;

        if(!$user_id)
        {
            $validator = Validator::make(
            $request->all(),
            array(
                'id' => 'required'
                )
            );
            if($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages'=> $error_messages);
                return response()->json($response_array, 200);
            }
        }

        $message = Reply::where('id', $request->message_id)->first();

        if( $message->status == 'open' ) {
            return 'true';
        } else { // status is close.

            // get child reply message.
            return Reply::where('parent',  $message->id )->first();
        }


        # Fire an event.
        // event(new MessageReceived($request->all()));
        try {
          //   event(new MessageReceived($contactus));
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    // public function updatenotification(Request $request, Reply $reply) {
    //     $id = $request->get('id');
    //     $update = $reply::where('receiver_id', 1);
    //     if($id){
    //         $update =    $update->where('id', $id);
    //     }
    //     // $update =  $update->update(['status' => 'seen']);
    //     $updatedList = $this->notification($reply);
    //     if($update){
    //         $data = array('status' => 200, 'response' => $updatedList);
    //     }else{
    //         $data = array('status' => 500, 'response' => $updatedList);
    //     }
    //     return response()->json($data);
    // }
}