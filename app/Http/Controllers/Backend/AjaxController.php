<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Contracts\TagRepository;
use Mcamara\LaravelLocalization\LaravelLocalization;
use App\Models\User;
use App\Models\Payments;
use App\Models\Recharge;
use App\Models\Recipient;
use App\Models\Ledger;
use App\Models\Banks;
use App\Models\Branches;
use App\Models\PayingAgents;
use Auth;
use Illuminate\Support\Carbon;
use DB;
use App;
use Mail;
use App\Models\Countries;


class AjaxController extends Controller
{
    /**
     * @var PostRepository
     */

    /**
     * @var TagRepository
     */
    protected $tags;

    /**
     * @var LaravelLocalization
     */
    protected $localization;

    /**
     * AjaxController constructor.
     *
     * @param \App\Repositories\Contracts\PostRepository       $posts
     * @param TagRepository                                    $tags
     * @param \Mcamara\LaravelLocalization\LaravelLocalization $localization
     */
    public function __construct(TagRepository $tags, LaravelLocalization $localization)
    {
        $this->tags = $tags;
        $this->localization = $localization;
    }

    /**
     * Global index search.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function search(Request $request)
    {
        $query = $request->get('q');
    }

    /**
     * Search internal transatables routes.
     *
     * @param Request $request
     *
     * @throws \InvalidArgumentException
     *
     * @return array
     */
    public function routesSearch(Request $request)
    {
        $items = [];

        $routes = __('routes');

        foreach ($routes as $name => $uri) {
            $items[$name] = $uri;
        }

        return [
            'items' => $items,
        ];
    }

    /**
     * Search tags.
     *
     * @param Request $request
     *
     * @throws \InvalidArgumentException
     *
     * @return array
     */
    public function tagsSearch(Request $request)
    {
        $query = $request->get('q');

        $locale = app()->getLocale();

        return [
            'items' => $this->tags->query()
                ->where("name->{$locale}", 'like', "%$query%")
                ->pluck('name'),
        ];
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function imageUpload(Request $request)
    {
        $uploadedImage = $request->file('upload');

        // Resize image below 600px width if needed
        $image = Image::make($uploadedImage->openFile())->widen(600, function ($constraint) {
            $constraint->upsize();
        });

        $imagePath = "/tmp/{$uploadedImage->getClientOriginalName()}";
        Storage::disk('public')->put($imagePath, $image->stream());

        return [
            'uploaded' => true,
            'url'      => "/storage{$imagePath}",
            'width'    => $image->width(),
            'height'   => $image->height(),
        ];
    }

    public function getRole(User $user)
    {
        $user_id = auth()->user()->id;
        if ($user_id !== 1) {
            return response()->json([
                'user_id' => auth()->user()->id,
                'user_role' => auth()->user()->roles->first()->name
            ]);
            // return auth()->user()->roles->first()->name;
        }
    }

    public function getPayingAgents(User $user)
    {
        // $data = $user::whereHas('roles', function($q){
        //     $q->where('id', '11');
        // })->get();
        $data = DB::table('paying_agents')->select('P_A_ID AS value', 'P_A_Name as text')->get();
        if ($data) {
            return response()->json([
                'status' => 200,
                'data' => $data
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Error Occured! Please Try Again'
            ]);
        }
    }

    public function addPayingAgent(Request $request, Payments $payment)
    {
        $id = $request->id;
        $agent_id = $request->agent_id;
        $saved = $payment::where('id', $id)->update(['paying_agent_id' => $agent_id]);
        //dd($saved);
        if ($saved) {
            return response()->json([
                'status' => 200,
                'reponse' => 'Paying Agent Updated Successfully'
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }

    public function changePaymentStatus(Request $request, Payments $payment, User $user, Recipient $recipient, PayingAgents $paying_agent, Banks $bank_model, Branches $branch_model, Ledger $ledger)
    {
        DB::enableQueryLog();

        $id = $request->id;
        $approve_status = $request->approve_status;

        // $saved = $payment::where('id', $id)->update(['approve_status' => $approve_status]);
        $saved =  true; // $payment::where('id', $id)->update(['approve_status' => $approve_status]);

        $import_data = [];

        $payments_data = $payment::where('id', $id)->select('*')->get()->first();
        \Log::info("---------------- payments_data ----------------");
        \Log::info($payments_data);

        $sender_data = $user::where('id', $payments_data->user_id)->first();
        \Log::info("---------------- Sender_data ----------------");
        \Log::info($sender_data);

        $paying_agent_data = $paying_agent::where('P_A_ID', $payments_data->paying_agent_id)->first();

        // $recipient_data = $recipient::where('id', $payments_data->recipient_id)->withTrashed()->first();
        $recipient_data = $recipient::where('recipient.id', $payments_data->recipient_id)
            ->select('*', 'cities.City_name as city')
            ->leftJoin('countrycodes', 'countrycodes.id', '=', 'recipient.country')
            ->join('cities', 'cities.Seq', '=', 'recipient.city')
            ->withTrashed()
            ->first();


        $bank_data = $bank_model->where('Bank_ID', $recipient_data->bank)->first();
        $branch_data = $branch_model->where('Branch_ID', $recipient_data->branch)->first();
        $queries = DB::getQueryLog();

        /* IF TRANSACTION REJECTED THEN REVERT ACCOUNT BALANCE*/
        if ($approve_status == 'rejected') {
            $user_id = $payments_data->user_id;
            $payment_details = json_decode($payments_data->payment_details, true);
            $send_amount = $payment_details["sendAmount"] + $payment_details["totalFee"];

            $userdata = $user::find($user_id);
            $old_balance = $userdata->balance;

            $calculate_ammount = $old_balance + $send_amount;
            $balance_update = $user::where('id', $user_id)->update(['balance' => $calculate_ammount]);
            if (!$balance_update) {
                return response()->json([
                    'status' => 400,
                    'response' => 'Unable to Update User Balance, Please Try Again'
                ]);
            } else {
                $update_arr = [
                    'description' => 'Balance Restored',
                    'debit_amount' => $send_amount,
                    'user_id' => $payments_data->user_id,
                    'balance' => $calculate_ammount
                ];

                $ledger_create = $ledger::insert($update_arr);
            }
        }

        //$import_data['paying_agent_id'] = $paying_agent_data->paying_agent_id;
        if ($approve_status == 'approved') {
            /**************** INSERTING INTO SECOND DATABASE ****************/
            // $this->insertEDetails($payments_data);
            // $this->insertGlobalOrders($payments_data, $sender_data, $recipient_data, $bank_data, $branch_data);
            // $this->insertGlobalOrdersHistory($payments_data);

            // \Log::info("--------sender_data inside approved ------");
            // \Log::info( $sender_data );

            $this->data_file($payments_data, $sender_data, $recipient_data, $bank_data, $branch_data, $recipient);
            /**************** INSERTING INTO SECOND DATABASE ****************/
        }
        if ($saved) {
            return response()->json([
                'status' => 200,
                'reponse' => 'Payment Updated Successfully'
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }
    /* 'pending' payment migrate */
    public function pendingPaymentMigrate(Request $request, Payments $payment, User $user, Recipient $recipient, PayingAgents $paying_agent, Banks $bank_model, Branches $branch_model, Ledger $ledger)
    {

        // get payment id from post.
        $id = $request->id;

        // get payment data from database.
        $payments_data = $payment::where('id', $id)->select('*')->get()->first();

        $sender_data = $user::where('id', $payments_data->user_id)->withTrashed()->first();
        $paying_agent_data = $paying_agent::where('P_A_ID', $payments_data->paying_agent_id)->get()->first();


        $recipient_data = $recipient::where('id', $payments_data->recipient_id)->withTrashed()->first();

        $bank_data = "";
        $branch_data = "";

        if (is_object($recipient_data)) {
            $bank_data = $bank_model->where('Bank_ID', $recipient_data->bank)->first();
            $branch_data = $branch_model->where('Branch_ID', $recipient_data->branch)->first();
        } else {
            // get with trashed records.
            $bank_data = $bank_model->where('Bank_ID', $recipient_data->bank)->withTrashed()->first();
            $branch_data = $branch_model->where('Branch_ID', $recipient_data->branch)->withTrashed()->first();
        }

        // $import_data = [];
        /* SENDER INFORMATION */

        // $import_data['sender_id'] = $sender_data->id;
        // $import_data['sender_first_name'] = $sender_data->first_name;
        // $import_data['sender_middle_name'] = $sender_data->middle_name;
        // $import_data['sender_last_name'] = $sender_data->last_name;
        // $import_data['sender_dob'] = $sender_data->dob;
        // $import_data['sender_postcode'] = $sender_data->postcode;
        // $import_data['sender_mobile_no'] = $sender_data->mobile_no;
        // $import_data['sender_address']  = $sender_data->address;
        // $import_data['sender_country_id'] = $sender_data->country_id;
        // $import_data['sender_place_of_birth'] = $sender_data->place_of_birth;
        // $import_data['sender_city'] = $sender_data->city;
        // $import_data['sender_nationality'] = $sender_data->nationality;
        // $import_data['sender_account_status'] = $sender_data->active;


        // /* RECIPIENT INFORMATION */

        // $import_data['recipient_id'] = $recipient_data->id;
        // $import_data['recipient_first_name'] = $recipient_data->first_name;
        // $import_data['recipient_middle_name'] = $recipient_data->middle_name;
        // $import_data['recipient_last_name'] = $recipient_data->last_name;
        // $import_data['recipient_father_name'] = $recipient_data->father_name ;
        // $import_data['recipient_address_1'] = $recipient_data->address_1;
        // $import_data['recipient_mobile'] = $recipient_data->mobile;
        // $import_data['recipient_city'] = $recipient_data->city;
        // $import_data['recipient_country'] = $recipient_data->country;
        // $import_data['recipient_relation'] = $recipient_data->recipient_relation;
        // $import_data['recipient_user_id'] = $recipient_data->user_id;
        // $import_data['recipient_account_number'] = $recipient_data->account_number;
        // $import_data['recipient_account_type'] = $recipient_data->account_type;
        // $import_data['recipient_bank'] = $recipient_data->bank;
        // $import_data['recipient_swift_code'] = $recipient_data->swift_code;
        // $import_data['recipient_branch'] = $recipient_data->branch;
        // $import_data['recipient_branch_code'] = $recipient_data->branch_code;
        // $import_data['recipient_branch_address'] = $recipient_data->branch_address;

        //$import_data['paying_agent_id'] = $paying_agent_data->paying_agent_id;
        // if ($approve_status == 'approved') {
        /**************** INSERTING INTO SECOND DATABASE ****************/
        // $this->insertEDetails($payments_data);
        // $this->insertGlobalOrders($payments_data, $sender_data, $recipient_data, $bank_data, $branch_data);
        // $this->insertGlobalOrdersHistory($payments_data);
        /**************** INSERTING INTO SECOND DATABASE ****************/
        // }

        $saved = $this->migrateOrder($payments_data, $sender_data, $recipient_data, $bank_data, $branch_data);


        if ($saved) {
            return response()->json([
                'status' => 200,
                'reponse' => 'Payment Updated Successfully'
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }
    /** insert order to other database */

    public function pendingPaymentduplication(Request $request)
    {
        if ($request->id != "") {
            $transaction =  DB::connection('mysql2')->table('orders')->where('online-order-id', $request->id)->first();
            if ($transaction === null) {
                $response = "true";
            } else {
                $response = "false";
            }
            return response()->json([
                'status' => 200,
                'response_data' => $response
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something went wrong, please try again'
            ]);
        }
    }
    /********** OTHER DATABASE INSERTION METHODS *****************/
    private function insertEDetails($payments_data)
    {
        $payment_details = json_decode($payments_data->payment_details, true);
        $data['order_id'] = $payments_data->transaction_id;
        $data['local_amount'] = $payment_details["calculate"]["sendAmount"];
        $data['local_currency'] = $payment_details["sendCurrency"];
        $data['exch_rate'] = $payment_details["calculate"]["exchangeRate"];
        $data['fee'] = $payment_details["calculate"]["totalFee"];
        $data['buying_rate'] = 0;
        $data['date'] = date('Y-m-d');

        $insert = DB::connection('mysql2')->table('app-order_edetails')->insert($data);
        if (!$insert) {
            return response()->json([
                'status' => 400,
                'response' => 'Something went wrong, please try again'
            ]);
        }
    }
    function array_to_xml($array, &$xml_user_info)
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                if (!is_numeric($key)) {
                    $subnode = $xml_user_info->addChild("$key");
                    $this->array_to_xml($value, $subnode);
                } else {
                    $subnode = $xml_user_info->addChild("item$key");
                    $this->array_to_xml($value, $subnode);
                }
            } else {
                $xml_user_info->addChild("$key", htmlspecialchars("$value"));
            }
        }
    }

    function arrayToXml($array, $rootElement = null, $xml = null)
    {
        $_xml = $xml;

        if ($_xml === null) {
            $_xml = new \SimpleXMLElement($rootElement !== null ? $rootElement : '<root/>');
        }

        foreach ($array as $k => $v) {
            if (is_array($v)) { //nested array
                $this->arrayToXml($v, $k, $_xml->addChild($k));
            } else {
                $_xml->addChild($k, htmlspecialchars($v));
            }
        }

        return $_xml->asXML();
    }

    // 217
    public function getBeneficiary(Request $request, Recipient $recipient)
    {
        $beneficiary_id =  $request->beneficiary_id;

        return response()->json([
            'status' => 200,
            'response' => $recipient::where('user_id', $beneficiary_id)->withTrashed()->select('id', 'first_name', 'last_name', 'old_beneficiary_id')->get()
            // 'response' => $recipient::where('user_id', $beneficiary_id)->select('id','first_name','last_name','old_beneficiary_id')->get()
        ]);
    }

    public function data_file($payments_data, $customer_data, $beneficiary_data, $bank_data, $branch_data, $recipient)
    {
        // \Log::info("++++++++++++ curl executed ++++++++++++");
        // \Log::info("---------customer_data inside data_file---------");
        // \Log::info("payments data: " . $payments_data);
        // \Log::info("customer data: " .  $customer_data. gettype($customer_data));
        // \Log::info("beneficiary data: " . $beneficiary_data);
        // \Log::info("bank_data: " . $bank_data);
        // \Log::info("branch_data: " . $branch_data);

        // payments_data
        $payments_data = json_decode($payments_data, true); // convert json object to array.
        $payments_data['payment_details'] = json_decode($payments_data['payment_details'], true); // convert json object to array.
        $payments_data['user']['doc_verify_json'] = json_decode($payments_data['user']['doc_verify_json'], true); // convert json object to array.

        // customer_data
        $customer_data = $payments_data['user']; // convert json object to array.

        // beneficiary_data
        $beneficiary_data = json_decode($beneficiary_data, true); // convert json object to array.
        \Log::info("beneficiary_data : " . print_r($beneficiary_data, true));
        // exit;

        // bank_data
        $bank_data = json_decode($bank_data, true); // convert json object to array.

        // branch_data
        $branch_data = json_decode($branch_data, true); // convert json object to array.

        // Fields Array
        $fields = array(
            'payments_data' => $payments_data,
            'customer_data' => $customer_data,
            'beneficiary_data' => $beneficiary_data,
            'bank_data' => $bank_data,
            'branch_data' => $branch_data
        );

        \Log::info($beneficiary_data);
        // die("");

        // exit;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://www.an2money.com/receive/index_local_test.php",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30000,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => serialize($fields),
            CURLOPT_HTTPHEADER =>
            array('Content-Type: text/xml')
            // array(
            // Set here requred headers
            // "accept: */*",
            // "accept-language: en-US,en;q=0.8",
            // "content-type: text/json",
            //    "content-type: application/xml; charset=utf-8",
            // ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            echo "curl Error #:" . $err;
            \Log::info("curl error:");
            $e = print_r($err, true);
            \Log::info($e);
        } else {

            //die($response);
            $dt = unserialize($response);

            print_r(json_decode($response));
            \Log::info("++++++++++++curl response section++++++++++++");


            if (is_array($dt)) {
                if (isset($dt['ben_id']) && isset($dt['recipient_id'])) {
                    // todo save in old ben id
                    // echo $dt['ben_id'];
                    // echo "<br>";          //
                    // echo $dt['recipient_id']; // recipient_id

                    $recipient::where('id', $dt['recipient_id'])->withTrashed()->update(array('old_beneficiary_id' => $dt['ben_id']));
                    die;
                }
            }

            //  print_r(json_decode($response));
            //  \Log::info("++++++++++++curl response section++++++++++++");

            $res = json_decode($response);
            if (is_object($res)) {
                // update migrated transaction id.
                $payment = Payments::find($payments_data['id']);
                $payment->migrated_transaction_id = $res->last_insert_id;
                $payment->save();

                // \Log::info( "transaction record id : " . $payments_data['id'] );
                // \Log::info( "server last_insert_id : " . $res->last_insert_id );
            }

            $r = print_r($response, true);
            \Log::info("curl response: " . $r);
        }
    }

    private function cc()
    { }
    private function insertGlobalOrders($payments_data, $customer_data, $beneficiary_data, $bank_data, $branch_data)
    {
        $payment_details = json_decode($payments_data->payment_details, true);

        $data['Order_ID'] = $payments_data->transaction_id;
        $data['AG_ID'] = 2345;
        $data['Cust_ID'] = $customer_data->id;
        $data['Cust_Name'] = $customer_data->first_name . ' ' . $customer_data->middle_name . ' ' .  $customer_data->last_name;
        $data['C_DOB'] = $customer_data->dob;
        if (isset($customer_data->title)) {
            if ($customer_data->title == "Mr.") {
                $data['CGender'] = "M";
            }
            if ($customer_data->title == "Mrs." || $customer_data->title == "Miss") {
                $data['CGender'] = "F";
            }
        }
        $data['Cust_Address'] = $customer_data->address_1;
        $data['Cust_PostCode'] = $customer_data->postcode;
        $data['Cust_Mobile'] = $customer_data->mobile_no;
        $data['Cust_City'] = $customer_data->city;
        $data['Cust_Country'] = $customer_data->country->country;
        $data['PlaceOfBirth'] = $customer_data->place_of_birth;

        $data['SubAgentName'] = '';
        $data['PA_Seq'] = '';
        $data['pa_serial'] = '';

        $data['Ben_ID'] = $beneficiary_data->id;
        $data['Ben_Name'] = $beneficiary_data->first_name . ' ' . $beneficiary_data->middle_name . ' ' . $beneficiary_data->last_name;
        $data['Ben_Address'] = $beneficiary_data->address_1;
        $data['Ben_Mobile'] = $beneficiary_data->mobile;
        $data['Ben_City'] = $beneficiary_data->city;
        $data['Ben_Country'] = $beneficiary_data->country;

        $data['Paying_Agent_ID'] = $payments_data->paying_agent_id;

        $data['Bank_Name'] = $bank_data->Bank_Name;
        $data['Bank_Swift_Code'] = $bank_data->Bank_Swift_Code;

        $data['Branch_Name'] = $branch_data->Branch_Name;
        $data['Branch_Address'] = $branch_data->Branch_Address;
        $data['Branch_City'] = $branch_data->Branch_City;
        $data['Branch_Country'] = $branch_data->Branch_Country;
        $data['BR_Code'] = $branch_data->BR_Code;

        $data['Foreign_Amount'] = str_replace(",", "", $payment_details["calculate"]["recipientTotal"]);
        $data['Purpose_of_Remittance'] = $beneficiary_data->sending_reason;
        $data['SRC_FUND'] = substr($beneficiary_data->source_of_income, 0, 9);
        $data['BenRel'] = $beneficiary_data->recipient_relation;
        $data['Datetime'] = Carbon::now();

        $insert = DB::connection('mysql2')->table('app_global_orders')->insert($data);
        if (!$insert) {
            return response()->json([
                'status' => 400,
                'response' => 'Something went wrong, please try again'
            ]);
        }
    }
    public function insertGlobalOrdersHistory($payments_data)
    {
        $data['order_id'] = $payments_data->transaction_id;
        $data['ag_id'] = 2345;
        $data['pa_id'] = $payments_data->paying_agent_id;
        $data['submitted_date'] = $payments_data->updated_at;
        $data['ag2an_date'] = '';
        $data['an2_global_date'] = '';

        $data['status'] = 'AN-Global';
        $data['last_updated_date'] = Carbon::now();
        $insert = DB::connection('mysql2')->table('app_global_orders_history')->insert($data);
        if (!$insert) {
            return response()->json([
                'status' => 400,
                'response' => 'Something went wrong, please try again'
            ]);
        }
    }
    public function saveinBulk(Request $request, Payments $payment, User $user, Recipient $recipient)
    {


        $checkboxvalue = $request->arras;
        // dd($request->all());
        // for check
        if ($checkboxvalue) {

            $import_data = [];
            $payments_data = $payment::whereIn('id', $checkboxvalue)->where('approve_status', 'pending')->get();
            $savedpayment = $payment::whereIn('id', $checkboxvalue)->update(['approve_status' => 'approved']);

            foreach ($payments_data as $single) {

                $sender_data = $user::where('id', $single->user_id)->get()->first();
                $paying_agent_data = $user::where('id', $single->paying_agent_id)->get()->first();
                $recipient_data = $recipient::where('id', $single->recipient_id)->get()->first();
                /* SENDER INFORMATION */
                $import_data['sender_id'] = $single->user_id;
                if ($sender_data) {
                    $import_data['sender_first_name'] = $sender_data->first_name;
                    $import_data['sender_middle_name'] = $sender_data->middle_name;
                    $import_data['sender_last_name'] = $sender_data->last_name;
                    $import_data['sender_dob'] = $sender_data->dob;
                    $import_data['sender_postcode'] = $sender_data->postcode;
                    $import_data['sender_mobile_no'] = $sender_data->mobile_no;
                    $import_data['sender_address']  = $sender_data->address ? $sender_data->address : $sender_data->address_1;
                    $import_data['sender_country_id'] = $sender_data->country_id;
                    $import_data['sender_place_of_birth'] = $sender_data->place_of_birth;
                    $import_data['sender_city'] = $sender_data->city;
                    $import_data['sender_nationality'] = $sender_data->nationality;
                    $import_data['sender_account_status'] = $sender_data->active;
                }

                /* RECIPIENT INFORMATION */
                $import_data['recipient_id'] = $single->recipient_id;

                if ($recipient_data) {
                    $import_data['recipient_first_name'] = $recipient_data["first_name"];
                    $import_data['recipient_middle_name'] = $recipient_data["middle_name"];
                    $import_data['recipient_last_name'] = $recipient_data["last_name"];
                    $import_data['recipient_father_name'] = $recipient_data["father_name"];
                    $import_data['recipient_address_1'] = $recipient_data["address_1"];
                    $import_data['recipient_mobile'] = $recipient_data["mobile"];
                    $import_data['recipient_city'] = $recipient_data["city"];
                    $import_data['recipient_country'] = $recipient_data["country"];
                    $import_data['recipient_relation'] = $recipient_data["recipient_relation"];
                    $import_data['recipient_user_id'] = $recipient_data["user_id"];
                    $import_data['recipient_account_number'] = $recipient_data["account_number"];
                    $import_data['recipient_account_type'] = $recipient_data["account_type"];
                    $import_data['recipient_bank'] = $recipient_data["bank"];
                    $import_data['recipient_swift_code'] = $recipient_data["swift_code"];
                    $import_data['recipient_branch'] = $recipient_data["branch"];
                    $import_data['recipient_branch_code'] = $recipient_data["branch_code"];
                    $import_data['recipient_branch_address'] = $recipient_data["bank_branch"]->Branch_Address;
                    $import_data['paying_agent_id'] = $single->paying_agent_id;
                }

                $saved = DB::connection('mysql2')->table('importdata')->insert($import_data);
            }
            if (!$saved) {

                return response()->json([
                    'status' => 404,
                    'response' => 'Unable to save data'
                ]);
            } else {
                return response()->json([
                    'status' => 200,
                    'reponse' => 'Data Imported Successfully'
                ]);
            }
        } else {
            $import_data = [];
            $payments_data = $payment::select('*')->where('approve_status', 'pending')->get();
            $savedpayment = $payment::select('*')->update(['approve_status' => 'approved']);

            foreach ($payments_data as $single) {

                $sender_data = $user::where('id', $single->user_id)->get()->first();
                $paying_agent_data = $payment::where('id', $single->paying_agent_id)->get()->first();
                $recipient_data = $recipient::where('id', $single->recipient_id)->get()->first();

                /* SENDER INFORMATION */
                $import_data['sender_id'] = $single->user_id;
                if ($sender_data) {
                    $import_data['sender_first_name'] = $sender_data->first_name;
                    $import_data['sender_middle_name'] = $sender_data->middle_name;
                    $import_data['sender_last_name'] = $sender_data->last_name;
                    $import_data['sender_dob'] = $sender_data->dob;
                    $import_data['sender_postcode'] = $sender_data->postcode;
                    $import_data['sender_mobile_no'] = $sender_data->mobile_no;
                    $import_data['sender_address']  = $sender_data->address;
                    $import_data['sender_country_id'] = $sender_data->country_id;
                    $import_data['sender_place_of_birth'] = $sender_data->place_of_birth;
                    $import_data['sender_city'] = $sender_data->city;
                    $import_data['sender_nationality'] = $sender_data->nationality;
                    $import_data['sender_account_status'] = $sender_data->active;
                }

                /* RECIPIENT INFORMATION */
                $import_data['recipient_id'] = $single->recipient_id;
                if ($recipient_data) {
                    $import_data['recipient_first_name'] = $recipient_data["first_name"];
                    $import_data['recipient_middle_name'] = $recipient_data["middle_name"];
                    $import_data['recipient_last_name'] = $recipient_data["last_name"];
                    $import_data['recipient_father_name'] = $recipient_data["father_name"];
                    $import_data['recipient_address_1'] = $recipient_data["address_1"];
                    $import_data['recipient_mobile'] = $recipient_data["mobile"];
                    $import_data['recipient_city'] = $recipient_data["city"];
                    $import_data['recipient_country'] = $recipient_data["country"];
                    $import_data['recipient_relation'] = $recipient_data["recipient_relation"];
                    $import_data['recipient_user_id'] = $recipient_data["user_id"];
                    $import_data['recipient_account_number'] = $recipient_data["account_number"];
                    $import_data['recipient_account_type'] = $recipient_data["account_type"];
                    $import_data['recipient_bank'] = $recipient_data["bank"];
                    $import_data['recipient_swift_code'] = $recipient_data["swift_code"];
                    $import_data['recipient_branch'] = $recipient_data["branch"];
                    $import_data['recipient_branch_code'] = $recipient_data["branch_code"];
                    $import_data['recipient_branch_address'] = $recipient_data["branch_address"];
                }

                $saved = DB::connection('mysql2')->table('importdata')->insert($import_data);
            }

            if (!$saved) {

                return response()->json([
                    'status' => 500,
                    'response' => 'Unable to save data'
                ]);
            } else {
                return response()->json([
                    'status' => 200,
                    'reponse' => 'Data Imported Successfully'
                ]);
            }
        }
    }

    public function cheakPendingStatus(Request $request, Payments $payment)
    {
        $savedpending = $payment::where('approve_status', 'pending')->get();
        $savependingcount = $savedpending->count();

        if ($savependingcount) {
            return response()->json([
                'status' => 200,
                'pending_transactions' => $savependingcount,
                'pending_transactions_' => $this->getSendToPendingTransactions($savedpending),
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }

    public function getSendToPendingTransactions($savedpending)
    {
        $output = array();
        foreach ($savedpending as $key) {
            $payment_details_ = json_decode($key->payment_details, true);
            $output[$payment_details_['sendToCountryName']][] = $payment_details_;
        }

        $counter = 1;
        $amount_local = 0;
        $amount_foreign = 0;
        $str = null;

        $arrayName = array();
        foreach ($output as $key => $value) {
            foreach ($value as $arr) {
                $amount_local  +=  $arr['calculate']['sendAmount'];
                $amount_foreign  +=  str_replace(",", "", $arr['calculate']['recipientTotal']);
            }
            $arrayName[] =  array(
                'country' => $key,
                'no_of_pending_transactions' => count($value),
                'amount_pending_local' => $amount_local,
                'amount_in_foreign' => $amount_foreign
            );
        }
        return $arrayName;
    }

    public function stats(Request $request)
    {

        $data = $request->all();
        $customer_id = $request->customer_id;

        // $data = $request->all();
        // $request->id
        // week
        $weeks = DB::select("SELECT id, payment_details FROM `payments` where created_at > DATE_SUB(now(), INTERVAL 1 WEEK) AND user_id = '" . $customer_id . "'");
        $week = $this->getDetails($weeks);

        // month
        $months = DB::select("SELECT id, payment_details FROM `payments` WHERE created_at > DATE_SUB(now(), INTERVAL 1 MONTH) AND user_id = '" . $customer_id . "'");
        $month = $this->getDetails($months);

        // year
        $years = DB::select("SELECT id, payment_details FROM `payments` WHERE created_at > DATE_SUB(now(), INTERVAL 1 YEAR) AND user_id = '" . $customer_id . "'");
        $year = $this->getDetails($years);

        return response()->json([
            'week' => $week,
            'month' => $month,
            'year' => $year,
            'status' => 200,
        ]);
    }

    public function getDetails($records = null)
    {
        $total_ammount = 0;

        if ($records) {
            foreach ($records as $row) {
                $paymentDetails = json_decode($row->payment_details, true);  // decode payment details.
                $total_ammount +=  $paymentDetails['sendAmount'];
            }
        }

        return array('amount' => $total_ammount, 'no_of_transactions' => count($records));
    }

    // get recharge by id.
    public function rechargeView(Request $request, Recharge $recharge)
    {

        // return $request->id;
        $savedrechargepending = $recharge::where('id',  $request->id)->get();
        $recharge_pendingcount = $savedrechargepending->count();
        if ($recharge_pendingcount) {
            return response()->json([
                'status' => 200,
                'recharge' => $savedrechargepending
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }

    public function pendingRechargeRequest(Request $request, Recharge $recharge)
    {
        $savedrechargepending = $recharge::where('status', 'pending')->get();
        $recharge_pendingcount = $savedrechargepending->count();
        if ($recharge_pendingcount) {
            return response()->json([
                'status' => 200,
                'pending_recharge' => $recharge_pendingcount
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }

    public function getNewRegisteredUsers(Request $request, User $user)
    {
        // $data = $request->all();
        // dd($data);
        $savedNewUser = $user::whereDate('created_at', Carbon::today())->get();
        // $savedNewUser  =  $users::where('created_at','new Date().toISOString()');
        $savedNewUserCount    =   $savedNewUser->count();
        //dd($savedNewUserCount);

        if ($savedNewUserCount) {
            return response()->json([
                'status' => 200,
                'new_Registered_user' => $savedNewUserCount
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }
    public function changeRechargeStatus(User $user, Request $request, Recharge $recharge, Ledger $ledger)
    {
        $user_id = $request->user_id;
        $id = $request->id;
        $status = $request->status;
        $amount = $request->amount;

        $userdata = $user::find($user_id);
        $rechargedata = $recharge::find($id);
        $old_status = $rechargedata->status;
        $old_balance = $userdata->balance;

        $saved = $recharge::where('id', $id)->update(['status' => $status]);
        //dd($saved);
        if ($saved) {
            if ($status == 'approved') {
                $calculate_ammount = $old_balance + $amount;
                $balance_update = $user::where('id', $user_id)->update(['balance' => $calculate_ammount]);
                $update_arr = [
                    'description' => 'Balance Added',
                    'debit_amount' => $amount,
                    'user_id' => $user_id,
                    'balance' => $calculate_ammount
                ];
                $ledger_create = $ledger::insert($update_arr);
            }
            if ($old_status == 'approved' && $status == 'rejected') {
                $calculate_ammount =  $old_balance - $amount;
                $balance_update = $user::where('id', $user_id)->update(['balance' => $calculate_ammount]);
            }
            return response()->json([
                'status' => 200,
                'reponse' => 'Balance Request Updated Successfully'
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }

    public function getCustomerPayments(Request $request, Payments $payment)
    {
        $data = $request->all();
        $customer_id = $request->customer_id;
        $date = $request->date;
        if ($date) {


            if ($date == '1_week') {
                $result = $payment::where("created_at", ">", Carbon::now()->subWeeks(1))  // 1 week
                    ->where('user_id', $customer_id)
                    ->select('*')
                    ->get();
            }

            if ($date == '1_month') {
                $result = $payment::where("created_at", ">", Carbon::now()->subMonths(1))
                    ->where('user_id', $customer_id)
                    ->select('*')
                    ->get();
            }

            if ($date == '3_month') {
                $result = $payment::where("created_at", ">", Carbon::now()->subMonths(3))
                    ->where('user_id', $customer_id)
                    ->select('*')
                    ->get();
            }
            if ($date == '6_month') {
                $result = $payment::where("created_at", ">", Carbon::now()->subMonths(6))
                    ->where('user_id', $customer_id)
                    ->select('*')
                    ->get();
            }
            if ($date == 'year') {
                $result = $payment::where("created_at", ">", Carbon::now()->subMonths(12))
                    ->where('user_id', $customer_id)
                    ->select('*')
                    ->get();
            }
        } else {
            $result = $payment::where('user_id', $customer_id)->select('*')->get();
        }
        if ($result) {
            return response()->json([
                'status' => 200,
                'data' => $result
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'response' => 'No Payments for Customer Found'
            ]);
        }
    }

    public function getLedgerValues(Request $request, Ledger $ledger)
    {
        $data = $request->all();
        $customer_id = $request->customer_id;
        $result = $ledger::where('user_id', $customer_id)->select('*')->get();
        if ($result) {
            return response()->json([
                'status' => 200,
                'data' => $result
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'data' => 'No Ledger For Customer Found'
            ]);
        }
    }




    public function acceptDoc(Request $request, User $user)
    {
        $user_id = $request->customer_id;
        $type = $request->type;
        $doc_verify = $user::where('id', $user_id)->value('doc_verify_json');
        $doc_verify_decde = json_decode($doc_verify, true);
        /* ********  IDCARD, LICENSE, PASSPORT */
        if ($type == 'id_card' || $type == 'passport' || $type == 'license') {
            $doc_verify_decde["verify_Id"]["status"] = 'accepted';
            $update_arr = [
                'doc_verify_json' => json_encode($doc_verify_decde)
            ];
            $update = $user::where('id', $user_id)->update($update_arr);
            if ($update) {
                return response()->json([
                    'status' => 200,
                    'response' => 'Accepted Successfully'
                ]);
            } else {
                return response()->json([
                    'status' => 403,
                    'response' => 'Something went Wrong, Please Try Again!'
                ]);
            }
        }
        if ($type == 'face') {
            $doc_verify_decde["verify_face"]["status"] = 'accepted';
            $update_arr = [
                'doc_verify_json' => json_encode($doc_verify_decde)
            ];
            $update = $user::where('id', $user_id)->update($update_arr);
            if ($update) {
                return response()->json([
                    'status' => 200,
                    'response' => 'Accepted Successfully'
                ]);
            } else {
                return response()->json([
                    'status' => 403,
                    'response' => 'Something went Wrong, Please Try Again!'
                ]);
            }
        }
        if ($type == 'bill' || $type == 'bank_statement') {
            $doc_verify_decde["verify_doc"]["status"] = 'accepted';
            $update_arr = [
                'doc_verify_json' => json_encode($doc_verify_decde)
            ];
            $update = $user::where('id', $user_id)->update($update_arr);
            if ($update) {
                return response()->json([
                    'status' => 200,
                    'response' => 'Accepted Successfully'
                ]);
            } else {
                return response()->json([
                    'status' => 403,
                    'response' => 'Something went Wrong, Please Try Again!'
                ]);
            }
        }
        if ($type == 'additional_doc') {
            $doc_verify_decde["additional_doc"]["status"] = 'accepted';
            $update_arr = [
                'doc_verify_json' => json_encode($doc_verify_decde)
            ];
            $update = $user::where('id', $user_id)->update($update_arr);
            if ($update) {
                return response()->json([
                    'status' => 200,
                    'response' => 'Accepted Successfully'
                ]);
            } else {
                return response()->json([
                    'status' => 403,
                    'response' => 'Something went Wrong, Please Try Again!'
                ]);
            }
        }
    }

    public function rejectDoc(Request $request, User $user)
    {
        $user_id = $request->customer_id;
        $type = $request->type;
        $user_data = $user::where('id', $user_id)->get()[0];
        $doc_verify = $user::where('id', $user_id)->value('doc_verify_json');
        $doc_verify_decde = json_decode($doc_verify, true);
        /* ********  IDCARD, LICENSE, PASSPORT */
        if ($type == 'id_card' || $type == 'passport' || $type == 'license') {
            $doc_verify_decde["verify_Id"]["status"] = 'rejected';
            $update_arr = [
                'doc_verify_json' => json_encode($doc_verify_decde)
            ];
            $update = $user::where('id', $user_id)->update($update_arr);
            if ($update) {
                $this->sendEmail($type, $user_data);
                return response()->json([
                    'status' => 200,
                    'response' => 'Rejected Successfully'
                ]);
            } else {
                return response()->json([
                    'status' => 403,
                    'response' => 'Something went Wrong, Please Try Again!'
                ]);
            }
        }
        if ($type == 'face') {
            $doc_verify_decde["verify_face"]["status"] = 'rejected';
            $update_arr = [
                'doc_verify_json' => json_encode($doc_verify_decde)
            ];
            $update = $user::where('id', $user_id)->update($update_arr);
            if ($update) {
                $this->sendEmail($type, $user_data);
                return response()->json([
                    'status' => 200,
                    'response' => 'Rejected Successfully'
                ]);
            } else {
                return response()->json([
                    'status' => 403,
                    'response' => 'Something went Wrong, Please Try Again!'
                ]);
            }
        }
        if ($type == 'bill' || $type == 'bank_statement') {
            $doc_verify_decde["verify_doc"]["status"] = 'rejected';
            $update_arr = [
                'doc_verify_json' => json_encode($doc_verify_decde)
            ];
            $update = $user::where('id', $user_id)->update($update_arr);
            if ($update) {
                $this->sendEmail($type, $user_data);
                return response()->json([
                    'status' => 200,
                    'response' => 'Rejected Successfully'
                ]);
            } else {
                return response()->json([
                    'status' => 403,
                    'response' => 'Something went Wrong, Please Try Again!'
                ]);
            }
        }
        if ($type == 'additional_doc') {
            $doc_verify_decde["additional_doc"]["status"] = 'rejected';
            $update_arr = [
                'doc_verify_json' => json_encode($doc_verify_decde)
            ];
            $update = $user::where('id', $user_id)->update($update_arr);
            if ($update) {
                $this->sendEmail($type, $user_data);
                return response()->json([
                    'status' => 200,
                    'response' => 'Rejected Successfully'
                ]);
            } else {
                return response()->json([
                    'status' => 403,
                    'response' => 'Something went Wrong, Please Try Again!'
                ]);
            }
        }
    }

    /* **************************************************** */
    //    SEND DOCUMENT REJECTED/ ACCEPTED CONFIRMATION VIA EMAIL
    /* **************************************************** */

    public function sendEmail($type, $userdata)
    {
        $name = $userdata->name;
        $email = $userdata->email;
        $to_name = $name;
        $to_email = $email;
        $mail_data = array(
            'type' => $type,
            'name'    => $name
        );

        // return view('emails.receipt', $mail_data);
        try {
            Mail::send('emails.document-email', $mail_data, function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                    ->subject('ANExpress');
                $message->from('support@anexpress.com', 'ANEXPRESS Support');
            });
            return response()->json([
                'status' => 200,
                'response' => 'Mail Sent Successfuly to User Email'
            ]);
        } catch (Exception $e) { }
    }


    public function getCountriesCodes(Countries $countries)
    {

        return response()->json([
            'status' => 200,
            'data' => Countries::select('code', 'country')->get()
        ]);
    }
}
