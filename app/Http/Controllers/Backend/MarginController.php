<?php

namespace App\Http\Controllers\Backend;

use App\Models\Margin;
use Illuminate\Http\Request;
use App\Utils\RequestSearchQuery;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreMarginRequest;
use App\Http\Requests\UpdateMarginRequest;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\Contracts\MarginRepository;

class MarginController extends BackendController
{
    /**
     * @var JobcardRepository
     */
    protected $margin;

    public function __construct(MarginRepository $margin)
    {
        //dd($jobcards);
        $this->margin = $margin;
    }
    
    /**
     * Show the application dashboard.
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function search(Request $request)
    {

        /** @var Builder $query */
        $query = $this->margin->query();
        
        $requestSearchQuery = new RequestSearchQuery($request, $query, [
            'type',
            'rate_margin',
            'fee_margin',
        ]);

        if ($request->get('exportData')) {
            return $requestSearchQuery->export([
                'type_margin.id',
                'type_margin.rate_margin',
                'type_margin.fee_margin',
            ],
                [
                    'Margin',
                ],
                'type_margin');
        }

        return $requestSearchQuery->result([
            'type_margin.id',
            'type',
            'rate_margin',
            'fee_margin',
            'created_at',
            'updated_at',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMarginRequest $request)
    {
        $data = $request->all();
        $type = $data['type'];
        $type_value = strtolower(preg_replace('/\s+/', '_', $type));
        $data['type_value'] = $type_value;
        
        $margin = $this->margin->make($data);
       
       $this->margin->save($margin, $data);

       return $this->redirectResponse($request, __('alerts.backend.margin.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Margin  $margin
     * @return \Illuminate\Http\Response
     */
    public function show(Margin $margin)
    {
       return $margin;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Margin  $margin
     * @return \Illuminate\Http\Response
     */
    public function edit(Margin $margin)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Margin  $margin
     * @return \Illuminate\Http\Response
     */
    public function update(Margin $margin, UpdateMarginRequest $request)
    {
        $data = $request->all();
        $type = $data['type'];
        $type_value = strtolower(preg_replace('/\s+/', '_', $type));
        $data['type_value'] = $type_value;

        $margin->fill($data);
        
        $this->margin->save($margin, $data);
           
        return $this->redirectResponse($request, __('alerts.backend.margin.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Margin  $margin
     * @return \Illuminate\Http\Response
     */
    public function destroy(Margin $margin, Request $request)
    {
        $this->margin->destroy($margin);

        return $this->redirectResponse($request, __('alerts.backend.margin.deleted'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function batchAction(Request $request)
    {
        $action = $request->get('action');
        $ids = $request->get('ids');
        
        switch ($action) {
            case 'destroy':                
                    $this->margin->batchDestroy($ids);
                    return $this->redirectResponse($request, __('alerts.backend.margin.bulk_destroyed'));
                break;
        }

        return $this->redirectResponse($request, __('alerts.backend.actions.invalid'), 'error');
    }

    public function updateMargin(Request $request, Margin $margin) {
        $field = $request->field;
        $id = $request->id;
        $amount = $request->amount;
        $update_arr = [
            $field => $amount
        ];
        $update = $margin::where('id', $id)->update($update_arr);
        if ($update) {
            return response()->json([
                'status' => 200,
                'message' => 'Margin Updated'
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'Something Went Wrong, Please Try Again'
            ]);
        }
    }
}
