<?php

namespace App\Http\Controllers\Backend;

use App\Models\Fee;
use Illuminate\Http\Request;
use App\Utils\RequestSearchQuery;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreFeeRequest;
use App\Http\Requests\UpdateFeeRequest;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\Contracts\FeeRepository;

class FeeController extends BackendController
{
    /**
     * @var JobcardRepository
     */
    protected $fee;

    public function __construct(FeeRepository $fee)
    {
        //dd($jobcards);
        $this->fee = $fee;
    }
    
    /**
     * Show the application dashboard.
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function search(Request $request)
    {

        /** @var Builder $query */
        $query = $this->fee->query();
        
        $requestSearchQuery = new RequestSearchQuery($request, $query, [
            'FEE',
        ]);

        if ($request->get('exportData')) {
            return $requestSearchQuery->export([
                'FEE',
            ],
                [
                    'FEE',
                ],
                'fee');
        }

        return $requestSearchQuery->feeResult([
            'fee.id',
            'MIN',
            'MAX',
            'FEE',
            'Agent_ID',
            'Country',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFeeRequest $request)
    {
        $fee = $this->fee->make(
            $request->all()
        );
       $this->fee->save($fee, $request->all());

       return $this->redirectResponse($request, __('alerts.backend.fee.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function show(Fee $fee)
    {
       return $fee;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function edit(Fee $fee)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function update(Fee $fee, UpdateFeeRequest $request)
    {
        $fee->fill(
            $request->all()
        );
        
        $this->fee->save($fee, $request->input());
           
        return $this->redirectResponse($request, __('alerts.backend.fee.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fee  $fee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fee $fee, Request $request)
    {
        $this->fee->destroy($fee);

        return $this->redirectResponse($request, __('alerts.backend.fee.deleted'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function batchAction(Request $request)
    {
        $action = $request->get('action');
        $ids = $request->get('ids');
        
        switch ($action) {
            case 'destroy':                
                    $this->fee->batchDestroy($ids);
                    return $this->redirectResponse($request, __('alerts.backend.fee.bulk_destroyed'));
                break;
        }

        return $this->redirectResponse($request, __('alerts.backend.actions.invalid'), 'error');
    }

    public function updateFee(Request $request, Fee $fee) {
        $field = $request->field;
        $id = $request->id;
        $amount = $request->amount;
        $update_arr = [
            $field => $amount
        ];
        $update = $fee::where('id', $id)->update($update_arr);
        if ($update) {
            return response()->json([
                'status' => 200,
                'message' => 'Fee Updated'
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'Something Went Wrong, Please Try Again'
            ]);
        }
    }
}
