<?php

namespace App\Http\Controllers\Backend;

use App\Models\Banks;
use Illuminate\Http\Request;
use App\Utils\RequestSearchQuery;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreBanksRequest;
use App\Http\Requests\UpdateBanksRequest;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\Contracts\BanksRepository;

class BanksController extends BackendController
{
    /**
     * @var JobcardRepository
     */
    protected $bank;

    public function __construct(BanksRepository $bank)
    {
        //dd($jobcards);
        $this->banks = $bank;
    }
    
    /**
     * Show the application dashboard.
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function search(Request $request)
    {

        /** @var Builder $query */
        $query = $this->banks->query();
        
        $requestSearchQuery = new RequestSearchQuery($request, $query, [
                'Bank_Name',
                'Bank_Code',
                'Bank_Swift_Code',
                'Address',
                'City',
                'Country',
                'State',
                'Express_Bank',
                'Instant_Bank',
                'Rate'
        ]);

        if ($request->get('exportData')) {
            return $requestSearchQuery->export([
                'Bank_Name',
                'Bank_Code',
                'Bank_Swift_Code',
                'Address',
                'City',
                'Country',
                'State',
                'Express_Bank',
                'Instant_Bank',
                'Rate'

            ],
                [
                    __('validation.attributes.bank_name'),
                    __('validation.attributes.bank_code'),
                    __('validation.attribute.bank_swift_code'),
                    __('validation.attributes.address'),
                    __('validation.attributes.city'),
                    __('validation.attributes.country'),
                    __('validation.attributes.state'),
                    __('validation.attributes.express_bank'),
                    __('validation.attributes.instant_bank'),
                    __('validation.attributes.rate'),
                ],
                'banks');
        }

        return $requestSearchQuery->result([
                'Bank_ID',
                'Bank_Name',
                'Bank_Code',
                'Bank_Swift_Code',
                'Address',
                'City',
                'Country',
                'State',
                'Express_Bank',
                'Instant_Bank',
                'Rate'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBanksRequest $request)
    {
        //dd($request->all());  
        $data = $request->all();
        // dd($data);
        $bank = $this->banks->make($data); 
       $this->banks->save($bank, $data);

       return $this->redirectResponse($request, __('alerts.backend.banks.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banks  $bank
     * @return \Illuminate\Http\Response
     */
    public function show(Banks $bank)
    {
       //dd($bank);
       return $bank;
    }

    /**se
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brands  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(Banks $bank)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brands  $brand
     * @return \Illuminate\Http\Response
     */
    public function update(Banks $bank, UpdateBanksRequest $request)
    {
        $data = $request->all();
        $bank->fill($data);
        // dd($data);
        // dd($bank);
        
        $this->banks->save($bank, $data);
           
        return $this->redirectResponse($request, __('alerts.backend.banks.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brands  $brand
     * @return \Illuminate\Http\Response
     */
    public function destroy(Banks $bank, Request $request)
    {
        $this->banks->destroy($bank);

        return $this->redirectResponse($request, __('alerts.backend.banks.deleted'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function batchAction(Request $request)
    {
        $action = $request->get('action');
        $ids = $request->get('ids');
        switch ($action) {
            case 'destroy':                
                    $this->banks->batchDestroy($ids);
                    return $this->redirectResponse($request, __('alerts.backend.banks.bulk_destroyed'));
                break;
        }

        return $this->redirectResponse($request, __('alerts.backend.actions.invalid'), 'error');
    }
}
