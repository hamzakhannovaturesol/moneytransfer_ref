<?php

namespace App\Http\Controllers\Backend;

use App\Models\Branches;
use Illuminate\Http\Request;
use App\Utils\RequestSearchQuery;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreBranchesRequest;
use App\Http\Requests\UpdateBranchesRequest;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\Contracts\BranchesRepository;

class BranchesController extends BackendController
{
    /**
     * @var BranchesRepository
     */
    protected $branch;

    public function __construct(BranchesRepository $branch)
    {
        $this->branch = $branch;

    }
    
    /**
     * Show the application dashboard.
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function search(Request $request)
    {

        /** @var Builder $query */

        $query = $this->branch->query();
        // dd($query);
        $requestSearchQuery = new RequestSearchQuery($request, $query, [
        'Branch_Name',
        'Branch_Address',
        'Branch_City',
        'Country',
        'State',
        'Pay_Type',
        'Sort_BR_Wise',
        'extra_details',
        'BR_Code',
            
        ]);

        if ($request->get('exportData')) {
            return $requestSearchQuery->export([ 

        'Branch_Name',
        'Branch_Address',
        'Branch_City',
        'Country',
        'State',
        'Pay_Type',
        'Sort_BR_Wise',
        'extra_details',
        'BR_Code',
               
            ],
                [
                    __('validation.Branches.branches_name'),
                    __('validation.Branches.branches_address'),
                    __('validation.Branches.branches_city'),
                    __('validation.Branches.country'),
                    __('validation.Branches.state'),
                    __('validation.Branches.pay_type'),
                    __('validation.Branches.sort_br_wise'),
                    __('validation.Branches.extra_details'),
                    __('validation.Branches.br_code'),
                ],
                'branches');
        }
        return $requestSearchQuery->result([
        'Branch_ID',
        //'Bank_ID',
        'Branch_Name',
        'Branch_Address',
        'Branch_City',
        'Country',
        'State',
        'Pay_Type',
        'Sort_BR_Wise',
        'extra_details',
        'BR_Code',
           
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBranchesRequest $request)
    {
       
       // dd($request->all());   
        $data = $request->input();
        
        //dd($data);
        $branch = $this->branch->make($data); 
       
       //dd($request->input());     
       $this->branch->save($branch, $data);

       return $this->redirectResponse($request, __('alerts.backend.Branches.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Branches  $branch
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Branches $branch)
    {   
       return $branch;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Branches  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit(Branches $branch)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Branches  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(Branches $branch, UpdateBranchesRequest $request)
    {   
        // $logo = $request->selectedLogo;
        // $logo_name_random = rand(0,9999). $logo->getClientOriginalName();
        // $logo_name = str_replace(' ', '', $logo_name_random);
        // $logo->move(public_path('uploads'),$logo_name);

        $data = $request->input();

        $new_logo = $request->selectedLogo;
        $old_logo = $request->company_logo;

        if($new_logo){
            $logo_name_random = rand(0,9999). $new_logo->getClientOriginalName();
            $logo_name = str_replace(' ', '', $logo_name_random);
            $data['company_logo'] = $logo_name; 
            
            if(file_exists(public_path('uploads/').$old_logo)){
                unlink(public_path('uploads/').$old_logo);
            }
             // remove old file 
            $new_logo->move(public_path('uploads'),$logo_name);
        }

        $branch->fill($data);
        $this->branch->save($branch, $data);
        return $this->redirectResponse($request, __('alerts.backend.Branches.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Branches  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Branches $branch, Request $request)
    {
        $this->branch->destroy($branch);

        return $this->redirectResponse($request, __('alerts.backend.Branches.deleted'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function batchAction(Request $request)
    {
        $action = $request->get('action');
        //$ids = $request->get('ids');
        $ids = $request->get('ids');
        
        switch ($action) {
            case 'destroy':                
                    $this->branch->batchDestroy($ids);
                    return $this->redirectResponse($request, __('alerts.backend.Branches.bulk_destroyed'));
                break;
        }

        return $this->redirectResponse($request, __('alerts.backend.actions.invalid'), 'error');
    }
}
