<?php

namespace App\Http\Controllers\Backend;

use App\Models\User;
use Illuminate\Http\Request;
use App\Utils\RequestSearchQuery;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\Contracts\RoleRepository;
use App\Repositories\Contracts\UserRepository;
use App\Reply;

class CustomerController extends BackendController
{
    /**
     * @var UserRepository
     */
    protected $customers;

    /**
     * @var RoleRepository
     */
    protected $roles;
    protected $reply;

    /**
     * Create a new controller instance.
     *
     * @param UserRepository                             $customers
     * @param \App\Repositories\Contracts\RoleRepository $roles
     */
    public function __construct(UserRepository $customers, RoleRepository $roles, Reply $reply)
    {
        $this->customers = $customers;
        $this->roles = $roles;
        $this->reply = $reply;
    }

    public function getActiveUserCounter()
    {
        return $this->customers->query()->whereActive(true)->count();
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function search(Request $request)
    {
        $requestSearchQuery = new RequestSearchQuery($request, $this->customers->query(), [
            'name',
            'email',
        ]);

        if ($request->get('exportData')) {
            return $requestSearchQuery->export([
                'name',
                'email',
                'active',
                'last_access_at',
                'created_at',
                'updated_at',
            ],
                [

                    ('validation.attributes.name'),
                    __('validation.attributes.email'),
                    __('validation.attributes.active'),
                    __('labels.last_access_at'),
                    __('labels.created_at'),
                    __('labels.updated_at'),
                    __('labels.last_access_at'),
                    __('labels.created_at'),
                    __('labels.updated_at'),
                ],
                'users');
        }
        return $requestSearchQuery->customerResult([
            'id',
            'name',
            'email',
            'mobile_no',
            'last_access_at',
            'created_at',
            'updated_at',
                        ]);
    }

    public function searchValue(Request $request)
    {
        $requestSearchQuery = new RequestSearchQuery($request, $this->customers->query(), [
            'name',
            'email',
        ]);

        return $requestSearchQuery->customerSearchResult([
            'id',
            'name',
            'email',
            'mobile_no',

        ]);
    }
    /**
     * @param User $user
     *
     * @return User
     */
    public function show(User $user)
    {
        if (! $user->can_edit) {
            // Only Super admin can access himself
            abort(403);
        }
           //dd($user);
            return false;
        //return $user;
    }

    public function getRoles()
    {
        return $this->roles->getAllowedRoles();
    }

    /**
     * @param StoreUserRequest $request
     *
     * @return mixed
     */
    public function store(StoreUserRequest $request)
    {

        //dd($request->all());
       // exit;
        $this->authorize('create customer');

        //dd($this->customer);
        $this->customers->store($request->input());

        return $this->redirectResponse($request, __('alerts.backend.customers.created'));
    }

    /**
     * @param User              $user
     * @param UpdateUserRequest $request
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     *
     * @return mixed
     */
    public function update(User $user, UpdateUserRequest $request)
    {
        $this->authorize('edit customer');

        $this->customers->update($user, $request->input());

        return $this->redirectResponse($request, __('alerts.backend.customers.updated'));
    }

    /**
     * @param User    $user
     * @param Request $request
     *
     * @return mixed
     */
    public function destroy(User $user, Request $request)
    {
        $this->authorize('delete customer');

        $this->customers->destroy($user);

        return $this->redirectResponse($request, __('alerts.backend.customers.deleted'));
    }

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function impersonate(User $user)
    {
        $this->authorize('impersonate customer');

        return $this->customers->impersonate($user);
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function batchAction(Request $request)
    {
        $action = $request->get('action');
        $ids = $request->get('ids');

        switch ($action) {
            case 'destroy':
                $this->authorize('delete customer');

                $this->customers->batchDestroy($ids);

                return $this->redirectResponse($request, __('alerts.backend.customers.bulk_destroyed'));
                break;
            case 'enable':
                $this->authorize('edit customer');

                $this->customers->batchEnable($ids);

                return $this->redirectResponse($request, __('alerts.backend.customers.bulk_enabled'));
                break;
            case 'disable':
                $this->authorize('edit customer');

                $this->customers->batchDisable($ids);

                return $this->redirectResponse($request, __('alerts.backend.customers.bulk_disabled'));
                break;
        }

        return $this->redirectResponse($request, __('alerts.backend.actions.invalid'), 'error');
    }

    public function activeToggle(User $user)
    {
        $this->authorize('edit customer');
        $user->update(['active' => ! $user->active]);
    }

    // show single reply.
    public function showMessage(Reply $reply)
    {
        // dd($reply);
        return $reply;
    }
    // search for message
    public function messages(Request $request)
    {
        $query = $this->reply->query();
        // dd($query);

        $requestSearchQuery = new RequestSearchQuery($request, $query, [
                   'id',
                   'name',
                   'email',
                   'phone',
                   'message',
                   'status',
        ]);

       if ($request->get('exportData')) {
            return $requestSearchQuery->export([
                'name',
                'email',
                'phone',
                'message',
                'status',
            ],
                [
                    __('validation.attributes.name'),
                    __('validation.attributes.email'),
                    __('validation.attributes.phone'),
                    __('validation.attributes.message'),
                    __('validation.attributes.status'),
                ],
                'replies');
        }

        return $requestSearchQuery->resultCustomerMessages([
            'id',
            'name',
            'email',
            'phone',
            'message',
            'status',
        ]);
    }
}
