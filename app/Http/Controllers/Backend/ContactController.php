<?php

namespace App\Http\Controllers\Backend;

use App\Models\ContactUs;
use Illuminate\Http\Request;
use App\Utils\RequestSearchQuery;
use Illuminate\Support\Facades\Gate;
//use App\Http\Requests\UpdateBanksRequest;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\Contracts\ContactRepository;
use Illuminate\Database\Eloquent\Model;
use App\Repositories\Contracts\UserRepository;
use App\Models\AdminMessages;
use App\Events\PublicMessages;
use DB;
use App;
use App\Reply;


class ContactController extends BackendController
{
    /**
     * @var ContactRepository
     */
    protected $contact;

    public function __construct(ContactRepository $contact)
    {
        $this->contact = $contact;
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function search(Request $request)
    {
        //dd($request);
        /** @var Builder $query */
        $query = $this->contact->query();
        // dd($query);

        $requestSearchQuery = new RequestSearchQuery($request, $query, [
                'customer_id',
                   'name',
                   'email',
                   'phone',
                   'company',
                   'message'
        ]);

        if ($request->get('exportData')) {
            //dd($request->get('exportData'));
            return $requestSearchQuery->export([
                'customer_id',
                'name',
                'email',
                'phone',
                'company',
                'message'

            ],
                [
                    __('validation.attributes.name'),
                    __('validation.attributes.email'),
                    __('validation.attributes.phone'),
                    __('validation.attributes.company'),
                    __('validation.attribute.message'),
                ],
                'contactus');
        }

        return $requestSearchQuery->resultmessage([
            'id',
            'customer_id',
            'name',
            'email',
            'phone',
            'company',
            'message'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(StoreRequest $request)
    // {
    //     //dd($request->all());
    //     $data = $request->all();
    //     // dd($data);
    //     $bank = $this->banks->make($data);
    //    $this->banks->save($bank, $data);

    //    return $this->redirectResponse($request, __('alerts.backend.banks.created'));
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Banks  $bank
     * @return \Illuminate\Http\Response
     */
    public function show(ContactUS $contactus)
    {
        // dd($contactus);
        return $contactus;
    }

    public function store(Request $request, UserRepository $customers) {

        $requestSearchQuery = new RequestSearchQuery($request, $customers->query(), [
            'name',
            'email',
        ]);

        $getcustomer = $requestSearchQuery->customerSearchResult(['name'])->count();
        // $data = $request->data;
        // $user_id = auth()->user()->id;
        $data = $request->data['userList'][0]['libs'];
        if($getcustomer == count($data)-1){
            $data['customer_id'] = '0';
        }


        if($getcustomer == count($data)) {
            $contactus = new AdminMessages();
            $name = 'Admin';
            $email = 'info@anexpress.com';
            $phone =  '020 7426 0113';
            $company = 'AN Express';
            $message = $request->data['message'];
            $contactus->customer_id = '0';
            $contactus->sender_id = '0';
            $contactus->name = $name;
            $contactus->email = $email;
            $contactus->phone = $phone;
            $contactus->company = $company;
            $contactus->message = $message;
            $contactus->status = 'pending';
            $saved = $contactus->save();
            event(new PublicMessages($contactus));
        }else{
            $data = $request->data['value'];
            foreach($data as $details) {
                $contactus = new AdminMessages();
                $name =$details['name'];
                $email =$details['email'];
                $phone =$details['mobile_no'];
                $company = 'User';
                $message = $request->data['message'];
                $contactus->customer_id = $details['id'];
                $contactus->sender_id = '0';
                $contactus->name = $name;
                $contactus->email = $email;
                $contactus->phone = $phone;
                $contactus->company = $company;
                $contactus->message = $message;
                $contactus->status = 'pending';
                $saved = $contactus->save();
                // event(new MessageReceived($contactus));
            }
        }

        $response = array('status' => 200);

        return response()->json($response);

    }
    /**se
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brands  $brand
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactUs $contactus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Brands  $brand
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $second_database = DB::connection('mysql2')->select("SELECT * FROM test WHERE id = ?",[1]);
        // $second_database = test::find(1);
        dd($second_database);
    }

    public function notification(Reply $reply) {
        $reply = $reply::where('receiver_id', 1)->get();
        if(count($reply) > 0){
            $data = array('status' => 200, 'response' => $reply);
        }else{
            $data = array('status' => 500, 'response' => $reply);
        }

        // $contact = $contactus::where('status', 'pending')->get();
        // if(count($contact) > 0){
        //     $data = array('status' => 200, 'response' => $contact);
        // }else{
        //     $data = array('status' => 500, 'response' => $contact);
        // }

        return response()->json($data);
    }

    public function updatenotification(Request $request, ContactUS $contactus) {
        $id = $request->get('id');
        $update = $contactus::where('status', 'pending');
        if($id){
            $update =    $update->where('id', $id);
        }
        $update =  $update->update(['status' => 'seen']);

        $updatedList = $this->notification($contactus);

        if($update){
            $data = array('status' => 200, 'response' => $updatedList);
        }else{
            $data = array('status' => 500, 'response' => $updatedList);
        }

        return response()->json($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brands  $brand
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Banks $bank, Request $request)
    // {
    //     $this->banks->destroy($bank);

    //     return $this->redirectResponse($request, __('alerts.backend.banks.deleted'));
    // }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\RedirectResponse
     */
    // public function batchAction(Request $request)
    // {
    //     $action = $request->get('action');
    //     $ids = $request->get('ids');
    //     switch ($action) {
    //         case 'destroy':
    //                 $this->banks->batchDestroy($ids);
    //                 return $this->redirectResponse($request, __('alerts.backend.banks.bulk_destroyed'));
    //             break;
    //     }

    //     return $this->redirectResponse($request, __('alerts.backend.actions.invalid'), 'error');
    // }
}
