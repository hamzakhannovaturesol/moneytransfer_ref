<?php

namespace App\Http\Controllers\Backend;

use App\Models\Recharge;
use Illuminate\Http\Request;
use App\Utils\RequestSearchQuery;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreRechargeRequest;
use App\Http\Requests\UpdateRechargeRequest;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\Contracts\RechargeRepository;

class RechargeController extends BackendController
{
    /**
     * @var JobcardRepository
     */
    protected $recharge;

    public function __construct(RechargeRepository $recharge)
    {
        //dd($jobcards);
        $this->recharge = $recharge;
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function search(Request $request)
    {
        /** @var Builder $query */
        $query = $this->recharge->query();

        $requestSearchQuery = new RequestSearchQuery($request, $query, [
            'details',

            'amount',
            'status',
        ]);

        if ($request->get('exportData')) {
            return $requestSearchQuery->export([
                'name',
                'items_count',
                'recharge.created_at',
                'recharge.updated_at',
            ],
                [
                    __('validation.recharge.name'),
                    __('validation.recharge.items_count'),
                    __('labels.created_at'),
                    __('labels.updated_at'),
                ],
                'recharge');
        }

        return $requestSearchQuery->rechargeResult([
            'recharge.id',
            'details',
            'image',
            'amount',
            'user_id',
            'status',
            'recharge.created_at',
            'recharge.updated_at',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRechargeRequest $request)
    {
        // dd($request->all());
        $recharge = $this->recharge->make(
            $request->all()
        );

       $this->recharge->save($recharge, $request->input());

       return $this->redirectResponse($request, __('alerts.backend.recharge.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recharge  $recharge
     * @return \Illuminate\Http\Response
     */
    public function show(Recharge $recharge)
    {
       return $recharge;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recharge  $recharge
     * @return \Illuminate\Http\Response
     */
    public function edit(Recharge $recharge)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recharge  $recharge
     * @return \Illuminate\Http\Response
     */
    public function update(Recharge $recharge, UpdateRechargeRequest $request)
    {
        $recharge->fill(
            $request->all()
        );

        $this->recharge->save($recharge, $request->input());

        return $this->redirectResponse($request, __('alerts.backend.recharge.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recharge  $recharge
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recharge $recharge, Request $request)
    {
        $this->recharge->destroy($recharge);

        return $this->redirectResponse($request, __('alerts.backend.recharge.deleted'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function batchAction(Request $request)
    {
        $action = $request->get('action');
        $ids = $request->get('ids');

        switch ($action) {
            case 'destroy':
                    $this->recharge->batchDestroy($ids);
                    return $this->redirectResponse($request, __('alerts.backend.recharge.bulk_destroyed'));
                break;
        }

        return $this->redirectResponse($request, __('alerts.backend.actions.invalid'), 'error');
    }
}
