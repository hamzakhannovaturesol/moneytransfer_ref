<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Utils\RequestSearchQuery;
use App\Models\Voucher;
use App\Models\User;
use App\Repositories\Contracts\VoucherRepository;

class VoucherController extends BackendController
{
    protected $vouchers;
    public function __construct(VoucherRepository $vouchers)
    {
        $this->vouchers = $vouchers;
    }
    public function search(Request $request)
    {
        $requestSearchQuery = new RequestSearchQuery($request, $this->vouchers->query(), [
            'code',
            'type',
            'title',
            'discount_amount',
            'start_date',
            'end_date',
        ]);
        if ($request->get('exportData')) {
            return $requestSearchQuery->export(
                [
                    'code',
                    'type',
                    'title',
                    'discount_amount',
                    'start_date',
                    'end_date',
                    'created_at',
                    'updated_at',
                ],
                [
                    "code",
                    "type",
                    "title",
                    "discount_amount",
                    "start_date",
                    "end_date",
                    __('labels.created_at'),
                    __('labels.updated_at'),
                ],
                'vouchers'
            );
        }
        return $requestSearchQuery->result([
            'id',
            'type',
            'code',
            'title',
            'discount_amount',
            'start_date',
            'end_date',
            'created_at',
            'updated_at',
        ]);
    }
    public function store(Request $request)
    {
        $this->vouchers->store($request->input());
        return $this->redirectResponse($request, 'Voucher Created');
    }
    public function show(Voucher $voucher)
    {
        return $voucher;
    }
    public function update(Voucher $voucher, Request $request)
    {
        $data = $request->all();
        $this->vouchers->update($voucher, $request->input());
        return $this->redirectResponse($request, "Voucher Updated");
    }
    public function destroy(Voucher $voucher, Request $request)
    {
        \Log::info('voucher' . $voucher);
        $this->vouchers->destroy($voucher);
        return $this->redirectResponse($request, 'Voucher deleted.');
    }
    public function getCode()
    {
        $code = $this->unique_code(6);
        if (gettype($this->checkCode($code)) === "string") {
            return response()->json([
                'status' => 200,
                'response' => $code
            ]);
        } else {
            $this->getCode();
        }
    }
    protected function checkCode($code = null)
    {
        $result = User::where('ref_key', '=', $code)->first();
        if ($result === null) {
            return $code; // code does not exit.
        }
        return false;
    }
    /**
     *  Generate random alpha-string
     */
    public function unique_code($limit)
    {
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }
}
