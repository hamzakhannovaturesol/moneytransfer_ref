<?php
namespace App\Http\Controllers;

ini_set("max_execution_time", 0);
ini_set('memory_limit','32M');

use Illuminate\Http\Request;
use App\Models\User;
use Carbon\Carbon;

class CronJobController extends Controller
{
    public $today;
    public $production;

    public function __construct()
    {
        $date = Carbon::today();
        $this->today =  $date->toDateString();


        $this->today =  "2020-01-18";



        $this->admin_email =  "admin@admin.com"; // admin email.
        $this->production =  true;
    }

    private function getDocuments()
    {
        $records = User::select('doc_verify_json', 'email','name','id')->get();
        // echo "<pre>";
        // print_r($records);
        // exit;
        $expiry_dates = array();
        $counter = 1;

        if(is_object($records))
        {
            $id_proof = array();
            $address_proof = array();
            $verify_face = array();

            foreach($records as $d)
            {
                // decode json and convert to array
                $id = (isset($d->id))? $d->id : '' ;
                $email = (isset($d->email))? $d->email : '' ;
                $name = (isset($d->name))? $d->name : '' ;
                $result = json_decode($d->doc_verify_json, true);

                if ( $d->id == 1 ) // skip admin.
                 continue;

                // get "id proof" docs expiry.
                if ( isset($result['verify_Id'])  &&  is_array($result['verify_Id']))
                {
                    $__expiry_date_ = $this->getExpiryDate($result['verify_Id']['expiry_date']); // get expiry date.
                    $id_proof_expiry_date = $this->addSixMonth($__expiry_date_); // added six month to issue date and before one day expiry send email.

                    $id_proof[$counter]['expiry_date'] = $id_proof_expiry_date; // document expiry

                    $id_proof[$counter]['issue_date'] = $result['verify_Id']['expiry_date']; // entered date.
                    $id_proof[$counter]['document_type'] = $result['verify_Id']['type']; // type of document.
                    $id_proof[$counter]['email'] = $email; // user email.
                    $id_proof[$counter]['name'] = $name; // user email.
                    $id_proof[$counter]['id'] = $id; // user id.
                }

                // get "face" docs expiry.
                if ( isset($result['verify_face']) && is_array($result['verify_face']) && isset($result['verify_face']['created_at']) )
                {
                    $_expiry_date__ = $this->getExpiryDate($result['verify_face']['created_at']); // get created_at.
                    $verify_face_expiry_date = $this->addOneYear($_expiry_date__); // added one year to created date and before one day of expiry send email.

                    // add six months to issue date.
                    $verify_face[$counter]['expiry_date'] =  $verify_face_expiry_date;
                    $verify_face[$counter]['created_at'] =  $result['verify_face']['created_at'];

                    $verify_face[$counter]['document_type'] = $result['verify_face']['type']; // type of document
                    $verify_face[$counter]['email'] = $email; // user email.
                    $verify_face[$counter]['name'] = $name; // user email.
                    $verify_face[$counter]['id'] = $id; // user id.
                }

                // get "address proof" docs expiry.
                if ( isset($result['verify_doc'])  &&  is_array($result['verify_doc']) && isset($result['verify_doc']['expiry_date']))
                {
                    $_expiry_date_ = $this->getExpiryDate($result['verify_doc']['expiry_date']); // get expiry date.
                    $address_proof_expiry_date = $this->addSixMonth($_expiry_date_); // added six month to issue date and before one day expiry send email.

                    // add six months to issue date.
                    $address_proof[$counter]['expiry_date'] =  $address_proof_expiry_date;
                    $address_proof[$counter]['issue_date'] =  $result['verify_doc']['expiry_date'];
                    $address_proof[$counter]['document_type'] = $result['verify_doc']['type']; // type of document
                    $address_proof[$counter]['email'] = $email; // user email.
                    $address_proof[$counter]['name'] = $name; // user email.
                    $address_proof[$counter]['id'] = $id; // user id.
                }

                $counter++;
            }
            // print_r($id_proof);
            // exit;
            $expiry_dates = ['id_proof' => $id_proof, 'verify_face' => $verify_face, 'address_proof' => $address_proof];
        }
        return $expiry_dates;
    }

    private function documentExpireyCheck()
    {
        $records = $this->getDocuments();
        echo "<pre>";
        print_r($records);
        // exit;

        // id_proof
        if (isset($records['id_proof']) && is_array($records['id_proof'])) {
            // print_r($records['id_proof']);

            if (count($records['id_proof']))
            {
                foreach ($records['id_proof'] as $row) {

                    $expiry_date = $row['expiry_date'];
                    $document_type = $row['document_type'];
                    $email = $row['email'];
                    $name = $row['name'];
                    $subject = " ANEXPRESS Support " . strtoupper($document_type) . " Expiry Alert";
                    $message = "Your " . strtoupper($document_type) . " is about to expire in one day!";

                    // send customer message.
                    $this->sendEmail($expiry_date, $email, $subject, $message, $name);

                     // Send email to admin.
                    $subject_admin = " ANEXPRESS Customer  $name " . strtoupper($document_type) . " Expiry Alert";
                    $message_admin = "Customer $name " . strtoupper($document_type) . " is about to expire in one day!";
                    $this->sendEmail($expiry_date, $this->admin_email, $subject_admin, $message_admin, $name);
                }
            }

            // echo $this->today;
            // exit;
        }


        // verify face send email
        if (isset($records['verify_face']) && is_array($records['verify_face'])) {
            if (count($records['verify_face']))
            {
                foreach ($records['verify_face'] as $row)
                {
                    $expiry_date = $row['expiry_date'];
                    $document_type = $row['document_type'];
                    $email = $row['email'];
                    $name = $row['name'];
                    $subject = " ANEXPRESS Support " . strtoupper($document_type) . " Image Expiry Alert";
                    $message = "Your " . strtoupper($document_type) . " Image is about to expire in one day!";
                    $this->sendEmail($expiry_date, $email, $subject, $message, $name);
                }
            }
        }

        // address proof send email
        if (isset($records['address_proof']) && is_array($records['address_proof'])) {
            if (count($records['address_proof']))
            {
                foreach ($records['address_proof'] as $row)
                {
                    $expiry_date = $row['expiry_date'];
                    $document_type = $row['document_type'];
                    $email = $row['email'];
                    $name = $row['name'];
                    $subject = " ANEXPRESS Support " . strtoupper($document_type) . " Expiry Alert";
                    $message = "Your " . strtoupper($document_type) . " is about to expire in one day!";
                    $this->sendEmail($expiry_date, $email, $subject, $message, $name);
                }
            }
        }
    }

    public function index()
    {
       $this->documentExpireyCheck();
    }

    public function sendEmail($expiry_date = "",  $email = "", $subject = "", $message = "", $name)
    {

        if( $this->production == false)
        {
            // development

            if( $this->today == $expiry_date )
            {
                @mail($email, $subject, $message);
                echo "Successfully Sent All Emails";
            }

           

        } else {

            // Production mail code.
            if( $this->today == $expiry_date )
            {
                \Mail::send( [], [], function ($m) use($email, $name, $subject, $message) {
                    $m->from('testerz.client@gmail.com', 'ANEXPRESS Support');
                    $m->to($email, $name)->subject($subject);
                    $m->setBody("$message", 'text/html');
                });
                
                echo "Successfully Sent All Emails";

            }

        }
    }

    private function getExpiryDate($expiry_date = null)
    {
        $array = explode("-", $expiry_date);
        $dt =  Carbon::create($array[0], $array[1], $array[2]);
        //  return $dt->toDateString(); // only return date
        return Carbon::create($array[0], $array[1], $array[2]); // full date with time and timezone
    }

    // return added six months minu one day before expiry.
    private function addSixMonth($date = "")
    {
        $six_month_date = $date->addMonths(6);

        // echo $six_month_date;
        $fetch_date = explode(" ", $six_month_date); // date and skip time.
        // var_dump($fetch_date);
        // exit;

        // echo $fetch_date[0]; // date
        // exit;

        // yesterday.
        $_date_ = new \DateTime($fetch_date[0]);
        $_date_->modify('-1 day');
        return $_date_->format('Y-m-d');
    }

    // return added one year minu one day before.
    private function addOneYear($date = "")
    {
        $one_year_date = $date->addYears(1);

        // echo $six_month_date;
        $fetch_date = explode(" ", $one_year_date); // date and skip time.
        // var_dump($fetch_date);
        // exit;

        // echo $fetch_date[0]; // date
        // exit;

        // yesterday.
        $_date_ = new \DateTime($fetch_date[0]);
        $_date_->modify('-1 day');
        return $_date_->format('Y-m-d');
    }
}