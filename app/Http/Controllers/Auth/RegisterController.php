<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\Referral;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Arcanedev\NoCaptcha\Rules\CaptchaRule;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Repositories\Contracts\AccountRepository;
use Illuminate\Auth\Events\Registered;
use Session;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * @var \App\Repositories\Contracts\AccountRepository
     */
    protected $account;

    /**
     * RegisterController constructor.
     *
     * @param AccountRepository $account
     */
    public function __construct(AccountRepository $account)
    {
        $this->middleware('guest');

        $this->account = $account;
    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'                  => 'required|string|max:255',
            'email'                 => 'required|string|email|max:255|unique:users',
            'mobile_no'             => 'string|unique:users',
            'password'              => 'required|string|min:6|confirmed',
            // 'g-recaptcha-response'  => [new CaptchaRule()],
        ]);
    }
    protected function customerValidator(array $data)
    {
        return Validator::make($data, [
            // 'name'               => 'required|string|max:255',
            'first_name'            => 'required|string|max:255',
            // 'middle_name'           => 'string|max:255',
            'last_name'             => 'required|string|max:255',
            'title'                 => 'required|max:255',
            'dob'                   => 'required',
            'place_of_birth'        => 'required',
            'country_id'            => 'required',
            'city'                  => 'required',
            'nationality'           => 'required',
            'postcode'              => 'required',
            'address_1'             => 'required',
            // 'address_2'             => 'required',
            'email'                 => 'required|string|email|max:255|unique:users',
            'mobile_no'             => 'string|unique:users',
            // 'phone_no'              => 'string|unique:users',
            'password'              => 'required|string|min:6|confirmed',
            // 'g-recaptcha-response'  => [new CaptchaRule()],

            // new fields added.
            'occupation'             => 'required',
            'annual_income'          => 'required',
            'exp_month_volume'       => 'required',
        ]);
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     *
     * @return User
     */

    /**
     *  Generate random alpha-string
     */
    protected function unique_code($limit)
    {
        return substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
    }
    /**
     * Show the application registration form.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm(Request $request)
    {
        // update existing user.
        // $records =  User::whereNull('ref_key', '')->get();
        // foreach ($records as $row) {
        //     $uc = $this->unique_code(6);
        //     if (User::where('ref_key', '!=', $uc)->exists()) {
        //         $u = User::find($row->id);
        //         $u->ref_key = $uc;
        //         $u->save();
        //     }
        // }

        // print "<pre>";
        // print_r($records);
        // exit;

        if ($request->has('ref')) {
            session(['ref_key' => $request->query('ref')]);
        }
        // echo session('ref_key');
        // echo $this->getReferedUserID('b2wdg9');
        return view('auth.register');
    }
    protected function create(array $data)
    {
        if (isset($data['role'])) {
            if ($data['role'] == 'customer') {
                $middle_name = ' ';
                isset($data['middle_name']) ? $middle_name = ' ' . $data['middle_name'] . ' ' : ' ';
                $data['name'] = $data['first_name'] . $middle_name . $data['last_name'];

                // referrer
                $data['ref_key'] = $this->unique_code(6);

                // new fields.
                // $data['occupation'] = 'occupation goes here';
                // $data['occupation'] = $data['occupation'];
                // $data['annual_income'] = $data['annual_income'];
                // $data['exp_month_volume'] = $data['exp_month_volume'];
                $data['senior_gov_pos'] = ($data['gov_position'] == 'y') ? $data['gov_position_box'] : $data['gov_position'];
                $ref_key = 0;
                // store ref information.
                if (Session::has('ref_key')) {
                    $ref_key = session('ref_key');
                    $ref_key = $this->getReferedUserID($ref_key);
                }
                return $this->account->registerCustomer($data, $ref_key);
            }

            if ($data['role'] == 'paying') {
                return $this->account->registerPaying($data);
            }
        } else {
            return $this->account->register($data);
        }
    }
    // get the refered user id by ref code.
    protected function getReferedUserID($ref_code = null)
    {
        \DB::enableQueryLog();
        $user = User::where('ref_key', $ref_code)->select('id')->first();
        \Log::info('db get query log : ' . print_r(\DB::getQueryLog(), true));
        return (isset($user->id)) ? $user->id : '';
    }

    /* CUSTOMER REGISTER */
    public function register(Request $request)
    {
        // echo "<pre>";
        // print_r(  $_POST );
        // exit;

        // echo "<pre>";
        // print_r( $request->all() );

        // $user = $this->create($request->all());
        // echo $user;
        // echo gettype($user);
        // exit;

        $this->customerValidator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    protected function createPaying(array $data)
    {
        return $this->account->registerPaying($data);
    }

    public function showPayingRegistrationForm()
    {
        return view('auth.paying.register');
    }

    public function registerPaying(Request $request)
    {
        // dd('coming here in auth register controller');
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

        return $this->registeredPaying($request, $user)
            ?: redirect($this->redirectPathPaying());
    }

    /**
     * The user has been registered.
     *
     * @param \Illuminate\Http\Request $request
     * @param m ixed                    $user
     *
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        return redirect(home_route());
    }

    /**
     * The user has been registered.
     *
     * @param \Illuminate\Http\Request $request
     * @param mixed                    $user
     *
     * @return mixed
     */
    protected function registeredPaying(Request $request, $user)
    {
        return redirect(home_route_paying());
    }

    protected function redirectPathPaying()
    {
        if (method_exists($this, 'redirectToPaying')) {
            return $this->redirectToPaying();
        }

        return property_exists($this, 'redirectToPaying') ? $this->redirectToPaying : '/home';
    }
}
