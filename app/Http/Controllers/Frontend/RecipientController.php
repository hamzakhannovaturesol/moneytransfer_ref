<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Recipient;
use Illuminate\Http\Request;
use App\Utils\RequestSearchQuery;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreRecipientRequest;
use App\Http\Requests\UpdateRecipientRequest;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\Contracts\RecipientRepository;
use DB;

class RecipientController extends FrontendController
{
    /**
     * @var JobcardRepository
     */
    protected $recipient;

    public function __construct(RecipientRepository $recipient)
    {
        //dd($jobcards);
        $this->recipient = $recipient;
    }
    
    /**
     * Show the application dashboard.
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function search(Request $request)
    {
		\Log::info("inside recipientcontroller.php ");
        /** @var Builder $query */
        $query = $this->recipient->query();
        
        $requestSearchQuery = new RequestSearchQuery($request, $query, [
            'recipient.first_name',
            'recipient.middle_name',
            'recipient.last_name',
            'recipient.father_name',
            'recipient.address_1',
            'recipient.address_2',
            'recipient.city',
            'recipient.postal_code',
            'recipient.country',
            'recipient.mobile',
            'recipient.phone',
            'recipient.email',
            'recipient.source_of_income',
            'recipient.sending_reason',
            'recipient.recipient_relation',
        ]);

        if ($request->get('exportData')) {
            return $requestSearchQuery->export([
                'recipient.id',
                'Recipient.created_at',
                'Recipient.updated_at',
            ],
                [
                    'id',
                    __('labels.created_at'),
                    __('labels.updated_at'),
                ],
                'recipient');
        }

        return $requestSearchQuery->recipientResult([
            'recipient.id',
            'recipient.user_id',
            'recipient.first_name',
            'recipient.middle_name',
            'recipient.last_name',
            'recipient.father_name',
            'recipient.address_1',
            'recipient.address_2',
            'recipient.city',
            'recipient.postal_code',
            'recipient.country',
            'recipient.mobile',
            'recipient.phone',
            'recipient.email',
            'recipient.source_of_income',
            'recipient.sending_reason',
            'recipient.recipient_relation',
            'recipient.bank',
            'recipient.branch',
            'recipient.account_number',
            'recipient.created_at',
            'recipient.updated_at',
        ]);


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRecipientRequest $request)
    {
        $data = $request->all();
        $user_id = auth()->user()->id;
        $data['user_id'] = $user_id;

        $recipient = $this->recipient->make($data); 
        
       $this->recipient->save($recipient, $request->input());

       return $this->redirectResponse($request, __('alerts.frontend.recipient.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Recipient  $recipient
     * @return \Illuminate\Http\Response
     */
    public function show(Recipient $recipient)
    {
       return $recipient;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Recipient  $recipient
     * @return \Illuminate\Http\Response
     */
    public function edit(Recipient $recipient)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Recipient  $recipient
     * @return \Illuminate\Http\Response
     */
    public function update(Recipient $recipient, UpdateRecipientRequest $request)
    {
        $data = $request->all();
        $recipient->fill($data);
        
        $this->recipient->save($recipient, $data);
           
        return $this->redirectResponse($request, __('alerts.frontend.recipient.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Recipient  $recipient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Recipient $recipient, Request $request)
    {
        $destroyed = $this->recipient->destroy($recipient);

        return $this->redirectResponse($request, __('alerts.frontend.recipient.deleted'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function batchAction(Request $request)
    {
        $action = $request->get('action');
        $ids = $request->get('ids');
        
        switch ($action) {
            case 'destroy':                
                    $this->recipient->batchDestroy($ids);
                    return $this->redirectResponse($request, __('alerts.frontend.recipient.bulk_destroyed'));
                break;
        }

        return $this->redirectResponse($request, __('alerts.frontend.actions.invalid'), 'error');
    }
}
