<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;
use App\Repositories\Contracts\TagRepository;
use App\Repositories\Contracts\UserRepository;
use Illuminate\Http\Request;
use Auth;
use DB;
use Redirect;

class FrontendController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {

        Route::bind('tag', function ($value) {
            $tags = app(TagRepository::class);

            return $tags->findBySlug($value);
        });

        Route::bind('user', function ($value) {
            $users = app(UserRepository::class);

            return $users->findBySlug($value);
        });

        // $this->request = $request;
    }

    public function userLogout(Request $request)
    {
        // redirect()->route('logout');
        // \Log::info("---------- userLogout -----------");
        // Redirect::away('/logout');
        // return redirect()->away('http://www.google.com');
        // return redirect()->route('logout');
        // return redirect('http://www.google.com');
    }

    public function home()
    {
        // return redirect('admin/login');
        return view('frontend.homestatic');
    }

    public function index()
    {
        /*********** CHECK IF CONFIRMATION/VERIFY TOKEN EXISTS ON LOGIN ************/
        if(session()->has('token')) {
            $session_token = session('token');
            $user_id = auth()->user()->id;
            $user_token = DB::table('users')->where('id', $user_id)->where('token', $session_token)->get();
            if (count($user_token) > 0) {
                $update_data = DB::table('users')->where('id', $user_id)->update(['token' => NULL]);
                if($update_data) {
                   session()->forget('token');
                   return view('frontend.home');
                }
            }
        } else {
            /************** CHECK IF CONFIRMATION PENDING **************/
            $user = auth()->user();
            $confirm_pending = DB::table('users')->where('id', $user->id)->where(function($query){
                $query->whereNotNull('token');
            })->Where('token', '!=' , '')->get();

            // dd($confirm_pending);
            if(count($confirm_pending) > 0) {
                return view('frontend.home')->with('pendingConfirm', true);
            } else {
                return view('frontend.home');
            }
        }
    }

    public function confirmUser($link) {
        /* CHECK IF TOKEN EXISTS */

        $token_check = DB::table('users')->where('token', $link)->get();

        if(count($token_check) > 0) {
            $user = auth()->user();
            if(\Auth::check()) {
                /* CHECK IF TOKEN EXISTS OF SAME USER */
                $user_token = DB::table('users')->where('id', $user->id)->where('token', $link)->get();
                if(count($user_token) > 0) {
                    $update = DB::table('users')->where('id', $user->id)->update(['token' => NULL]);
                    if ($update) {
                        if(session()->has('token')) {
                           session()->forget('token');
                        }
                        return redirect('/front/dashboard');
                    }
                }
            } else {
                if(!session()->has('token')) {
                    session(['token' => $link]);
                }
                return redirect('/login');
            }
        } else {
            abort(404);
        }

    }

    public function confirm()
    {
        // return redirect('admin/login');
        // return view('frontend.confirm');
        return view('frontend.home');
    }

    public function process(Request $request) {
        // print_r($this->ip_info($_SERVER['REMOTE_ADDR'], "Location"));
        return view('frontend.home');
    }
    /**
     * Push attributes in order to correctly localize slugs.
     *
     * @param $attributes
     */
    protected function setLocalesAttributes($attributes)
    {
        View::composer(['partials.alternates', 'partials.locales'], function (\Illuminate\View\View $view) use ($attributes) {
            $view->withAttributes($attributes);
        });
    }

    /**
     * Push translatable object in order to correctly localize slugs.
     *
     * @param $translatable
     */
    protected function setTranslatable($translatable)
    {
        View::composer(['partials.alternates', 'partials.locales'], function (\Illuminate\View\View $view) use ($translatable) {
            $view->withTranslatable($translatable);
        });
    }

    protected function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city"           => @$ipdat->geoplugin_city,
                            "state"          => @$ipdat->geoplugin_regionName,
                            "country"        => @$ipdat->geoplugin_countryName,
                            "country_code"   => @$ipdat->geoplugin_countryCode,
                            "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }

    protected function redirectResponse(Request $request, $message, $type = 'success')
    {
        if ($request->wantsJson()) {
            return response()->json([
                'status'  => $type,
                'message' => $message,
            ]);
        }

        return redirect()->back()->with("flash_{$type}", $message);
    }

}
