<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Referral;
use Illuminate\Http\Request;
use App\Utils\RequestSearchQuery;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StoreRecipientRequest;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\Contracts\ReferralRepository;


class ReferralController extends FrontendController
{
    protected $recipient;

    public function __construct(ReferralRepository $referral)
    {
        $this->referral = $referral;
    }

    public function search(Request $request)
    {
        $query = $this->referral->query();

        $requestSearchQuery = new RequestSearchQuery($request, $query, [
            'referrals.id',
            'referrals.user_id',
            'referrals.created_at',
            'referrals.updated_at',
        ]);

        return $requestSearchQuery->referralResult([
            'referrals.id',
            'referrals.user_id',
            'referrals.created_at',
            'referrals.updated_at',
        ]);
    }
}
