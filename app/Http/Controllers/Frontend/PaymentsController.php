<?php

namespace App\Http\Controllers\Frontend;

use App\Models\Payments;
use Illuminate\Http\Request;
use App\Utils\RequestSearchQuery;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\StorePaymentsRequest;
use App\Http\Requests\UpdatePaymentsRequest;
use Illuminate\Database\Eloquent\Builder;
use App\Repositories\Contracts\PaymentsRepository;
use DB;

class PaymentsController extends FrontendController
{
    /**
     * @var JobcardRepository
     */
    protected $payment;

    public function __construct(PaymentsRepository $payment)
    {
        //dd($jobcards);
        $this->payment = $payment;
    }

    /**
     * Show the application dashboard.
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function search(Request $request)
    {
        ini_set('memory_limit', '-1');

        /** @var Builder $query */
        $query = $this->payment->query();

        $requestSearchQuery = new RequestSearchQuery($request, $query, [
            'approve_status',
            'transaction_id'
        ]);

        if ($request->get('exportData')) {
            return $requestSearchQuery->export([
                'approve_status',
                'payments.created_at',
                'payments.updated_at',
            ],
                [
                    __('validation.payments.approve_status'),
                    __('labels.created_at'),
                    __('labels.updated_at'),
                ],
                'payments');
        }

        return $requestSearchQuery->paymentsHistoryresult([
            'payments.id',
            'payments.transaction_id',
            'payments.user_id',
            'payments.recipient_id',
            'payments.paying_agent_id',
            'payment_details',
            'approve_status',
            'payments.created_at',
            'payments.updated_at',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePaymentsRequest $request)
    {
        //dd($request->all());
        $payment = $this->payment->make($request->all());

       $this->payment->save($payment, $request->input());

       return $this->redirectResponse($request, __('alerts.backend.payments.created'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Payments  $payment
     * @return \Illuminate\Http\Response
     */
    public function show(Payments $payment)
    {
       return $payment;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Payments  $payment
     * @return \Illuminate\Http\Response
     */
    public function edit(Payments $payment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Payments  $payment
     * @return \Illuminate\Http\Response
     */
    public function update(Payments $payment, UpdatePaymentsRequest $request)
    {
        $payment->fill($request->only());

        $this->payment->save($payment, $request->input());

        return $this->redirectResponse($request, __('alerts.backend.payments.updated'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Payments  $payment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Payments $payment, Request $request)
    {
        $this->payment->destroy($payment);

        return $this->redirectResponse($request, __('alerts.backend.payments.deleted'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function batchAction(Request $request)
    {
        $action = $request->get('action');
        $ids = $request->get('ids');

        switch ($action) {
            case 'destroy':
                    $this->payment->batchDestroy($ids);
                    return $this->redirectResponse($request, __('alerts.backend.payments.bulk_destroyed'));
                break;
        }

        return $this->redirectResponse($request, __('alerts.backend.actions.invalid'), 'error');
    }
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array|\Illuminate\Http\RedirectResponse
     */
    public function getUserLatestPaymentTransationID()
    {
        $results = \App\Models\Payments::select('transaction_id')->where('user_id', \Auth::id() )->orderBy('created_at', 'desc')->first();
        // get last insert id from payment table
        if( is_object($results) )
        {
          return response()->json([
              'status' => 200,
              'transaction_id' => (isset($results->transaction_id))?  $results->transaction_id : ''
          ]);
        }
        return "";
    }
}
