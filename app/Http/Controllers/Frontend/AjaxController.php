<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Repositories\Contracts\TagRepository;
use Mcamara\LaravelLocalization\LaravelLocalization;
use App\Http\Requests\StoreRecipientRequest;
use App\Repositories\Contracts\RecipientRepository;
use App\Models\Ledger;
use App\Models\User;
use App\Models\Countries;
use App\Models\Currency;
use App\Models\Fee;
use App\Models\Banks;
use App\Models\Margin;
use App\Models\Payments;
use App\Models\Recipient;
use App\Models\Recharge;
use App\Models\RechargeAccount;
use App\Models\ContactUs;
use App\Models\AdminMessages;
use App\Models\Voucher;
use App\Models\Point;
use App\Models\Referral;
use App\Http\Requests\StoreContactUsRequest;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Auth;
use DB;
use Mail;
use Illuminate\Support\Str;
use App\Events\MessageReceived;
use Validator;
use App\Reply; // use sent messages model
// use App\Http\Controller\FrontendController;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Log;


class AjaxController extends Controller
{
    public $datakey;
    public function __construct(User $user)
    {
        $this->datakey = "W_3743C4EB3C494672B130948092C4CC"; // client given
        // $this->datakey = "W_54D7D281E74444EBA3C1B5025B3A8E"; // development
    }
    /*
    *******************************************************
     // CHECK Localisation of column
    *******************************************************
    */
    private function getLocalizedColumn(Model $model, $column)
    {
        if (property_exists($model, 'translatable') && \in_array($column, $model->translatable, true)) {
            $locale = app()->getLocale();
            return "$column->$locale";
        }
        return $column;
    }
    public function getBankName(Request $request)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $data = $request->all();
        if (isset($data['bankId']) && !empty($data['bankId'])) {
            $bank_id = Banks::where('Bank_ID', $data['bankId'])->select('Bank_Name')->first();
            return response()->json([
                'status' => 200,
                'bank' => $bank_id
            ]);
        }
        return response()->json([
            'status' => 404,
            'data' => 'No Record Found'
        ]);
    }
    public function getLedgerValues(Request $request, Ledger $ledger)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $data = $request->all();
        $pageNumber = $request->current_page;
        $searchables = [
            'description',
            'debit_amount',
            'credit_amount'
        ];
        $model = $ledger->getModel();
        if ($column = $request->get('column')) {
            $ledger->orderBy(
                $this->getLocalizedColumn($model, $column),
                $request->get('direction') ?? 'asc'
            );
        }
        if ($search = $request->get('search')) {
            foreach ($searchables as $key => $searchableColumn) {
                // dd($model);
                $ledger = $ledger->orWhere($searchableColumn, 'like', "%{$search}%");
                // echo $searchableColumn;
            }
        }
        $result = $ledger->where('user_id', $user_id)->select('*')->paginate($request->get('perPage'), $searchables);;
        // $result = str_replace_array('?', $result->getBindings(), $result->toSql());
        // dd($result);
        // dd($result);
        if (count($result) > 0) {
            return response()->json($result, 200);
        } else {
            return response()->json([
                'status' => 404,
                'data' => 'No Ledger For Customer Found'
            ]);
        }
    }

    public function checkUserDocsRejected(User $user_model)
    {
        $user = auth()->user();
        if ($user->doc_verify_json) {
            $doc_verify = json_decode($user->doc_verify_json, true);
            // dd($doc_verify);
            if (isset($doc_verify["verify_Id"]["status"]) && $doc_verify["verify_Id"]["status"] == 'rejected') {
                $type = ucwords(str_replace("_", " ", $doc_verify["verify_Id"]["type"]));
                return response()->json([
                    'status' => 200,
                    'response' => 'Your ' . $type . ' has been rejected. Kindly re-upload to proceed with transaction'
                ]);
            }
            if (isset($doc_verify["verify_face"]["status"]) && $doc_verify["verify_face"]["status"] == 'rejected') {
                $type = ucwords(str_replace("_", " ", $doc_verify["verify_face"]["type"]));
                return response()->json([
                    'status' => 200,
                    'response' => 'Your ' . $type . ' has been rejected. Kindly re-upload to proceed with transaction'
                ]);
            }
            if (isset($doc_verify["verify_doc"]["status"]) && $doc_verify["verify_doc"]["status"] == 'rejected') {
                $type = ucwords(str_replace("_", " ", $doc_verify["verify_doc"]["type"]));
                return response()->json([
                    'status' => 200,
                    'response' => 'Your ' . $type . ' has been rejected. Kindly re-upload to proceed with transaction'
                ]);
            } else {
                return response()->json([
                    'status' => 303,
                    'response' => 'No Rejected Doc'
                ]);
            }
        } else {
            return response()->json([
                'status' => 404,
                'response' => 'No Record Found'
            ]);
        }
    }
    public function checkUserVerify(User $user_model)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $user = $user_model::select('*')->where('id', $user_id)->get()->first();
        $otp = $user->otp;
        // dd($otp);
        if ($otp) {
            /* Not Verify*/
            return response()->json([
                'status' => 500,
                'response' => 'not_verify',
                'user_id' => $user_id
            ]);
        } else {
            /* Already Verify*/
            return response()->json([
                'status' => 200,
                'response' => 'verify',
                'user_id' => $user_id
            ]);
        }
    }
    /*
    *******************************************************
     // CONFIRM OTP OF REGISTER
    *******************************************************
    */
    public function registerOtpConfirm(User $user_model, Request $request)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $otp = $request->otp;
        // dd($otp);
        $user = $user_model::select('*')->where('id', $user_id)->where('otp', $otp)->get()->first();
        // dd($user);
        if ($user) {
            $update_otp = [
                'otp' => NULL
            ];
            if ($user_model::where('id', $user_id)->update($update_otp)) {
                return response()->json([
                    'status' => 200,
                    'response' => 'Successfully Verified'
                ]);
            }
        } else {
            return response()->json([
                'status' => 404,
                'response' => 'Password doesn\'t match. Please Try Again!'
            ]);
        }
    }
    /*
    *******************************************************
     // UPLOAD VERIFY FILES (PASSPORT, LICENSE , ID CARD)
    *******************************************************
    */
    public function uploadFirstVerifyFiles(User $user_model, Request $request)
    {
        //   dd($request->all());
        //   return response()->json([
        //     'status'    => 200,
        //     'response'  => $request->all(),
        //     'php_file' => $_FILES,
        //     'file' => $request->file('passport_image'),
        //     'test' => $request->hasFile('passport_image')
        // ]);
        // dd($request->all());
        //   return response()->json([
        //     'status'    => 200,
        //     'response'  => $request->all()
        // ]);
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $type = $request->type;
        $issuing_authority = $request->issuing_authority;
        $expiry_date = $request->expiry_date;
        $passport_image = isset($request->passport_image) ? $request->passport_image : '';
        $license_image = isset($request->license_image) ? $request->license_image : '';
        $id_card_image = isset($request->IdCard_image) ? $request->IdCard_image : '';
        $face_image = isset($request->face_image) ? $request->face_image : '';
        /* GET USER DATA */
        $userdata = $user_model::where('id', $user_id)->select('doc_verify_json')->get()->first();
        $verify = $userdata->doc_verify_json;
        /* PASSPORT IMAGE */
        if ($type == 'passport' && $passport_image) {
            /* INSERT // push data*/
            $image = $passport_image[0];
            $cleanImageName = $user_model->cleanImageName($passport_image[0]->getClientOriginalName());
            $imageName = rand(0, 10000000) . $cleanImageName;
            $uploaded = $image->move(base_path('/public/images/verify_uploads/'), $imageName);
        }
        if ($type == 'license' && $license_image) {
            /* INSERT LICENSE IMAGE */
            $image = $license_image[0];
            $cleanImageName = $user_model->cleanImageName($license_image[0]->getClientOriginalName());
            $imageName = rand(0, 10000000) . $cleanImageName;
            $uploaded = $image->move(base_path('/public/images/verify_uploads/'), $imageName);
        }
        if ($type == 'id_card' && $id_card_image) {
            $image = $id_card_image[0];
            $cleanImageName = $user_model->cleanImageName($id_card_image[0]->getClientOriginalName());
            $imageName = rand(0, 10000000) . $cleanImageName;
            $uploaded = $image->move(base_path('/public/images/verify_uploads/'), $imageName);
        }
        if ($type == 'id_card' || $type == 'passport' || $type == 'license') {
            if ($verify) {
                $array_to = [
                    'type' => $type,
                    'image_name' => $imageName,
                    'path' => 'images/verify_uploads/',
                    'issuing_authority' => $issuing_authority,
                    'expiry_date' => $expiry_date
                ];
                $get_verify = json_decode($verify, true);
                $get_verify['verify_Id'] = $array_to;
                $get_verify_json = json_encode($get_verify);
                $update_array = [
                    'doc_verify_json'   => $get_verify_json
                ];
                if ($user_model::where('id', $user_id)->update($update_array)) {
                    /* Update Doc Verify Status to 1 */
                    if ($user_model::where('id', $user_id)->update(['doc_verify_status' => 'verify_uploaded'])) {
                        return response()->json([
                            'status'    => 200,
                            'response'  => 'Successfully Uploaded ' . $type . ' Image'
                        ]);
                    }
                } else {
                    return response()->json([
                        'status'    => 400,
                        'response'  => 'Error! Please Try Again!'
                    ]);
                }
            }
        }
        if ($type == 'face' && $face_image) {
            $current_date = Carbon::now();
            $created_at = $current_date->toDateString();
            /* FACE IMAGE */
            $image = $face_image[0];
            $cleanImageName = $user_model->cleanImageName($face_image[0]->getClientOriginalName());
            $imageName = rand(0, 10000000) . $cleanImageName;
            $uploaded = $image->move(base_path('/public/images/verify_uploads/'), $imageName);
            $array_to = [
                'type' => $type,
                'image_name' => $imageName,
                'path' => 'images/verify_uploads/',
                'created_at' => $created_at // hamza
            ];
            $array_to_json = json_encode($array_to);
            if ($verify) {
                /* UPDATE FACE IMAGE */
                $get_verify = json_decode($verify, true);
                $get_verify['verify_face'] = $array_to;
                $get_verify_json = json_encode($get_verify);
                $update_array = [
                    'doc_verify_json'   => $get_verify_json
                ];
                if ($user_model::where('id', $user_id)->update($update_array)) {
                    /* Update Doc Verify Status to 1 */
                    if ($user_model::where('id', $user_id)->update(['doc_verify_status' => 'face_uploaded'])) {
                        return response()->json([
                            'status'    => 200,
                            'response'  => 'Successfully Uploaded ' . $type . ' Image'
                        ]);
                    }
                } else {
                    return response()->json([
                        'status'    => 400,
                        'response'  => 'Error! Please Try Again!'
                    ]);
                }
            } else {
                return response()->json([
                    'status'    => 401,
                    'response'  => 'Unknown Error'
                ]);
            }
        }
        return response()->json([
            'status'    => 409,
            'response'  => 'Nothing to Update'
        ]);
    }
    /*
    *******************************************************
     // UPDATE VERIFY TYPE SELECTED AND COUNTRY SELECT
    *******************************************************
    */
    public function verifyTypeSelected(Request $request, User $user_model)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $type = $request->type;
        $country = $request->country;
        // dd($request->all());
        $userdata = $user_model::where('id', $user_id)->select('doc_verify_json')->get()->first();
        $verify = $userdata->doc_verify_json;
        /* If Verify Json Empty */
        if (!$verify) {
            $array_to = [
                'verify_type' => [
                    'type'  => $type,
                    'country'   => $country
                ]
            ];
            $array_to_json = json_encode($array_to);
            if ($user_model::where('id', $user_id)->update(['doc_verify_json' => $array_to_json])) {
                if ($user_model::where('id', $user_id)->update(['doc_verify_status' => 'verify_type_selected'])) {
                    $user_model::where('id', $user_id)->update(['preferred_country' => $country]);
                    return response()->json([
                        'status'    => 200,
                        'response'  => 'Country added successfuly!'
                    ]);
                } else {
                    return response()->json([
                        'status' => 300
                    ]);
                }
            } else {
                return response()->json([
                    'status' => 300
                ]);
            }
        } else {
            return response()->json([
                'status' => 409,
                'message' => 'Already Submitted',
                'data' => $verify
            ]);
        }
    }
    /*
    **********************************************
     // DOCUMENT TYPE SELECTED BEFORE UPLOAD
    **********************************************
    */
    public function docTypeSelected(Request $request, User $user_model)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $type = $request->type;
        $verify_status = $user_model::where('id', $user_id)->value('doc_verify_status');
        $verify_json = $user_model::where('id', $user_id)->value('doc_verify_json');
        $decode_verify = json_decode($verify_json, true);
        /* UPDATE BILL IMAGE */
        $array_to = [
            'type'  => $type
        ];
        $decode_verify['doc_type'] = $array_to;
        $decode_verify_json = json_encode($decode_verify);
        $update_array = [
            'doc_verify_json'   => $decode_verify_json
        ];
        if ($user_model::where('id', $user_id)->update($update_array)) {
            /* Update Doc Verify Status to 1 */
            if ($user_model::where('id', $user_id)->update(['doc_verify_status' => 'doc_type_selected'])) {
                return response()->json([
                    'status'    => 200,
                    'response'  => 'Document Type Selected Successfully'
                ]);
            }
        } else {
            return response()->json([
                'status'    => 400,
                'response'  => 'Error! Please Try Again!'
            ]);
        }
    }
    /*
    **********************************************
     // UPLOAD DOCUMENT FILES (BILL, BANK STATEMENT)
    **********************************************
    */
    public function uploadDocFiles(Request $request, User $user_model)
    {
        $type = $request->type;
        $bill_image = isset($request->bill_image) ? $request->bill_image : '';
        $bank_statement_image = isset($request->bank_statement_image) ? $request->bank_statement_image : '';
        $additional_doc_image = isset($request->additional_doc_image) ? $request->additional_doc_image : '';
        $issuing_authority = $request->issuing_authority;
        $expiry_date = $request->expiry_date;
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $userdata = $user_model::where('id', $user_id)->select('doc_verify_json')->get()->first();
        $verify = $userdata->doc_verify_json;
        if ($type == 'bill' && $bill_image) {
            /* Bill Image */
            $image = $bill_image[0];
            // $imageName = rand(0,10000000).$image->getClientOriginalName();
            $cleanImageName = $user_model->cleanImageName($image->getClientOriginalName());
            $imageName = rand(0, 10000000) . $cleanImageName;
            $uploaded = $image->move(base_path('/public/images/verify_uploads/'), $imageName);
            $array_to = [
                'type'  => $type,
                'image_name'   => $imageName,
                'path' => 'images/verify_uploads/',
                'issuing_authority' => $issuing_authority,
                'expiry_date' => $expiry_date
            ];
            $get_verify = json_decode($verify, true);
            if ($verify) {
                /* UPDATE BILL IMAGE */
                $get_verify = json_decode($verify, true);
                $get_verify['verify_doc'] = $array_to;
                $get_verify_json = json_encode($get_verify);
                $update_array = [
                    'doc_verify_json'   => $get_verify_json
                ];
                if ($user_model::where('id', $user_id)->update($update_array)) {
                    /* Update Doc Verify Status to 1 */
                    if ($user_model::where('id', $user_id)->update(['doc_verify_status' => 'doc_uploaded'])) {
                        return response()->json([
                            'status'    => 200,
                            'response'  => 'Successfully Uploaded ' . $type . ' Image'
                        ]);
                    }
                } else {
                    return response()->json([
                        'status'    => 400,
                        'response'  => 'Error! Please Try Again!'
                    ]);
                }
            } else {
                /* INSERT BILL IMAGE */ }
        }
        if ($type == 'bank_statement' && $bank_statement_image) {
            /* Bill Image */
            $image = $bank_statement_image[0];
            $cleanImageName = $user_model->cleanImageName($image->getClientOriginalName());
            $imageName = rand(0, 10000000) . $cleanImageName;
            $uploaded = $image->move(base_path('/public/images/verify_uploads/'), $imageName);
            $array_to = [
                'type'  => $type,
                'image_name'   => $imageName,
                'path' => 'images/verify_uploads/',
                'issuing_authority' => $issuing_authority,
                'expiry_date' => $expiry_date
            ];
            $get_verify = json_decode($verify, true);
            if ($verify) {
                /* UPDATE BILL IMAGE */
                $get_verify = json_decode($verify, true);
                $get_verify['verify_doc'] = $array_to;
                $get_verify_json = json_encode($get_verify);
                $update_array = [
                    'doc_verify_json'   => $get_verify_json
                ];
                if ($user_model::where('id', $user_id)->update($update_array)) {
                    /* Update Doc Verify Status to 1 */
                    if ($user_model::where('id', $user_id)->update(['doc_verify_status' => 'doc_uploaded'])) {
                        return response()->json([
                            'status'    => 200,
                            'response'  => 'Successfully Uploaded ' . $type . ' Image'
                        ]);
                    }
                } else {
                    return response()->json([
                        'status'    => 400,
                        'response'  => 'Error! Please Try Again!'
                    ]);
                }
            } else {
                /* INSERT BILL IMAGE */ }
        }
        if ($type == 'additional_doc' && $additional_doc_image) {
            // dd("coming here");
            /* Bill Image */
            $image = $additional_doc_image[0];
            // dd($additional_doc_image);
            $cleanImageName = $user_model->cleanImageName($image->getClientOriginalName());
            $imageName = rand(0, 10000000) . $cleanImageName;
            $uploaded = $image->move(base_path('/public/images/verify_uploads/'), $imageName);
            $array_to = [
                'type'  => $type,
                'image_name'   => $imageName,
                'path' => 'images/verify_uploads/'
            ];
            $get_verify = json_decode($verify, true);
            if ($verify) {
                /* UPDATE BILL IMAGE */
                $get_verify = json_decode($verify, true);
                $get_verify['additional_doc'] = $array_to;
                $get_verify_json = json_encode($get_verify);
                $update_array = [
                    'doc_verify_json'   => $get_verify_json
                ];
                if ($user_model::where('id', $user_id)->update($update_array)) {
                    /* Update Doc Verify Status to 1 */
                    // if($user_model::where('id', $user_id)->update(['doc_verify_status' => 'doc_uploaded'])){
                    return response()->json([
                        'status'    => 200,
                        'response'  => 'Successfully Uploaded ' . $type . ' Image'
                    ]);
                    // }
                } else {
                    return response()->json([
                        'status'    => 400,
                        'response'  => 'Error! Please Try Again!'
                    ]);
                }
            }
        }
    }
    /*
    **************************************************************
     // CHECK USER VERIFICATION STATUS (DOCUMENTS VERIFICATION)
    **************************************************************
    */
    public function userVerifyStatus(User $user_model)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $verify_status = $user_model::where('id', $user_id)->value('doc_verify_status');
        $verify_json = $user_model::where('id', $user_id)->value('doc_verify_json');
        $decode_verify_json = json_decode($verify_json, true);
        $type = 'auto';
        if ($verify_status) {
            if ($verify_status == 'verify_type_selected') {
                $type = $decode_verify_json['verify_type']['type'];
            }
            if ($verify_status == 'verify_uploaded') {
                $type = $decode_verify_json['verify_Id']['type'];
            }
            if ($verify_status == 'face_uploaded') {
                $type = $decode_verify_json['verify_face']['type'];
            }
            if ($verify_status == 'doc_type_selected') {
                $type = $decode_verify_json['doc_type']['type'];
            }
            return response()->json([
                'status' => 200,
                'verify_type' => $type,
                'verify_status' => $verify_status
            ]);
        }
    }
    /*
    **************************************************************
     // GET USER PREFFERED COUNTRY
    **************************************************************
    */
    // public function getUserCountry(User $user_model, Countries $country) {
    //     $user_id = auth()->user() ? auth()->user()->id : $request->id;
    //     $country = $user_model::select('*')->where('id',$user_id)->get()->first();
    //     return response()->json([
    //         'status'        => 200,
    //         'country_id'    => $country->country,
    //         // 'country_id'    => $country->preferred_country,
    //         // 'country_code'  => $country->country->code,
    //         // 'country_name'  => $country->country->country
    //     ]);
    // }
    /*
    **************************************************************
     // GET USER SEND COUNTRY
    **************************************************************
    */
    public function getUserSendCountry(User $user_model, Countries $country)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $country = $user_model::select('*')->where('id', $user_id)->get()->first();
        return response()->json([
            'status'        => 200,
            'country_id'    => $country->send_amount_country,
            // 'country_code'  => $country->country->code,
            // 'country_name'  => $country->country->country
        ]);
    }
    /*
    **************************************************************
     // GET COUNTRY CURRENCY
    **************************************************************
    */
    public function getCountryCurrency(Request $request, Countries $country, Currency $currency, Fee $fee)
    {
        //dd($country);
        $convert = isset($request->convert) ? $request->convert : '';
        $type = $request->type;
        if ($convert) {
            $send_code = $request->send_country_code;
            $receive_code = $request->receive_country_code;
        } else {
            $country_id = $request->country_id;
            //dd($country_id);
            $get_country = $country::where('id', $country_id)->select('*')->get()->first();
            // dd($get_country['code'], $country_id);
            $country_code = $get_country['code'];
            //dd($country_code);
            $currency_code = $currency::where('Currency_Country', 'LIKE', '%' . $country_code . '%')->value('Currency_Code');
            $currency_code = isset($currency_code) ? $currency_code : 'not_available';
            // $get_fee = $fee::where('MIN', '<=', 9025)->where('MAX', '>=', 9025)->where('country','NP')->select('*')->get();
            if ($type == 'sendTo') {
                return response()->json([
                    'status'  => 200,
                    'sendTo' => [
                        'currency_code' => $currency_code,
                        'country_code'  => $country_code
                    ]
                ]);
            }
            if ($type == 'sendFrom') {
                return response()->json([
                    'status'  => 200,
                    'sendFrom' => [
                        'currency_code' => $currency_code,
                        'country_code'  => $country_code
                    ]
                ]);
            }
        }
        // dd($request->all());
    }
    /*
    **************************************************************
     // GET COUNTRY FEE
    **************************************************************
    */
    public function getCountryFee(Request $request, Fee $fee, Countries $country, Margin $margin)
    {
        $amount = $request->amount;
        $country_id = $request->send_to_country;
        $select_type = $request->type;
        // \Log::info("request data ");
        // \Log::info("request ammount " . $amount);
        // \Log::info("request country_id " . $country_id);
        // \Log::info("request select_type " . $select_type);
        $country_data = $country::find($country_id);
        $country_code = $country_data->code;
        // \Log::info("query data");
        // \Log::info("request country_data :" . $country_data);
        // \Log::info("request country_code :" . $country_code);
        //dd($amount,  $country_id,  $country_data,  $country_code);
        if ($country_code) {
            // \Log::info("inside country_code");
            $get_fee = $fee::where('MIN', '<=', $amount)->where('MAX', '>=', $amount)->where('Country', $country_data->code)->value('FEE');
            //dd($get_fee);
            if ($get_fee) {
                // \Log::info("inside get_fee");
                $get_margin = $margin::where('type_value', $select_type)->value('fee_margin');
                $result = $get_fee / 100;
                $marginpercentage = $result * $get_margin;
                $calculatefee = $get_fee + $marginpercentage;
                return response()->json([
                    'status' => 200,
                    'fee'   => $calculatefee
                ]);
            } else {
                return response()->json([
                    'status' => 404,
                    'fee' => 0
                ]);
            }
        }
    }
    /*
    **************************************************************
     // CALCULATE AMOUNT / CONVERT AMOUNT
    **************************************************************
    */
    public function getAmount(Request $request, Currency $currency, Margin $margin)
    {
        $amount = $request->amount;
        $send_to = $request->send_to;
        $send_from = $request->send_from;
        $select_type = $request->type;
        $send_from_exchange_rate = $currency::where('Currency_code', $send_from)->value('Exchange_Rate');
        $send_to_exchange_rate = $currency::where('Currency_code', $send_to)->value('Exchange_Rate');
        // \Log::info("send_from_exchange_rate: " . $send_from_exchange_rate);
        // \Log::info("send_to_exchange_rate: " . $send_to_exchange_rate);
        if ($send_from_exchange_rate == $send_to_exchange_rate) {
            $net_amount = $amount;
            $rate = 1;
        } else {
            $rate = number_format($send_to_exchange_rate / $send_from_exchange_rate, 2);
            // $net_amount = number_format($amount * $exchange_rate, 2);
            // dd($rate);
            $get_marginRate = $margin::where('type_value', $select_type)->value('rate_margin');
            // dd($get_marginRate);
            $rate = $rate + $get_marginRate;
            $net_amount = number_format($amount * $rate, 2);
        }
        return response()->json([
            'status'    => 200,
            'net_amount' => $net_amount,
            'exchange_rate' => $rate
        ]);
    }
    /*
    **************************************************************
     // SAVE USER PREFERRED COUNTRY
    **************************************************************
    */
    public function saveUserCountry(Request $request, User $user)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $country_id = $request->country_id;
        $update_arr = [
            'preferred_country' => $country_id
        ];
        $save = $user::where('id', $user_id)->update($update_arr);
        if ($save) {
            return response()->json([
                'status' => 200,
                'message' => 'country updated Successfully'
            ]);
        } else {
            return response()->json([
                'status' => 403,
                'message' => 'Error, Please Try again!'
            ]);
        }
    }
    /*
    **************************************************************
     // SAVE USER SEND TO PREFERRED COUNTRY
    **************************************************************
    */
    public function saveUserSendCountry(Request $request, User $user)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $country_id = $request->country_id;
        $update_arr = [
            'send_amount_country' => $country_id
        ];
        $save = $user::where('id', $user_id)->update($update_arr);
        if ($save) {
            return response()->json([
                'status' => 200,
                'message' => 'country updated Successfully'
            ]);
        } else {
            return response()->json([
                'status' => 403,
                'message' => 'Error, Please Try again!'
            ]);
        }
    }
    /*
    **************************************************************
     // SAVE PAYMENT DETAILS
    **************************************************************
    */
    public function deletePaymentDetails(Request $request, Payments $payment)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }

        if (isset($request->data) && !empty($request->data)) {
            $row = $payment::find($request->data)->where('status', '!=', 'paid');
            $row->delete();
            return response()->json([
                'status' => 200,
                'response' => 'Payment details deleted successfully!',
            ]);
        }
    }
    public function savePaymentDetails(Request $request, Countries $country, Payments $payment)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $data = $request->data;
        $promocode = (isset($request->promocode) && !empty($request->promocode)) ? $request->promocode : null;
        $sendFromCountry = $data['sendFromCountry'];
        $sendToCountry = $data['sendToCountry'];
        // $get_transaction_id = $payment::orderBy('created_at', 'desc')->skip(1)->take(1)->value('transaction_id');
        // $get_transaction_id = $payment::orderBy('created_at', 'desc')->value('transaction_id');
        $get_transaction_id = $payment::max('transaction_id');
        if (!$get_transaction_id) {
            $transaction_id = 800000001;
        } else {
            $transaction_id = $get_transaction_id + 1;
        }
        // dd($get_transaction_id);
        /* Get Names of Countries */
        $sendCountry = $country::where('id', $sendFromCountry)->select('*')->get()->first();
        $getCountry = $country::where('id', $sendToCountry)->select('*')->get()->first();
        $data['sendFromCountry'] = json_decode(json_encode($sendCountry), true);
        $data['sendToCountry'] = json_decode(json_encode($getCountry), true);
        $json_data = json_encode($data);
        /*
          $record = $payment::where('transaction_id', '=', $transaction_id)->first();
          if ($record === null) {
             // insert transaction
          } else {
            $transaction_id = $transaction_id + 1;
          }
        */
        $payment->user_id = $user_id;
        $payment->payment_details = $json_data;
        $payment->transaction_id = $transaction_id;
        $payment->status = 'recipient';
        $save = $payment::updateOrCreate(
            ['user_id' => $user_id, 'status' => 'recipient', 'promocode' => $promocode],
            ['payment_details' => $json_data, 'transaction_id' => $transaction_id]
        );
        if ($save) {
            return response()->json([
                'status' => 200,
                'response' => 'Payment details added successfully!',
                't_id' => (isset($save->id) && !empty($save->id) && is_object($save)) ? $save->id : ''
            ]);
        }
    }
    /*
    **************************************************************
     // GET USER INDEX PAYMENT DETAILS
    **************************************************************
    */
    public function getIndexPaymentDetails(Request $request, Payments $payment)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $status = isset($request->status) ? $request->status : '';
        if ($status) {
            if ($status == 'recipient-payment') {
                $record = $payment::where('user_id', $user_id)->where('status', '!=', 'paid')->orderBy('created_at', 'desc')->first();
            }
            if ($record) {
                $details = json_decode($record->payment_details, true);
                return response()->json([
                    'status' => 200,
                    'payment_details' => $details,
                    'user_id'   => $record->user_id,
                    'id'    => $record->id
                ]);
            } else {
                return response()->json([
                    'status' => 400
                ]);
            }
        }
    }
    /*
    **************************************************************
     // GET USER PAYMENT DETAILS
    **************************************************************
    */
    public function getPaymentDetails(Request $request, Payments $payment)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $status = isset($request->status) ? $request->status : '';
        if ($status) {
            $record = $payment::where('user_id', $user_id)->where('status', $status)->orderBy('created_at', 'desc')->first();
            if ($record) {
                $details = json_decode($record->payment_details, true);
                $details['payment_id'] = $record->id;
                return response()->json([
                    'status' => 200,
                    'payment_details' => $details,
                    'user_id'   => $record->user_id,
                    'id'    => $record->id,
                    'transaction_id' => $record->transaction_id,
                ]);
            } else {
                return response()->json([
                    'status' => 404,
                    'response' => 'No Payments Found'
                ]);
            }
        }
    }
    /*
    **************************************************************
     // ADD RECIPIENT
    **************************************************************
    */
    public function addRecipient(Recipient $recip_model, RecipientRepository $recip_rep, StoreRecipientRequest $request, Payments $payment)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $recipient = $request->all();
        $choose_recipient = $recipient['chooseRecipient'];
        $payment_id = $recipient['payment_id'];
        unset($recipient['chooseRecipient']);
        unset($recipient['payment_id']);
        // $data = $request->data;
        // $choose_recipient = $data['chooseRecipient'];
        // $recipient = $data['recipient'];
        // dd($recipient);
        if ($choose_recipient == 'new') {
            $recipient['user_id'] = $user_id;
            $make = $recip_rep->make($recipient);
            $save = $recip_rep->save($make, $recipient);
            if ($save) {
                /* Change Payment Status */
                $this->changePaymentDetailStatus('payment', $make->id, $payment_id);
                return response()->json([
                    'status' => 200,
                    'response' => 'Recipient Added Successfully!',
                    'id' => $make->id
                ]);
            }
        } else {
            $update = $recip_model::where('id', $choose_recipient)->update($recipient);
            if ($update) {
                $this->changePaymentDetailStatus('payment', $choose_recipient, $payment_id);
                return response()->json([
                    'status' => 200,
                    'response' => 'Recipient Added Successfully!',
                    'id' => $choose_recipient
                ]);
            }
        }
    }
    /*
    **************************************************************
     // PROTECTED FUNCTION TO CHANGE PAYMENT STATUS
    **************************************************************
    */
    protected function changePaymentDetailStatus($status, $recipient_id, $payment_id)
    {
        // $user_id = auth()->user()->id;
        $payment = new Payments;
        $update_arr = [
            'status' => $status,
            'recipient_id' => $recipient_id
        ];
        $update = $payment::where('id', $payment_id)->update($update_arr);
    }
    /*
    **************************************************************
     //  CHANGE PAYMENT STATUS
    **************************************************************
    */
    public function changePaymentStatus(Request $request, Payments $payment)
    {
        // dd($request->all());
        $status = $request->status;
        // $recipient_id = $request->recipient_id;
        $payment_id = $request->payment_id;
        $update_arr = [
            'status' => $status,
            'recipient_id' => NULL
        ];
        $update = $payment::where('id', $payment_id)->update($update_arr);
        if ($update) {
            return response()->json([
                'status' => 200,
                'response' => 'Successfully status changed'
            ]);
        }
    }

    public function updateUserPoints(Request $request,  User $user_model, Ledger $ledger)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $data = $request->all();

        //   dd($data);

        $points = isset($data['points']) ? $data['points'] : 0;

        // TODO: Update points
        $insert_points = array(
            'user_id' => $user_id,
            'type' => 'used',
            'points' => "-".$points,
            'date' => date('Y-m-d'),
        );

        Point::insert($insert_points);

        // calculate points.
        $to_pay_amount = (0.001 * $points);

        // get balance
        $balance = $user_model::where('id', $user_id)->value('balance');
        $current_balance = $balance - $to_pay_amount;
        /******* Deduct Balance ************/
        $update_bal = $user_model::where('id', $user_id)->update(['balance' => $current_balance]);
        if ($update_bal) {
            /******* ADD INTO LEDGER ************/
            $update_arr = [
                'description' => "Balance deducted points used - $points",
                'credit_amount' => $to_pay_amount,
                'user_id' => $user_id,
                'balance' => $current_balance
            ];
            $ledger_create = $ledger::insert($update_arr);
            return response()->json([
                'status'    => 200,
                'balance'   => 'available'
            ]);
        }


        return response()->json([
            'status'    => 200
        ]);
    }

    // check points
    protected function checkPoints_()
    {
        $user_id = auth()->user()->id;
        $count = Point::where('user_id', $user_id)->sum('points');

        $points = false;
        if ($count >= 12) {
            return true;
        }

        return false;
    }
    /*
    **************************************************************
     //  CHECK USER BALANCE
    **************************************************************
    */
    public function checkUserBalance(Request $request, User $user_model, Payments $payment, Ledger $ledger)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }

        // $this->checkPoints_()
        // check transaction limit of 12
        if ($request->pointsUsed == "yes") {
            // Update fee and totaltopay in payment.
            $record = $payment::where('id', '=', $request->payment_id)->select('payment_details')->first();
            $payment_details = json_decode($record->payment_details, true);

            // calculate fee and total to pay.
            $sendAmount_update = $payment_details['calculate']['sendAmount'];
            $totalFee_update   = ($payment_details['totalFee'] - 1.00); // subtract from fee.
            $totalToPay_update = ($sendAmount_update + $totalFee_update);

            // update fee and total to pay.
            $payment::where('id', $request->payment_id)->update(['payment_details->totalFee' => "$totalFee_update"]); // update fee.
            $payment::where('id', $request->payment_id)->update(['payment_details->calculate->totalFee' => "$totalFee_update"]); // update fee.
            $payment::where('id', $request->payment_id)->update(['payment_details->calculate->totalToPay' => "$totalToPay_update"]); // update fee.

            // Log::info('payment_json : ' . $sendAmount_calculate_update);
            // Log::info('payment_json : ' . $totalFee_calculate_update);
            // Log::info('payment_json : ' . $totalToPay_calculate_update);
            // Log::info('payment_json : ' . print_r($payment_details, true));

            // add discount.
            $payment::where('id', $request->payment_id)->update(['payment_details->calculate->discount' => "1.00"]); // update discount.
        } else {
            // add discount empty column.
            $payment::where('id', $request->payment_id)->update(['payment_details->calculate->discount' => "0.00"]); // update discount.
        }


        $transaction_id = $payment::orderBy('created_at', 'desc')->skip(1)->take(1)->value('transaction_id');
        $transaction_id = $transaction_id + 1;
        //dd($user_id);
        $to_pay_amount = (isset($totalToPay_update) && !empty($totalToPay_update)) ? $totalToPay_update : $request->to_pay_amount;
        $balance = $user_model::where('id', $user_id)->value('balance');
        if ($balance >= $to_pay_amount) {
            $current_balance = $balance - $to_pay_amount;
            /******* Deduct Balance ************/
            $update_bal = $user_model::where('id', $user_id)->update(['balance' => $current_balance]);
            $payment::where('id', $request->payment_id)->update(['transaction_id' => $transaction_id, 'status' => 'paid']);
            // dd($to_pay_amount, $balance, 'yes you can pay');
            if ($update_bal) {
                /******* ADD INTO LEDGER ************/
                $update_arr = [
                    'description' => "Balance deducted - $transaction_id",
                    'credit_amount' => $to_pay_amount,
                    'user_id' => $user_id,
                    'balance' => $current_balance
                ];
                $ledger_create = $ledger::insert($update_arr);

                // check first transaction.
                if ( $this->checkFirstTransaction($user_id) )
                {
                    $this->giveRewards();
                }

                return response()->json([
                    'status'    => 200,
                    'balance'   => 'available'
                ]);
            }

        } else {
            // dd($to_pay_amount, $balance, 'no you are not allowed to pay');
            return response()->json([
                'status' => 403,
                'balance' => 'not_available',
                'response' => 'You do not have enough balance to proceed. Please update your balance'
            ]);
        }
        // dd($request->all());
    }
    protected function checkFirstTransaction($user_id='')
    {
        $result = Payments::where('user_id', '=', $user_id)->where('status', 'paid')->count();
        if ( $result == 1 ) {
            return true;
        }
        return false;
    }
    protected function giveRewards()
    {
        \Log::info("-- On first transactino give reward - giveReward --");
        $user_id = auth()->user()->id;
        $parent_ids = Referral::select('parent_id')->where('user_id', "=", $user_id)->get();
        // \Log::info("Parents : " . print_r($parent_ids, true));

        $counter = 1;
        if ($parent_ids !== null)
        {
            foreach ($parent_ids as $parentId)
            {                
                // \Log::info('Index 0 the first parent '. $parentId->parent_id);
                if ( $counter == 1 ) {
                    $this->addRewardPoint($parentId, 10);  // first parent 10 points.
                }else {
                    $this->addRewardPoint($parentId, 1); // all other
                }                
                $counter++;
            }            
        }    
    }
    protected function addRewardPoint($parentId = 0, $points = 0)
    {
        $add_points = array(
                        'user_id' => $parentId->parent_id,
                        'points' => $points,
                        'type' => 'First Transaction Reward',
                        'date' => date('Y-m-d'),
                        'transaction_id' => null
                    );
        Point::insert($add_points);
    }
    /*
    **************************************************************
     //  GET USER LOCATION FROM IP , STATIC NOW i.e 103.255.6.77
    **************************************************************
    */
    //     function getUserIpAddr(){
    //     if(!empty($_SERVER['HTTP_CLIENT_IP'])){
    //         //ip from share internet
    //         $ip = $_SERVER['HTTP_CLIENT_IP'];
    //     }elseif(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
    //         //ip pass from proxy
    //         $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    //     }else{
    //         $ip = $_SERVER['REMOTE_ADDR'];
    //     }
    //     return $ip;
    // }
    public function getUserLocation(Request $request, Countries $country)
    {
        // $ips = Request::getClientIp();
        // dd($ips);
        $ip = $request->ip();
        // echo 'User Real IP - '. $this->getUserIpAddr();
        // dd($ip);
        // $location = $this->ip_info("103.255.6.77", "Location");
        $location = $this->ip_info($ip, "Location");
        // dd($location);
        if ($location) {
            $country_id = $country::where('country', $location['country'])->value('id');
            // dd($country_id);
            return response()->json([
                'status' => 200,
                'country' => $country_id
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'response' => 'Not Found'
            ]);
        }
    }
    /*
    **************************************************************
     //  GET USER LOCATION FROM IP , STATIC NOW i.e 103.255.6.77
    **************************************************************
    */
    public function getUserCountry(Request $request, Countries $country, User $user)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $countries = $user::where('id', $user_id)->value('country_id');
        //dd($countries);
        if ($countries) {
            //$country_id = $country::where('id', $countries)->value('country');
            //dd($country_id);
            return response()->json([
                'status' => 200,
                'country' => $countries
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'response' => 'Not Found'
            ]);
        }
    }
    /*
    **************************************************************
     //  FUNCTION TO GET COUNTRY FROM IP
    **************************************************************
    */
    protected function ip_info($ip = NULL, $purpose = "location", $deep_detect = TRUE)
    {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );
        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                        $output = array(
                            "city"           => @$ipdat->geoplugin_city,
                            "state"          => @$ipdat->geoplugin_regionName,
                            "country"        => @$ipdat->geoplugin_countryName,
                            "country_code"   => @$ipdat->geoplugin_countryCode,
                            "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                            "continent_code" => @$ipdat->geoplugin_continentCode
                        );
                        break;
                    case "address":
                        $address = array($ipdat->geoplugin_countryName);
                        if (@strlen($ipdat->geoplugin_regionName) >= 1)
                            $address[] = $ipdat->geoplugin_regionName;
                        if (@strlen($ipdat->geoplugin_city) >= 1)
                            $address[] = $ipdat->geoplugin_city;
                        $output = implode(", ", array_reverse($address));
                        break;
                    case "city":
                        $output = @$ipdat->geoplugin_city;
                        break;
                    case "state":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "region":
                        $output = @$ipdat->geoplugin_regionName;
                        break;
                    case "country":
                        $output = @$ipdat->geoplugin_countryName;
                        break;
                    case "countrycode":
                        $output = @$ipdat->geoplugin_countryCode;
                        break;
                }
            }
        }
        return $output;
    }
    public function backFromVerifyType(Request $request, User $user_model)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $update = $user_model::where('id', $user_id)->update(['doc_verify_status' => 'none', 'doc_verify_json' => NULL]);
        if ($update) {
            return response()->json([
                'status' => 200,
                'response' => 'success'
            ]);
        }
    }
    public function rechargeRequest(Request $request, Recharge $recharge)
    {
        $data = $request->all();
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $details = $request->transaction_details;
        $amount = $request->amount;
        $flag_upload = false;
        if (isset($request->image)) {
            $image = $request->image[0];
            // $imageName = rand(0,10000000).$image->getClientOriginalName();
            $cleanImageName = $recharge->cleanImageName($image->getClientOriginalName());
            $imageName = rand(0, 10000000) . $cleanImageName;
            $recharge->image = $imageName;
            $flag_upload = true;
        }
        /* data to save */
        $recharge->details = $details;
        $recharge->user_id = $user_id;
        $recharge->amount = $amount;
        if ($recharge->save()) {
            if ($flag_upload) {
                $uploaded = $image->move(base_path('/public/images/verify_uploads/'), $imageName);
            }
            return response()->json([
                'status' => 200,
                'response' => 'Balance Request Successfuly sent for approval'
            ]);
        } else {
            return response()->json([
                'status' => 500,
                'response' => 'Something went wrong, Please try again'
            ]);
        }
        // dd($imageName);
    }

    public function getUserRef()
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        if (!isset($user_id)) {
            return response()->json([
                'status' => 403,
                'response' => 'Not Allowed'
            ]);
        }
        return response()->json([
            'status' => 200,
            'response' => auth()->user()->ref_key
        ]);
    }
    public function getUserBalance(User $user, Request $request)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        if (!isset($user_id)) {
            return response()->json([
                'status' => 403,
                'response' => 'Not Allowed'
            ]);
        }
        $balance = $user::where('id', $user_id)->value('balance');
        // $articles = DB::table('articles')
        //     ->select('articles.id as articles_id',)
        //     ->join('categories', 'articles.categories_id', '=', 'categories.id')
        //     ->join('users', 'articles.user_id', '=', 'user.id')
        //     ->get();
        $currency_user = $user::where('users.id', $user_id)
            ->join('countrycodes', 'users.country_id', '=', 'countrycodes.id')
            ->join('currency', 'countrycodes.code', '=', 'currency.Currency_Country')
            ->select('currency.*')->get();
        $currency = '';
        if (count($currency_user) > 0) {
            $currency = $currency_user[0]->Currency_Code;
        }
        if ($balance) {
            return response()->json([
                'status' => 200,
                'balance' => $currency . ' ' . $balance
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'response' => 'Not Found'
            ]);
        }
    }
    public function checkbalance(User $user)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        //dd($user_id);
        $balance = $user::where('id', $user_id)->value('balance');
        //dd($balance, $user_id);
        if ($balance) {
            return response()->json([
                'status' => 200,
                'balance' => $balance
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'response' => 'Not Found'
            ]);
        }
    }
    public function getRecipientsList(Request $request, User $user, Recipient $recipient)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $country = $request->country;
        $data = $recipient::where('user_id', $user_id)->where('country', $country)
            ->select(DB::raw("CONCAT(first_name, ' ' , last_name) AS text, id AS value"))
            ->get('text', 'value');
        if ($data) {
            return response()->json([
                'status' => 200,
                'recipient' => $data
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'message' => 'Not Found'
            ]);
        }
    }
    /*
    **************************************************************
     // ADD USER POINTS
    **************************************************************
    */
    public function addPoints(Request $request, Point $points)
    {
        $data = $request->all();
        $transaction_id = isset($data['paymentId']) ? $data['paymentId'] : null;
        $pointsUsed = isset($data['pointsUsed']) ? $data['pointsUsed'] : null;
        $user = auth()->user();
        $user_id = $user->id;

        Log::info("Points Used" . $pointsUsed);

        // total points.
        $count = $points::where('user_id', $user_id)->sum('points');

        if ($count >= 12) {
            if ($pointsUsed == "yes") {

                $record = [
                    'user_id' => $user_id,
                    'points' => -12,
                    'type' => 'used',
                    'date' => date('Y-m-d'),
                    'transaction_id' => $transaction_id
                ];
            } else {
                $record = [
                    'user_id' => $user_id,
                    'points' => 1,
                    'type' => 'award',
                    'date' => date('Y-m-d'),
                    'transaction_id' => $transaction_id,
                ];
            }
        } else {
            $record = [
                'user_id' => $user_id,
                'points' => 1,
                'type' => 'award',
                'date' => date('Y-m-d'),
                'transaction_id' => $transaction_id,
            ];
        }

        $point_add = $points::insert($record);

        if ($point_add) {
            return response()->json([
                'status' => 200,
                'response' => 'Point added successfully.'
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'response' => 'Something went wrong.'
            ]);
        }
    }
    /*
    **************************************************************
     // CHECK USER POINTS
    **************************************************************
    */
    public function checkPoints(Request $request, Point $points, Payments $payment)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }

        // total points.
        $count = $points::where('user_id', $user_id)->sum('points');

        $points = false;
        if ($count >= 12) {
            $points = true;
        }

        if ($points) {
            return response()->json([
                'status' => 200,
                'maxLimitIncreased' => true
            ]);
        } else {
            return response()->json([
                'status' => 200,
                'maxLimitIncreased' => false
            ]);
        }
    }
    /*
    **************************************************************
     // TOTAL USER POINTS TO DISPLAY ON DASHBOARD
    **************************************************************
    */
    public function getTotalPoints(Point $points)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }

        // total points.
        $total_points = $points::where('user_id', $user_id)->sum('points');

        return response()->json([
            'status' => 200,
            'total_points' => $total_points
        ]);
    }
    /*
    **************************************************************
     // GET USER RECHARGE DETAILS
    **************************************************************
    */
    public function getRequestRecharge(Request $request, Recharge $recharge)
    {
        $total_amount = $request->total_amount;
        $user = auth()->user();
        $balance = $user->balance;
        $user_id = $user->id;
        $record = $recharge::where('user_id', $user_id)->where('status', 'pending')->value('amount');
        if ($record) {
            if ($balance == 0 || $total_amount > $balance) {
                return response()->json([
                    'status' => 200,
                    'requested_balance' => $record,
                    'response' => 'Please wait, requested balance pending for approval.'
                ]);
            } else {
                return response()->json([
                    'status' => 404,
                    'response' => 'No Pending Requests for Recharge'
                ]);
            }
        }
    }
    /*
    **************************************************************
     // GET USER RECHARGE DETAILS
    **************************************************************
    */
    public function getUserRequestRecharge(Request $request, Recharge $recharge)
    {
        $user = auth()->user();
        $user_id = $user->id;
        $currency_user = $user::where('users.id', $user_id)
            ->join('countrycodes', 'users.country_id', '=', 'countrycodes.id')
            ->join('currency', 'countrycodes.code', '=', 'currency.Currency_Country')
            ->select('currency.*')->get();
        $currency = '';
        if (count($currency_user) > 0) {
            $currency = $currency_user[0]->Currency_Code;
        }
        $record = $recharge::where('user_id', $user_id)->where('status', 'pending')->select('amount', 'created_at')->get();
        if ($record) {
            return response()->json([
                'status' => 200,
                'requested_balance' => $record,
                'currency'  => $currency
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'response' => 'No Pending Requests for Recharge'
            ]);
        }
    }
    /*
    **************************************************************
     // GET RECIPIENT BY ID
    **************************************************************
    */
    public function getRecipientById(Request $request, Recipient $recipient)
    {
        $id = $request->id;
        $record = $recipient::where('id', $id)->get()->first();
        if ($record) {
            return response()->json([
                'status' => 200,
                'response' => $record,
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'response' => 'No Recipient Found'
            ]);
        }
    }
    /*
    **************************************************************
     // GET USER VERIFY IDENTITY UPLOADS
    **************************************************************
    */
    public function getIdentityUploads(Request $request, User $user)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $doc_verify = $user::where('id', $user_id)->value('doc_verify_json');
        if ($doc_verify) {
            $doc_verify = json_decode($doc_verify, true);
            $identity_image = '/' . $doc_verify['verify_Id']['path'] . '' . $doc_verify['verify_Id']['image_name'];
            $face_image = '/' . $doc_verify['verify_face']['path'] . '' . $doc_verify['verify_face']['image_name'];
            $doc_image = '/' . $doc_verify['verify_doc']['path'] . '' . $doc_verify['verify_doc']['image_name'];
            if (isset($doc_verify['additional_doc'])) {
                $doc_additional_image = '/' . $doc_verify['additional_doc']['path'] . '' . $doc_verify['additional_doc']['image_name'];
            } else {
                $doc_additional_image = null;
            }
            return response()->json([
                'status' => 200,
                'doc_verify' => $doc_verify,
                'identity_image' => $identity_image,
                'face_image' => $face_image,
                'doc_image' => $doc_image,
                'doc_additional_image' => $doc_additional_image
            ]);
        } else {
            return response()->json([
                'status' => 404,
                'response' => 'Not Found Any Images'
            ]);
        }
    }
    /*
    **************************************************************
     // UPDATE USER VERIFY IMAGE
    **************************************************************
    */
    public function updateVerifyImage(Request $request, User $user)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        $issuing_authority = $request->issuing_authority;
        $expiry_date = $request->expiry_date;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $verify_type = $request->verify_type;
        $image = $request->identity_image;
        $doc_verify = $user::where('id', $user_id)->value('doc_verify_json');
        if ($doc_verify) {
            /******* REMOVE OLD IMAGE ***********/
            $doc_verify = json_decode($doc_verify, true);
            $old_image = '/' . $doc_verify['verify_Id']['path'] . '' . $doc_verify['verify_Id']['image_name'];
            $oldfilename  = public_path($old_image);
            if (file_exists($oldfilename)) {
                unlink($oldfilename);
            }
            /******* UPDATE NEW IMAGE ***********/
            $new_image = $image[0];
            // $imageName = rand(0,10000000).$image[0]->getClientOriginalName();
            $cleanImageName = $user->cleanImageName($new_image->getClientOriginalName());
            $imageName = rand(0, 10000000) . $cleanImageName;
            $doc_verify['verify_Id']['image_name'] = $imageName;
            $doc_verify['verify_Id']['expiry_date'] = $expiry_date;
            $doc_verify['verify_Id']['issuing_authority'] = $issuing_authority;
            $doc_verify['verify_Id']['status'] = 'pending';
            $doc_verify = json_encode($doc_verify);
            $update_db = $user::where('id', $user_id)->update(['doc_verify_json' => $doc_verify]);
            if ($update_db) {
                /********* MOVE NEW IMAGE **********/
                $uploaded = $new_image->move(public_path('images/verify_uploads/'), $imageName);
                return response()->json([
                    'status' => 200,
                    'response' => 'Image Updated Successfully'
                ]);
            } else {
                return response()->json([
                    'status' => 300,
                    'response' => 'Something Went Wrong! New Image not updating'
                ]);
            }
        }
    }
    /*
    **************************************************************
     // UPDATE USER FACE IMAGE
    **************************************************************
    */
    public function updateFaceImage(Request $request, User $user)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        // $issuing_authority = $request->issuing_authority;
        // $expiry_date = $request->expiry_date;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $verify_type = $request->verify_type;
        $image = $request->face_image;
        $doc_verify = $user::where('id', $user_id)->value('doc_verify_json');
        if ($doc_verify) {
            /******* REMOVE OLD IMAGE ***********/
            $doc_verify = json_decode($doc_verify, true);
            $old_image = '/' . $doc_verify['verify_face']['path'] . '' . $doc_verify['verify_face']['image_name'];
            $oldfilename  = public_path($old_image);
            if (file_exists($oldfilename)) {
                unlink($oldfilename);
            }
            /******* UPDATE NEW IMAGE ***********/
            $new_image = $image[0];
            // $imageName = rand(0,10000000).$image[0]->getClientOriginalName();
            $cleanImageName = $user->cleanImageName($new_image->getClientOriginalName());
            $imageName = rand(0, 10000000) . $cleanImageName;
            $doc_verify['verify_face']['image_name'] = $imageName;
            // $doc_verify['verify_face']['expiry_date'] = $expiry_date;
            // $doc_verify['verify_face']['issuing_authority'] = $issuing_authority;
            $doc_verify['verify_face']['status'] = 'pending';
            $doc_verify = json_encode($doc_verify);
            $update_db = $user::where('id', $user_id)->update(['doc_verify_json' => $doc_verify]);
            if ($update_db) {
                /********* MOVE NEW IMAGE **********/
                $uploaded = $new_image->move(public_path('images/verify_uploads/'), $imageName);
                return response()->json([
                    'status' => 200,
                    'response' => 'Image Updated Successfully'
                ]);
            } else {
                return response()->json([
                    'status' => 300,
                    'response' => 'Something Went Wrong! New Image not updating'
                ]);
            }
        }
    }
    /*
    **************************************************************
     // UPDATE USER DOCUMENT IMAGE
    **************************************************************
    */
    public function updateDocImage(Request $request, User $user)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        $issuing_authority = $request->issuing_authority;
        $expiry_date = $request->expiry_date;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $verify_type = $request->verify_type;
        $image = $request->doc_image;
        $doc_verify = $user::where('id', $user_id)->value('doc_verify_json');
        if ($doc_verify) {
            /******* REMOVE OLD IMAGE ***********/
            $doc_verify = json_decode($doc_verify, true);
            $old_image = '/' . $doc_verify['verify_doc']['path'] . '' . $doc_verify['verify_doc']['image_name'];
            $oldfilename  = public_path($old_image);
            if (file_exists($oldfilename)) {
                unlink($oldfilename);
            }
            /******* UPDATE NEW IMAGE ***********/
            $new_image = $image[0];
            // $imageName = rand(0,10000000).$image[0]->getClientOriginalName();
            $cleanImageName = $user->cleanImageName($new_image->getClientOriginalName());
            $imageName = rand(0, 10000000) . $cleanImageName;
            $doc_verify['verify_doc']['image_name'] = $imageName;
            $doc_verify['verify_doc']['expiry_date'] = $expiry_date;
            $doc_verify['verify_doc']['issuing_authority'] = $issuing_authority;
            $doc_verify['verify_doc']['status'] = 'pending';
            $doc_verify = json_encode($doc_verify);
            $update_db = $user::where('id', $user_id)->update(['doc_verify_json' => $doc_verify]);
            if ($update_db) {
                /********* MOVE NEW IMAGE **********/
                $uploaded = $new_image->move(public_path('images/verify_uploads/'), $imageName);
                return response()->json([
                    'status' => 200,
                    'response' => 'Image Updated Successfully'
                ]);
            } else {
                return response()->json([
                    'status' => 300,
                    'response' => 'Something Went Wrong! New Image not updating'
                ]);
            }
        }
    }
    /* // UPDATE USER ADDITIONAL DOCUMENT IMAGE
    **************************************************************
    */
    public function updateAdditionalDocImage(Request $request, User $user)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        $issuing_authority = $request->issuing_authority;
        $expiry_date = $request->expiry_date;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $image = $request->additional_doc_image;
        $doc_verify = $user::where('id', $user_id)->value('doc_verify_json');
        if ($doc_verify) {
            /******* REMOVE OLD IMAGE ***********/
            $doc_verify = json_decode($doc_verify, true);
            $old_image = '/' . $doc_verify['additional_doc']['path'] . '' . $doc_verify['additional_doc']['image_name'];
            $oldfilename  = public_path($old_image);
            if (file_exists($oldfilename)) {
                unlink($oldfilename);
            }
            /******* UPDATE NEW IMAGE ***********/
            $new_image = $image[0];
            // $imageName = rand(0,10000000).$image[0]->getClientOriginalName();
            $cleanImageName = $user->cleanImageName($new_image->getClientOriginalName());
            $imageName = rand(0, 10000000) . $cleanImageName;
            $doc_verify['additional_doc']['image_name'] = $imageName;
            $doc_verify['additional_doc']['expiry_date'] = $expiry_date;
            $doc_verify['additional_doc']['issuing_authority'] = $issuing_authority;
            $doc_verify['additional_doc']['status'] = 'pending';
            $doc_verify = json_encode($doc_verify);
            $update_db = $user::where('id', $user_id)->update(['doc_verify_json' => $doc_verify]);
            if ($update_db) {
                /********* MOVE NEW IMAGE **********/
                $uploaded = $new_image->move(public_path('images/verify_uploads/'), $imageName);
                return response()->json([
                    'status' => 200,
                    'response' => 'Image Updated Successfully'
                ]);
            } else {
                return response()->json([
                    'status' => 300,
                    'response' => 'Something Went Wrong! New Image not updating'
                ]);
            }
        }
    }
    /*
    **************************************************************
     // Pending Transactions on dashboard
    **************************************************************
    */
    public function customerpendingTransaction(Request $request, Payments $payments)
    {
        $user = auth()->user();
        $user_id = $user->id;
        $savedpending = $payments::where('user_id', $user_id)->where('status', 'paid')->where('approve_status', 'pending')->get();
        $savependingcount = $savedpending->count();
        //dd($savependingcount);
        if ($savependingcount) {
            return response()->json([
                'status' => 200,
                'pending_transactions' => $savependingcount
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }
    public function pendingSendMoney(Request $request, User $user)
    {
        $user = auth()->user();
        $user_id = $user->id;
        //dd($user_id);
        // $savedpending = $users::where('user_id', $user_id)->where('approve_status','pending')->get();
        $savedpending = $user::where('id', $user_id)->value('balance');
        //$savependingcount = $savedpending->count();
        // dd($savedpending);
        // dd('hello');
        if ($savedpending) {
            return response()->json([
                'status' => 200,
                'pending_sendMoney' => $savedpending
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }
    // verify voucher code.
    public function verifyVoucherCode(Request $request, User $user, Voucher $voucher, Payments $payments)
    {
        // validate user
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }

        $user = auth()->user();
        $user_id = $user->id;
        $data = $request->all();
        $promocode = $data['promocode'];

        // check promocode
        $result = $voucher::where('code', $promocode)->first();

        // check if user already used promocode in existing payment.
        $used_promocode = $payments::where('promocode', '=', $promocode)->where('user_id', '=', $user_id)->first();
        if ($used_promocode !== null) {
            return response()->json([
                'status' => 400,
                'response' => "You have already used PromoCode '" . $promocode
            ]);
        }

        if ($result === null) {  // code not found.
            return response()->json([
                'status' => 400,
                'response' => "PromoCode '" . $promocode . "' does not exist."
            ]);
        }

        // check pomocode expiry.
        $expire = strtotime($result->end_date);
        $today = strtotime("today midnight");

        if ($today >= $expire) {
            return response()->json([
                'status' => 400,
                'response' => "Promo Code '" . $promocode . "' is expired,"
            ]);
        }

        // promocode calculation.
        $feeDiscountPercentage = $result->discount_amount;
        $totalFee_ = (isset($data['totalFee_']) && !empty($data['totalFee_'])) ? $data['totalFee_'] : 0;

        // \Log::info('fee Discount Percentage' . $feeDiscountPercentage);
        // \Log::info('totalFee_' . $totalFee_);
        // \Log::info("------------- " . $this->getPercentOfNumber($totalFee_, $feeDiscountPercentage));


        $feeDiscount = $this->getPercentOfNumber($totalFee_, $feeDiscountPercentage);
        // dd("Promocode is active.");

        return response()->json([
            'status' => 200,
            'fee_discount' => $feeDiscount
        ]);
    }
    public function getPercentOfNumber($number, $percent)
    {
        return ($percent / 100) * $number;
    }

    public function  showBankDetail(Request $request, RechargeAccount $rechargeaccount, Countries $countries)
    {
        //dd($rechargeaccount);
        $user = auth()->user();
        $user_id = $user->id;
        // dd($user_id); // showBankDetail  // 228 user id
        $country_code = $user->country->code;
        $country_code = "PT";
        // print(" Country code: " . $country_code . "  user: " . $user_id); // AS country code.  // 4	 AS	American Samoa
        DB::enableQueryLog(); // Enable query log
        $account_details = $rechargeaccount->where('County', $country_code)->select('*')->get()->first();
        // dd(DB::getQueryLog()); // Show results of log
        // dd($account_details);
        if ($account_details) {
            return response()->json([
                'status' => 200,
                'showaccount_data' => $account_details
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }
    public function getBankAccountDetails(Request $request, RechargeAccount $rechargeaccount, Countries $countries)
    {
        $user = auth()->user();
        $user_id = $user->id;
        //dd($user_id);
        $country_code = $user->country->code;
        //dd($country_code);
        $account_details = $rechargeaccount->where('County', $country_code)->select('*')->get()->first();
        //dd($account_details);
        if ($account_details) {
            return response()->json([
                'status' => 200,
                'accountDetails_Data' => $account_details
            ]);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }
    public function saveContactDetails(Request $request, ContactUs $contactus)
    {
        // public function saveContactDetails(StoreContactUsRequest $storecontactusrequest, ContactUs $contactus){
        //dd($contactus);
        $data = $request->data;
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $data['customer_id'] = $user_id;
        $name = $data['name'];
        $email = $data['email'];
        $phone = $data['phone'];
        // $company =$data['company'];
        $message = $data['message'];
        // save data to contactus.
        $contactus->customer_id = $user_id;
        $contactus->name = $name;
        $contactus->email = $email;
        $contactus->phone = $phone;
        // $contactus->company = $company;
        $contactus->message = $message;
        $saved = $contactus->save();
        // event(new MessageReceived($request->all()));
        try {
            //   event(new MessageReceived($contactus));
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    /******* Customer Messages **********/
    // Sent messages to admin.
    public function customermessagesent(Request $request, Reply $reply)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $messages = $reply::where('sender_id', $user_id)->where('parent', '0')->orderBy('created_at', 'DESC')->paginate(10);
        if ($messages) {
            return response()->json($messages, 200);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }
    // Received messages from admin.
    public function customermessagereceived(Request $request, Reply $reply)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        // get messages from admin.
        $messages = $reply::where('receiver_id', $user_id)->where('sender_id', '1')->where('parent', '0')->orderBy('created_at', 'DESC')->paginate(10);
        if ($messages) {
            return response()->json($messages, 200);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }
    // view single message.
    public function CustomerMessageView($id, Reply $reply, Request $request)
    {
        $res = $reply::where('id', $id)->first();
        return response()->json($res);
    }
    // Customer Compose Message ( front-end )
    public function saveCustomerComposeMessage(Request $request, Reply $reply)
    {
        $data = $request->data;
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $reply->parent = 0; // user is parent sender.
        $reply->sender_id = $user_id;
        $reply->receiver_id = 1; // `super admin` will receive the message.
        $reply->name =  $data['name'];
        $reply->email = $data['email'];
        $reply->phone = $data['phone'];
        $reply->message = $data['message'];
        $reply->save();
        # Fire an event.
        // event(new MessageReceived($request->all()));
        try {
            //   event(new MessageReceived($contactus));
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    // check message status.
    public function checkStatus(Request $request, Reply $reply)
    {
        // return "i am true";
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $message = Reply::where('id', $request->message_id)->first();
        if ($message->status == 'open') {
            return 'true';
        } else { // status is close.
            // get child reply message.
            return Reply::where('parent',  $message->id)->first();
        }
        # Fire an event.
        // event(new MessageReceived($request->all()));
        try {
            //   event(new MessageReceived($contactus));
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    // save custom reply.
    public function saveReply(Request $request, Reply $reply)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        // get message sender id.
        $message = Reply::where('id', $request->message_id)->first();
        // Update Message Status To Close.
        $message_update = Reply::find($request->message_id);
        $message_update->status =  'close';
        $message_update->save();
        // Save Admin reply.
        $reply->parent = isset($request->message_id) ? $request->message_id : ''; // message_id
        $reply->sender_id = $user_id; // loggedIn user id.
        $reply->receiver_id = 1; // `super admin` will receive the message.
        $reply->status = 'close'; // close the status of message.
        $reply->name = \Auth::user()->name;
        $reply->email = \Auth::user()->email;
        $reply->phone = \Auth::user()->phone;
        $reply->message = isset($request->message) ? $request->message : ''; // reply message.
        $reply->save();
        return response()->json([
            'status' => 200,
            'response' => 'Replied Successfully.'
        ]);
        # Fire an event.
        // event(new MessageReceived($request->all()));
        try {
            //   event(new MessageReceived($contactus));
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    /////////////////////// #Customer Message/////////////////////////////////////////////
    /* **************************************************** */
    //    List user messages
    /* **************************************************** */
    public function messagesent(Request $request, ContactUs $contactus)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $messages = $contactus::where('customer_id', $user_id)->paginate(10);
        if ($messages) {
            return response()->json($messages, 200);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }
    /* **************************************************** */
    //    For pusher notifications
    /* **************************************************** */
    public function realtimenotify(AdminMessages $adminMessages)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        // $messages = $adminMessages::where('status', 'pending');
        $messages = Reply::where('status', 'open');
        $res = $messages->where(function ($query) use ($user_id) {
            $query->where('receiver_id', $user_id)
                ->where('sender_id', 1);
        })->get();
        // if ($messages) {
        //     return response()->json($messages, 200);
        // } else {
        //     return response()->json([
        //         'status' => 400,
        //         'response' => 'Something Went wrong, Please Try Again'
        //     ]);
        // }
        if (count($res) > 0) {
            $data = array('status' => 200, 'response' => $res);
        } else {
            $data = array('status' => 500, 'response' => $res);
        }
        return response()->json($data);
    }
    public function updatenotification(Request $request, AdminMessages $adminMessages)
    {
        $id = $request->get('id');
        $update = $adminMessages::where('status', 'pending');
        if ($id) {
            $update =    $update->where('id', $id);
        }
        $update =  $update->update(['status' => 'seen']);
        $adminMessages = new AdminMessages();
        $updatedList = $this->realtimenotify($adminMessages);
        if ($update) {
            $data = array('status' => 200, 'response' => $updatedList);
        } else {
            $data = array('status' => 500, 'response' => $updatedList);
        }
        return response()->json($data);
    }
    public function notification(messagesUS $contactus)
    {
        $contact = $contactus::where('status', 'pending')->get();
        if (count($contact) > 0) {
            $data = array('status' => 200, 'response' => $contact);
        } else {
            $data = array('status' => 500, 'response' => $contact);
        }
        return response()->json($data);
    }
    /* **************************************************** */
    //    List admin messages to customer
    /* **************************************************** */
    public function messagereceived(Request $request, AdminMessages $adminMessages)
    {
        $user_id = auth()->user() ? auth()->user()->id : $request->id;
        if (!$user_id) {
            $validator = Validator::make(
                $request->all(),
                array(
                    'id' => 'required'
                )
            );
            if ($validator->fails()) {
                $error_messages = implode(',', $validator->messages()->all());
                $response_array = array('success' => false, 'error' => 'Error validating Data', 'error_code' => 101, 'error_messages' => $error_messages);
                return response()->json($response_array, 200);
            }
        }
        $messages = $adminMessages::where('customer_id', $user_id)->orwhere('customer_id', 0)->paginate(10);
        if ($messages) {
            return response()->json($messages, 200);
        } else {
            return response()->json([
                'status' => 400,
                'response' => 'Something Went wrong, Please Try Again'
            ]);
        }
    }
    public function adminMessageView($id, AdminMessages $adminMessages, Request $request)
    {
        $res = $adminMessages::where('id', $id)->first();
        return response()->json($res);
    }
    public function sentmessageview($id, ContactUs $contactus, Request $request)
    {
        $res = $contactus::where('id', $id)->first();
        return response()->json($res);
    }
    /* **************************************************** */
    //     RESEND CONFIRMATION EMAIL
    /* **************************************************** */
    public function resendConfirmEmail(Request $request)
    {
        $user = auth()->user();
        $email = $user->email;
        $token = DB::table('users')->where('id', $user->id)->value('token');
        // dd($email);
        $to_name = $user->name;
        $to_email = $email;
        $mail_data = array(
            'token' => $token,
            'name'    => $to_name
        );
        // return view('emails.receipt', $mail_data);
        try {
            Mail::send('emails.confirm', $mail_data, function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                    ->subject('ANExpress');
                $message->from('support@anexpress.com', 'ANEXPRESS Support');
            });
            return response()->json([
                'status' => 200,
                'response' => 'Mail Sent Successfuly to User Email'
            ]);
        } catch (Exception $e) {
            return response()->json([
                'status' => 400,
                'response' => 'Somethign went wrong, Please try again'
            ]);
        }
    }
    /* **************************************************** */
    //     GET USER DETAILS
    /* **************************************************** */
    public function getUserDetails()
    {
        $user = auth()->user();
        return response()->json([
            'status' => 200,
            'name' => $user->name,
            'email' => $user->email,
            'phone' => $user->mobile_no
        ]);
    }
    public function getaddresses(Request $request)
    {
        if (\Request::ajax()) {
            $simplyserver = "http://www.simplylookupadmin.co.uk/JSONservice";
            $usernameID = "";
            $postcode = "";
            $XMLData = "";
            $data = "";
            $postcode = $request->postcode;
            $postcode = str_replace(" ", "", $postcode);
            header('Expires: Wed, 23 Dec 1980 00:30:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: no-cache, must-revalidate');
            header('Pragma: no-cache');
            header('Content-Type: text/xml');
            $XMLService = $simplyserver . "/JSONSearchForAddress.aspx?datakey=" . $this->datakey . "&postcode=" . $postcode . "&username=" . $usernameID . "&user=" . urlencode($_SERVER['HTTP_USER_AGENT']) . "&ip=127.1.1.0";
            #Set Message when on Mobile
            $XMLService = $XMLService . "&textmob=Please%20Select%20Address";
            #Set to 1 to show License status
            $XMLService = $XMLService . "&showlic=0";
            #Set Number of lines in list
            #Set to 0 to display Combo box
            $XMLService = $XMLService . "&lines=8";
            #Future use
            $XMLService = $XMLService . "&style=1";
            #Message at bottom of combo
            #Set to nothing to remove
            $XMLService = $XMLService . "&text=Please%20Select%20Address";
            #get XML
            $DataToSend = file_get_contents($XMLService);
            return response()->json(array('msg' => "Success", 'data' => $DataToSend), 200);
        }
    }
    public function getaddressDetails(Request $request)
    {
        if (\Request::ajax()) {
            $simplyserver = "http://www.simplylookupadmin.co.uk/xmlservice";
            $usernameID = "";
            $postcode = "";
            $XMLData = "";
            $data = "";
            $datakey = "W_54D7D281E74444EBA3C1B5025B3A8E";
            $address_id = $request->address_id;
            $address_id = str_replace(" ", "", $address_id);
            $XMLService = $simplyserver . "/GetAddressRecord.aspx?datakey=" . $this->datakey . "&id=" . $address_id . "&username=" . $usernameID . "&user=" . urlencode($_SERVER['HTTP_USER_AGENT']) . "&ip=127.1.1.0";
            $DataToSend = file_get_contents($XMLService);
            $xml = simplexml_load_string($DataToSend, "SimpleXMLElement", LIBXML_NOCDATA);
            $json = json_encode($xml);
            return response()->json(array('msg' => "Success", 'data' => $json), 200);
        }
    }
    public function subscribe()
    {
        echo "@Hello Subscriber";
    }
    // Payment accepted
    public function accepted()
    {
        // http://127.0.0.1:8000/user/checkUserBalance?to_pay_amount=24.92&payment_id=125&id=125
        // GET
        if (count($_GET)) {
            if (isset($_GET['orderID'])) {
                $order_id = $_GET['orderID']; // order_id
                $amount = $_GET['amount']; // amount
                $record_id = explode("-", $order_id);
                if (isset($record_id[3])) {
                    // echo $record_id[1];
                    // echo $record_id[2];
                    echo $record_id[3];
                    echo "<br>";
                    echo $amount;
                    // $client = new Client(); //GuzzleHttp\Client
                    // $result = $client->get("http://127.0.0.1:8000/user/checkUserBalance?to_pay_amount=$amount&payment_id=$record_id[3]&id=$record_id[3]");
                    // $client->get("http://127.0.0.1:8000/user/checkUserBalance?to_pay_amount=$amount&payment_id=$record_id[3]&id=$record_id[3]");
                } // offset 3 is set.
            }
            // print "<pre>";
            // print_r($_POST);
            // echo "get request is below................";
            // print_r($_GET);
        }
        /*
		[orderID] => anexpress-800000073-261566819638461
		[currency] => GBP
		[amount] => 6.36
		[PM] => CreditCard
		[ACCEPTANCE] => test123
		[STATUS] => 5
		[CARDNO] => XXXXXXXXXXXX5454
		[ED] => 0221
		[CN] => sdfasfasf
		[TRXDATE] => 08/26/19
		[PAYID] => 3053847037
		[PAYIDSUB] => 0
		[NCERROR] => 0
		[BRAND] => MasterCard
		[IPCTY] => PK
		[CCCTY] => 99
		[ECI] => 7
		[CVCCheck] => NO
		[AAVCheck] => NO
		[VC] => NO
		[IP] => 110.36.231.166
		[SHASIGN] => CD76E776E7EE6BD264B3ADB4AC98E6E57247DCA6
		*/
    }
    // Payment accepted
    public function cancel()
    {
        echo "Payment is cancel";
    }
    // get hash key for barclaycard
    public function getHashKey(Request $request)
    {
        $amount = (isset($request->amount) && $request->amount != "") ? $request->amount : null;
        $currency = (isset($request->currency) && $request->currency != "") ? $request->currency : 'GBP';
        $language = (isset($request->language) && $request->language != "") ? $request->language : 'en_US';
        $order_id = (isset($request->order_id) && $request->order_id != "") ? $request->order_id : null;
        // redirects.
        $accept_url  = (isset($request->accept_url) && $request->accept_url != "") ? $request->accept_url : null; // accept_url it will return back to your store after successful transation.
        $cancel_url  = (isset($request->cancel_url) && $request->cancel_url != "") ? $request->cancel_url : null; // cancel_url
        $back_url    = (isset($request->back_url) && $request->back_url != "") ? $request->back_url : null; // back_url
        $decline_url = (isset($request->decline_url) && $request->decline_url != "") ? $request->decline_url : null; // decline_url
        // development
        $pspid = "hamzakhan";
        // $sha_pass_pharase = "Mysecretsig1875!?";
        $sha_pass_pharase = "Mysecretsig1875!";
        // production
        // $pspid = "epdq56928";
        // $sha_pass_pharase = "anexpresspp";
        // $pspid = env('BARCLAYCARD_PSPID', 'hamzakhan');
        // $sha_pass_pharase = env('BARCLAYCARD_SHA_PASS_PHARASE', 'Mysecretsig1875!?');
        $sha_signature =
            "ACCEPTURL=$accept_url" . $sha_pass_pharase .
            "AMOUNT=$amount" . $sha_pass_pharase .
            // redirections.
            "BACKURL=$back_url" . $sha_pass_pharase .
            "CANCELURL=$cancel_url" . $sha_pass_pharase .
            "CURRENCY=$currency" . $sha_pass_pharase .
            "DECLINEURL=$decline_url" . $sha_pass_pharase .
            "LANGUAGE=$language" . $sha_pass_pharase .
            "ORDERID=$order_id" . $sha_pass_pharase .
            "PSPID=$pspid" . $sha_pass_pharase;
        $sha_sign       = sha1($sha_signature);
        return response()->json([
            'status' => 200,
            'sha_sign' => $sha_sign
        ]);
    }
}
