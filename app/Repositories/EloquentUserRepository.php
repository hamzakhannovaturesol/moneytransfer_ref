<?php

namespace App\Repositories;

use Exception;
use Carbon\Carbon;
use App\Models\User;
use App\Events\UserCreated;
use App\Events\UserDeleted;
use App\Events\UserUpdated;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Config\Repository;
use App\Repositories\Contracts\RoleRepository;
use App\Repositories\Contracts\UserRepository;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Mail;

/**
 * Class EloquentUserRepository.
 */
class EloquentUserRepository extends EloquentBaseRepository implements UserRepository
{
    /**
     * @var \Mcamara\LaravelLocalization\LaravelLocalization
     */
    protected $localization;

    /**
     * @var \Illuminate\Contracts\Config\Repository
     */
    protected $config;

    /**
     * @var RoleRepository
     */
    protected $roles;

    /**
     * EloquentUserRepository constructor.
     *
     * @param User                                             $user
     * @param \App\Repositories\Contracts\RoleRepository       $roles
     * @param \Mcamara\LaravelLocalization\LaravelLocalization $localization
     * @param \Illuminate\Contracts\Config\Repository          $config
     */
    public function __construct(
        User $user,
        RoleRepository $roles,
        LaravelLocalization $localization,
        Repository $config
    ) {
        parent::__construct($user);
        $this->roles = $roles;
        $this->localization = $localization;
        $this->config = $config;
    }

    /**
     * @param string $slug
     *
     * @return User
     */
    public function findBySlug($slug)
    {
        return $this->query()->whereSlug($slug)->first();
    }

    /**
     * @param array $input
     * @param bool  $confirmed
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return \App\Models\User
     */
    public function store(array $input, $confirmed = false)
    {
        /** @var User $user */
        // echo __FILE__;

        // dd($input);
        if (isset($input['role'])) {
            $user = $this->make(Arr::only($input, ['name', 'email', 'active', 'mobile_no']));
            if ($input['role'] == 'customer') {
                $user = $this->make(
                    Arr::only(
                        $input,
                        [
                            'first_name',
                            'middle_name',
                            'last_name',
                            'name',
                            'title',
                            'email',
                            'password',
                            'role',
                            'dob',
                            'place_of_birth',
                            'country_id',
                            'city',
                            'nationality',
                            'postcode',
                            'address_1',
                            'address_2',
                            'mobile_no',
                            'phone_no',
                            'consent_marketing',
                            'postcode',
                            'locale',
                            'timezone',
                            'highrisk',
                            'doc_verify_status',
                            'preferred_country',
                            'send_amount_country',
                            'balance',
                            'thrashold',

                            // new fileds
                            'occupation',
                            'annual_income',
                            'exp_month_volume',
                            'senior_gov_pos',

                            'username',
                            'ref_key'
                        ]
                    )
                );
            }
        } else {
            $user = $this->make(Arr::only($input, ['first_name', 'middle_name', 'last_name', 'name', 'title', 'email', 'address', 'place_of_birth', 'country_id', 'city', 'nationality', 'postcode', 'mobile_no', 'phone_no', 'active', 'consent_marketing', 'postcode', 'locale', 'timezone', 'doc_verify_status', 'preferred_country', 'highrisk', 'send_amount_country', 'balance', 'thrashold', 'occupation', 'annual_income', 'exp_month_volume', 'gov_position',]));
        }
        // $user = $this->make(Arr::only($input, ['name', 'email', 'active',]));

        if (isset($input['password'])) {
            $user->password = Hash::make($input['password']);
        }

        if ($confirmed) {
            $user->email_verified_at = Carbon::now();
        }

        if (empty($user->locale)) {
            $user->locale = $this->localization->getDefaultLocale();
        }

        if (empty($user->timezone)) {
            $user->timezone = $this->config->get('app.timezone');
        }

        $input['otp'] = rand(1, 1000000);
        $input['token'] = str_random(30);
        $user->otp = $input['otp'];
        $user->token = $input['token'];



        // new fields.
        // 'occupation','annual_income','exp_month_volume','gov_position'
        // $user->occupation = "" . $input['occupation'];


        // dd($input);
        if (!$this->save($user, $input)) {
            throw new GeneralException(__('exceptions.backend.users.create'));
        }
        /************ SEND OTP SMS TO CUSTOMER ON REGISTER  ************/
        if ($input['role'] == 'customer') {
            // dd($input['otp'], $input['mobile_no']);
            /* *********** SEND EMAIL OTP *************** */
            $this->sendEmail($input['email'], $input['otp']);
            /* *********** SEND EMAIL OTP *************** */
            /* *********** SEND EMAIL Confirmation *************** */
            $this->sendConfirmationEmail($input['email'], $input['token']);
            /* *********** SEND EMAIL Confirmation *************** */
            /******** SEND SMS OTP ********/
            $username = 'info@an-express.com';
            $hash = 'oDPCCTpfR9g-pJemTl4130kr4MVazHweuEBJiOIFva';
            $pword = "pakistan786";
            // Account details
            $apiKey = urlencode($hash);

            // Message details
            $mobile_no = $input['mobile_no'];
            // $numbers = array(923153444574);
            $numbers = array($mobile_no);
            $sender = urlencode('ANExpress');
            $message = rawurlencode('Password: ' . $input['otp']);

            $numbers = implode(',', $numbers);

            // Prepare data for POST request
            $data = array('pword' => $pword, 'apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);

            // Send the POST request with cURL
            $ch = curl_init('http://txtlocal.com/sendsmspost.php');
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response = curl_exec($ch);
            curl_close($ch);
            /******** SEND SMS OTP ********/
        }
        event(new UserCreated($user));

        return $user;
    }

    /**
     * @param User  $user
     * @param array $input
     *
     * @throws Exception
     * @throws \Exception|\Throwable
     *
     * @return \App\Models\User
     */
    public function update(User $user, array $input)
    {
        //dd($input.'balance');

        if (!$user->can_edit) {
            throw new GeneralException(__('exceptions.backend.users.first_user_cannot_be_edited'));
        }

        $user->fill(Arr::except($input, 'password'));

        if ($user->is_super_admin && !$user->active) {
            throw new GeneralException(__('exceptions.backend.users.first_user_cannot_be_disabled'));
        }

        if (!$this->save($user, $input)) {
            throw new GeneralException(__('exceptions.backend.users.update'));
        }
        //if($this->save($user, $input)){

        event(new UserUpdated($user));
        // $input->field = json_encode($input->field[]);

        return $user;
        //}
    }

    /**
     * @param \App\Models\User $user
     * @param array            $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    private function save(User $user, array $input)
    {
        if (isset($input['password']) && !empty($input['password'])) {
            $user->password = Hash::make($input['password']);
        }

        if (isset($input['otp']) && !empty($input['otp'])) {
            $user->otp = $input['otp'];
        }

        if (isset($input['role']) && !empty($input['role'])) {
            if ($input['role'] == 'customer') {
                $customer_role = 'customer';
            }
            if ($input['role'] == 'paying') {
                $paying_role = 'paying';
            }
        }

        if (isset($customer_role)) {
            $cust_roles = [0 => '10'];
        }
        if (isset($paying_role)) {
            $payng_roles = [0 => '11'];
        }
        $roles = $input['roles'] ?? [];
        if (!$user->save()) {
            return false;
        }

        if (!empty($roles)) {
            $allowedRoles = $this->roles->getAllowedRoles()->keyBy('id');

            foreach ($roles as $id) {
                if (!$allowedRoles->has($id)) {
                    throw new GeneralException(__('exceptions.backend.users.cannot_set_superior_roles'));
                }
            }
        }
        /* SYNCING ROLES */
        if (isset($cust_roles)) {
            $user->roles()->sync($cust_roles);
        } else if (isset($payng_roles)) {
            $user->roles()->sync($payng_roles);
        } else {
            $user->roles()->sync($roles);
        }

        return true;
    }

    /**
     * @param User $user
     *
     * @throws \Exception|\Throwable
     *
     * @return bool|null
     */
    public function destroy(User $user)
    {
        if (!$user->can_delete) {
            throw new GeneralException(__('exceptions.backend.users.first_user_cannot_be_destroyed'));
        }

        if (!$user->delete()) {
            throw new GeneralException(__('exceptions.backend.users.delete'));
        }

        event(new UserDeleted($user));

        return true;
    }

    /**
     * @param User $user
     *
     * @throws Exception
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function impersonate(User $user)
    {
        if ($user->is_super_admin) {
            throw new GeneralException(__('exceptions.backend.users.first_user_cannot_be_impersonated'));
        }

        $authenticatedUser = auth()->user();

        if (
            $authenticatedUser->id === $user->id
            || session()->get('admin_user_id') === $user->id
        ) {
            return redirect()->route('admin.home');
        }

        if (!session()->get('admin_user_id')) {
            session(['admin_user_id' => $authenticatedUser->id]);
            session(['admin_user_name' => $authenticatedUser->name]);
            session(['temp_user_id' => $user->id]);
        }

        //Login user
        auth()->loginUsingId($user->id);

        return redirect(home_route());
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDestroy(array $ids)
    {
        DB::transaction(function () use ($ids) {
            // This wont call eloquent events, change to destroy if needed
            if ($this->query()->whereIn('id', $ids)->delete()) {
                return true;
            }

            throw new GeneralException(__('exceptions.backend.users.delete'));
        });

        return true;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchEnable(array $ids)
    {
        DB::transaction(function () use ($ids) {
            if ($this->query()->whereIn('id', $ids)
                ->update(['active' => true])
            ) {
                return true;
            }

            throw new GeneralException(__('exceptions.backend.users.update'));
        });

        return true;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDisable(array $ids)
    {
        DB::transaction(function () use ($ids) {
            if ($this->query()->whereIn('id', $ids)
                ->update(['active' => false])
            ) {
                return true;
            }

            throw new GeneralException(__('exceptions.backend.users.update'));
        });

        return true;
    }

    /* **************************************************** */
    //    SEND OTP EMAIL
    /* **************************************************** */

    public function sendEmail($email, $password)
    {
        // dd($email);
        $to_name = 'User Name';
        $to_email = $email;
        $mail_data = array(
            'password' => $password,
            'name'    => $to_name
        );

        // return view('emails.receipt', $mail_data);
        try {
            Mail::send('emails.password-email', $mail_data, function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                    ->subject('ANExpress');
                $message->from('support@anexpress.com', 'ANEXPRESS Support');
            });
            return response()->json([
                'status' => 200,
                'response' => 'Mail Sent Successfuly to User Email'
            ]);
        } catch (Exception $e) { }
    }

    /* **************************************************** */
    //    SEND CONFIRMATION EMAIL
    /* **************************************************** */

    public function sendConfirmationEmail($email, $token)
    {
        // dd($email);
        $to_name = 'User Name';
        $to_email = $email;
        $mail_data = array(
            'token' => $token,
            'name'    => $to_name
        );

        // return view('emails.receipt', $mail_data);
        try {
            Mail::send('emails.confirm', $mail_data, function ($message) use ($to_name, $to_email) {
                $message->to($to_email, $to_name)
                    ->subject('ANExpress');
                $message->from('support@anexpress.com', 'ANEXPRESS Support');
            });
            return response()->json([
                'status' => 200,
                'response' => 'Mail Sent Successfuly to User Email'
            ]);
        } catch (Exception $e) { }
    }
}
