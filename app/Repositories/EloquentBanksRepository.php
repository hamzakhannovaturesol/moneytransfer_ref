<?php

namespace App\Repositories;

use App\Models\Banks;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Gate;
use App\Repositories\Contracts\BanksRepository;

/**
 * Class EloquentBanksRepository.
 */
class EloquentBanksRepository extends EloquentBaseRepository implements BanksRepository
{
    /**
     * EloquentUserRepository constructor.
     *
     * @param Banks $bank
     */
    public function __construct(
        Banks $bank
    ) {
        parent::__construct($bank);
    }

    /**
     * @param Banks                              $bank
     * @param array                              $input
     * @param \Illuminate\Http\UploadedFile|null $image
     *
     * @throws \App\Exceptions\GeneralException|\Exception|\Throwable
     *
     * @return mixed
     */
    public function save(Banks $bank, array $input)
    {
        DB::transaction(function () use ($bank, $input) {
            if (! $bank->save()) {
                throw new GeneralException(__('exceptions.backend.banks.save'));
            }           
            return true;
        });

        return true;
    }

    /**
     * @param Banks $bank
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function destroy(Banks $bank)
    {
        if (! $bank->delete()) {
            throw new GeneralException(__('exceptions.backend.banks.delete'));
        }

        return true;
    }

    /**
     * @param array $ids
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function batchQuery(array $ids)
    {
        $query = $this->query()->whereIn('Bank_ID', $ids);

        return $query;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDestroy(array $ids)
    {
        DB::transaction(function () use ($ids) {
            $query = $this->batchQuery($ids);            

            /** @var Banks[] $bank */
            $bank = $query->get();

            foreach ($bank as $bank) {
                $this->destroy($bank);
            }

            return true;
        });

        return true;
    }

}
