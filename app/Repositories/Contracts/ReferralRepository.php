<?php

namespace App\Repositories\Contracts;

use App\Models\Referral;

/**
 * Interface ReferralRepository.
 */
interface ReferralRepository extends BaseRepository
{

    public function save(Referral $referral, array $input);

    /**
     * @param Recipient $recipient
     *
     * @return mixed
     */
    public function destroy(Referral $recipient);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDestroy(array $ids);
}
