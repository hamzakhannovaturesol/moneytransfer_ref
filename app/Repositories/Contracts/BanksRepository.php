<?php

namespace App\Repositories\Contracts;

use App\Models\Banks;

/**
 * Interface BanksRepository.
 */
interface BanksRepository extends BaseRepository
{

    public function save(Banks $bank, array $input);

    /**
     * @param Banks $bank
     *
     * @return mixed
     */
    public function destroy(Banks $bank);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDestroy(array $ids);
}
