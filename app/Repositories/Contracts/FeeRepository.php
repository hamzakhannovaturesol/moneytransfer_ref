<?php

namespace App\Repositories\Contracts;

use App\Models\Fee;

/**
 * Interface FeeRepository.
 */
interface FeeRepository extends BaseRepository
{

    public function save(Fee $fee, array $input);

    /**
     * @param Fee $fee
     *
     * @return mixed
     */
    public function destroy(Fee $fee);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDestroy(array $ids);
}
