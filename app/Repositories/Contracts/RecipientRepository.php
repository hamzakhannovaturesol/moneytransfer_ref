<?php

namespace App\Repositories\Contracts;

use App\Models\Recipient;

/**
 * Interface RecipientRepository.
 */
interface RecipientRepository extends BaseRepository
{

    public function save(Recipient $recipient, array $input);

    /**
     * @param Recipient $recipient
     *
     * @return mixed
     */
    public function destroy(Recipient $recipient);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDestroy(array $ids);
}
