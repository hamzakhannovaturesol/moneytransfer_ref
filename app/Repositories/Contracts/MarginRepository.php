<?php

namespace App\Repositories\Contracts;

use App\Models\Margin;

/**
 * Interface MarginRepository.
 */
interface MarginRepository extends BaseRepository
{

    public function save(Margin $margin, array $input);

    /**
     * @param Margin $margin
     *
     * @return mixed
     */
    public function destroy(Margin $margin);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDestroy(array $ids);
}
