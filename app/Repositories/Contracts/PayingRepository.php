<?php

namespace App\Repositories\Contracts;

use App\Models\PayingAgents;
use Illuminate\Http\Request;

/**
 * Interface PayingAgentsRepository.
 */
interface PayingRepository extends BaseRepository
{
    /**
     * @param string $slug
     *
     * @return PayingAgents
     */
    public function findBySlug($slug);

    /**
     * @param array $input
     * @param bool  $confirmed
     *
     * @return mixed
     */
    public function store(array $input, $confirmed = false);

    /**
     * @param PayingAgents  $paying
     * @param array $input
     *
     * @return mixed
     */
    public function update(PayingAgents $paying, array $input);

    /**
     * @param PayingAgents $paying
     *
     * @return mixed
     */
    public function destroy(PayingAgents $paying);

    /**
     * @param PayingAgents $paying
     *
     * @return mixed
     */
    public function impersonate(PayingAgents $paying);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDestroy(array $ids);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchEnable(array $ids);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDisable(array $ids);
}
