<?php

namespace App\Repositories\Contracts;

use App\Models\Currency;

/**
 * Interface CurrencyRepository.
 */
interface CurrencyRepository extends BaseRepository
{

    public function save(Currency $currency, array $input);

    /**
     * @param Currency $currency
     *
     * @return mixed
     */
    public function destroy(Currency $currency);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDestroy(array $ids);
}
