<?php

namespace App\Repositories\Contracts;

use App\Models\PayingAgents;
use Laravel\Socialite\AbstractUser;
use Illuminate\Contracts\Auth\Authenticatable;

/**
 * Interface AccountRepository.
 */
interface PayingAccountRepository extends BaseRepository
{
    /**
     * @param $input
     *
     * @return mixed
     */
    public function registerCustomer(array $input);
    /**
     * @param $input
     *
     * @return mixed
     */
    public function register(array $input);

    /**
     * @param $input
     *
     * @return mixed
     */
    public function registerPaying(array $input);


    /**
     * @param Authenticatable $paying
     *
     * @return mixed
     */
    public function login(Authenticatable $paying);

    /**
     * @param              $provider
     * @param AbstractUser $data
     *
     * @return PayingAgents
     */
    public function findOrCreateSocial($provider, AbstractUser $data);

    /**
     * @param \Illuminate\Contracts\Auth\Authenticatable $paying
     * @param                                            $name
     *
     * @return bool
     */
    public function hasPermission(Authenticatable $paying, $name);

    /**
     * @param $input
     *
     * @return mixed
     */
    public function update(array $input);

    /**
     * @param $oldPassword
     * @param $newPassword
     *
     * @return mixed
     */
    public function changePassword($oldPassword, $newPassword);

    /**
     * @return mixed
     */
    public function delete();
}
