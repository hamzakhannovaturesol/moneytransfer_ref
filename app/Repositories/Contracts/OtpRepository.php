<?php

namespace App\Repositories\Contracts;

use App\Models\Otp;

/**
 * Interface OtpRepository.
 */
interface OtpRepository extends BaseRepository
{

    public function save(Otp $otp, array $input);

    /**
     * @param Otp $otp
     *
     * @return mixed
     */
    public function destroy(Otp $otp);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDestroy(array $ids);
}
