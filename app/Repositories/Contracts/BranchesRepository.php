<?php

namespace App\Repositories\Contracts;

use App\Models\Branches;

/**
 * Interface BranchesRepository.
 */
interface BranchesRepository extends BaseRepository
{

    public function save(Branches $branch, array $input);

    /**
     * @param Branches $branch
     *
     * @return mixed
     */
    public function destroy(Branches $branch);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDestroy(array $ids);
}
