<?php

namespace App\Repositories\Contracts;

use App\Models\Voucher;

/**
 * Interface UserRepository.
 */
interface VoucherRepository extends BaseRepository
{
    /**
     * @param string $slug
     *
     * @return User
     */
    public function findBySlug($slug);

    /**
     * @param array $input
     * @param bool  $confirmed
     *
     * @return mixed
     */
    public function store(array $input, $confirmed = false);

    /**
     * @param User  $user
     * @param array $input
     *
     * @return mixed
     */
    public function update(Voucher $voucher, array $input);

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function destroy(Voucher $voucher);

    /**
     * @param User $user
     *
     * @return mixed
     */
    public function impersonate(Voucher $voucher);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDestroy(array $ids);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchEnable(array $ids);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDisable(array $ids);
}
