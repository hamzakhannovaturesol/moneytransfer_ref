<?php

namespace App\Repositories\Contracts;

use App\Models\Recharge;

/**
 * Interface RechargeRepository.
 */
interface RechargeRepository extends BaseRepository
{

    public function save(Recharge $recharge, array $input);

    /**
     * @param Recharge $recharge
     *
     * @return mixed
     */
    public function destroy(Recharge $recharge);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDestroy(array $ids);
}
