<?php

namespace App\Repositories\Contracts;

use App\Models\Payments;

/**
 * Interface PaymentsRepository.
 */
interface PaymentsRepository extends BaseRepository
{

    public function save(Payments $payment, array $input);

    /**
     * @param Payments $payment
     *
     * @return mixed
     */
    public function destroy(Payments $payment);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDestroy(array $ids);
}
