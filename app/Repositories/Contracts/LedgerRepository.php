<?php

namespace App\Repositories\Contracts;

use App\Models\Ledger;

/**
 * Interface RechargeRepository.
 */
interface LedgerRepository extends BaseRepository
{

    public function save(Ledger $ledger, array $input);

    /**
     * @param Recharge $recharge
     *
     * @return mixed
     */
    public function destroy(Ledger $ledger);

    /*
     * @param array $ids
     *
     * @return mixed
    */
    public function batchDestroy(array $ids);
}
