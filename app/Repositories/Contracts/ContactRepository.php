<?php

namespace App\Repositories\Contracts;

use App\Models\ContactUs;

/**
 * Interface BanksRepository.
 */
interface ContactRepository extends BaseRepository
{

    public function save(ContactUs $contactus, array $input);

    /**
     * @param ContactsUs $contactusus
     *
     * @return mixed
     */
    public function destroy(ContactUs $contactus);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function batchDestroy(array $ids);
}
