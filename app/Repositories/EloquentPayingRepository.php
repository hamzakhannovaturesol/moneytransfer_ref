<?php

namespace App\Repositories;

use Exception;
use Carbon\Carbon;
use App\Models\PayingAgents;
use App\Events\PayingCreated;
use App\Events\PayingDeleted;
use App\Events\PayingUpdated;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Contracts\Config\Repository;
use App\Repositories\Contracts\RoleRepository;
use App\Repositories\Contracts\PayingRepository;
use Illuminate\Http\Request;
use Mcamara\LaravelLocalization\LaravelLocalization;
use Mail;

/**
 * Class EloquentPayingRepository.
 */
class EloquentPayingRepository extends EloquentBaseRepository implements PayingRepository
{
    /**
     * @var \Mcamara\LaravelLocalization\LaravelLocalization
     */
    protected $localization;

    /**
     * @var \Illuminate\Contracts\Config\Repository
     */
    protected $config;

    /**
     * @var RoleRepository
     */
    protected $roles;

    /**
     * EloquentPayingRepository constructor.
     *
     * @param User                                             $paying
     * @param \App\Repositories\Contracts\RoleRepository       $roles
     * @param \Mcamara\LaravelLocalization\LaravelLocalization $localization
     * @param \Illuminate\Contracts\Config\Repository          $config
     */
    public function __construct(
        PayingAgents $paying,
        RoleRepository $roles,
        LaravelLocalization $localization,
        Repository $config
    ) {
        parent::__construct($paying);
        $this->roles = $roles;
        $this->localization = $localization;
        $this->config = $config;
    }

    /**
     * @param string $slug
     *
     * @return User
     */
    public function findBySlug($slug)
    {
        return $this->query()->whereSlug($slug)->first();
    }

    /**
     * @param array $input
     * @param bool  $confirmed
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return \App\Models\User
     */
    public function store(array $input, $confirmed = false)
    {
        /** @var PayingAgents $paying */
        $paying = $this->make($input);
        // $paying = $this->make(Arr::only($input, ['name', 'email', 'active',]));

        if (isset($input['PA_Password'])) {
            $paying->PA_Password = Hash::make($input['PA_Password']);
        }

        // dd($input);
        if (! $this->save($paying, $input)) {
            throw new GeneralException(__('exceptions.backend.users.create'));
        }
        event(new PayingCreated($paying));

        return $paying;
    }

    /**
     * @param User  $paying
     * @param array $input
     *
     * @throws Exception
     * @throws \Exception|\Throwable
     *
     * @return \App\Models\User
     */
    public function update(PayingAgents $paying, array $input)
    {
        //dd($input.'balance');

        if (! $paying->can_edit) {
            throw new GeneralException(__('exceptions.backend.users.first_user_cannot_be_edited'));
        }

        $paying->fill(Arr::except($input, 'password'));

        if ($paying->is_super_admin && ! $paying->active) {
            throw new GeneralException(__('exceptions.backend.users.first_user_cannot_be_disabled'));
        }

        if (! $this->save($paying, $input)) {
            throw new GeneralException(__('exceptions.backend.users.update'));
        }
         //if($this->save($paying, $input)){ 
        
        event(new PayingUpdated($paying)); 
       // $input->field = json_encode($input->field[]);

        return $paying;
        //}
    }

    /**
     * @param \App\Models\PayingAgents $paying
     * @param array            $input
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return bool
     */
    private function save(PayingAgents $paying, array $input)
    {
        if (isset($input['password']) && ! empty($input['password'])) {
            $paying->password = Hash::make($input['password']);
        }

        return true;
    }

    /**
     * @param PayingAgents $paying
     *
     * @throws \Exception|\Throwable
     *
     * @return bool|null
     */
    public function destroy(PayingAgents $paying)
    {
        if (! $paying->can_delete) {
            throw new GeneralException(__('exceptions.backend.users.first_user_cannot_be_destroyed'));
        }

        if (! $paying->delete()) {
            throw new GeneralException(__('exceptions.backend.users.delete'));
        }

        event(new PayingDeleted($paying));

        return true;
    }

    /**
     * @param PayingAgents $paying
     *
     * @throws Exception
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function impersonate(PayingAgents $paying)
    {
        if ($paying->is_super_admin) {
            throw new GeneralException(__('exceptions.backend.users.first_user_cannot_be_impersonated'));
        }

        $authenticatedUser = auth()->paying();

        if ($authenticatedUser->id === $paying->id
            || session()->get('admin_user_id') === $paying->id
        ) {
            return redirect()->route('paying.home');
        }

        if (! session()->get('admin_user_id')) {
            session(['admin_user_id' => $authenticatedUser->id]);
            session(['admin_user_name' => $authenticatedUser->name]);
            session(['temp_user_id' => $paying->id]);
        }

        //Login user
        auth()->loginUsingId($paying->id);

        return redirect(home_route());
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDestroy(array $ids)
    {
        DB::transaction(function () use ($ids) {
            // This wont call eloquent events, change to destroy if needed
            if ($this->query()->whereIn('id', $ids)->delete()) {
                return true;
            }

            throw new GeneralException(__('exceptions.backend.users.delete'));
        });

        return true;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchEnable(array $ids)
    {
        DB::transaction(function () use ($ids) {
            if ($this->query()->whereIn('id', $ids)
                ->update(['active' => true])
            ) {
                return true;
            }

            throw new GeneralException(__('exceptions.backend.users.update'));
        });

        return true;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDisable(array $ids)
    {
        DB::transaction(function () use ($ids) {
            if ($this->query()->whereIn('id', $ids)
                ->update(['active' => false])
            ) {
                return true;
            }

            throw new GeneralException(__('exceptions.backend.users.update'));
        });

        return true;
    }

    /* **************************************************** */
    //    SEND OTP EMAIL
    /* **************************************************** */  

    public function sendEmail($email, $password) {
        // dd($email);
        $to_name = 'User Name';
        $to_email = $email;
        $mail_data = array(
          'password' => $password,
          'name'    => $to_name
        );

        // return view('emails.receipt', $mail_data);
        try{       
        Mail::send('emails.password-email', $mail_data, function($message) use ($to_name, $to_email) {
              $message->to($to_email, $to_name)
                      ->subject('ANExpress');
              $message->from('support@anexpress.com','ANEXPRESS Support');
            });
            return response()->json([
              'status' => 200,
              'response' => 'Mail Sent Successfuly to User Email'
            ]);
          }
            catch(Exception $e){
        }
    }

    /* **************************************************** */
    //    SEND CONFIRMATION EMAIL
    /* **************************************************** */  

    public function sendConfirmationEmail($email, $token) {
        // dd($email);
        $to_name = 'User Name';
        $to_email = $email;
        $mail_data = array(
          'token' => $token,
          'name'    => $to_name
        );

        // return view('emails.receipt', $mail_data);
        try{       
        Mail::send('emails.confirm', $mail_data, function($message) use ($to_name, $to_email) {
              $message->to($to_email, $to_name)
                      ->subject('ANExpress');
              $message->from('support@anexpress.com','ANEXPRESS Support');
            });
            return response()->json([
              'status' => 200,
              'response' => 'Mail Sent Successfuly to User Email'
            ]);
          }
            catch(Exception $e){
        }
    }
}
