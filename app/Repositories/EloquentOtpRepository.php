<?php

namespace App\Repositories;

use App\Models\Otp;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Gate;
use App\Repositories\Contracts\OtpRepository;

/**
 * Class EloquentOtpRepository.
 */
class EloquentOtpRepository extends EloquentBaseRepository implements OtpRepository
{
    /**
     * EloquentUserRepository constructor.
     *
     * @param Otp $otp
     */
    public function __construct(
        Otp $otp
    ) {
        parent::__construct($otp);
    }

    /**
     * @param Otp                               $otp
     * @param array                              $input
     * @param \Illuminate\Http\UploadedFile|null $image
     *
     * @throws \App\Exceptions\GeneralException|\Exception|\Throwable
     *
     * @return mixed
     */
    public function save(Otp $otp, array $input)
    {

        DB::transaction(function () use ($otp, $input) {
            if (! $otp->save()) {
                throw new GeneralException(__('exceptions.backend.Otp.save'));
            }           
            return true;
        });

        return true;
    }

    /**
     * @param Otp $otp
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function destroy(Otp $otp)
    {
        if (! $otp->delete()) {
            throw new GeneralException(__('exceptions.backend.Otp.delete'));
        }

        return true;
    }

    /**
     * @param array $ids
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function batchQuery(array $ids)
    {
        $query = $this->query()->whereIn('id', $ids);

        return $query;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDestroy(array $ids)
    {
        DB::transaction(function () use ($ids) {
            $query = $this->batchQuery($ids);            

            /** @var Otp[] $otps */
            $otps = $query->get();

            foreach ($otps as $otp) {
                $this->destroy($otp);
            }

            return true;
        });

        return true;
    }

}
