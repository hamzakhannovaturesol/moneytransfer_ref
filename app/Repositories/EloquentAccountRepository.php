<?php

namespace App\Repositories;


use Exception;
use Carbon\Carbon;
use App\Models\User;
use App\Models\Referral;
use App\Models\SocialLogin;
use Illuminate\Support\Arr;
use Laravel\Socialite\AbstractUser;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Contracts\UserRepository;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Repositories\Contracts\AccountRepository;

/**
 * Class EloquentAccountRepository.
 */
class EloquentAccountRepository extends EloquentBaseRepository implements AccountRepository
{
    /**
     * @var UserRepository
     */
    protected $users;

    /**
     * EloquentUserRepository constructor.
     *
     * @param User                                       $user
     * @param \App\Repositories\Contracts\UserRepository $users
     *
     * @internal param \Mcamara\LaravelLocalization\LaravelLocalization
     *           $localization
     * @internal param \Illuminate\Contracts\Config\Repository $config
     */
    public function __construct(User $user, UserRepository $users)
    {
        parent::__construct($user);
        $this->users = $users;
    }

    /**
     * @param array $input
     *
     * @throws \Throwable
     * @throws \Exception
     *
     * @return \App\Models\User
     */
    public function register(array $input)
    {
        $user = $this->users->store(Arr::only($input, ['name', 'email', 'password']));

        return $user;
    }

    public function registerCustomer(array $input, $parent_id = 0)
    {
        $user = $this->users->store(Arr::only(
            $input,
            [
                'first_name',
                'middle_name',
                'last_name',
                'name',
                'title',
                'email',
                'password',
                'role',
                'dob',
                'place_of_birth',
                'country_id',
                'city',
                'nationality',
                'postcode',
                'address_1',
                'address_2',
                'mobile_no',
                'phone_no',
                'consent_marketing',

                // new fileds
                'occupation',
                'annual_income',
                'exp_month_volume',
                'senior_gov_pos',

                'username',
                'ref_key'                
            ]
        ));
       

       \Log::info("parent_id : " . $parent_id);

        // save eferral - Level - 1
        if (isset($parent_id) && $parent_id > 0) {
            Referral::create([
                'user_id' => isset($user->id) ? $user->id : '',
                'parent_id'  => isset($parent_id) ? $parent_id : '',
                'level'   => 1,
            ]);
        }

        $this->addReferral( $user->id, $parent_id);

        // Check parent in user_id        
        // $record =  Referral::where('user_id', $parent_id)->get();
        // if ( $record === null) {
        //     \Log::info( "no user_id found for parent_id : " . $parent_id);
        // } else {

            // \Log::info( "" . $record);

            

            // $referrals = Referral::all();

            // foreach ($referrals as $parent_ids)
            // {
            //     $parent_id = $parent_ids['parent_id'];
            //     $result =  $this->checkUserID($parent_id);
            //     // parent found in user_id
            //     if ( $result != false )
            //     {
            //         \Log::info( "Found Parent Record : " . $result);

            //         Referral::create([
            //             'parent_id' => isset($result->parent_id) ?  $result->parent_id : '',
            //             'user_id'   => isset($user->id) ? $user->id : '',
            //             'level'     => $result->level + 1,
            //         ]);
            //     }
            // }
        // }
        return $user;
    }

public function addReferral($user_id=0, $parent_id=0)
{
        // $parents = $this->orderBy('level', 'asc')->get(['user_id' => $parent_id]);
        $parents = Referral::where('user_id', $parent_id)->orderBy('level','asc')->get();
        

        if (count($parents) > 0) {
            // Lets Insert Other Levels

            $level = 2;
            foreach ($parents as $parent) {
                $parent_id = $parent->parent_id == 0 ? 1 : $parent->parent_id;

                // $this->save([
                //     'user_id' => $user_id,
                //     'parent_id' => $parent_id,
                //     'level' => $level
                // ]);

                Referral::create([
                        'user_id'   => isset($user_id) ? $user_id : '',
                        'parent_id' => isset($parent_id) ?  $parent_id : '',                        
                        'level'     => $level,
                ]);


                $level++;
            }
        }



}
    public function checkUserID($parent_id)
    {
        // \Log::info("parent id : " . $parent_id);
         $result = Referral::where('user_id', $parent_id)->first();
         if ($result === null) {
             return false;
         }

        // \Log::info("checkuser id result " . $result);
         return $result;
    }
    public function registerPaying(array $input)
    {
        $user = $this->users->store(Arr::only($input, ['name', 'email', 'password', 'role', 'mobile_no']));

        return $user;
    }

    /**
     * @param Authenticatable $user
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return \App\Models\User
     */
    public function login(Authenticatable $user)
    {
        /* @var User $user */
        $user->last_access_at = Carbon::now();

        if (!$user->save()) {
            throw new GeneralException(__('exceptions.backend.users.update'));
        }

        session(['permissions' => $user->getPermissions()]);

        return $user;
    }

    /**
     * @param              $provider
     * @param AbstractUser $data
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return User
     */
    public function findOrCreateSocial($provider, AbstractUser $data)
    {
        // Email can be not provided, so set default provider email.
        $user_email = $data->getEmail() ?: "{$data->getId()}@{$provider}.com";

        // Get user with this email or create new one.
        /** @var User $user */
        $user = $this->users->query()->whereEmail($user_email)->first();

        if (!$user) {
            // Registration is not enabled
            if (!config('account.can_register')) {
                throw new GeneralException(__('exceptions.frontend.auth.registration_disabled'));
            }

            $user = $this->users->store([
                'name'   => $data->getName(),
                'email'  => $user_email,
                'active' => true,
            ], true);
        }

        // Save new provider if needed
        if (!$user->getProvider($provider)) {
            $user->providers()->save(new SocialLogin([
                'provider'    => $provider,
                'provider_id' => $data->getId(),
            ]));
        }

        return $user;
    }

    /**
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param                                            $name
     *
     * @return bool
     */
    public function hasPermission(Authenticatable $user, $name)
    {
        /** @var User $user */
        // First user is always super admin and cannot be deleted
        if ($user->is_super_admin) {
            return true;
        }

        /** @var \Illuminate\Support\Collection $permissions */
        $permissions = session()->get('permissions');

        if ($permissions->isEmpty()) {
            return false;
        }

        return $permissions->contains($name);
    }

    /**
     * @param $input
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     * @throws \App\Exceptions\GeneralException
     *
     * @return mixed
     */
    public function update(array $input)
    {
        if (!config('account.updating_enabled')) {
            throw new GeneralException(__('exceptions.frontend.user.updating_disabled'));
        }

        /** @var User $user */
        $user = auth()->user();

        $user->fill(Arr::only($input, ['name', 'email', 'locale', 'timezone']));

        return $user->save();
    }

    /**
     * @param $oldPassword
     * @param $newPassword
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return mixed
     */
    public function changePassword($oldPassword, $newPassword)
    {
        if (!config('account.updating_enabled')) {
            throw new GeneralException(__('exceptions.frontend.user.updating_disabled'));
        }

        /** @var User $user */
        $user = auth()->user();

        if (empty($user->password) || Hash::check($oldPassword, $user->password)) {
            $user->password = Hash::make($newPassword);

            return $user->save();
        }

        throw new GeneralException(__('exceptions.frontend.user.password_mismatch'));
    }

    /**
     * @throws \App\Exceptions\GeneralException|Exception
     *
     * @return mixed
     */
    public function delete()
    {
        /** @var User $user */
        $user = auth()->user();

        if ($user->is_super_admin) {
            throw new GeneralException(__('exceptions.backend.users.first_user_cannot_be_destroyed'));
        }

        if (!$user->delete()) {
            throw new GeneralException(__('exceptions.frontend.user.delete_account'));
        }

        return true;
    }
}
