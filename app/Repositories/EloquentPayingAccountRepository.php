<?php

namespace App\Repositories;

use Exception;
use Carbon\Carbon;
use App\Models\PayingAgents;
use App\Models\SocialLogin;
use Illuminate\Support\Arr;
use Laravel\Socialite\AbstractUser;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Contracts\PayingRepository;
use Illuminate\Contracts\Auth\Authenticatable;
use App\Repositories\Contracts\PayingAccountRepository;

/**
 * Class EloquentAccountRepository.
 */
class EloquentPayingAccountRepository extends EloquentBaseRepository implements PayingAccountRepository
{
    /**
     * @var PayingRepository
     */
    protected $paying;

    /**
     * EloquentPayingRepository constructor.
     *
     * @param PayingAgents                                       $paying
     * @param \App\Repositories\Contracts\PayingRepository $payings
     *
     * @internal param \Mcamara\LaravelLocalization\LaravelLocalization
     *           $localization
     * @internal param \Illuminate\Contracts\Config\Repository $config
     */
    public function __construct(PayingAgents $paying, PayingRepository $payings)
    {
        parent::__construct($paying);
        $this->paying = $payings;
    }

    /**
     * @param array $input
     *
     * @throws \Throwable
     * @throws \Exception
     *
     * @return \App\Models\PayingAgents
     */
    public function register(array $input)
    {
        $paying = $this->paying->store($input);

        return $paying;
    }

    public function registerCustomer(array $input)
    {
        $paying = $this->paying->store(Arr::only($input,
            [
                'first_name',
                'middle_name',
                'last_name',
                'name',
                'title',
                'email',
                'password',
                'role',
                'dob',
                'place_of_birth',
                'country_id',
                'city',
                'nationality',
                'postcode',
                'address_1',
                'address_2',
                'mobile_no',
                'phone_no',
                'consent_marketing',
            ]
        ));

        return $paying;
    }

    public function registerPaying(array $input)
    {
        $paying = $this->paying->store(Arr::only($input, ['name', 'email', 'password', 'role', 'mobile_no']));

        return $paying;
    }

    /**
     * @param Authenticatable $paying
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return \App\Models\PayingAgents
     */
    public function login(Authenticatable $paying)
    {
        /* @var PayingAgents $paying */
        $paying->last_access_at = Carbon::now();

        if (! $paying->save()) {
            throw new GeneralException(__('exceptions.backend.PayingAgentss.update'));
        }

        // session(['permissions' => $paying->getPermissions()]);

        return $paying;
    }

    /**
     * @param              $provider
     * @param AbstractUser $data
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return PayingAgents
     */
    public function findOrCreateSocial($provider, AbstractUser $data)
    {
        // Email can be not provided, so set default provider email.
        $paying_email = $data->getEmail() ?: "{$data->getId()}@{$provider}.com";

        // Get user with this email or create new one.
        /** @var PayingAgents $paying */
        $paying = $this->paying->query()->whereEmail($paying_email)->first();

        if (! $paying) {
            // Registration is not enabled
            if (! config('account.can_register')) {
                throw new GeneralException(__('exceptions.frontend.auth.registration_disabled'));
            }

            $paying = $this->paying->store([
                'name'   => $data->getName(),
                'email'  => $paying_email,
                'active' => true,
            ], true);
        }

        // Save new provider if needed
        if (! $paying->getProvider($provider)) {
            $paying->providers()->save(new SocialLogin([
                'provider'    => $provider,
                'provider_id' => $data->getId(),
            ]));
        }

        return $paying;
    }

    /**
     * @param \Illuminate\Contracts\Auth\Authenticatable $paying
     * @param                                            $name
     *
     * @return bool
     */
    public function hasPermission(Authenticatable $paying, $name)
    {
        /** @var PayingAgents $paying */
        // First user is always super admin and cannot be deleted
        if ($paying->is_super_admin) {
            return true;
        }

        /** @var \Illuminate\Support\Collection $permissions */
        $permissions = session()->get('permissions');

        if ($permissions->isEmpty()) {
            return false;
        }

        return $permissions->contains($name);
    }

    /**
     * @param $input
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     * @throws \App\Exceptions\GeneralException
     *
     * @return mixed
     */
    public function update(array $input)
    {
        if (! config('account.updating_enabled')) {
            throw new GeneralException(__('exceptions.frontend.user.updating_disabled'));
        }

        /** @var PayingAgents $paying */
        $paying = auth()->paying();

        $paying->fill(Arr::only($input, ['name', 'email', 'locale', 'timezone']));

        return $paying->save();
    }

    /**
     * @param $oldPassword
     * @param $newPassword
     *
     * @throws \App\Exceptions\GeneralException
     *
     * @return mixed
     */
    public function changePassword($oldPassword, $newPassword)
    {
        if (! config('account.updating_enabled')) {
            throw new GeneralException(__('exceptions.frontend.user.updating_disabled'));
        }

        /** @var PayingAgents $paying */
        $paying = auth()->paying();

        if (empty($paying->password) || Hash::check($oldPassword, $paying->password)) {
            $paying->password = Hash::make($newPassword);

            return $paying->save();
        }

        throw new GeneralException(__('exceptions.frontend.user.password_mismatch'));
    }

    /**
     * @throws \App\Exceptions\GeneralException|Exception
     *
     * @return mixed
     */
    public function delete()
    {
        /** @var PayingAgents $paying */
        $paying = auth()->paying();

        if ($paying->is_super_admin) {
            throw new GeneralException(__('exceptions.backend.users.first_user_cannot_be_destroyed'));
        }

        if (! $paying->delete()) {
            throw new GeneralException(__('exceptions.frontend.user.delete_account'));
        }

        return true;
    }
}
