<?php

namespace App\Repositories;

use App\Models\Branches;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Gate;
use App\Repositories\Contracts\BranchesRepository;

/**
 * Class EloquentBranchesRepository.
 */
class EloquentBranchesRepository extends EloquentBaseRepository implements BranchesRepository
{
    /**
     * EloquentUserRepository constructor.
     *
     * @param Branches $branch
     */
    public function __construct(
        Branches $branch
    ) {
        parent::__construct($branch);
    }

    /**
     * @param Branches                               $branch
     * @param array                              $input
     * @param \Illuminate\Http\UploadedFile|null $image
     *
     * @throws \App\Exceptions\GeneralException|\Exception|\Throwable
     *
     * @return mixed
     */
    public function save(Branches $branch, array $input)
    {

        DB::transaction(function () use ($branch, $input) {
            if (! $branch->save()) {
                throw new GeneralException(__('exceptions.backend.Branches.save'));
            }           
            return true;
        });

        return true;
    }

    /**
     * @param Branches $branch
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function destroy(Branches $branch)
    {
        if (! $branch->delete()) {
            throw new GeneralException(__('exceptions.backend.Branches.delete'));
        }

        return true;
    }

    /**
     * @param array $ids
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function batchQuery(array $ids)
    {
        $query = $this->query()->whereIn('Branch_ID', $ids);

        return $query;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDestroy(array $ids)
    {
        DB::transaction(function () use ($ids) {
            $query = $this->batchQuery($ids);            

            /** @var Branches[] $branch */
            $branch = $query->get();

            foreach ($branch as $branch) {
                $this->destroy($branch);
            }

            return true;
        });

        return true;
    }

}
