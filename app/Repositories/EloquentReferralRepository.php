<?php

namespace App\Repositories;

use App\Models\Referral;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use App\Repositories\Contracts\ReferralRepository;
use Illuminate\Support\Facades\Gate;

/**
 * Class EloquentReferralRepository.
 */
class EloquentReferralRepository extends EloquentBaseRepository implements ReferralRepository
{
    public function __construct(
        Referral $referral
    ) {
        parent::__construct($referral);
    }

    public function save(Referral $referral, array $input)
    {

        // DB::transaction(function () use ($referral, $input) {
        //     if (!$referral->save()) {
        //         // throw new GeneralException(__('exceptions.backend.Recipient.save'));
        //     }
        //     return true;
        // });

        return true;
    }

    public function destroy(Referral $referral)
    {
        if (!$referral->delete()) {
            // throw new GeneralException(__('exceptions.backend.Recipient.delete'));
        }

        return true;
    }

    private function batchQuery(array $ids)
    {
        $query = $this->query()->whereIn('id', $ids);

        return $query;
    }

    public function batchDestroy(array $ids)
    {
        DB::transaction(function () use ($ids) {
            $query = $this->batchQuery($ids);

            $Referral = $query->get();

            foreach ($Referral as $referral) {
                $this->destroy($referral);
            }

            return true;
        });

        return true;
    }
}
