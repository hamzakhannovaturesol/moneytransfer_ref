<?php

namespace App\Repositories;

use App\Models\Fee;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Gate;
use App\Repositories\Contracts\FeeRepository;

/**
 * Class EloquentFeeRepository.
 */
class EloquentFeeRepository extends EloquentBaseRepository implements FeeRepository
{
    /**
     * EloquentUserRepository constructor.
     *
     * @param Fee $fee
     */
    public function __construct(
        Fee $fee
    ) {
        parent::__construct($fee);
    }

    /**
     * @param Fee                               $fee
     * @param array                              $input
     * @param \Illuminate\Http\UploadedFile|null $image
     *
     * @throws \App\Exceptions\GeneralException|\Exception|\Throwable
     *
     * @return mixed
     */
    public function save(Fee $fee, array $input)
    {

        DB::transaction(function () use ($fee, $input) {
            if (! $fee->save()) {
                throw new GeneralException(__('exceptions.backend.fee.save'));
            }           
            return true;
        });

        return true;
    }

    /**
     * @param Fee $fee
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function destroy(Fee $fee)
    {
        if (! $fee->delete()) {
            throw new GeneralException(__('exceptions.backend.fee.delete'));
        }

        return true;
    }

    /**
     * @param array $ids
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function batchQuery(array $ids)
    {
        $query = $this->query()->whereIn('id', $ids);

        return $query;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDestroy(array $ids)
    {
        DB::transaction(function () use ($ids) {
            $query = $this->batchQuery($ids);            

            /** @var Fee[] $fees */
            $fees = $query->get();

            foreach ($fees as $fee) {
                $this->destroy($fee);
            }

            return true;
        });

        return true;
    }

}
