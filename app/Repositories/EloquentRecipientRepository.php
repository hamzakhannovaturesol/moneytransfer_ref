<?php

namespace App\Repositories;

use App\Models\Recipient;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Gate;
use App\Repositories\Contracts\RecipientRepository;

/**
 * Class EloquentRecipientRepository.
 */
class EloquentRecipientRepository extends EloquentBaseRepository implements RecipientRepository
{
    /**
     * EloquentUserRepository constructor.
     *
     * @param Recipient $recipient
     */
    public function __construct(
        Recipient $recipient
    ) {
        parent::__construct($recipient);
    }

    /**
     * @param Recipient                               $recipient
     * @param array                              $input
     * @param \Illuminate\Http\UploadedFile|null $image
     *
     * @throws \App\Exceptions\GeneralException|\Exception|\Throwable
     *
     * @return mixed
     */
    public function save(Recipient $recipient, array $input)
    {

        DB::transaction(function () use ($recipient, $input) {
            if (! $recipient->save()) {
                throw new GeneralException(__('exceptions.backend.Recipient.save'));
            }           
            return true;
        });

        return true;
    }

    /**
     * @param Recipient $recipient
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function destroy(Recipient $recipient)
    {
        if (! $recipient->delete()) {
            throw new GeneralException(__('exceptions.backend.Recipient.delete'));
        }

        return true;
    }

    /**
     * @param array $ids
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function batchQuery(array $ids)
    {
        $query = $this->query()->whereIn('id', $ids);

        return $query;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDestroy(array $ids)
    {
        DB::transaction(function () use ($ids) {
            $query = $this->batchQuery($ids);            

            /** @var Recipient[] $Recipient */
            $Recipient = $query->get();

            foreach ($Recipient as $recipient) {
                $this->destroy($recipient);
            }

            return true;
        });

        return true;
    }

}
