<?php

namespace App\Repositories;

use App\Models\Ledger;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Gate;
use App\Repositories\Contracts\LedgerRepository;

/**
 * Class EloquentRechargeRepository.
 */
class EloquentLedgerRepository extends EloquentBaseRepository implements LedgerRepository
{
    /**
     * EloquentUserRepository constructor.
     *
     * @param Recharge $recharge
     */
    public function __construct(
        Ledger $ledger
    ) {
        parent::__construct($ledger);
    }

    /**
     * @param Recharge                               $recharge
     * @param array                              $input
     * @param \Illuminate\Http\UploadedFile|null $image
     *
     * @throws \App\Exceptions\GeneralException|\Exception|\Throwable
     *
     * @return mixed
     */
    public function save(Ledger $ledger, array $input)
    {

        DB::transaction(function () use ($ledger, $input) {
            if (! $ledger->save()) {
                throw new GeneralException(__('exceptions.backend.recharge.save'));
            }           
            return true;
        });

        return true;
    }

    /**
     * @param Recharge $recharge
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function destroy(ledger $ledger)
    {
        if (! $ledger->delete()) {
            throw new GeneralException(__('exceptions.backend.recharge.delete'));
        }

        return true;
    }

    /**
     * @param array $ids
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function batchQuery(array $ids)
    {
        $query = $this->query()->whereIn('id', $ids);

        return $query;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDestroy(array $ids)
    {
        DB::transaction(function () use ($ids) {
            $query = $this->batchQuery($ids);            

            /** @var Recharge[] $recharges */
            $ledger = $query->get();

            foreach ($ledgers as $ledger) {
                $this->destroy($ledger);
            }

            return true;
        });

        return true;
    }

}
