<?php

namespace App\Repositories;

use Exception;
use App\Models\User;
use App\Models\Voucher;
use Illuminate\Support\Arr;
use App\Exceptions\GeneralException;
use Illuminate\Contracts\Config\Repository;
use App\Repositories\Contracts\VoucherRepository;

/**
 * Class EloquentVoucherRepository.
 */
class EloquentVoucherRepository extends EloquentBaseRepository implements VoucherRepository
{
    protected $localization;
    protected $config;

    public function __construct(
        Voucher $voucher,
        Repository $config
    ) {
        parent::__construct($voucher);
        $this->config = $config;
    }
    public function findBySlug($slug)
    {
        return $this->query()->whereSlug($slug)->first();
    }
    public function store(array $input, $confirmed = false)
    {
        $voucher = $this->make(Arr::only($input, ['code', 'type', 'title', 'discount_amount', 'start_date', 'end_date']));
        if (!$this->save($voucher, $input)) {
            throw new GeneralException(__('exceptions.backend.vouchers.create'));
        }

        return $voucher;
    }
    public function update(Voucher $voucher, array $input)
    {
        if (!$this->save($voucher, $input)) {
            throw new GeneralException(__('exceptions.backend.vouchers.update'));
        }
    }
    private function save(Voucher $voucher, array $input)
    {
        // dd($input);
        $voucher->code = isset($input['code']) ? $input['code'] : '';
        $voucher->type = isset($input['type']) ? $input['type'] : '';
        $voucher->title = isset($input['title']) ? $input['title'] : '';
        $voucher->discount_amount = isset($input['discount_amount']) ? $input['discount_amount'] : '';
        $voucher->start_date = isset($input['start_date']) ? $input['start_date'] : '';
        $voucher->end_date = isset($input['end_date']) ? $input['end_date'] : '';
        if (!$voucher->save()) {
            return false;
        }
        return true;
    }
    public function destroy(Voucher $voucher)
    {
        if (!$voucher->delete()) {
            throw new GeneralException(__('exceptions.backend.vouchers.delete'));
        }

        return true;
    }
    public function batchEnable(array $ids)
    { }
    public function batchDisable(array $ids)
    { }
    public function impersonate(Voucher $voucher)
    { }
    public function batchDestroy(array $ids)
    { }
}
