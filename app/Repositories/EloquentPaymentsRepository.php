<?php

namespace App\Repositories;

use App\Models\Payments;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Gate;
use App\Repositories\Contracts\PaymentsRepository;

/**
 * Class EloquentPaymentsRepository.
 */
class EloquentPaymentsRepository extends EloquentBaseRepository implements PaymentsRepository
{
    /**
     * EloquentUserRepository constructor.
     *
     * @param Payments $payment
     */
    public function __construct(
        Payments $payment
    ) {
        parent::__construct($payment);
    }

    /**
     * @param Payments                               $payment
     * @param array                              $input
     * @param \Illuminate\Http\UploadedFile|null $image
     *
     * @throws \App\Exceptions\GeneralException|\Exception|\Throwable
     *
     * @return mixed
     */
    public function save(Payments $payment, array $input)
    {

        DB::transaction(function () use ($payment, $input) {
            if (! $payment->save()) {
                throw new GeneralException(__('exceptions.backend.Payments.save'));
            }           
            return true;
        });

        return true;
    }

    /**
     * @param Payments $payment
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function destroy(Payments $payment)
    {
        if (! $payment->delete()) {
            throw new GeneralException(__('exceptions.backend.Payments.delete'));
        }

        return true;
    }

    /**
     * @param array $ids
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function batchQuery(array $ids)
    {
        $query = $this->query()->whereIn('id', $ids);

        return $query;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDestroy(array $ids)
    {
        DB::transaction(function () use ($ids) {
            $query = $this->batchQuery($ids);            

            /** @var Payments[] $Payments */
            $Payments = $query->get();

            foreach ($Payments as $payment) {
                $this->destroy($payment);
            }

            return true;
        });

        return true;
    }

}
