<?php

namespace App\Repositories;

use App\Models\ContactUs;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Gate;
use App\Repositories\Contracts\ContactRepository;

/**
 * Class EloquentBanksRepository.
 */
class EloquentContactRepository extends EloquentBaseRepository implements ContactRepository
{
    /**
     * EloquentUserRepository constructor.
     *
     * @param ContactUs $contactus
     */
    public function __construct(
        ContactUs $contact
    ) {
        parent::__construct($contact);
    }

    /**
     * @param ContactUs                          $contactus
     * @param array                              $input
     * @param \Illuminate\Http\UploadedFile|null $image
     *
     * @throws \App\Exceptions\GeneralException|\Exception|\Throwable
     *
     * @return mixed
     */
    public function save(ContactUs $contactus, array $input)
    {
        DB::transaction(function () use ($ContactUs, $input) {
            if (! $contactus->save()) {
                throw new GeneralException(__('exceptions.backend.contact.save'));
            }           
            return true;
        });

        return true;
    }

    /**
     * @param ContactUs $contactus
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function destroy(ContactUs $contactus)
    {
        if (! $contactus->delete()) {
            throw new GeneralException(__('exceptions.backend.contact.delete'));
        }

        return true;
    }

    /**
     * @param array $ids
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function batchQuery(array $ids)
    {
        $query = $this->query()->whereIn('Bank_ID', $ids);

        return $query;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDestroy(array $ids)
    {
        DB::transaction(function () use ($ids) {
            $query = $this->batchQuery($ids);            

            /** @var Banks[] $bank */
            $bank = $query->get();

            foreach ($contact as $contact) {
                $this->destroy($contact);
            }

            return true;
        });

        return true;
    }

}
