<?php

namespace App\Repositories;

use App\Models\Currency;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Gate;
use App\Repositories\Contracts\CurrencyRepository;

/**
 * Class EloquentCurrencyRepository.
 */
class EloquentCurrencyRepository extends EloquentBaseRepository implements CurrencyRepository
{
    /**
     * EloquentUserRepository constructor.
     *
     * @param Currency $currency
     */
    public function __construct(
        Currency $currency
    ) {
        parent::__construct($currency);
    }

    /**
     * @param Currency                               $currency
     * @param array                              $input
     * @param \Illuminate\Http\UploadedFile|null $image
     *
     * @throws \App\Exceptions\GeneralException|\Exception|\Throwable
     *
     * @return mixed
     */
    public function save(Currency $currency, array $input)
    {

        DB::transaction(function () use ($currency, $input) {
            if (! $currency->save()) {
                throw new GeneralException(__('exceptions.backend.currency.save'));
            }           
            return true;
        });

        return true;
    }

    /**
     * @param Currency $currency
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function destroy(Currency $currency)
    {
        if (! $currency->delete()) {
            throw new GeneralException(__('exceptions.backend.currency.delete'));
        }

        return true;
    }

    /**
     * @param array $ids
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function batchQuery(array $ids)
    {
        $query = $this->query()->whereIn('id', $ids);

        return $query;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDestroy(array $ids)
    {
        DB::transaction(function () use ($ids) {
            $query = $this->batchQuery($ids);            

            /** @var Currency[] $currencys */
            $currencys = $query->get();

            foreach ($currencys as $currency) {
                $this->destroy($currency);
            }

            return true;
        });

        return true;
    }

}
