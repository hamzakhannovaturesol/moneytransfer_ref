<?php

namespace App\Repositories;

use App\Models\Margin;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Gate;
use App\Repositories\Contracts\MarginRepository;

/**
 * Class EloquentMarginRepository.
 */
class EloquentMarginRepository extends EloquentBaseRepository implements MarginRepository
{
    /**
     * EloquentUserRepository constructor.
     *
     * @param Margin $margin
     */
    public function __construct(
        Margin $margin
    ) {
        parent::__construct($margin);
    }

    /**
     * @param Margin                               $margin
     * @param array                              $input
     * @param \Illuminate\Http\UploadedFile|null $image
     *
     * @throws \App\Exceptions\GeneralException|\Exception|\Throwable
     *
     * @return mixed
     */
    public function save(Margin $margin, array $input)
    {

        DB::transaction(function () use ($margin, $input) {
            if (! $margin->save()) {
                throw new GeneralException(__('exceptions.backend.Margin.save'));
            }           
            return true;
        });

        return true;
    }

    /**
     * @param Margin $margin
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function destroy(Margin $margin)
    {
        if (! $margin->delete()) {
            throw new GeneralException(__('exceptions.backend.Margin.delete'));
        }

        return true;
    }

    /**
     * @param array $ids
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function batchQuery(array $ids)
    {
        $query = $this->query()->whereIn('id', $ids);

        return $query;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDestroy(array $ids)
    {
        DB::transaction(function () use ($ids) {
            $query = $this->batchQuery($ids);            

            /** @var Margin[] $margins */
            $margins = $query->get();

            foreach ($margins as $margin) {
                $this->destroy($margin);
            }

            return true;
        });

        return true;
    }

}
