<?php

namespace App\Repositories;

use App\Models\Recharge;
use Illuminate\Support\Facades\DB;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Gate;
use App\Repositories\Contracts\RechargeRepository;

/**
 * Class EloquentRechargeRepository.
 */
class EloquentRechargeRepository extends EloquentBaseRepository implements RechargeRepository
{
    /**
     * EloquentUserRepository constructor.
     *
     * @param Recharge $recharge
     */
    public function __construct(
        Recharge $recharge
    ) {
        parent::__construct($recharge);
    }

    /**
     * @param Recharge                               $recharge
     * @param array                              $input
     * @param \Illuminate\Http\UploadedFile|null $image
     *
     * @throws \App\Exceptions\GeneralException|\Exception|\Throwable
     *
     * @return mixed
     */
    public function save(Recharge $recharge, array $input)
    {

        DB::transaction(function () use ($recharge, $input) {
            if (! $recharge->save()) {
                throw new GeneralException(__('exceptions.backend.recharge.save'));
            }           
            return true;
        });

        return true;
    }

    /**
     * @param Recharge $recharge
     *
     * @throws \Exception
     *
     * @return mixed
     */
    public function destroy(Recharge $recharge)
    {
        if (! $recharge->delete()) {
            throw new GeneralException(__('exceptions.backend.recharge.delete'));
        }

        return true;
    }

    /**
     * @param array $ids
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    private function batchQuery(array $ids)
    {
        $query = $this->query()->whereIn('id', $ids);

        return $query;
    }

    /**
     * @param array $ids
     *
     * @throws \Exception|\Throwable
     *
     * @return mixed
     */
    public function batchDestroy(array $ids)
    {
        DB::transaction(function () use ($ids) {
            $query = $this->batchQuery($ids);            

            /** @var Recharge[] $recharges */
            $recharges = $query->get();

            foreach ($recharges as $recharge) {
                $this->destroy($recharge);
            }

            return true;
        });

        return true;
    }

}
