<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Ledger extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
	    'description',
	    'user_id',
	    'credit_amount',
	    'debit_amount',
	    'balance',
	];

   	protected $table = 'user_ledger';
}
