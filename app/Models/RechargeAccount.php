<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class RechargeAccount extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
	    'SNO',
	    'Country',
	    'County',
	    'CurrencyCode',
	    'CurrencyName',
	    'AccountNumber',
	    'SortCode',
	    'BIC-Swift-Number',
	    'IBAN',
	    'Account-Name',
	    'Bank-Name-Address',
	    'Beneficiary-Address',
	];

   	protected $table = 'recharge-acc';
}
