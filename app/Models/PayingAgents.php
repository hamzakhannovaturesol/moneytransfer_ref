<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;

class PayingAgents extends Authenticatable
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $guard = 'paying';

	protected $fillable = [
	    'P_A_Name',
	    'P_A_Address',
	    'P_A_City',
	    'P_A_Country',
	    'P_A_Email',
	    'P_A_Contactname',
	    'P_A_Contactnumber',
	    'P_A_Fax',
	    'P_A_Acc_Status',
	    'AN_Balance',
	    'PA_Username',
	    'PA_Password',
	    'PA_CODE',
	    'PA_SEQ',
	    'INS_SEQ',
	    'PA_TYPE',
	    'Buying_Rate',
	    'Buying_Rate_Euro',
	    'Buying_Rate_Dollar',
	    'Dtime_Rate',
	    'IP_Reqd',
	    'IP',
	    'serial',
	];

   	protected $table = 'paying_agents';

   	protected $primaryKey = 'P_A_ID';
   	public $timestamps = false;
}
