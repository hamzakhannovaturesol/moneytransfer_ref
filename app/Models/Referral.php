<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Referral extends Model
{
    protected $table = 'referrals';
    public $timestamps = true;

    protected $fillable = [
        'id',
        'user_id',
        'parent_id',
        'level',
    ];
  
}
