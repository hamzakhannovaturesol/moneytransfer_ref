<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Margin extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
	    'type',
	    'type_value',
	    'rate_margin',
	    'fee_margin',
	];

   	protected $table = 'type_margin';
}
