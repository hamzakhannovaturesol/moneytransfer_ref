<?php

namespace App\Models;

use Illuminate\Support\Facades\Gate;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\ResetPassword as ResetPasswordNotification;

class Voucher extends Authenticatable
{
    protected $table = "vouchers";
    public $timestamps = true;

    protected $fillable = [
        'id',
        'code',
        'type',
        'title',
        'discount_amount',
        'start_date',
        'end_date',
        'created_at',
        'updated_at',
    ];
}
