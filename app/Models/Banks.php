<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Banks extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'Bank_Name',
		'Bank_Code',
		'Bank_Swift_Code',
		'Address',
		'City',
		'Country',
		'State',
		'Express_Bank',
		'Instant_Bank',
		'Rate'
	];

	protected $table = 'banks';

	protected $primaryKey = 'Bank_ID';
}
