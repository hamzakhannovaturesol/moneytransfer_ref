<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Branches extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
	    'Branch_Name',
	    'Branch_Address',
	    'Branch_City',
	    'Country',
	    'State',
	    'Pay_Type',
	    'Sort_BR_Wise',
	    'extra_details',
	    'BR_Code',
	];

   	protected $table = 'branch';

   	protected $primaryKey = 'Branch_ID';
}
