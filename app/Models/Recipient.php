<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Recipient extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'first_name',
		'middle_name',
		'last_name',
		'father_name',
		'address_1',
		'address_2',
		'city',
		'postal_code',
		'country',
		'mobile',
		'phone',
		'email',
		'sending_reason',
		'source_of_income',
		'recipient_relation',
		'bank',
		'branchcity',
		'branch',
		'branch_code',
		'account_number',
		'user_id',
		'bank_branch'
	];

	protected $with = [
		'getcountry',
		'bank_branch'
	];

	public function getcountry()
	{
		return $this->belongsTo(Countries::class, 'country', 'id');
	}

	public function bank_branch()
	{
		return $this->belongsTo('App\Models\Branches', 'branch', 'Branch_ID');
	}
	protected $table = 'recipient';
}
