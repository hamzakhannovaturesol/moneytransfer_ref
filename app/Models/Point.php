<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Point extends Model
{
    protected $table = 'points';
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'points',
        'type',
        'date',
        'transaction_id',
        'created_at',
        'updated_at'
    ];
}
