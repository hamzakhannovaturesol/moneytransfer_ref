<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Currency extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
	    'Currency_Name',
	    'Currency_Code',
	    'Currency_Country',
	    'Exchange_Rate',
	    'Special_Agent_Rate',
	    'Special_Member_Rate',
	];

	public $timestamps = false;

	// protected $with = [
	// 	'get_country'
	// ];

	// public function get_country() {
	// 	return $this->BelongsTo(Countries::class, 'Currency_Country', 'code');
	// }
	
   	protected $table = 'currency';
}
