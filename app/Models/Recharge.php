<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Recharge extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $fillable = [
	    'details',
	    'image',
	    'amount',
	    'user_id',
	    'status',
	    'County'
	];

	protected $with = [
		'user',
	];
   	protected $table = 'recharge';

   	public function user() {
   		return $this->belongsTo(User::class, 'user_id', 'id');
    }


    public function cleanImageName($image) {
        $image = trim($image);
        $cleanImage = preg_replace('/[^a-zA-Z0-9-_.]/','', $image);
        return $cleanImage;
    }

}
