<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Fee extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
	    'MIN',
	    'MAX',
	    'FEE',
	    'Country',
	];

	protected $with = [
		'get_country'
	];

	public function get_country() {
		return $this->BelongsTo(Countries::class, 'Country', 'code');
	}
	
	public $timestamps = false;
	
   	protected $table = 'fee';
}
