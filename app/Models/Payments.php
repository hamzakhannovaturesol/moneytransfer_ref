<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Validator;

class Payments extends Model
{
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */

	protected $with = [
		'recipient',
		'user',
		'paying_agent',
	];

	protected $fillable = [
		'user_id',
		'transaction_id',
		'payment_details',
		'status',
		'approve_status',
		'recipient_id',
		'paying_agent_id',
		'promocode',
	];


	protected $table = 'payments';

	public function recipient()
	{
		return $this->belongsTo('App\Models\Recipient', 'recipient_id', 'id')->withTrashed();
	}

	public function user()
	{
		return $this->belongsTo('App\Models\User', 'user_id', 'id');
	}

	public function paying_agent()
	{
		// return $this->belongsTo('App\Models\Banks', 'paying_agent_id', 'Bank_ID');
		return $this->belongsTo('App\Models\PayingAgents', 'paying_agent_id', 'P_A_ID');
	}

	public function randomAlphaNumValidate($alphanum)
	{
		$validator = Validator::make($alphanum, [
			'transaction_id' => 'required|unique:payments,transaction_id,' . $alphanum['transaction_id']
		]);

		if ($validator->fails()) {
			return false;
		} else {
			return $alphanum['transaction_id'];
		}
	}
}
