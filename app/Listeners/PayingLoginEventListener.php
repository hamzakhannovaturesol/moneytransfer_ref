<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use App\Repositories\Contracts\PayingAccountRepository;

class PayingLoginEventListener
{
    /**
     * @var \App\Repositories\Contracts\AccountRepository
     */
    protected $paying;

    /**
     * Create the event listener.
     *
     * @param \App\Repositories\Contracts\AccountRepository $paying
     */
    public function __construct(PayingAccountRepository $paying)
    {
        dd('coming here');
        $this->paying = $paying;
    }

    /**
     * Handle the event.
     *
     * @param \Illuminate\Auth\Events\Authenticated $event
     */
    public function handle(Login $event)
    {
        $this->paying->login($event->paying);
    }
}
