<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;

class PayingRegistered
{
    use SerializesModels;

    /**
     * The authenticated user.
     *
     * @var \Illuminate\Contracts\Auth\Authenticatable
     */
    public $paying;

    /**
     * Create a new event instance.
     *
     * @param  \Illuminate\Contracts\Auth\Authenticatable  $paying
     * @return void
     */
    public function __construct($paying)
    {
        $this->paying = $paying;
    }
}
