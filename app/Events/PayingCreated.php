<?php

namespace App\Events;

use App\Models\PayingAgents;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\InteractsWithSockets;

class PayingCreated
{
    use InteractsWithSockets, SerializesModels;

    /**
     * @var PayingAgents
     */
    public $paying;

    /**
     * @param $paying
     */
    public function __construct(PayingAgents $paying)
    {
        $this->paying = $paying;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('paying');
    }
}
