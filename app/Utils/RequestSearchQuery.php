<?php

namespace App\Utils;

use Illuminate\Http\Request;
use App\Exports\DataTableExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\User;
use App\Models\Countries;
use App\Models\Fee;
use DB;

class RequestSearchQuery
{
    /**
     * @var \Request
     */
    private $request;

    /**
     * @var \Illuminate\Database\Eloquent\Builder
     */
    private $query;

    public function __construct(Request $request, Builder $query, $searchables = [])
    {
        $this->request = $request;
        // dd($this->request);

        $this->query = $query;
        $this->user = new User();
        $this->searchables  = $searchables;
        // dd($this->request);
        // $this->initializeQuery($searchables);
    }

    private function getLocalizedColumn(Model $model, $column)
    {
        if (property_exists($model, 'translatable') && \in_array($column, $model->translatable, true)) {
            $locale = app()->getLocale();

            return "$column->$locale";
        }

        return $column;
    }
    /**
     * @param array $searchables
     */
    public function initializeQuery($searchables = [])
    {
        $model = $this->query->getModel();
        //dd($model);
        if ($column = $this->request->get('column')) {
            // dd($this->query->toSql());
            $this->query->orderBy(
                $this->getLocalizedColumn($model, $column),
                $this->request->get('direction') ?? 'asc'
            );
        }

        if ($search = $this->request->get('search')) {
            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }
    }
    /**
     * @param $columns
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function result($columns)
    {
        $searchables = $this->searchables;
        $model = $this->query->getModel();
        if ($search = $this->request->get('search')) {
            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }

        return $this->query->paginate($this->request->get('perPage'), $columns);
    }
    /**
     * Admin - Custome messages
     */
    public function resultCustomerMessages($columns, $customer_id = 0)
    {
        $searchables = $this->searchables;
        $model = $this->query->getModel();
        if ($search = $this->request->get('search')) {
            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }

        // \DB::enableQueryLog();
        $customer_id = $this->request->get('customer_id');
        $this->query->where('replies.parent', '0')->where('replies.sender_id', $customer_id)->get();


        return $this->query->paginate($this->request->get('perPage'), $columns);
    }
    public function resultmessage($columns)
    {
        $searchables = $this->searchables;
        // dd($searchables);
        $model = $this->query->getModel();
        if ($search = $this->request->get('search')) {
            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }
        return $this->query->paginate($this->request->get('perPage'), $columns);
    }

    public function paymentResult()
    {

        $searchables = $this->searchables;
        $model = $this->query->getModel();
        //dd($model,$searchables);

        $searchByStatus = $this->request->get('searchByStatus');


        if ($search = $this->request->get('search')) {
            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }
        $order = 'desc';

        if (!empty($searchByStatus) && $searchByStatus == "Approved") {
            $this->query->leftjoin('users', 'payments.user_id', '=', 'users.id')
                ->where('payments.transaction_id', '!=', NULL)
                ->where('payments.status', '=', 'paid')
                // ->orwhere('payments.status', '=', 'payment')

                ->where('payments.approve_status', '=', 'approved')


                ->orderBy('payments.approve_status')
                ->orderBy('users.highrisk', 'desc')
                ->select('payments.*', 'users.name');
        } else if (!empty($searchByStatus) && $searchByStatus == "Pending") {

            $this->query->leftjoin('users', 'payments.user_id', '=', 'users.id')
                ->where('payments.transaction_id', '!=', NULL)
                ->where('payments.status', '=', 'paid')
                // ->orwhere('payments.status', '=', 'payment')
                ->where('payments.approve_status', '=', 'pending')

                ->orderBy('payments.approve_status')
                ->orderBy('users.highrisk', 'desc')
                ->select('payments.*', 'users.name');
        } else {

            $this->query->leftjoin('users', 'payments.user_id', '=', 'users.id')
                ->where('payments.transaction_id', '!=', NULL)
                ->where('payments.status', '=', 'paid')
                // ->orwhere('payments.status', '=', 'payment')
                ->orderByRaw("FIELD(payments.approve_status , 'pending', 'approved', 'rejected')")

                ->orderBy('payments.approve_status')
                ->orderBy('users.highrisk', 'desc')
                ->select('payments.*', 'users.name');
        }

        // $query = str_replace(array('?'), array('\'%s\''), $this->query->toSql());
        // $query = vsprintf($query, $this->query->getBindings());
        // dd($query);

        // $result = $builder->get();
        // $query->with('user')->orderBy('users.highrisk', 'desc');

        return $this->query->paginate($this->request->get('perPage'));
    }


    public function feeResult()
    {

        $searchables = $this->searchables;
        $model = $this->query->getModel();
        //dd($model,$searchables);

        $searchByCountry = $this->request->get('searchByCountry');


        if ($search = $this->request->get('search')) {

            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }

        if (!empty($searchByCountry)) {

            $this->query->where('Country', "$searchByCountry");
        }

        return $this->query->paginate($this->request->get('perPage'));
    }

    public function PayingPaymentResult()
    {


        $paying_agent_id = auth()->user()->id;
        $searchables = $this->searchables;
        $model = $this->query->getModel();
        //dd($model,$searchables);


        if ($search = $this->request->get('search')) {
            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }
        $order = 'desc';

        $this->query->leftjoin('users', 'payments.user_id', '=', 'users.id')
            ->where('payments.transaction_id', '!=', NULL)
            ->where('payments.paying_agent_id', '=', $paying_agent_id)
            ->where('payments.approve_status', '=', 'approved')
            // ->orderByRaw("FIELD(payments.approve_status , 'pending', 'approved', 'rejected')")
            ->orderBy('payments.approve_status')
            ->orderBy('users.highrisk', 'desc')
            ->select('payments.*', 'users.name');

        return $this->query->paginate($this->request->get('perPage'));
    }
    /**
     * @param $columns
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function userResult($columns)
    {
        $searchables = $this->searchables;
        $model = $this->query->getModel();
        if ($search = $this->request->get('search')) {
            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }


        if (auth()->user()->is_super_admin) {
            $sql = $this->query;
        } else {
            $any_role = auth()->user()->roles->first()->name;
            if ($any_role == 'Agent') {
                $sql = $this->query->whereHas('roles', function ($q) {
                    $q->where('name', 'Customer')->orWhere('name', 'Agent Sub');
                });
            } elseif ($any_role == 'Agent Sub') {
                $sql = $this->query->whereHas('roles', function ($q) {
                    $q->where('name', 'Customer');
                });
            }
        }
        return $sql->paginate($this->request->get('perPage'), $columns);
    }

    /**
     * @param $columns
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function customerResult($columns)
    {
        $searchables = $this->searchables;
        $model = $this->query->getModel();
        if ($search = $this->request->get('search')) {
            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }
        $sql = $this->query->whereHas('roles', function ($q) {
            $q->where('name', 'Customer');
        });
        return $sql->paginate($this->request->get('perPage'), $columns);
    }

    public function customerSearchResult($columns)
    {
        $searchables = $this->searchables;
        $model = $this->query->getModel();
        if ($search = $this->request->get('search')) {
            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }
        $sql = $this->query->whereHas('roles', function ($q) {
            $q->where('name', 'Customer');
        });
        return $sql->paginate(50000, $columns);
    }

    /**
     * @param $columns
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function agentResult($columns)
    {
        $searchables = $this->searchables;
        $model = $this->query->getModel();
        if ($search = $this->request->get('search')) {
            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }
        // dd($columns);
        $sql = $this->query->whereHas('roles', function ($q) {
            $q->where('name', 'Agent');
        });
        return $sql->paginate($this->request->get('perPage'), $columns);
    }

    /**
     * @param $columns
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function subagentResult($columns)
    {
        $searchables = $this->searchables;
        $model = $this->query->getModel();
        if ($search = $this->request->get('search')) {
            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }

        $sql = $this->query->whereHas('roles', function ($q) {
            $q->where('name', 'Agent Sub');
        });
        return $sql->paginate($this->request->get('perPage'), $columns);
    }

    /**
     * @param $columns
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paymentsHistoryresult($columns)
    {

        $user_id = auth()->user()->id;
        $searchables = $this->searchables;
        $model = $this->query->getModel();

        if ($search = $this->request->get('search')) {
            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }
        // recipient

        // $this->query->orWhereHas('recipient', function ($q) {
        //     // $q->whereNotNull('deleted_at', 'or');
        //     $q->whereNotNull('deleted_at');
        // });

        // users
        // $this->query->where('payments.user_id', '==', 'users.id');
        // ->leftJoin('recipient', 'users.id', '=', 'posts.user_id')

        $sql = $this->query
            // ->leftJoin('recipient', 'recipient.user_id', '=', 'payments.user_id')
            ->where('payments.status', 'paid')
            ->where('payments.user_id', $user_id);
        //            ->orWhere('recipient.deleted_at', '!=', 'null');

        $result = $sql->paginate($this->request->get('perPage'), $columns);
        return $result;
    }

    public function referralResult($columns)
    {

        $user_id = auth()->user()->id;
        $searchables = $this->searchables;
        $model = $this->query->getModel();

        if ($search = $this->request->get('search')) {
            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }

        $sql = $this->query
            ->select(
                "referrals.id",
                "referrals.created_at",
                'referrals.updated_at',
                DB::raw("CONCAT(users.first_name, ' ', users.last_name) as user_id")
            )
            ->where('referrals.parent_id', $user_id)
            ->leftJoin('users', 'users.id', '=', 'referrals.user_id');
        $result = $sql->paginate($this->request->get('perPage'), $columns);
        return $result;
    }

    public function recipientResult($columns)
    {
        $user_id = auth()->user()->id;
        $searchables = $this->searchables;
        $model = $this->query->getModel();

        if ($search = $this->request->get('search')) {
            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }
        $sql = $this->query->where('user_id', $user_id); // ->where('deleted_at', '!=', null);
        // dd($this->request->get('perPage'));
        return $sql->paginate($this->request->get('perPage'), $columns);
    }

    // public function resultByCustomer($columns)
    // {
    //     $user_id = auth()->user()->id;
    //     $sql = $this->query->where('customer_id', $user_id);

    //     return $sql->paginate($this->request->get('perPage'), $columns);
    // }
    /**
     * @param       $columns
     * @param array $headings
     * @param       $fileName
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Writer\Exception
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export($columns, $headings, $fileName)
    {
        $currentDate = date('dmY-His');

        return Excel::download(
            new DataTableExport($headings, $this->query, $columns),
            "$fileName-export-$currentDate.xlsx"
        );
    }
    // recharge result.
    public function rechargeResult($columns)
    {
        $searchables = $this->searchables;
        $model = $this->query->getModel();
        if ($search = $this->request->get('search')) {
            $this->query->where(function (Builder $query) use ($model, $searchables, $search) {
                foreach ($searchables as $key => $searchableColumn) {
                    $query->orWhere(
                        $this->getLocalizedColumn($model, $searchableColumn),
                        'like',
                        "%{$search}%"
                    );
                }
            });
        }

        return $this->query->paginate($this->request->get('perPage'), $columns);
    }
}
