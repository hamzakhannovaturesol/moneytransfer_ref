<?php
// Route::get('logout/user', 'LoginController@logout_user')->name('logout.user');
// Route::get('logout/user', 'FrontendController@logout_user')->name('logout.user');
Route::get('user/logout', 'FrontendController@userLogout')->name('user.logout');




Route::get('/', 'FrontendController@home')->name('home');
Route::get('/confirm/{link}', 'FrontendController@confirmUser')->name('confirm');

/* *************** AJAX REQUESTS *************** */

/* *************** GET REQUESTS *************** */
Route::get('/confirm', 'FrontendController@confirm')->name('front.confirm');
Route::get('user/checkVerify', 'AjaxController@checkUserVerify')->name('verifyConfirm');
Route::get('user/checkUserDocsRejected', 'AjaxController@checkUserDocsRejected')->name('userDocsRejected');
Route::get('user/resendConfirmEmail', 'AjaxController@resendConfirmEmail')->name('resendConfirmEmail');
Route::get('user/userVerifyStatus', 'AjaxController@userVerifyStatus')->name('userVerifyStatus');
Route::get('user/backFromVerifyType', 'AjaxController@backFromVerifyType')->name('backFromVerifyType');
Route::get('user/getUserCountry', 'AjaxController@getUserCountry')->name('getUserCountry');
Route::get('user/getUserSendCountry', 'AjaxController@getUserSendCountry')->name('getUserSendCountry');
Route::get('user/getCountryCurrency', 'AjaxController@getCountryCurrency')->name('getCountryCurrency');
Route::get('user/getCountryFee', 'AjaxController@getCountryFee')->name('getCountryFee');
Route::get('user/getAmount', 'AjaxController@getAmount')->name('getAmount');
Route::get('user/getPaymentDetails', 'AjaxController@getPaymentDetails')->name('getPaymentDetails');
Route::get('user/getIndexPaymentDetails', 'AjaxController@getIndexPaymentDetails')->name('getIndexPaymentDetails');
Route::get('user/checkUserBalance', 'AjaxController@checkUserBalance')->name('checkUserBalance');
Route::get('user/getIdentityUploads', 'AjaxController@getIdentityUploads')->name('user.getIdentityUploads');
Route::get('/getUserLocation', 'AjaxController@getUserLocation')->name('getUserLocation');
Route::get('/getUserBalance', 'AjaxController@getUserBalance')->name('getUserBalance');


Route::get('/updateUserPoints', 'AjaxController@updateUserPoints')->name('updateUserPoints');


Route::get('getUserDetails', 'AjaxController@getUserDetails')->name('getUserDetails');
Route::get('/getRecipientsList', 'AjaxController@getRecipientsList')->name('getRecipientsList');
Route::get('user/getRequestRecharge', 'AjaxController@getRequestRecharge')->name('user.getRequestRecharge');

// points
Route::get('user/addPoint', 'AjaxController@addPoints')->name('user.addPoint');
Route::get('user/checkPoint', 'AjaxController@checkPoints')->name('user.checkPoint');
Route::get('user/getTotalPoints', 'AjaxController@getTotalPoints')->name('user.getTotalPoints'); // total user points.

Route::get('user/updateUserPoints', 'AjaxController@updateUserPoints')->name('user.updateUserPoints'); // update User Points.


Route::get('user/getUserRequestRecharge', 'AjaxController@getUserRequestRecharge')->name('user.getUserRequestRecharge');
Route::post('user/rechargeRequest', 'AjaxController@rechargeRequest')->name('rechargeRequest');
Route::get('recipient/getRecipientById', 'AjaxController@getRecipientById')->name('getRecipientById');
Route::post('user/updateVerifyImage', 'AjaxController@updateVerifyImage')->name('user.updateVerifyImage');
Route::post('user/updateFaceImage', 'AjaxController@updateFaceImage')->name('user.updateFaceImage');
Route::post('user/updateDocImage', 'AjaxController@updateDocImage')->name('user.updateDocImage');
Route::post('user/updateAdditionalDocImage', 'AjaxController@updateAdditionalDocImage')->name('user.updateAdditionalDocImage');
Route::get('index/customerpendingTransactions', 'AjaxController@customerpendingTransaction')->name('index.customerpendingTransactions');
Route::get('choosemoney/pendingSendMoneyRequest', 'AjaxController@pendingSendMoney')->name('choosemoney.pendingSendMoneyRequest');

// verify voucher code
Route::post('choosemoney/verifyVoucherCode', 'AjaxController@verifyVoucherCode')->name('choosemoney.verifyVoucherCode');


// payment detail page
Route::get('selectPayment/showBankDetails', 'AjaxController@showBankDetail')->name('selectPayment.showBankDetails');

// get bank name.
Route::post('user/getBankName', 'AjaxController@getBankName')->name('user.getBankName');


Route::post('contactus/submit', 'AjaxController@saveContactDetails')->name('contactus.submit');

Route::get('recharge/BankAccountDetails', 'AjaxController@getBankAccountDetails')->name('recharge.BankAccountDetails');
Route::get('dashboard/checkbalance', 'AjaxController@CheckBalance')->name('dashboard.checkbalance');
Route::get('balancelog/getLedger', 'AjaxController@getLedgerValues')->name('balancelog.getLedger');
/* *************** GET REQUESTS *************** */

/* *************** POST REQUESTS *************** */
Route::post('user/register_confirm_otp', 'AjaxController@registerOtpConfirm')->name('registerOtpConfirm');
Route::post('user/uploadVerifyFiles', 'AjaxController@uploadFirstVerifyFiles')->name('uploadVerifyFile');
Route::post('user/uploadDocFiles', 'AjaxController@uploadDocFiles')->name('uploadDocFile');
Route::post('user/verifyTypeSelected', 'AjaxController@verifyTypeSelected')->name('verifyTypeSelected');
Route::post('user/docTypeSelected', 'AjaxController@docTypeSelected')->name('docTypeSelected');
Route::post('user/saveUserCountry', 'AjaxController@saveUserCountry')->name('saveUserCountry');
Route::post('user/saveUserSendCountry', 'AjaxController@saveUserSendCountry')->name('saveUserSendCountry');
Route::post('user/savePaymentDetails', 'AjaxController@savePaymentDetails')->name('savePaymentDetails');
Route::post('user/deletePaymentDetails', 'AjaxController@deletePaymentDetails')->name('deletePaymentDetails'); // delete payment.
Route::post('user/addRecipient', 'AjaxController@addRecipient')->name('addRecipient');
Route::post('user/changePaymentStatus', 'AjaxController@changePaymentStatus')->name('changePaymentStatus');
/* *************** POST REQUESTS *************** */


Route::get('/getUserRef', 'AjaxController@getUserRef')->name('getUserRef');


/* *************** AJAX REQUESTS *************** */

Route::get(
    LaravelLocalization::transRoute('routes.about'),
    'PagesController@about'
)->name('about');

Route::match(
    ['GET', 'POST'],
    LaravelLocalization::transRoute('routes.contact'),
    'PagesController@contact'
)->name('contact');

Route::get(
    LaravelLocalization::transRoute('routes.contact-sent'),
    'PagesController@contactSent'
)->name('contact-sent');

Route::get(
    LaravelLocalization::transRoute('routes.legal-mentions'),
    'PagesController@legalMentions'
)->name('legal-mentions');



if (config('blog.enabled')) {
    Route::get('blog', 'BlogController@index')->name('blog.index');
    Route::get('blog/{post}', 'BlogController@show')->name('blog.show');
    Route::get('blog/tags/{tag}', 'BlogController@tag')->name('blog.tag');

    Route::get(
        LaravelLocalization::transRoute('routes.redactors'),
        'BlogController@owner'
    )->name('blog.owner');
}

/* **************** CUSTOMERS/USERS RESOURCES ************** */

Route::group(
    ['middleware' => ['auth', 'can:access customer']],
    function () {
        /* **************** Messages ROUTE RESOURCES ************** */
        Route::get('contact/messagereceived', 'AjaxController@messagereceived')->name('message.received');
        Route::get('contact/messagesent', 'AjaxController@messagesent')->name('message.sent');
        Route::get('messages/{message}/show', 'AjaxController@adminMessageView')->name('messages.show');
        Route::get('sentmessages/{message}/show', 'AjaxController@sentmessageview')->name('sentmessages.show');
        Route::get('sentmessages/realtimenotify', 'AjaxController@realtimenotify')->name('message.realtimenotify');
        Route::get('sentmessages/updatenotification', 'AjaxController@updatenotification')->name('message.updatenotification');
        /* **************** Messages ROUTE RESOURCES ************** */



        /***** Customer Messsages ***/
        Route::get('customer/messagereceived', 'AjaxController@customermessagereceived')->name('customer.message.received'); // get received messages.
        Route::get('customer/messagesent', 'AjaxController@customermessagesent')->name('customer.message.sent'); // get sent messages.
        Route::get('customer/messages/{CustomerMessage}/show', 'AjaxController@CustomerMessageView')->name('customer.messages.show'); // show single message.
        Route::post('customer/sendmessage', 'AjaxController@saveCustomerComposeMessage')->name('customer.sendmessage'); // Composer message route.

        Route::post('customer/replymessage', 'AjaxController@saveReply')->name('customer.replymessage');

        // check message status.
        Route::post('customer/message/check/status', 'AjaxController@checkStatus')->name('customer.message.check.status');
        /***** #Customer Messsages ***/


        /* **************** PAYMENTS ROUTE RESOURCES ************** */
        Route::get('payments/search', 'PaymentsController@search')->name('payments.search');
        Route::get('/payment/transation', 'PaymentsController@getUserLatestPaymentTransationID')->name('payment.transation.id'); // get User TransationID
        /* **************** PAYMENTS ROUTE RESOURCES ************** */

        /* **************** RECIPIENTS ROUTE RESOURCES ************** */
        Route::get('recipients/search', 'RecipientController@search')->name('recipients.search');
        Route::get('recipients/{recipient}/show', 'RecipientController@show')->name('recipients.show');
        Route::resource('recipients', 'RecipientController', [
            'only' => ['store', 'update', 'destroy'],
        ]);
        /* **************** RECIPIENTS ROUTE RESOURCES ************** */

        /* **************** REFERRAL ROUTE RESOURCES ************** */
        Route::get('referrals/search', 'ReferralController@search')->name('referrals.search');
        /* **************** REFERRAL ROUTE RESOURCES ************** */
        
        
        Route::get('/front/{vue_capture?}', 'FrontendController@index')
            ->where('vue_capture', '[\/\w\.-]*')
            ->name('fronthome');
    }
);

Route::get('/process/{vue_capture?}', 'FrontendController@process')
    ->where('vue_capture', '[\/\w\.-]*')
    ->name('process');

// get address.
Route::get('/getaddresses', 'AjaxController@getaddresses');
// get address details.
Route::get('/getaddressDetails', 'AjaxController@getaddressDetails');
// subscribe
Route::post('/subscribe', 'AjaxController@subscribe');

//
Route::post('/getHashKey', 'AjaxController@getHashKey')->name('getHashKey');
