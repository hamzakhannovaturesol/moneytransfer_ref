<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/paying/login', 'Paying\PayingLoginController@showLoginForm')->name('paying.login');
// Route::post('/paying/login', 'Paying\PayingLoginController@login')->name('paying.login.submit');
// Route::get('/paying/logout/', 'Paying\PayingLoginController@logout')->name('paying.logout');
// Route::get('/paying/password/reset', 'ForgotPasswordController@showPayingLinkRequestForm')->name('paying.password.request');
// Route::get('/paying/register', 'Paying\PayingRegisterController@showRegisterForm')->name('paying.register');
// Route::post('/paying/register', 'Paying\PayingRegisterController@register');

Route::get('robots.txt', 'SeoController@robots');
Route::get('sitemap.xml', 'SeoController@sitemap');

// CronJob controller.
Route::get('/cron', 'CronJobController@index');




