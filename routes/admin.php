<?php
// get from countrycodes table.
Route::get('get/countries/codes', 'AjaxController@getCountriesCodes')->name('get.countries.codes');

/* ************* AJAX REQUESTS ************* */
Route::get('index/search', 'AjaxController@search')->name('search');
Route::get('routes/search', 'AjaxController@routesSearch')->name('routes.search');
Route::get('tags/search', 'AjaxController@tagsSearch')->name('tags.search');
Route::post('images/upload', 'AjaxController@imageUpload')->name('images.upload');
Route::get('user/role', 'AjaxController@getRole')->name('user.get_role');
Route::get('user/payingagents', 'AjaxController@getPayingAgents')->name('user.get_paying_agents');
Route::post('user/acceptdoc', 'AjaxController@acceptDoc')->name('user.acceptdoc');
Route::post('user/rejectdoc', 'AjaxController@rejectDoc')->name('user.rejectdoc');
Route::get('customers/getpayments', 'AjaxController@getCustomerPayments')->name('customers.getpayments');

// get list of beneficies
Route::get('customers/getbeneficiary', 'AjaxController@getBeneficiary')->name('customers.getbeneficiary');



Route::get('ledgers/getLedger', 'AjaxController@getLedgerValues')->name('ledgers.getLedger');
Route::post('payments/addpayingagent', 'AjaxController@addPayingAgent')->name('payments.addpayingagent');
Route::post('payments/changestatus', 'AjaxController@changePaymentStatus')->name('payments.changestatus');

// recharge view
Route::post('recharge/view', 'AjaxController@rechargeView')->name('recharge.view');

Route::post('recharge/changestatus', 'AjaxController@changeRechargeStatus')->name('recharge.changestatus');
Route::post('fee/updatefee', 'FeeController@updateFee')->name('updateFee');
Route::post('currency/updatefee', 'CurrencyController@updateCurrency')->name('updateCurrency');
Route::get('dashboard/cheakstatus', 'AjaxController@cheakPendingStatus')->name('dashboard.cheakstatus');
Route::get('dashboard/stats', 'AjaxController@stats')->name('dashboard.stats');

Route::get('dashboard/pending_Recharge_Request', 'AjaxController@pendingRechargeRequest')->name('dashboard.pending_Recharge_Request');
Route::get('dashboard/getNewUser', 'AjaxController@getNewRegisteredUsers')->name('dashboard.getNewUser');
Route::get('dashboard/tester', 'ContactController@update')->name('dashboard.tester');
Route::get('payments/saveinBulk', 'AjaxController@saveinBulk')->name('payments.saveinBulk');
//Route::get('importdata/SenderData', 'AjaxController@getSenderData')->name('importdata.SenderData');
Route::post('pending/payments/migrate/', 'AjaxController@pendingPaymentMigrate')->name('pending.payment.migrate'); // migrate to other database.
Route::post('pending/payments/duplication/', 'AjaxController@pendingPaymentduplication')->name('pending.payment.duplication'); //

/* ************* AJAX REQUESTS ************* */

/* **************** USERS ROUTE RESOURCE ************** */
Route::group(
    ['middleware' => ['can:view users']],
    function () {
        Route::get('users/active_counter', 'UserController@getActiveUserCounter')->name('users.active.counter');
        Route::get('users/roles', 'UserController@getRoles')->name('users.get_roles');
        Route::get('users/search', 'UserController@search')->name('users.search');
        Route::get('users/{user}/show', 'UserController@show')->name('users.show');
        Route::resource('users', 'UserController', [
            'only' => ['store', 'update', 'destroy'],
        ]);
        Route::post('users/batch_action', 'UserController@batchAction')->name('users.batch_action');
        Route::post('users/{user}/active', 'UserController@activeToggle')->name('users.active');
        Route::get('users/{user}/impersonate', 'UserController@impersonate')->name('users.impersonate');
    }
);

/* **************** VOUCHERS ROUTE RESOURCE ************** */
Route::get('vouchers/search', 'VoucherController@search')->name('vouchers.search');
Route::get('vouchers/{voucher}/show', 'VoucherController@show')->name('vouchers.show');
Route::resource('vouchers', 'VoucherController', [
    'only' => ['store', 'update', 'destroy'],
]);
Route::get('vouchers/getcode', 'VoucherController@getCode')->name('vouchers.getcode');


/* **************** CUSTOMERS ROUTE RESOURCE ************** */

Route::group(
    ['middleware' => ['can:view customer']],
    function () {
        Route::get('customers/active_counter', 'CustomerController@getActiveUserCounter')->name('customers.active.counter');
        Route::get('customers/roles', 'CustomerController@getRoles')->name('customers.get_roles');
        Route::get('customers/search', 'CustomerController@search')->name('customers.search');
        Route::get('customers/searchValue', 'CustomerController@searchValue')->name('customers.searchValue');

        Route::get('customers/{customer}/show', 'CustomerController@show')->name('customers.show');

        Route::resource('customers', 'CustomerController', [
            'only' => ['store', 'update', 'destroy'],
        ]);
        Route::post('customers/batch_action', 'CustomerController@batchAction')->name('customers.batch_action');
        Route::post('customers/{customer}/active', 'CustomerController@activeToggle')->name('customers.active');
        Route::get('customers/{customer}/impersonate', 'CustomerController@impersonate')->name('customers.impersonate');


        // get Customer(specific) messages.
        Route::get('customer/messages/search', 'CustomerController@messages')->name('customer.messages.search');

        // get Customer display message details.
        Route::get('customer/message/{reply}/show', 'CustomerController@showMessage')->name('customer.message.show');
    }
);

/* **************** AGENTS ROUTE RESOURCE ************** */

Route::group(
    ['middleware' => ['can:view Agent']],
    function () {
        Route::get('agents/active_counter', 'AgentController@getActiveUserCounter')->name('agents.active.counter');
        Route::get('agents/roles', 'AgentController@getRoles')->name('agents.get_roles');
        Route::get('agents/search', 'AgentController@search')->name('agents.search');
        Route::get('agents/{agent}/show', 'AgentController@show')->name('agents.show');
        Route::resource('agents', 'AgentController', [
            'only' => ['store', 'update', 'destroy'],
        ]);
        Route::post('agents/batch_action', 'AgentController@batchAction')->name('agents.batch_action');
        Route::post('agents/{agent}/active', 'AgentController@activeToggle')->name('agents.active');
        Route::get('agents/{agent}/impersonate', 'AgentController@impersonate')->name('agents.impersonate');
    }
);

/* **************** SUB AGENTS ROUTE RESOURCE ************** */

Route::group(
    ['middleware' => ['can:view Agent Sub']],
    function () {
        Route::get('subagents/active_counter', 'SubAgentController@getActiveUserCounter')->name('subagents.active.counter');
        Route::get('subagents/roles', 'SubAgentController@getRoles')->name('subagents.get_roles');
        Route::get('subagents/search', 'SubAgentController@search')->name('subagents.search');
        Route::get('subagents/{agent}/show', 'SubAgentController@show')->name('subagents.show');
        Route::resource('subagents', 'SubAgentController', [
            'only' => ['store', 'update', 'destroy'],
        ]);
        Route::post('subagents/batch_action', 'SubAgentController@batchAction')->name('subagents.batch_action');
        Route::post('subagents/{agent}/active', 'SubAgentController@activeToggle')->name('subagents.active');
        Route::get('subagents/{agent}/impersonate', 'SubAgentController@impersonate')->name('subagents.impersonate');
    }
);

/* **************** ROLES ROUTE RESOURCES ************** */
Route::group(
    ['middleware' => ['can:view roles']],
    function () {
        Route::get('roles/permissions', 'RoleController@getPermissions')->name('roles.get_permissions');

        Route::get('roles/search', 'RoleController@search')->name('roles.search');
        Route::get('roles/{role}/show', 'RoleController@show')->name('roles.show');

        Route::resource('roles', 'RoleController', [
            'only' => ['store', 'update', 'destroy'],
        ]);
    }
);

/* **************** PAYMENTS ROUTE RESOURCES ************** */
Route::group(
    ['middleware' => ['can:view payments']],
    function () {
        Route::get('payments/search', 'PaymentsController@search')->name('payments.search');
        Route::post('payments/batch_action', 'PaymentsController@batchAction')->name('payments.batch_action');
        Route::get('payments/{payment}/show', 'PaymentsController@show')->name('payments.show');
        Route::resource('payments', 'PaymentsController', [
            'only' => ['store', 'update', 'destroy'],
        ]);
    }
);

/* **************** RECHARGE ROUTE RESOURCES ************** */
Route::group(
    ['middleware' => ['can:view recharge']],
    function () {
        Route::get('recharge/search', 'RechargeController@search')->name('recharge.search');
        Route::post('recharge/batch_action', 'RechargeController@batchAction')->name('recharge.batch_action');
        Route::get('recharge/{recharge}/show', 'RechargeController@show')->name('recharge.show');
        Route::resource('recharge', 'RechargeController', [
            'only' => ['store', 'update', 'destroy'],
        ]);
    }
);

/* **************** Customer Ledger ROUTE RESOURCES ************** */
Route::group(
    ['middleware' => ['can:view ledger']],
    function () {
        Route::get('ledger/search', 'LedgerController@search')->name('ledger.search');
        Route::post('ledger/batch_action', 'LedgerController@batchAction')->name('ledger.batch_action');
        Route::get('ledger/{ledger}/show', 'LedgerController@show')->name('ledger.show');
        Route::resource('ledger', 'LedgerController', [
            'only' => ['store', 'update', 'destroy'],
        ]);
    }
);
/* **************** FEE ROUTE RESOURCES ************** */
Route::group(
    ['middleware' => ['can:view fee']],
    function () {
        Route::get('fee/search', 'FeeController@search')->name('fee.search');
        Route::post('fee/batch_action', 'FeeController@batchAction')->name('fee.batch_action');
        Route::get('fee/{fee}/show', 'FeeController@show')->name('fee.show');
        Route::resource('fee', 'FeeController', [
            'only' => ['store', 'update', 'destroy'],
        ]);
    }
);

/* **************** CURRENCY / EXCHANGE RATE ROUTE RESOURCES ************** */
Route::group(
    ['middleware' => ['can:view currency']],
    function () {
        Route::get('currency/search', 'CurrencyController@search')->name('currency.search');
        Route::post('currency/batch_action', 'CurrencyController@batchAction')->name('currency.batch_action');
        Route::get('currency/{currency}/show', 'CurrencyController@show')->name('currency.show');
        Route::resource('currency', 'CurrencyController', [
            'only' => ['store', 'update', 'destroy'],
        ]);
    }
);

/* **************** MARGIN RATE ROUTE RESOURCES ************** */
Route::group(
    ['middleware' => ['can:view margin']],
    function () {
        Route::get('margin/search', 'MarginController@search')->name('margin.search');
        Route::post('margin/batch_action', 'MarginController@batchAction')->name('margin.batch_action');
        Route::get('margin/{margin}/show', 'MarginController@show')->name('margin.show');
        Route::resource('margin', 'MarginController', [
            'only' => ['store', 'update', 'destroy'],
        ]);
    }
);

/* **************** BRANDS ROUTE RESOURCES ************** */
Route::get('brands/search', 'BrandsController@search')->name('brands.search');
Route::post('brands/batch_action', 'BrandsController@batchAction')->name('brands.batch_action');
Route::get('brands/{brand}/show', 'BrandsController@show')->name('brands.show');
Route::resource('brands', 'BrandsController', [
    'only' => ['store', 'update', 'destroy'],
]);


/* **************** Banks ROUTE RESOURCE ************** */

Route::group(
    ['middleware' => ['can:view banks']],
    function () {
        // Route::get('banks/active_counter', 'BanksController@getActiveUserCounter')->name('banks.active.counter');
        //Route::get('banks/roles', 'BanksController@getRoles')->name('banks.get_roles');
        Route::get('banks/search', 'BanksController@search')->name('banks.search');
        Route::get('banks/{bank}/show', 'BanksController@show')->name('banks.show');
        Route::resource('banks', 'BanksController', [
            'only' => ['store', 'update', 'destroy'],
        ]);
        Route::post('banks/batch_action', 'BanksController@batchAction')->name('banks.batch_action');
        // Route::post('banks/{banks}/active', 'BanksController@activeToggle')->name('banks.active');
        //Route::get('banks/{banks}/impersonate', 'BanksController@impersonate')->name('banks.impersonate');
    }
);


/* **************** Branches ************** */
Route::group(
    ['middleware' => ['can:view branches']],
    function () {
        Route::post('branches/batch_action', 'BranchesController@batchAction')->name('branches.batch_action');
        Route::get('branches/{branch}/show', 'BranchesController@show')->name('branches.show');
        Route::get('branches/search', 'BranchesController@search')->name('branches.search');
        Route::resource('branches', 'BranchesController', ['only' => ['store', 'update', 'destroy'],]);
    }
);

/***************** Contact ROUTE RESOURCE ************** */

Route::group(
    ['middleware' => ['can:view contact']],
    function () {
        // Route::get('banks/active_counter', 'BanksController@getActiveUserCounter')->name('banks.active.counter');
        // Route::get('banks/roles', 'BanksController@getRoles')->name('banks.get_roles');

        Route::get('contact/search', 'ContactController@search')->name('contact.search');

        // admin contact notification.
        Route::get('contact/notification', 'ContactController@notification')->name('contact.notification');

        Route::get('contact/updatenotification', 'ContactController@updatenotification')->name('contact.updatenotification');
        Route::get('contact/{contactus}/show', 'ContactController@show')->name('contact.show');
        Route::resource('contact', 'ContactController', [
            'only' => ['store', 'update', 'destroy'],
        ]);
        Route::post('contact/batch_action', 'ContactController@batchAction')->name('contact.batch_action');
        // Route::post('banks/{banks}/active', 'BanksController@activeToggle')->name('banks.active');
        // Route::get('banks/{banks}/impersonate', 'BanksController@impersonate')->name('banks.impersonate');
    }
);



/************* Admin Messages ************/
Route::group(
    ['middleware' => ['can:view contact']],
    function () {
        // admin List messages in datatable grid.
        Route::get('message/search', 'MessageController@search')->name('message.search');

        // admin messages notification to display on top notification.
        Route::get('message/notification', 'MessageController@notification')->name('message.notification');

        // admin display message details.
        Route::get('message/{reply}/show', 'MessageController@show')->name('message.show');

        // admin reply.
        Route::post('message/saveReply', 'MessageController@saveReply')->name('message.saveReply');

        // check message status.
        Route::post('message/check/status', 'MessageController@checkStatus')->name('message.check.status');


        // store message
        Route::resource('message', 'MessageController', [
            'only' => ['store', 'update', 'destroy'],
        ]);
    }
);

Route::get('/{vue_capture?}', 'BackendController@index')
    ->where('vue_capture', '[\/\w\.-]*')
    ->name('home');
