<?php

Route::get('transaction/search', 'PaymentsController@search')->name('transaction.search');
Route::post('transaction/pay', 'AjaxController@payTransaction')->name('transaction.pay');

Route::get('/{vue_capture?}', 'PayingController@index')
        ->where('vue_capture', '[\/\w\.-]*')
        ->name('home');
