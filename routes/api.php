<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('customer/all', function () {
    echo 'fsadlfjasdklfjasklklasdjfkldfj';
});


Route::get('user/otp', 'Frontend@getUserOtp')->name('getUserOtp');
Route::post('/add_verify_images', 'Frontend@addVerifyImages')->name('add_verify_images');
Route::post('user/otp_verify', 'Frontend@verifyUserOtp')->name('verifyUserOtp');
Route::post('user/otp_resend', 'Frontend@resendUserOtp')->name('resendUserOtp');
Route::get('country/all', 'Frontend@getAllCountries')->name('getCountries');
Route::get('country/allCountries', 'Frontend@AllCountries')->name('AllCountries');
Route::get('country/sendto', 'Frontend@getSendToCountries')->name('getSendToCountries');
Route::get('country/sno', 'Frontend@getAllCountriesSNO')->name('getCountriesSNO');
Route::get('country/code', 'Frontend@getAllCountriesCode')->name('getCountriesCode');
Route::get('country/total', 'Frontend@getTotalCountries')->name('getTotalCountries');
Route::get('cities/country', 'Frontend@getCountryCities')->name('getCountryCities');
Route::get('banks/country', 'Frontend@getCountryBanks')->name('getCountryBanks');
Route::get('banks/countrybyid', 'Frontend@getCountryBanksById')->name('getCountryBanksById');
Route::get('banks/countrycashbyid', 'Frontend@getCountryCashBanksById')->name('getCountryCashBanksById');
Route::get('cities/bank', 'Frontend@getBankCities')->name('getBankCities');
Route::get('branches/bank', 'Frontend@getBankBranches')->name('getBankBranches');
Route::get('branch/code', 'Frontend@getBranchCode')->name('getBranchCode');
Route::get('service/code', 'Frontend@getServiceTypes')->name('getServiceTypes');
