<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Application Permissions
    |--------------------------------------------------------------------------
    */

    /* ACCESS BACKEND */
    'access backend' => [
        'display_name' => 'permissions.access.backend.display_name',
        'description'  => 'permissions.access.backend.description',
        'category'     => 'permissions.categories.access',
    ],
    /* ACCESS PAYING */
    'access paying' => [
        'display_name' => 'permissions.access.paying.display_name',
        'description'  => 'permissions.access.paying.description',
        'category'     => 'permissions.categories.access',
    ],
    /* ACCESS CUSTOMER */
    'access customer' => [
        'display_name' => 'permissions.access.customer.display_name',
        'description'  => 'permissions.access.customer.description',
        'category'     => 'permissions.categories.access',
    ],
    /* AGENT */
    'create Agent' => [
        'display_name' => 'permissions.create.agent.display_name',
        'description'  => 'permissions.create.agent.description',
        'category'     => 'permissions.categories.agent',
        'children'     => ['create own agent'],
    ],

    'view Agent' => [
        'display_name' => 'permissions.view.agent.display_name',
        'description'  => 'permissions.view.agent.description',
        'category'     => 'permissions.categories.agent',
        'children'     => ['view own agent'],
    ],

    'edit Agent' => [
        'display_name' => 'permissions.edit.agent.display_name',
        'description'  => 'permissions.edit.agent.description',
        'category'     => 'permissions.categories.agent',
        'children'     => ['edit own agent'],
    ],

    'delete Agent' => [
        'display_name' => 'permissions.delete.agent.display_name',
        'description'  => 'permissions.delete.agent.description',
        'category'     => 'permissions.categories.agent',
        'children'     => ['delete own agent'],
    ],

    /* SUB AGENT */
    'create Agent Sub' => [
        'display_name' => 'permissions.create.agent_sub.display_name',
        'description'  => 'permissions.create.agent_sub.description',
        'category'     => 'permissions.categories.agent_sub',
        'children'     => ['create own agent_sub'],
    ],

    'view Agent Sub' => [
        'display_name' => 'permissions.view.agent_sub.display_name',
        'description'  => 'permissions.view.agent_sub.description',
        'category'     => 'permissions.categories.agent_sub',
        'children'     => ['view own agent_sub'],
    ],

    'edit Agent Sub' => [
        'display_name' => 'permissions.edit.agent_sub.display_name',
        'description'  => 'permissions.edit.agent_sub.description',
        'category'     => 'permissions.categories.agent_sub',
        'children'     => ['edit own agent_sub'],
    ],

    'delete Agent Sub' => [
        'display_name' => 'permissions.delete.agent_sub.display_name',
        'description'  => 'permissions.delete.agent_sub.description',
        'category'     => 'permissions.categories.agent_sub',
        'children'     => ['delete own agent_sub'],
    ],

    /* CUSTOMER */
    'create customer' => [
        'display_name' => 'permissions.create.customer.display_name',
        'description'  => 'permissions.create.customer.description',
        'category'     => 'permissions.categories.customer',
        'children'     => ['create own customer'],
    ],

    'view customer' => [
        'display_name' => 'permissions.view.customer.display_name',
        'description'  => 'permissions.view.customer.description',
        'category'     => 'permissions.categories.customer',
        'children'     => ['view own customer'],
    ],

    'edit customer' => [
        'display_name' => 'permissions.edit.customer.display_name',
        'description'  => 'permissions.edit.customer.description',
        'category'     => 'permissions.categories.customer',
        'children'     => ['edit own customer'],
    ],

    'delete customer' => [
        'display_name' => 'permissions.delete.customer.display_name',
        'description'  => 'permissions.delete.customer.description',
        'category'     => 'permissions.categories.customer',
        'children'     => ['delete own customer'],
    ],

    /* CUSTOMER banks */
    'create banks' => [
        'display_name' => 'permissions.create.banks.display_name',
        'description'  => 'permissions.create.banks.description',
        'category'     => 'permissions.categories.banks',
        'children'     => ['create own banks'],
    ],

    'view banks' => [
        'display_name' => 'permissions.view.banks.display_name',
        'description'  => 'permissions.view.banks.description',
        'category'     => 'permissions.categories.banks',
        'children'     => ['view own banks'],
    ],

    'edit banks' => [
        'display_name' => 'permissions.edit.banks.display_name',
        'description'  => 'permissions.edit.banks.description',
        'category'     => 'permissions.categories.banks',
        'children'     => ['edit own banks'],
    ],

    'delete banks' => [
        'display_name' => 'permissions.delete.banks.display_name',
        'description'  => 'permissions.delete.banks.description',
        'category'     => 'permissions.categories.banks',
        'children'     => ['delete own banks'],
    ], 

 /* CUSTOMER branches */
    'create branches' => [
        'display_name' => 'permissions.create.branches.display_name',
        'description'  => 'permissions.create.branches.description',
        'category'     => 'permissions.categories.branches',
        'children'     => ['create own branches'],
    ],

    'view branches' => [
        'display_name' => 'permissions.view.branches.display_name',
        'description'  => 'permissions.view.branches.description',
        'category'     => 'permissions.categories.branches',
        'children'     => ['view own branches'],
    ],

    'edit branches' => [
        'display_name' => 'permissions.edit.branches.display_name',
        'description'  => 'permissions.edit.branches.description',
        'category'     => 'permissions.categories.branches',
        'children'     => ['edit own branches'],
    ],

    'delete branches' => [
        'display_name' => 'permissions.delete.branches.display_name',
        'description'  => 'permissions.delete.branches.description',
        'category'     => 'permissions.categories.branches',
        'children'     => ['delete own branches'],
    ], 

/* CUSTOMER ContactUS Form */
    'create contact' => [
        'display_name' => 'permissions.create.contact.display_name',
        'description'  => 'permissions.create.contact.description',
        'category'     => 'permissions.categories.contact',
        'children'     => ['create own contact'],
    ],

    'view contact' => [
        'display_name' => 'permissions.view.contact.display_name',
        'description'  => 'permissions.view.contact.description',
        'category'     => 'permissions.categories.contact',
        'children'     => ['view own contact'],
    ],

    'edit contact' => [
        'display_name' => 'permissions.edit.contact.display_name',
        'description'  => 'permissions.edit.contact.description',
        'category'     => 'permissions.categories.contact',
        'children'     => ['edit own contact'],
    ],

    'delete contact' => [
        'display_name' => 'permissions.delete.contact.display_name',
        'description'  => 'permissions.delete.contact.description',
        'category'     => 'permissions.categories.contact',
        'children'     => ['delete own contact'],
    ], 

    


/*    'view posts' => [
        'display_name' => 'permissions.view.posts.display_name',
        'description'  => 'permissions.view.posts.description',
        'category'     => 'permissions.categories.blog',
        'children'     => ['view own posts'],
    ],

    'create posts' => [
        'display_name' => 'permissions.create.posts.display_name',
        'description'  => 'permissions.create.posts.description',
        'category'     => 'permissions.categories.blog',
    ],

    'edit posts' => [
        'display_name' => 'permissions.edit.posts.display_name',
        'description'  => 'permissions.edit.posts.description',
        'category'     => 'permissions.categories.blog',
        'children'     => ['edit own posts'],
    ],

    'delete posts' => [
        'display_name' => 'permissions.delete.posts.display_name',
        'description'  => 'permissions.delete.posts.description',
        'category'     => 'permissions.categories.blog',
        'children'     => ['delete own posts'],
    ],

    'view own posts' => [
        'display_name' => 'permissions.view.own.posts.display_name',
        'description'  => 'permissions.view.own.posts.description',
        'category'     => 'permissions.categories.blog',
    ],

    'edit own posts' => [
        'display_name' => 'permissions.edit.own.posts.display_name',
        'description'  => 'permissions.edit.own.posts.description',
        'category'     => 'permissions.categories.blog',
    ],

    'delete own posts' => [
        'display_name' => 'permissions.delete.own.posts.display_name',
        'description'  => 'permissions.delete.own.posts.description',
        'category'     => 'permissions.categories.blog',
    ],

    'publish posts' => [
        'display_name' => 'permissions.publish.posts.display_name',
        'description'  => 'permissions.publish.posts.description',
        'category'     => 'permissions.categories.blog',
    ],

    'view form_settings' => [
        'display_name' => 'permissions.view.form_settings.display_name',
        'description'  => 'permissions.view.form_settings.description',
        'category'     => 'permissions.categories.form',
    ],

    'create form_settings' => [
        'display_name' => 'permissions.create.form_settings.display_name',
        'description'  => 'permissions.create.form_settings.description',
        'category'     => 'permissions.categories.form',
    ],

    'edit form_settings' => [
        'display_name' => 'permissions.edit.form_settings.display_name',
        'description'  => 'permissions.edit.form_settings.description',
        'category'     => 'permissions.categories.form',
    ],

    'delete form_settings' => [
        'display_name' => 'permissions.delete.form_settings.display_name',
        'description'  => 'permissions.delete.form_settings.description',
        'category'     => 'permissions.categories.form',
    ],

    'view form_submissions' => [
        'display_name' => 'permissions.view.form_submissions.display_name',
        'description'  => 'permissions.view.form_submissions.description',
        'category'     => 'permissions.categories.form',
    ],

    'delete form_submissions' => [
        'display_name' => 'permissions.delete.form_submissions.display_name',
        'description'  => 'permissions.delete.form_submissions.description',
        'category'     => 'permissions.categories.form',
    ],*/

    'view users' => [
        'display_name' => 'permissions.view.users.display_name',
        'description'  => 'permissions.view.users.description',
        'category'     => 'permissions.categories.access',
    ],

    'create users' => [
        'display_name' => 'permissions.create.users.display_name',
        'description'  => 'permissions.create.users.description',
        'category'     => 'permissions.categories.access',
    ],

    'edit users' => [
        'display_name' => 'permissions.edit.users.display_name',
        'description'  => 'permissions.edit.users.description',
        'category'     => 'permissions.categories.access',
    ],

    'delete users' => [
        'display_name' => 'permissions.delete.users.display_name',
        'description'  => 'permissions.delete.users.description',
        'category'     => 'permissions.categories.access',
    ],

    'impersonate users' => [
        'display_name' => 'permissions.impersonate.display_name',
        'description'  => 'permissions.impersonate.description',
        'category'     => 'permissions.categories.access',
    ],

    'view roles' => [
        'display_name' => 'permissions.view.roles.display_name',
        'description'  => 'permissions.view.roles.description',
        'category'     => 'permissions.categories.access',
    ],

    'create roles' => [
        'display_name' => 'permissions.create.roles.display_name',
        'description'  => 'permissions.create.roles.description',
        'category'     => 'permissions.categories.access',
    ],

    'edit roles' => [
        'display_name' => 'permissions.edit.roles.display_name',
        'description'  => 'permissions.edit.roles.description',
        'category'     => 'permissions.categories.access',
    ],

    'delete roles' => [
        'display_name' => 'permissions.delete.roles.display_name',
        'description'  => 'permissions.delete.roles.description',
        'category'     => 'permissions.categories.access',
    ],

    'view payments' => [
        'display_name' => 'permissions.view.payments.display_name',
        'description'  => 'permissions.view.payments.description',
        'category'     => 'permissions.categories.payments',
    ],

    'create payments' => [
        'display_name' => 'permissions.create.payments.display_name',
        'description'  => 'permissions.create.payments.description',
        'category'     => 'permissions.categories.payments',
    ],

    'edit payments' => [
        'display_name' => 'permissions.edit.payments.display_name',
        'description'  => 'permissions.edit.payments.description',
        'category'     => 'permissions.categories.payments',
    ],

    'delete payments' => [
        'display_name' => 'permissions.delete.payments.display_name',
        'description'  => 'permissions.delete.payments.description',
        'category'     => 'permissions.categories.payments',
    ],

    'view recharge' => [
        'display_name' => 'permissions.view.recharge.display_name',
        'description'  => 'permissions.view.recharge.description',
        'category'     => 'permissions.categories.recharge',
    ],

    'create recharge' => [
        'display_name' => 'permissions.create.recharge.display_name',
        'description'  => 'permissions.create.recharge.description',
        'category'     => 'permissions.categories.recharge',
    ],

    'edit recharge' => [
        'display_name' => 'permissions.edit.recharge.display_name',
        'description'  => 'permissions.edit.recharge.description',
        'category'     => 'permissions.categories.recharge',
    ],

    'delete recharge' => [
        'display_name' => 'permissions.delete.recharge.display_name',
        'description'  => 'permissions.delete.recharge.description',
        'category'     => 'permissions.categories.recharge',
    ],

    'view fee' => [
        'display_name' => 'permissions.view.fee.display_name',
        'description'  => 'permissions.view.fee.description',
        'category'     => 'permissions.categories.fee',
    ],

    'create fee' => [
        'display_name' => 'permissions.create.fee.display_name',
        'description'  => 'permissions.create.fee.description',
        'category'     => 'permissions.categories.fee',
    ],

    'edit fee' => [
        'display_name' => 'permissions.edit.fee.display_name',
        'description'  => 'permissions.edit.fee.description',
        'category'     => 'permissions.categories.fee',
    ],

    'delete fee' => [
        'display_name' => 'permissions.delete.fee.display_name',
        'description'  => 'permissions.delete.fee.description',
        'category'     => 'permissions.categories.fee',
    ],

    'view currency' => [
        'display_name' => 'permissions.view.currency.display_name',
        'description'  => 'permissions.view.currency.description',
        'category'     => 'permissions.categories.currency',
    ],

    'create currency' => [
        'display_name' => 'permissions.create.currency.display_name',
        'description'  => 'permissions.create.currency.description',
        'category'     => 'permissions.categories.currency',
    ],

    'edit currency' => [
        'display_name' => 'permissions.edit.currency.display_name',
        'description'  => 'permissions.edit.currency.description',
        'category'     => 'permissions.categories.currency',
    ],

    'delete currency' => [
        'display_name' => 'permissions.delete.currency.display_name',
        'description'  => 'permissions.delete.currency.description',
        'category'     => 'permissions.categories.currency',
    ],

    'view margin' => [
        'display_name' => 'permissions.view.margin.display_name',
        'description'  => 'permissions.view.margin.description',
        'category'     => 'permissions.categories.margin',
    ],

    'create margin' => [
        'display_name' => 'permissions.create.margin.display_name',
        'description'  => 'permissions.create.margin.description',
        'category'     => 'permissions.categories.margin',
    ],

    'edit margin' => [
        'display_name' => 'permissions.edit.margin.display_name',
        'description'  => 'permissions.edit.margin.description',
        'category'     => 'permissions.categories.margin',
    ],

    'delete margin' => [
        'display_name' => 'permissions.delete.margin.display_name',
        'description'  => 'permissions.delete.margin.description',
        'category'     => 'permissions.categories.margin',
    ],

    'view transaction' => [
        'display_name' => 'permissions.view.transaction.display_name',
        'description'  => 'permissions.view.transaction.description',
        'category'     => 'permissions.categories.transaction',
    ],

/*    'view metas' => [
        'display_name' => 'permissions.view.metas.display_name',
        'description'  => 'permissions.view.metas.description',
        'category'     => 'permissions.categories.seo',
    ],

    'create metas' => [
        'display_name' => 'permissions.create.metas.display_name',
        'description'  => 'permissions.create.metas.description',
        'category'     => 'permissions.categories.seo',
    ],

    'edit metas' => [
        'display_name' => 'permissions.edit.metas.display_name',
        'description'  => 'permissions.edit.metas.description',
        'category'     => 'permissions.categories.seo',
    ],

    'delete metas' => [
        'display_name' => 'permissions.delete.metas.display_name',
        'description'  => 'permissions.delete.metas.description',
        'category'     => 'permissions.categories.seo',
    ],

    'view redirections' => [
        'display_name' => 'permissions.view.redirections.display_name',
        'description'  => 'permissions.view.redirections.description',
        'category'     => 'permissions.categories.seo',
    ],

    'create redirections' => [
        'display_name' => 'permissions.create.redirections.display_name',
        'description'  => 'permissions.create.redirections.description',
        'category'     => 'permissions.categories.seo',
    ],

    'edit redirections' => [
        'display_name' => 'permissions.edit.redirections.display_name',
        'description'  => 'permissions.edit.redirections.description',
        'category'     => 'permissions.categories.seo',
    ],

    'delete redirections' => [
        'display_name' => 'permissions.delete.redirections.display_name',
        'description'  => 'permissions.delete.redirections.description',
        'category'     => 'permissions.categories.seo',
    ],*/
];
